VPCore
======

Core VanillaPod under-the-hood functionality

Provides a bunch of commands and features that are either not offered by
third-party plugins or are available but only from plugins which are bloated
or buggy or slow to update their Bukkit compatibility.
