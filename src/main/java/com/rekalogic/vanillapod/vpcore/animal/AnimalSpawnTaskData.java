package com.rekalogic.vanillapod.vpcore.animal;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;

public final class AnimalSpawnTaskData
{
	private final String name;
	private boolean enabled = false;
	private final int quantity;
	private final long frequency;
	private final List<SpawnDefinition> definitions = new ArrayList<SpawnDefinition>();
	private final List<Location> locations = new ArrayList<Location>();
	private final int mobLimit;
	private final int mobLimitRange;
	
	public String getName() { return this.name; }
	public boolean isEnabled() { return this.enabled; }
	public void disable() { this.enabled = false; }
	public int getQuantity() { return this.quantity; }
	public long getFrequency() { return this.frequency; }
	public int getMobLimit() { return this.mobLimit; }
	public int getMobLimitRange() { return this.mobLimitRange; }

	public AnimalSpawnTaskData(
			final String name,
			final boolean enabled,
			final int quantity,
			final long frequency,
			final int mobLimit,
			final int mobLimitRange)
	{
		this.name = name;
		this.enabled = enabled;
		this.quantity = quantity;
		this.frequency = frequency;
		this.mobLimit = mobLimit;
		this.mobLimitRange = mobLimitRange;
	}
	
	public void clear()
	{
		this.definitions.clear();
		this.locations.clear();
	}
	public void addDefinition(final EntityType type, final String metadata, int weight)
	{
		this.definitions.add(new SpawnDefinition(type, metadata, weight));
	}
	
	public void addLocation(final World world, final int x, final int y, final int z)
	{
		final Location location = new Location(world, x, y, z);
		this.locations.add(location);
	}
	
	public SpawnDefinition[] getDefinitions()
	{
		return this.definitions.toArray(new SpawnDefinition[0]);
	}
	
	public Location[] getLocations()
	{
		return this.locations.toArray(new Location[0]);
	}
	
	public Location getRandomLocation(final Random random)
	{
		final int index = random.nextInt(this.locations.size());
		return this.locations.get(index);
	}	
}
