package com.rekalogic.vanillapod.vpcore.animal;

import org.bukkit.entity.EntityType;

public final class SpawnDefinition
{
	private final EntityType type;
	private final String metadata;
	private final int weighting;
	
	public EntityType getType() { return this.type; }
	public String getMetadata() { return this.metadata; }
	public int getWeighting() { return this.weighting; }

	public SpawnDefinition(final EntityType type, final String metadata, final int weighting)
	{
		this.type = type;
		this.metadata = metadata;
		this.weighting = weighting;
	}
}
