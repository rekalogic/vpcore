package com.rekalogic.vanillapod.vpcore.animal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class AnimalExecutor extends ExecutorBase<AnimalSettings>
{
	public AnimalExecutor(final PluginBase plugin, final AnimalSettings settings)
	{
		super(plugin, settings, "checkowner", "entityinfo", "listanimals", "moveto", "releaseanimals", "setowner");
	}

	@Override
	protected boolean checkCommandAllowed(final String command, final boolean isConsole)
	{
		switch (command)
		{
		case "checkowner":
		case "entityinfo":
		case "moveto":
		case "setowner":
			return (!isConsole);
		}
		
		return super.checkCommandAllowed(command, isConsole);
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "listanimals": return (argCount <= 1);
		case "moveto": return (argCount >= 1 && argCount <= 4);
		case "releaseanimals": return (argCount == 1);
		case "setowner": return (argCount == 1);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "listanimals": return new String[] { ExecutorBase.FLAG_WORLD, ExecutorBase.FLAG_ORDERBY, ExecutorBase.FLAG_TOP };
		case "releaseanimals": return new String[] { ExecutorBase.FLAG_WORLD, ExecutorBase.FLAG_BOOL_CONFIRM, ExecutorBase.FLAG_BOOL_ALLWORLDS };
		}
		
		return super.getAllowedFlags(command);
	}
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "listanimals":
		case "releaseanimals":
		case "setowner":
			return LookupSource.OFFLINE_PLAYER;
		
		case "moveto":
			switch (argIndex)
			{
			case 0: return LookupSource.PLAYER;
			case 3: return LookupSource.WORLD;
			}
		}

		return super.getParameterLookupCollection(command, argIndex, args);
	}
	
	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		switch (command)
		{
		case "checkowner": return this.doCheckOwner(sender, args);
		case "entityinfo": return this.doEntityInfo(sender, args);
		case "listanimals": return this.doListAnimals(sender, args);
		case "moveto": return this.doMoveTo(sender, args);
		case "releaseanimals": return this.doReleaseAnimals(sender, args);
		case "setowner": return this.doSetOwner(sender, args);
		}
		
		return super.onCommand(sender, command, label, args);
	}
	
	private boolean doListAnimals(final PagedOutputCommandSender sender, final String[] args)
	{
		OfflinePlayer player = null;
		if (args.length == 1)
		{
			player = super.getPlugin().getOfflinePlayer(sender, args[0]);
			if (player == null) return true;
		}

		World world = null;
		if (!super.getFlagState(ExecutorBase.FLAG_BOOL_ALLWORLDS))
		{
			world = super.getWorld(sender);
			if (world == null) return true;
		}
		
		if (player != null && super.hasFlag(ExecutorBase.FLAG_ORDERBY, true, sender)) return true;
		int top = 0;
		if (super.hasFlag(ExecutorBase.FLAG_TOP))
		{
			top = super.getFlagInt(ExecutorBase.FLAG_TOP);
			if (top <= 0 || top > 10)
			{
				sender.sendFailure("Invalid top option $1 (1 <= top <= 10).", top);
				return true;
			}
		}

		final String orderBy = super.getSortOrder(sender, "name", "name", "namec", "quantity");
		if (orderBy.length() == 0) return true;

		final Map<String, List<Animals>> animalsByPlayer = new HashMap<String, List<Animals>>();
		for (final Animals animal: world.getEntitiesByClass(Animals.class))
		{
			if (animal instanceof Tameable)
			{
				final Tameable tameable = (Tameable)animal;
				if (tameable.getOwner() != null)
				{
					final String owner = tameable.getOwner().getName();
					if (owner != null && (player == null || owner.equalsIgnoreCase(player.getName())))
					{
						List<Animals> tameables = null;
						if (!animalsByPlayer.containsKey(owner))
						{
							tameables = new ArrayList<Animals>();
							animalsByPlayer.put(owner, tameables);
						} else tameables = animalsByPlayer.get(owner);
						tameables.add(animal);
					}
				}
			}
		}

		int animalCount = 0;
		final String[] names = animalsByPlayer.keySet().toArray(new String[0]);
		Arrays.sort(
				names,
				new Comparator<String>()
				{
					private final boolean isByName = orderBy.equalsIgnoreCase("name");
					private final boolean isByNameC = orderBy.equalsIgnoreCase("namec");
					private final boolean isByQuantity = orderBy.equalsIgnoreCase("quantity");
					@Override
					public int compare(final String s1, final String s2)
					{
						if (isByName) return s1.compareToIgnoreCase(s2);
						if (isByNameC) return s1.compareTo(s2);
						else if (isByQuantity) return Integer.compare(animalsByPlayer.get(s2).size(), animalsByPlayer.get(s1).size());
						else return 0;
					}
				});

		int count = 0;
		String playerHeader = "$1 has $2 loaded animal$3 in $4";
		if (names.length == 1) playerHeader += ":";
		else playerHeader += ".";
		for (final String name: names)
		{
			final Animals[] animals = animalsByPlayer.get(name).toArray(new Animals[0]);
			sender.sendNotification(
					playerHeader,
					name,
					animals.length,
					Helpers.getPlural(animals.length),
					world.getName());
			if (player != null)
			{
				for (int i = 0; i < animals.length; ++i)
					sender.sendInfoListItem(i, "$1 at $2", animals[i], animals[i].getLocation());
			}
			animalCount += animals.length;
			++count;
			if (top > 0 && count >= top) break;
		}
		if (animalCount != 0) sender.sendNotification("Animals shown: $1.", animalCount);
		else if(player == null) sender.sendNotification("No animals loaded in $1.", world.getName());
		else sender.sendNotification("$1 owns no loaded animals in $2.", player, world.getName());
		
		return true;
	}
	
	private boolean doCheckOwner(final PagedOutputCommandSender sender, final String[] args)
	{
		final Tameable tameable = this.getLeashedOrSelectedEntity(sender, true, true);
		if (tameable == null) return true;
		
		if (tameable.isTamed()) sender.sendNotification("This animal is owned by $1.", tameable.getOwner().getName());
		else sender.sendNotification("This animal is not owned.");

		return true;
	}
	
	private boolean doEntityInfo(final PagedOutputCommandSender sender, final String[] args)
	{
		final Entity entity = this.getSelectedEntity(sender, true);
		if (entity == null) return true;

		sender.suppressPaging();
		sender.sendNotification("Info for $1:", entity);
		if (entity.isDead()) sender.sendInfo("This entity is dead");
		if (entity instanceof Damageable)
		{
			final Damageable damageable = (Damageable)entity;
			int health = (int)((damageable.getHealth() / damageable.getMaxHealth()) * 100);
			if (health < 0) health = 0;
			sender.sendInfo("Health: $1% ($2)", health, damageable.getHealth());
		}
		sender.sendInfo("Alive for: $1", Helpers.DateTime.getHumanReadableFromTicks(entity.getTicksLived()));
		final int burningTicks = entity.getFireTicks();
		if (burningTicks > 0) sender.sendInfo("Burning for: $1", Helpers.DateTime.getHumanReadableFromTicks(burningTicks));
		sender.sendInfo("Location: $1", entity.getLocation());
		if (entity.getPassenger() != null) sender.sendInfo("Passenger: $1", entity.getPassenger());
		if (entity.getVehicle() != null) sender.sendInfo("Vehicle: $1", entity.getVehicle());
		if (entity instanceof Tameable)
		{
			final AnimalTamer tamer = ((Tameable)entity).getOwner();
			sender.sendInfo("Tamed by: $1", (tamer == null ? "nobody" : tamer.getName()));
		}
		
		if (entity instanceof LivingEntity)
		{
			final LivingEntity livingEntity = (LivingEntity)entity;
			if (livingEntity.getCustomName() != null) sender.sendInfo("Name: $1", livingEntity.getCustomName());
			if (livingEntity.getRemainingAir() < livingEntity.getMaximumAir())
			{
				int air = (int)(((double)(livingEntity.getRemainingAir()) / (double)(livingEntity.getMaximumAir())) * 100);
				if (air < 0) air = 0;
				sender.sendInfo("Remaining air: $1% ($2)", air, livingEntity.getRemainingAir());
			}
		}
		
		if (entity instanceof Creature)
		{
			final Creature creature = (Creature)entity;
			if (creature.isLeashed()) sender.sendInfo("Leashed by: $1", creature.getLeashHolder());
		}
		
		return true;
	}
	
	private boolean doReleaseAnimals(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;

		World[] worlds = null;
		if (super.getFlagState(ExecutorBase.FLAG_BOOL_ALLWORLDS)) worlds = Bukkit.getWorlds().toArray(new World[0]);
		else worlds = new World[] { super.getWorld(sender) };

		final boolean confirm = super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM);

		int setFreeCount = 0;
		for (final World releaseWorld: worlds)
		{
			for (final Animals animal: releaseWorld.getEntitiesByClass(Animals.class))
			{
				if (animal instanceof Tameable)
				{
					final Tameable tameable = (Tameable)animal;
					final AnimalTamer tamer = tameable.getOwner();
					if (tameable.isTamed() &&
							tamer != null &&
							tameable.getOwner().getName().equalsIgnoreCase(player.getName()))
					{
						if (confirm)
						{
							tameable.setOwner(null);
							tameable.setTamed(false);
						}
						++setFreeCount;
					}
				}
			}
		}

		if (setFreeCount > 0) sender.sendSuccess("Set free $1 animal$2 belonging to $3.",
				setFreeCount,
				Helpers.getPlural(setFreeCount),
				player);
		else sender.sendFailure("$1 does not have any animals to set free.", player);
	
		return true;
	}

	private boolean doSetOwner(PagedOutputCommandSender sender, String[] args)
	{
		final Tameable tameable = this.getLeashedOrSelectedEntity(sender, true, true);
		if (tameable == null) return true;
		
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;
		
		if (sender.isPlayer())
		{
			final boolean isModerator = sender.hasPermission("vpcore.moderator"); 
			if (!isModerator && player.equals(sender.getPlayer()))
			{
				sender.sendFailure( "You cannot set the owner to yourself!");
				return true;
			}
			
			if (tameable.getOwner() != null && !tameable.getOwner().equals(sender.getPlayer()))
			{
				if (!isModerator)
				{
					sender.sendFailure("You can only change ownership of your own animals.");
					return true;
				}
			}
		}		

		tameable.setOwner(player);
		LivingEntity livingEntity = (LivingEntity)tameable;
		livingEntity.setLeashHolder(null);
		sender.sendSuccess("You have changed this animal's owner to $1.", player.getName());
		if (player.isOnline())
		{
			final Player newOwner = player.getPlayer();
			if (livingEntity.getCustomName() == null)
			{
				Helpers.Messages.sendNotification(
						newOwner,
						Helpers.Entity.formatMessage(
								"$1 has given you ownership of $e.",
								(Entity)tameable),
								sender.getName());
			}
			else
			{
				Helpers.Messages.sendNotification(
						newOwner,
						Helpers.Entity.formatMessage(
								"$1 has given you ownership of $e named $2.",
								(Entity)tameable),
								sender.getName(),
								livingEntity.getCustomName());
			}
		}

		String logMessage = "";
		if (((LivingEntity)tameable).getCustomName() != null)
		{
			logMessage = Helpers.Parameters.replace(
					"Player $1 has transferred ownership of $e [$2] to $3.",
					sender.getName(),
					((LivingEntity)tameable).getCustomName(),
					player.getName());
		}
		else
		{
			logMessage = Helpers.Parameters.replace(
					"Player $1 has transferred ownership of $e to $2.",
					sender.getName(),
					player.getName());
		}
		super.info(Helpers.Entity.formatMessage(logMessage, (Entity)tameable));
	
		return true;
	}

	private boolean doMoveTo(final PagedOutputCommandSender sender, final String[] args)
	{
		final Entity entity = this.getSelectedEntity(sender, true);
		if (entity == null) return true;

		World world = null;
		Location targetLocation = null;
		
		if (args.length == 4)
		{
			world = Helpers.getWorld(sender, args[3]);
			if (world == null) return true;
			
			int x = 0;
			int y = 0;
			int z = 0;
			boolean validInput = true;

			try { x = Integer.parseInt(args[0]); }
			catch (final NumberFormatException e) { validInput = false; }
			try { y = Integer.parseInt(args[1]); }
			catch (final NumberFormatException e) { validInput = false; }
			try { z = Integer.parseInt(args[2]); }
			catch (final NumberFormatException e) { validInput = false; }

			if (validInput) targetLocation = new Location(world, x, y, z);
			else
			{
				sender.sendFailure("Invalid coordinates: $1, $2, $3", args[0], args[1], args[2]);
				return true;
			}
		}
		else
		{
			if (super.hasFlag(ExecutorBase.FLAG_WORLD, true, sender))
			{
				sender.sendFailure("The -w flag is not valid unless specifying coordinates.");
				return true;
			}
			switch (args[0].toLowerCase())
			{
			case "here":
			case "me":
				targetLocation = sender.getLocation();
				break;
			default:
				final Player onlinePlayer = super.getPlugin().getPlayer(sender, args[0]);
				if (onlinePlayer == null) return true;
				targetLocation = onlinePlayer.getLocation();
				break;
			}
		}

		if (entity.teleport(targetLocation, TeleportCause.COMMAND)) sender.sendSuccess("Entity teleported to $1.", targetLocation);
		else sender.sendFailure("Could not teleport entity to $1.", targetLocation);
		
		return true;
	}

	private Entity getSelectedEntity(final PagedOutputCommandSender sender, final boolean emitError)
	{
		final Entity entity = super.getSettings().getSelectedEntity(sender.getPlayer());
		if (entity == null && emitError) sender.sendFailure("You do not have an entity selected.");
		return entity;
	}

	private Tameable getLeashedOrSelectedEntity(final PagedOutputCommandSender sender, final boolean includeLeashed, final boolean mustBeTamed)
	{
		Tameable tameable = null;
		
		if (includeLeashed)
		{
			final Player player = sender.getPlayer();
			for (final Animals animal: player.getWorld().getEntitiesByClass(Animals.class))
			{
				if (animal.isLeashed() && animal.getLeashHolder() instanceof Player)
				{
					final Player leashHolder = (Player)animal.getLeashHolder();
					if (leashHolder.equals(player))
					{
						tameable = (Tameable)animal;
						break;
					}
				}
			}
		}
		
		final boolean isModerator = sender.hasPermission("vpcore.moderator"); 
		if (tameable == null)
		{
			if (isModerator)
			{
				final Entity entity = this.getSelectedEntity(sender, false);
				if (entity == null) sender.sendFailure("You must select an animal, or be holding its leash.");
				else if (entity instanceof Animals)
				{
					final Animals animal = (Animals)entity;
					if (animal.isDead()) sender.sendFailure("The selected animal ($1) is dead.", animal.getType());
					else if (animal instanceof Tameable) tameable = (Tameable)animal;
					else sender.sendFailure("The selected animal ($1) is not a tameable type.", animal.getType());
				}
				else sender.sendFailure("The selected entity is not an animal and cannot be owned.");
			}
			else if (includeLeashed) sender.sendFailure("You must be holding the leash of an animal.");
		}
		
		
		if (mustBeTamed && !isModerator && tameable != null && !tameable.isTamed())
		{
			sender.sendFailure("This animal is not tamed.");
			tameable = null;
		}
		
		return tameable;
	}
}
