package com.rekalogic.vanillapod.vpcore.animal;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class AnimalSpawnTask extends TaskBase
{
	private final Random random;
	private final AnimalSpawnTaskData taskData;

	public AnimalSpawnTask(final AnimalSettings settings, final AnimalSpawnTaskData taskData, final Random random)
	{
		super(settings);
		this.taskData = taskData;
		this.random = random;
	}

	@Override
	public void runTask()
	{
		final Location location = this.taskData.getRandomLocation(this.random);

		int totalWeight = 0;
		final SpawnDefinition[] definitions = this.taskData.getDefinitions();
		for (final SpawnDefinition definition: definitions) totalWeight += definition.getWeighting();

		int chosenWeight = this.random.nextInt(totalWeight);
		SpawnDefinition chosenDefinition = null;
		for (final SpawnDefinition definition: definitions)
		{
			if (chosenWeight < definition.getWeighting())
			{
				chosenDefinition = definition;
				break;
			}
			chosenWeight -= definition.getWeighting();
		}

		int nearbyCount = 0;
		if (this.taskData.getMobLimitRange() > 0 && this.taskData.getMobLimit() > 0)
		{
			for (final Entity entity: location.getWorld().getEntities())
			{
				if (entity.getType() == chosenDefinition.getType())
				{
					if (entity.getLocation().distance(location) < this.taskData.getMobLimitRange()) ++nearbyCount;
				}
			}
		}

		if (nearbyCount < this.taskData.getMobLimit())
		{
			int entityCount = this.random.nextInt(this.taskData.getQuantity()) + 1;
			if (nearbyCount + entityCount > this.taskData.getMobLimit())
				entityCount = this.taskData.getMobLimit() - nearbyCount;
			super.debug(
					"$1: Spawning $2x$3[$4] at $5 ($6 of a maximum $7 within $8 blocks).",
					this.taskData.getName(),
					entityCount,
					chosenDefinition.getType(),
					chosenDefinition.getMetadata(),
					location,
					nearbyCount,
					this.taskData.getMobLimit(),
					this.taskData.getMobLimitRange());
			Helpers.Spawn.spawnMobAt(chosenDefinition.getType(), location, entityCount, chosenDefinition.getMetadata());
		}
	}
}
