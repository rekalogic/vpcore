package com.rekalogic.vanillapod.vpcore.animal;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerUnleashEntityEvent;
import org.bukkit.inventory.ItemStack;

import com.rekalogic.vanillapod.vpcore.player.PlayerSettings;
import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class AnimalEventHandler extends EventHandlerBase<AnimalSettings>
{
	private final PlayerSettings playerSettings;
	private final ProtectedRegion orkidaRegion;
	private final Random random = new Random();
	
	public AnimalEventHandler(
			final PluginBase plugin,
			final AnimalSettings settings,
			final PlayerSettings playerSettings)
	{
		super(plugin, settings);
		
		this.playerSettings = playerSettings;
		
		if (super.getPlugin().isWorldGuardEnabled())
		{
			final World mainWorld = Bukkit.getWorlds().get(0); 
			final RegionManager manager = super.getPlugin().getWorldGuard().getRegionManager(mainWorld);
			this.orkidaRegion = manager.getRegion("orkida");
		}
		else this.orkidaRegion = null;
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onEntityDamageByEntity(final EntityDamageByEntityEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getEntity() instanceof Tameable)
		{
			final Tameable tameable = (Tameable)event.getEntity();
			if (tameable.isTamed())
			{
				Player attacker = null;
				if (event.getDamager() instanceof Player) attacker = (Player)event.getDamager();
				else if (event.getDamager() instanceof Projectile)
				{
					final Projectile projectile = (Projectile)event.getDamager();
					if (projectile.getShooter() instanceof Player) attacker = (Player)projectile.getShooter();
				}

				final AnimalTamer owner = tameable.getOwner();
				if (attacker != null &&
						(owner == null || !owner.getName().equalsIgnoreCase(attacker.getName())))
				{
					if (!attacker.hasPermission("vpcore.admin") && !attacker.hasPermission("vpcore.moderator"))
					{
						Helpers.Messages.sendFailure(attacker, super.getSettings().getKillOtherMessage(), (OfflinePlayer)tameable.getOwner());
						event.setCancelled(true);
					}
					else
					{
						if (owner == null) Helpers.Messages.sendNotification(attacker, "Warning: this animal belongs to $1.", "someone else");
						else Helpers.Messages.sendNotification(attacker, "Warning: this animal belongs to $1.", tameable.getOwner());
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onEntityTame(final EntityTameEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getOwner() instanceof Player)
		{
			final Player player = (Player)event.getOwner();
			final Entity entity = event.getEntity();
			if (!((Tameable)entity).isTamed())
			{
				super.info("Entity $1 was tamed by $2 at $3.", entity, player, entity.getLocation());
				Helpers.Messages.sendInfo(player, Helpers.Entity.formatMessage(super.getSettings().getTameMessage(), entity));
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onPlayerUnleashEntity(final PlayerUnleashEntityEvent event)
	{
		if (event.isCancelled()) return;

		final Player player = event.getPlayer();
		if (event.getEntity() instanceof Tameable)
		{
			final Tameable tameable = (Tameable)event.getEntity();
			final AnimalTamer owner = tameable.getOwner();
			if (owner != null
					&& !owner.getName().equalsIgnoreCase(player.getName())
					&& !player.hasPermission("vpcore.moderator"))
			{
				Helpers.Messages.sendFailure(player, super.getSettings().getUnleashOtherMessage(), (OfflinePlayer)tameable.getOwner());
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onCreatureSpawn(final CreatureSpawnEvent event)
	{
		if (event.isCancelled()) return;
		
		List<Player> checkForAfkPlayers = null;
		
		switch (event.getSpawnReason())
		{
		case SPAWNER_EGG: return;
		
		case NATURAL:
			if (!Helpers.Entity.isAHostileMob(event.getEntityType()))
			{
				checkForAfkPlayers = new ArrayList<Player>();
				checkForAfkPlayers.addAll(Bukkit.getOnlinePlayers());
			}
			break;

		case NETHER_PORTAL:
		case SPAWNER:
			final int oneIn = super.getSettings().getSpawnerSpawnChanceOneIn();
			if (oneIn > 1 && this.random.nextInt(oneIn) > 0)
			{
				super.debug("CANCELLED Spawn of $1 ($2): spawner chance.", event.getEntityType(), event.getSpawnReason());
				event.setCancelled(true);
				return;
			}

			final int spawnerSaturationRadius = super.getSettings().getSpawnerSaturationRadius();
			if (spawnerSaturationRadius > 0)
			{
				final EntityType spawnEntityType = event.getEntityType();
				int similarEntityCount = 0;
				for (final Entity entity: event.getEntity().getNearbyEntities(
						spawnerSaturationRadius,
						spawnerSaturationRadius,
						spawnerSaturationRadius))
				{
					if (entity.getType() == spawnEntityType) ++similarEntityCount;
				}
				
				if (similarEntityCount >= super.getSettings().getSpawnerMaxSpawned())
				{
					super.debug(
							"CANCELLED Spawn of $1 ($2): already $3 in a $4 radius.",
							spawnEntityType,
							event.getSpawnReason(),
							similarEntityCount,
							spawnerSaturationRadius);
					event.setCancelled(true);
					return;
				}
			}
			
			final int spawnerActivationRadius = super.getSettings().getSpawnerActivationRadius();
			if (spawnerActivationRadius > 0)
			{
				for (final Entity entity: event.getEntity().getNearbyEntities(
						spawnerActivationRadius,
						spawnerActivationRadius,
						spawnerActivationRadius))
				{
					if (entity instanceof Player)
					{
						if (checkForAfkPlayers == null) checkForAfkPlayers = new ArrayList<Player>();
						checkForAfkPlayers.add((Player)entity);
					}
				}
			}
			break;

		default: break;
		}
		
		if (checkForAfkPlayers != null)
		{
			boolean allAfk = true;
			for (final Player player: checkForAfkPlayers)
			{
				if (!super.getPlugin().isVanished(player) &&
						!this.playerSettings.isAfk(player))
				{
					allAfk = false;
					break;
				}
			}
			if (allAfk)
			{
				super.debug("CANCELLED Spawn of $1 ($2): all players are AFK.", event.getEntityType(), event.getSpawnReason());
				event.setCancelled(true);
				return;
			}
		}
		
		switch (event.getSpawnReason())
		{
		case NATURAL:
			if (this.orkidaRegion != null)
			{
				if (this.orkidaRegion.contains(
						event.getLocation().getBlockX(),
						event.getLocation().getBlockY(),
						event.getLocation().getBlockZ()))
				{
					final int oneIn = super.getSettings().getOrkidaSpawnChanceOneIn();
					if (oneIn > 1 && this.random.nextInt(oneIn) > 0)
					{
						super.debug("CANCELLED Spawn of $1 ($2): Orkida chance.", event.getEntityType(), event.getSpawnReason());
						event.setCancelled(true);
						return;
					}
				}
			}
			break;
			
		default: break;
		}

		if (super.getSettings().isAnimalLimiterEnabled())
		{
			boolean limitSpawning = false;
			final EntityType type = event.getEntityType();
			switch (type)
			{
			case CHICKEN: limitSpawning = (super.getSettings().isLimitChickens()); break;
			case COW: limitSpawning = (super.getSettings().isLimitCows()); break;
			case HORSE: limitSpawning = (super.getSettings().isLimitHorses()); break;
			case PIG: limitSpawning = (super.getSettings().isLimitPigs()); break;
			case SHEEP: limitSpawning = (super.getSettings().isLimitSheep()); break;

			case PIG_ZOMBIE:
				final World world = event.getLocation().getWorld();
				if (world.getEnvironment() == Environment.NORMAL)
				{
					int zombiePigmanCount = 0;
					for (final Entity entity: world.getEntities())
					{
						if (entity.getType() == EntityType.PIG_ZOMBIE) ++zombiePigmanCount;
					}
					if (zombiePigmanCount > super.getSettings().getMaxOverworldZombiePigmen())
					{
						event.setCancelled(true);
						return;
					}
				}
				break;

			default: break;
			}

			if (limitSpawning)
			{
				final Location location = event.getLocation();
				int count = 0;
				String checkType = "unknown";
				if (super.getSettings().isAnimalLimiterUsingChunkAlgorithm())
				{
					final Entity[] entities = location.getChunk().getEntities();
					for (final Entity entity: entities)
					{
						if (entity.getType() == type) ++count;
						if (count > super.getSettings().getAnimalLimiterMax())
						{
							checkType = "chunk";
							break;
						}
					}
				}

				if (count < super.getSettings().getAnimalLimiterMax())
				{
					final List<Entity> entities = location.getWorld().getEntities();
					for (final Entity entity: entities)
					{
						if (entity.getType() == type
								&& entity.getLocation().distance(location) <= super.getSettings().getAnimalLimiterRadius()) ++count;
						if (count >= super.getSettings().getAnimalLimiterMax())
						{
							checkType = "radius";
							break;
						}
					}
				}

				if (count >= super.getSettings().getAnimalLimiterMax())
				{
					if (super.getSettings().isAnimalLimiterUsingChunkAlgorithm()) checkType = "radius+chunk";
					super.debug("$1 spawn prevented at $2: limits exceeded ($3).", type, location, checkType);
					event.setCancelled(true);

					if (super.getSettings().isCullToChunk() &&
							(count > super.getSettings().getAnimalLimiterMax()))
					{
						final Entity[] entities = location.getChunk().getEntities();
						for (final Entity entity: entities)
						{
							if (entity.getType() == type)
							{
								entity.remove();
								break;
							}
						}
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onPlayerEggThrow(final PlayerEggThrowEvent event)
	{
		if (super.getSettings().isAnimalLimiterEnabled() && super.getSettings().getChickenSpawnRate() != 8)
		{
			final byte chickens = super.getSettings().canHatch();

			if (chickens == 0) event.setHatching(false);
			else
			{
				event.setHatching(true);
				event.setNumHatches(chickens);
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onPlayerInteractEntity(final PlayerInteractEntityEvent event)
	{
		if (event.isCancelled()) return;

		final Player player = event.getPlayer();
		ItemStack itemInHand = player.getItemInHand();

		if (itemInHand == null) itemInHand = new ItemStack(Material.AIR, 1);

		final Entity entity = event.getRightClicked();

		if (itemInHand.getType() == Material.STICK && player.hasPermission("vpcore.moderator"))
		{
			super.getSettings().selectEntity(player, entity);
			event.setCancelled(true);
		}
		else if (entity instanceof Animals)
		{
			Tameable tameable = null;
			if (entity instanceof Tameable)
			{
				tameable = (Tameable)entity;;
				final AnimalTamer owner = tameable.getOwner();
				final boolean playerOwnsAnimal = (tameable.isTamed()
						&& owner != null
						&& owner.getName().equalsIgnoreCase(player.getName()));
				final boolean playerCanInteract = (playerOwnsAnimal || player.hasPermission("vpcore.moderator"));
				if (player.isSneaking())
				{
					if (!playerCanInteract)
					{
						if (owner == null) Helpers.Messages.sendFailure(player, super.getSettings().getInteractOtherMessage(), "someone else");
						else Helpers.Messages.sendFailure(player, super.getSettings().getInteractOtherMessage(), (OfflinePlayer)owner);
						event.setCancelled(true);
					}
				}
				else if (itemInHand.getType() == Material.LEATHER)
				{
					if (playerOwnsAnimal) Helpers.Messages.sendSuccess(player, "This animal is owned by $1!", "you");
					else if (tameable.isTamed())
					{
						if (owner == null) Helpers.Messages.sendFailure(player, "This animal's ownership cannot be determined.");
						else Helpers.Messages.sendFailure(player, "This animal is owned by $1.", owner);
					}
					else Helpers.Messages.sendInfo(player, "This animal is not owned by anyone.");

					if (player.hasPermission("vpcore.moderator"))
					{
						if (tameable instanceof Horse)
						{
							Helpers.Messages.sendInfo(
									player,
									"Health: $1 ($2%), Jump Strength: $3, Age: $4.",
									Math.round(((Horse)tameable).getHealth()),
									Math.round((((Horse)tameable).getHealth() / ((Horse)tameable).getMaxHealth()) * 100),
									((Horse)tameable).getJumpStrength(),
									((Horse)tameable).getAge());
						}
					}

					if (tameable.isTamed())
					{
						final Animals animal = (Animals)entity;
						if (animal.isLeashed())
						{
							final Entity leashHolder = animal.getLeashHolder();
							if (leashHolder.getType() == EntityType.LEASH_HITCH)
							{
								final Chunk hitchChunk = leashHolder.getLocation().getWorld().getChunkAt(leashHolder.getLocation());
								final Chunk animalChunk = entity.getLocation().getWorld().getChunkAt(entity.getLocation());
								if (hitchChunk.hashCode() == animalChunk.hashCode()) Helpers.Messages.sendSuccess(player, "This animal is in the same chunk as its hitch.");
								else Helpers.Messages.sendFailure(player, "This animal is in a different chunk to its hitch.");
							}
						}
					}

					event.setCancelled(true);
				}
				else if (itemInHand.getType() == Material.LEASH)
				{
					if (tameable.isTamed() && !playerCanInteract)
					{
						if (owner == null) Helpers.Messages.sendFailure(player, super.getSettings().getLeashOtherMessage(), "someone else");
						else Helpers.Messages.sendFailure(player, super.getSettings().getLeashOtherMessage(), (OfflinePlayer)owner);
						event.setCancelled(true);
					}
				}
				else if (tameable.isTamed() && !playerCanInteract)
				{
					if (owner == null) Helpers.Messages.sendFailure(player, super.getSettings().getInteractOtherMessage(), "someone else");
					else Helpers.Messages.sendFailure(player, super.getSettings().getInteractOtherMessage(), (OfflinePlayer)owner);
					event.setCancelled(true);
				}
			}
		}
	}
}
