package com.rekalogic.vanillapod.vpcore.animal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.google.common.collect.ImmutableList;
import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class AnimalSettings extends SettingsBase
{
	private String tameMessage = "";
	private String leashOtherMessage = "";
	private String unleashOtherMessage = "";
	private String interactOtherMessage = "";
	private String killOtherMessage = "";
	private boolean isAnimalLimiterEnabled = false;
	private int animalLimiterMax = 0;
	private int animalLimiterRadius = 0;
	private boolean isAnimalLimiterUsingChunkAlgorithm = false;
	private int chickenSpawnRate = 0;
	private boolean isLimitChickens = false;
	private boolean isLimitCows = false;
	private boolean isLimitHorses = false;
	private boolean isLimitPigs = false;
	private boolean isLimitSheep = false;
	private boolean isCullToChunk = false;
	private List<String> animalLocations = null;
	private final List<AnimalSpawnTaskData> taskData = new ArrayList<AnimalSpawnTaskData>();
	private int maxOverworldZombiePigmen = 0;
	private int orkidaSpawnChanceOneIn = 0;
	private int spawnerActivationRadius = 0;
	private int spawnerSaturationRadius = 0;
	private int spawnerMaxSpawned = 0;
	private int spawnerSpawnChanceOneIn = 0;

	private final static String LOCATIONS_FILENAME = "animal.location";
	private final Map<String, Entity> selectedEntities = new ConcurrentHashMap<String, Entity>();
	private final Random chickenRandom = new Random();

	public String getTameMessage() { return this.tameMessage; }
	public String getLeashOtherMessage() { return this.leashOtherMessage; }
	public String getUnleashOtherMessage() { return this.unleashOtherMessage; }
	public String getInteractOtherMessage() { return this.interactOtherMessage; }
	public String getKillOtherMessage() { return this.killOtherMessage; }
	public boolean isAnimalLimiterEnabled() { return this.isAnimalLimiterEnabled; }
	public int getAnimalLimiterMax() { return this.animalLimiterMax; }
	public int getAnimalLimiterRadius() { return this.animalLimiterRadius; }
	public boolean isAnimalLimiterUsingChunkAlgorithm() { return this.isAnimalLimiterUsingChunkAlgorithm; }
	public int getChickenSpawnRate() { return this.chickenSpawnRate; }
	public boolean isLimitChickens() { return this.isLimitChickens; }
	public boolean isLimitCows() { return this.isLimitCows; }
	public boolean isLimitHorses() { return this.isLimitHorses; }
	public boolean isLimitPigs() { return this.isLimitPigs; }
	public boolean isLimitSheep() { return this.isLimitSheep; }
	public boolean isCullToChunk() { return this.isCullToChunk; }
	public ImmutableList<AnimalSpawnTaskData> getTaskData() { return ImmutableList.copyOf(this.taskData); }
	public int getOrkidaSpawnChanceOneIn() { return this.orkidaSpawnChanceOneIn; }
	public int getSpawnerActivationRadius() { return this.spawnerActivationRadius; }
	public int getSpawnerSaturationRadius() { return this.spawnerSaturationRadius; }
	public int getSpawnerMaxSpawned() { return this.spawnerMaxSpawned; }
	public int getSpawnerSpawnChanceOneIn() { return this.spawnerSpawnChanceOneIn; }

	public int getMaxOverworldZombiePigmen() { return this.maxOverworldZombiePigmen; }

	public AnimalSettings(final VPPlugin plugin)
	{
		super(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.tameMessage = super.getString("tame-message");
		this.leashOtherMessage = super.getString("leash-other-message");
		this.unleashOtherMessage = super.getString("unleash-other-message");
		this.interactOtherMessage = super.getString("interact-other-message");
		this.killOtherMessage = super.getString("kill-other-message");
		this.isAnimalLimiterEnabled = super.getBoolean("animal-limiter.enabled");
		this.animalLimiterMax = super.getInt("animal-limiter.max");
		this.animalLimiterRadius = super.getInt("animal-limiter.radius");
		this.isAnimalLimiterUsingChunkAlgorithm = super.getBoolean("animal-limiter.per-chunk");
		this.chickenSpawnRate = super.getInt("animal-limiter.chicken-spawn-rate");
		this.isLimitChickens = super.getBoolean("animal-limiter.limit-chickens");
		this.isLimitCows = super.getBoolean("animal-limiter.limit-cows");
		this.isLimitHorses = super.getBoolean("animal-limiter.limit-horses");
		this.isLimitPigs = super.getBoolean("animal-limiter.limit-pigs");
		this.isLimitSheep = super.getBoolean("animal-limiter.limit-sheep");
		this.isCullToChunk = super.getBoolean("animal-limiter.cull-to-chunk");
		this.maxOverworldZombiePigmen = super.getInt("max-overworld-zombiepigmen");
		this.orkidaSpawnChanceOneIn = super.getInt("orkida-spawn-chance-one-in");
		this.spawnerActivationRadius = super.getInt("spawner.activation-radius");
		this.spawnerSaturationRadius = super.getInt("spawner.saturation-radius");
		this.spawnerMaxSpawned = super.getInt("spawner.max-spawned");
		this.spawnerSpawnChanceOneIn = super.getInt("spawner.spawn-chance-one-in");

		if (this.isAnimalLimiterEnabled)
		{
			if (!this.isAnimalLimiterUsingChunkAlgorithm &&
					(this.animalLimiterMax == 0 || this.animalLimiterRadius == 0))
			{
				super.warning("Animal Limiter settings invalid; defaulting to OFF.");
				this.isAnimalLimiterEnabled = false;
			}
			else if (this.isLimitChickens == false &&
					this.isLimitCows == false &&
					this.isLimitHorses == false &&
					this.isLimitPigs == false &&
					this.isLimitSheep == false)
			{
				super.warning("No animals are limited; defaulting to OFF.");
				this.isAnimalLimiterEnabled = false;
			}
		}

		this.taskData.clear();
		if (super.isSet("tasks"))
		{
			for (final String taskName: super.getSubKeys("tasks", false))
			{
				this.debug("Found task '$1'.", taskName);
				final String taskPath = Helpers.Parameters.replace("tasks.$1.", taskName);
				final AnimalSpawnTaskData taskData = new AnimalSpawnTaskData(
						taskName,
						super.getBoolean(taskPath + "enabled"),
						super.getInt(taskPath + "quantity"),
						Helpers.DateTime.getMillis(super.getString(taskPath + "frequency")),
						super.getInt(taskPath + "mob-limit"),
						super.getInt(taskPath + "mob-limit-range"));
				this.configureTaskData(taskData, super.getStringList(taskPath + "mobs"), super.getStringList(taskPath + "locations"));

				if (taskData.isEnabled())
				{
					if (taskData.getLocations().length == 0)
					{
						super.warning("Task '$1' has no locations configured; disabling task.", taskData.getName());
						taskData.disable();
					}
					else if (taskData.getDefinitions().length == 0)
					{
						super.warning("Task '$1' has no mobs configured; disabling task.", taskData.getName());
						taskData.disable();
					}
					else if (taskData.getQuantity() == 0)
					{
						super.warning("Task '$1' has quantity=0; disabling task.", taskData.getName());
						taskData.disable();
					}
				}

				this.taskData.add(taskData);
			}
		}
	}

	@Override
	protected void onEmitSettings()
	{
		super.emitSetting("Animal limiter", this.isAnimalLimiterEnabled);
	}

	private void configureTaskData(
			final AnimalSpawnTaskData taskData,
			final List<String> spawnAnimals,
			final List<String> locationsRaw)
	{
		taskData.clear();

		for (final String animalString: spawnAnimals)
		{
			final String[] parts = animalString.split(" ");
			if (parts.length != 2)
			{
				super.warning("Ignoring deprecated or invalid spawning.mobs entry: $1", animalString);
				continue;
			}

			int weight = 1;
			if (parts.length == 2)
			{
				try
				{
					weight = Integer.parseInt(parts[0]);
				}
				catch (final Exception ex)
				{
					super.warning("Ignoring invalid weighting in spawning.mobs entry: $1", parts[1]);
					continue;
				}
			}

			String animal = parts[1];
			String metadata = "random";

			final int metaStart = animal.indexOf(":");
			if (metaStart >= 0)
			{
				metadata = animal.substring(metaStart + 1);
				animal = animal.substring(0, metaStart);
			}

			final EntityType type = Helpers.Entity.getMobType(animal);
			if (type == EntityType.UNKNOWN)
			{
				super.warning("Unknown animal type '$1'; ignored.", animal);
				continue;
			}

			taskData.addDefinition(type, metadata, weight);			
		}

		for (final String locationRaw: locationsRaw)
		{
			final String[] locationParts = locationRaw.split(" ");
			if (locationParts.length != 4) super.warning("Invalid mob spawn location: $1", locationRaw);
			else
			{
				final World world = Bukkit.getWorld(locationParts[0]);

				if (world == null) super.warning("Invalid mob spawn world: $1", locationParts[0]);
				else
				{
					int x = 0;
					int y = 0;
					int z = 0;
					try
					{
						x = Integer.parseInt(locationParts[1]);
						y = Integer.parseInt(locationParts[2]);
						z = Integer.parseInt(locationParts[3]);
					}
					catch (final Exception ex)
					{
						super.warning("Invalid mob spawn coordinates: $1", locationRaw);
						return;
					}

					taskData.addLocation(world, x, y, z);
				}
			}
		}
	}

	@Override
	protected void onSave()
	{
		super.set("tame-message", this.tameMessage);
		super.set("leash-other-message", this.leashOtherMessage);
		super.set("unleash-other-message", this.unleashOtherMessage);
		super.set("interact-other-message", this.interactOtherMessage);
		super.set("kill-other-message", this.killOtherMessage);
		super.set("animal-limiter.enabled", this.isAnimalLimiterEnabled);
		super.set("animal-limiter.max", this.animalLimiterMax);
		super.set("animal-limiter.radius", this.animalLimiterRadius);
		super.set("animal-limiter.per-chunk", this.isAnimalLimiterUsingChunkAlgorithm);
		super.set("animal-limiter.chicken-spawn-rate", this.chickenSpawnRate);
		super.set("animal-limiter.limit-chickens", this.isLimitChickens);
		super.set("animal-limiter.limit-cows", this.isLimitCows);
		super.set("animal-limiter.limit-horses", this.isLimitHorses);
		super.set("animal-limiter.limit-pigs", this.isLimitPigs);
		super.set("animal-limiter.limit-sheep", this.isLimitSheep);
		super.set("animal-limiter.cull-to-chunk", this.isCullToChunk);
		super.set("max-overworld-zombiepigmen", this.maxOverworldZombiePigmen);
		super.set("orkida-spawn-chance-one-in", this.orkidaSpawnChanceOneIn);
		super.set("spawner.activation-radius", this.spawnerActivationRadius);
		super.set("spawner.saturation-radius", this.spawnerSaturationRadius);
		super.set("spawner.max-spawned", this.spawnerMaxSpawned);
		super.set("spawner.spawn-chance-one-in", this.spawnerSpawnChanceOneIn);

		for (final AnimalSpawnTaskData taskData: this.taskData)
		{
			final ArrayList<String> mobDefinitions = new ArrayList<String>();
			for (final SpawnDefinition definition: taskData.getDefinitions())
			{
				String definitionString = Helpers.Parameters.replace(
						"$1 $2",
						definition.getWeighting(),
						Helpers.Entity.getMobFormalName(definition.getType()));
				if (definition.getMetadata().length() > 0)
					definitionString += ":" + definition.getMetadata();
				mobDefinitions.add(definitionString);
			}

			final ArrayList<String> locations = new ArrayList<String>();
			for (final Location location: taskData.getLocations())
				locations.add(
						Helpers.Parameters.replace(
								"$1 $2 $3 $4",
								location.getWorld().getName(),
								location.getBlockX(),
								location.getBlockY(),
								location.getBlockZ()));

			final String taskPath = Helpers.Parameters.replace("tasks.$1.", taskData.getName());
			super.set(taskPath + "enabled", taskData.isEnabled());
			super.set(taskPath + "quantity", taskData.getQuantity());
			super.set(taskPath + "frequency", Helpers.DateTime.getHumanReadableFromMillis(taskData.getFrequency()));
			super.set(taskPath + "mob-limit", taskData.getMobLimit());
			super.set(taskPath + "mob-limit-range", taskData.getMobLimitRange());
			super.set(taskPath + "mobs", mobDefinitions);
			super.set(taskPath + "locations", locations);
		}
	}

	@Override
	protected void onEmitInfo()
	{
		final String playerPlaceholder = "PLAYER-NAME";
		super.debug("Animal tamers will be told: $1", Helpers.Entity.formatMessage(this.tameMessage, EntityType.HORSE));
		super.debug("Third-party leashers will be told: $1", Helpers.Parameters.replace(this.leashOtherMessage, playerPlaceholder));
		super.debug("Third-party unleashers will be told: $1",Helpers.Parameters.replace(this.unleashOtherMessage, playerPlaceholder));
		super.debug("Third-party interacters will be told: $1", Helpers.Parameters.replace(this.interactOtherMessage, playerPlaceholder));
		super.debug("Third-party killers will be told: $1", Helpers.Parameters.replace(this.killOtherMessage, playerPlaceholder));

		if (this.isAnimalLimiterEnabled)
		{
			if (this.isLimitChickens) super.info("Chickens will be limited.");
			if (this.isLimitCows) super.info("Cows will be limited.");
			if (this.isLimitHorses) super.info("Horses will be limited.");
			if (this.isLimitPigs) super.info("Pigs will be limited.");
			if (this.isLimitSheep) super.info("Sheep will be limited.");
			if (this.isCullToChunk) super.info("Animals will be culled according to quantity in chunk.");
			if (this.isAnimalLimiterUsingChunkAlgorithm) super.info("Animals will be limited to $1 per chunk.", this.animalLimiterMax);
			else super.info("Animals will be limited to $1 in $2-block radius.", this.animalLimiterMax, this.animalLimiterRadius);
			super.info("Chickens will hatch from 1 in $1 thrown eggs.", this.chickenSpawnRate);
		}

		super.info("Configured spawn tasks: $1", this.taskData.size());
		for (final AnimalSpawnTaskData taskData: this.taskData)
			super.info(
					"Task '$1': 1-$2 animal$3 in $4 location$5$6.",
					taskData.getName(),
					taskData.getQuantity(),
					Helpers.getPlural(taskData.getQuantity()),
					taskData.getLocations(),
					Helpers.getPlural(taskData.getLocations()),
					(taskData.isEnabled() ? "" : " (DISABLED)"));
		
		if (this.orkidaSpawnChanceOneIn <= 1) super.info("Animal spawns in Orkida will be unaffected.");
		else super.info("Orkida animals will be reduced to 1 in $1.", this.orkidaSpawnChanceOneIn);
		
		super.info("Mob spawners will look for active players within a $1m radius.", this.spawnerActivationRadius);
	}

	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading animal location...");
		this.animalLocations = null;

		super.loadDataStore(
				AnimalSettings.LOCATIONS_FILENAME,
				new IDataStoreLoader()
				{
					@Override
					public void load(final ConfigurationSection section)
					{
						animalLocations = Helpers.Serialisation.getStringList(section);	
					}
				});
		
		if (this.animalLocations == null) this.animalLocations = new ArrayList<String>();
		else if (this.animalLocations.size() > 0)
		{
			int chunkLoadCount = 0;
			final int chunkAlreadyLoadedCount = 0;
			for (final String locationString: this.animalLocations)
			{
				final Location location = this.getLocationFromSignature(locationString);
				for (int x = -1; x <= 1; ++x)
				{
					for (int z = -1; z <= 1; ++z)
					{
						final Chunk chunk = location.getWorld().getChunkAt(
								location.getChunk().getX() + x,
								location.getChunk().getZ() + z);
						if (chunk.load(true)) ++chunkLoadCount;
					}
				}
			}

			super.debug(
					"Loaded $1 chunk$2 for $3 animal$4 ($5 already loaded).",
					chunkLoadCount,
					Helpers.getPlural(chunkLoadCount),
					this.animalLocations.size(),
					Helpers.getPlural(this.animalLocations.size()),
					chunkAlreadyLoadedCount);

			this.animalLocations.clear();
		}
		
		super.onLoadDataStore();
	}

	@Override
	protected void onSaveDataStore()
	{
		super.saveDataStore(
				AnimalSettings.LOCATIONS_FILENAME,
				new IDataStoreSaver()
				{
					@Override
					public YamlConfiguration getConfigToSave()
					{
						return Helpers.Serialisation.toConfig(animalLocations);
					}
				});
	}

	public boolean isSpawningEnabled()
	{
		return (this.taskData.size() > 0);
	}

	public void selectEntity(final Player player, final Entity entity)
	{
		this.selectedEntities.put(player.getName(), entity);
		Helpers.Messages.sendNotification(player, "You have selected $1.", entity);
	}

	public Entity getSelectedEntity(final Player player)
	{
		Entity entity = null;
		if (this.selectedEntities.containsKey(player.getName()))
			entity = this.selectedEntities.get(player.getName());
		return entity;
	}

	public void rememberAnimalLocation(final Location location)
	{
		final String locationString = this.getLocationSignature(location);
		if (!this.animalLocations.contains(locationString))
			this.animalLocations.add(locationString);
	}

	public byte canHatch()
	{
		byte chickens = 0;
		if (this.chickenSpawnRate > 0 &&
				this.chickenRandom.nextInt(this.chickenSpawnRate) == 0)
		{
			chickens = 1;
			if (this.chickenRandom.nextInt(4) == 0) chickens = 4;
		}
		return chickens;
	}
	
	private String getLocationSignature(final Location location)
	{
		return Helpers.Parameters.replace(
				"$1:$2:$3:$4",
				location.getWorld().getName(),
				location.getBlockX(),
				location.getBlockY(),
				location.getBlockZ());
	}

	private Location getLocationFromSignature(final String signature)
	{
		final String[] parts = signature.split(":");
		return new Location(
				Bukkit.getWorld(parts[0]),
				Integer.parseInt(parts[1]),
				Integer.parseInt(parts[2]),
				Integer.parseInt(parts[3]));
	}
}