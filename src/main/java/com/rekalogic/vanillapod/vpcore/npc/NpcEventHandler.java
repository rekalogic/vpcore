package com.rekalogic.vanillapod.vpcore.npc;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class NpcEventHandler extends EventHandlerBase<NpcSettings>
{
	public NpcEventHandler(final PluginBase plugin, final NpcSettings settings)
	{
		super(plugin, settings);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	private void onEntityDamage(final EntityDamageEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getEntityType() == EntityType.VILLAGER)
		{
			final Villager villager = (Villager)event.getEntity();
			if (super.getSettings().isProtected(villager))
			{
				switch (event.getCause())
				{
				case ENTITY_ATTACK: break;
				
				default:
					super.debug("Prevented $1 from being hurt by $2.", villager, event.getCause());
					event.setCancelled(true);
					break;
				}
			}
		}		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	private void onEntityDamageByEntity(final EntityDamageByEntityEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getEntityType() == EntityType.VILLAGER)
		{
			final Villager villager = (Villager)event.getEntity();
			if (super.getSettings().isProtected(villager))
			{
				if (event.getDamager() instanceof Player)
				{
					final Player attacker = (Player)event.getDamager();
					super.debug("Prevented $1 from being hurt by $2.", villager, attacker);
					Helpers.Messages.sendInfo(attacker, Helpers.Parameters.replace(super.getSettings().getNoDeathMessage(), villager));
				}
				else super.debug("Prevented $1 from being hurt by $2.", villager, event.getDamager());
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onPlayerInteractEntity(final PlayerInteractEntityEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getRightClicked() instanceof Villager)
		{
			final Villager villager = (Villager)event.getRightClicked();
			if (super.getSettings().isProtected(villager))
			{
				Helpers.Messages.sendInfo(
						event.getPlayer(),
						Helpers.Parameters.replace(
								super.getSettings().getNoTradeMessage(),
								villager));
				event.setCancelled(true);
			}
			else
			{
				final ItemStack item = event.getPlayer().getItemInHand();
				if (item != null &&
						item.getType() == Material.NAME_TAG)
				{
					final String name = item.getItemMeta().getDisplayName(); 
					if (super.getSettings().isProtectedName(name) &&
							!event.getPlayer().hasPermission("vpcore.moderator"))
					{
						Helpers.Messages.sendFailure(
								event.getPlayer(),
								"The name $1 cannot be given to villagers.",
								name);
							
						event.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onEntityDeath(final EntityDeathEvent event)
	{
		if (event.getEntityType() == EntityType.VILLAGER)
		{
			if (super.getSettings().isLogKillers())
			{
				final Villager villager = (Villager)event.getEntity();
				final Location location = villager.getLocation();
				final Player killer = villager.getKiller();

				if (killer == null) super.debug(
						"A villager ($1) at $2 has been killed by $3.",
						villager,
						location,
						villager.getLastDamageCause().getCause().toString());
				else super.debug(
						"A villager ($1) at $2 has been killed by $3.",
						villager,
						location,
						killer);
			}
		}
	}
}
