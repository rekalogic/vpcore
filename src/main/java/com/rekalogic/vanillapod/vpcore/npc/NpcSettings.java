package com.rekalogic.vanillapod.vpcore.npc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Villager;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class NpcSettings extends SettingsBase
{
	private final NpcDbInterface dbInterface;
	
	private String noDeathMessage = "";
	private String noTradeMessage = "";
	private boolean isLogKillers = false;	
	private Map<String, NpcData> npcsByName = null;
	private Map<Integer, NpcData> npcsById = null;

	public String getNoDeathMessage() { return this.noDeathMessage; }
	public String getNoTradeMessage() { return this.noTradeMessage; }
	public boolean isLogKillers() { return this.isLogKillers; }

	public NpcSettings(final VPPlugin plugin)
	{
		super(plugin);
		
		this.dbInterface = new NpcDbInterface(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.noDeathMessage = super.getString("no-death-message");
		this.noTradeMessage = super.getString("no-trade-message");
		this.isLogKillers = super.getBoolean("log-killers");
	}

	@Override
	protected void onSave()
	{
		super.set("no-death-message", this.noDeathMessage);
		super.set("no-trade-message", this.noTradeMessage);
		super.set("log-killers", this.isLogKillers);
	}
	
	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading NPCs...");
		this.npcsById = this.dbInterface.getProtectedNpcs();
		
		this.npcsByName = new ConcurrentHashMap<String, NpcData>(this.npcsById.size());
		for (final NpcData npc: this.npcsById.values())
			this.npcsByName.put(npc.getName().toLowerCase(), npc);

		super.onLoadDataStore();
	}

	@Override
	protected void onEmitInfo()
	{
		this.info(
				"The following $1 NPC$2 will be protected from death/trade:",
				this.npcsByName.size(),
				Helpers.getPlural(this.npcsByName.size()));
		for (final NpcData npc: this.npcsById.values())
			this.info("    " + npc.getName());
		this.info("Murderers will be told: $1", Helpers.Parameters.replace(this.noDeathMessage, "<villager>"));
		this.info("Traders will be told: $1", Helpers.Parameters.replace(this.noTradeMessage, "<villager>"));
	}

	public boolean isProtected(final Villager villager)
	{
		return this.isProtectedName(villager.getCustomName());
	}
	
	public boolean isProtectedName(final String name)
	{
		if (name == null) return false;
		if (this.npcsByName == null) return false;
		return (this.npcsByName.containsKey(name.toLowerCase()));
	}
	
	public NpcData getProtectedNpc(final int id)
	{
		NpcData npc = null;
		if (this.npcsById != null)
		{
			if (this.npcsById.containsKey(id))
				npc = this.npcsById.get(id);
		}
		return npc;
	}
	
	public NpcData getProtectedNpc(final String name)
	{
		NpcData npc = null;
		if (name != null &&
				this.npcsByName != null)
		{
			final String lowerName = name.toLowerCase();
			if (this.npcsByName.containsKey(lowerName))
				npc = this.npcsByName.get(lowerName);
		}
		return npc;
	}
	
	public List<Villager> getNpcVillagers(final NpcData npc)
	{
		final List<Villager> villagers = new ArrayList<Villager>();
		
		for (final World world: Bukkit.getWorlds())
		{
			final List<Entity> entities = world.getEntities();
			for (final Entity entity: entities)
			{
				if (entity instanceof Villager)						
				{
					final Villager villager = (Villager)entity;
					final String name = villager.getCustomName();
					if (name != null && name.equals(npc.getName())) villagers.add(villager);
				}
			}
		}

		return villagers;
	}
	
	public List<NpcData> getNpcList()
	{
		return new ArrayList<NpcData>(this.npcsById.values());
	}
	
	public NpcData createNpcProtection(final String name, final Location location)
	{
		final NpcData npc = new NpcData(
				this.dbInterface.createNpcProtection(
						name,
						location),
				name,
				location);
		
		this.npcsById.put(npc.getId(), npc);
		this.npcsByName.put(npc.getName().toLowerCase(), npc);
		return npc;
	}
	
	public void removeNpcProtection(final NpcData npc)
	{
		this.npcsById.remove(npc.getId());
		this.npcsByName.remove(npc.getName().toLowerCase());
		this.dbInterface.removeNpcProtection(npc);
	}
	
	public void updateNpcLocation(final NpcData npc, final Location location)
	{
		npc.setLocation(location);
		this.dbInterface.updateNpcLocation(npc);
	}
}
