package com.rekalogic.vanillapod.vpcore.npc;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Location;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class NpcDbInterface extends DatabaseInterfaceBase
{
	public NpcDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}

	public Map<Integer, NpcData> getProtectedNpcs()
	{
		final Map<Integer, NpcData> npcs = new ConcurrentHashMap<Integer, NpcData>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT id, name, locationid FROM vp_npc");
		while (result.moveNext())
		{
			final Location location = super.getLocationFromMySqlResult(connection, result);
			final NpcData npc = new NpcData(
					result.getInt("id"),
					result.getString("name"),
					location);
			npcs.put(npc.getId(), npc);
		}
		connection.close();
		return npcs;
	}
	
	public int createNpcProtection(final String name, final Location location)
	{		
		final MySqlConnection connection = super.getConnection();
		
		int locationid = 0;
		if (location != null) locationid = super.createLocation(connection, location);
		
		final int id = connection.insertInto(
				"vp_npc",
				"name, locationid",
				name,
				(location == null ? null : locationid));
		connection.close();
		
		return id;
	}
	
	public void removeNpcProtection(final NpcData npc)
	{
		final MySqlConnection connection = super.getConnection();
		this.updateNpcLocation(connection, npc, null);
		connection.deleteFrom("vp_npc", "id = ?", npc.getId());
		connection.close();
	}
	
	private void updateNpcLocation(final MySqlConnection connection, final NpcData npc, final Location location)
	{
		connection.deleteFrom("vp_location", "id IN (SELECT locationid FROM vp_npc WHERE id = ?)", npc.getId());
		if (location != null)
		{
			final int locationId = super.createLocation(connection, location);
			connection.update("vp_npc", "locationid = ?", "id = ?", locationId, npc.getId());
		}
	}
	
	public void updateNpcLocation(final NpcData npc)
	{
		final MySqlConnection connection = super.getConnection();
		this.updateNpcLocation(connection, npc, npc.getLocation());
		connection.close();
	}
}
