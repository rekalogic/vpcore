package com.rekalogic.vanillapod.vpcore.npc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;

import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class NpcExecutor extends ExecutorBase<NpcSettings>
{
	public NpcExecutor(final PluginBase plugin, final NpcSettings settings)
	{
		super(plugin, settings, "npc");
	}
	
	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "npc": return true;
		}
		
		return super.checkParameters(command, argCount);
	}
	
	@Override
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "npc":
		{
			switch (argIndex)
			{
			case 0: return Arrays.asList("fix", "list", "protect", "relocate", "setlocation", "unprotect");
			case 1:
			{
				switch (args[0].toLowerCase())
				{
				case "unprotect":
				case "setlocation":
				case "relocate":
					final List<NpcData> npcs = super.getSettings().getNpcList();
					final List<String> npcNames = new ArrayList<String>(npcs.size());
					for (final NpcData npc: npcs) npcNames.add(npc.getName());
					return npcNames;
				}
			} break;
			}
		} break;
		}
		return super.onTabComplete(sender, command, argIndex, args);
	}
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "npc": return LookupSource.FROM_PLUGIN;
		}

		return super.getParameterLookupCollection(command, argIndex, args);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		switch (command)
		{
		case "npc": return this.doNpc(sender, args);
		}

		return super.onCommand(sender, command, label, args);
	}

	private boolean doNpc(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length == 0) return false;
		
		switch (args[0].toLowerCase())
		{
		case "fix": return this.doNpcFix(sender, args);
		case "list": return this.doNpcList(sender, args);
		case "protect": return this.doNpcProtect(sender, args);
		case "relocate": return this.doNpcRelocate(sender, args);
		case "setlocation": return this.doNpcSetLocation(sender, args);
		case "unprotect": return this.doNpcUnprotect(sender, args);
		}
		return false;
	}
	
	private boolean doNpcFix(final PagedOutputCommandSender sender, final String[] args)
	{
		final List<NpcData> npcs = super.getSettings().getNpcList();
		final Profession profession = Profession.BLACKSMITH;
		for (final NpcData npc: npcs)
		{
			final List<Villager> villagers = super.getSettings().getNpcVillagers(npc);

			if (villagers.size() == 0)
			{
				if (npc.getLocation() == null) sender.sendFailure("Unable to respawn: $1 has no start location.", npc.getName());
				else
				{
					final Location location = npc.getLocation();
					if (location.getChunk().isLoaded())
					{
						sender.sendInfo("Respawning $1 at $2.", npc.getName(), location);
						final Villager villager = (Villager)location.getWorld().spawnEntity(location, EntityType.VILLAGER);
						villager.setCustomName(npc.getName());
						villager.setProfession(profession);
					}
					else sender.sendFailure("$1 not spawned at $2 (chunk not loaded).", npc.getName(), location);
				}
			}
			else if (villagers.size() == 1)
			{
				final Villager villager = villagers.get(0);
				if (villager.getProfession() != profession)
				{
					sender.sendInfo("Changing $1 back to $2.", npc.getName(), profession);
					villager.setProfession(profession);
				}
			}
			else
			{
				villagers.remove(0);
				sender.sendInfo("Multiple instances of $1 were found; removing.", npc.getName());
				for (final Villager villager: villagers) villager.remove();
			}
		}
		
		sender.sendSuccess("Fixed all NPCs.");
		
		return true;
	}

	private NpcData getNpcByNameOrId(final PagedOutputCommandSender sender, final String nameOrId)
	{
		NpcData npc = super.getSettings().getProtectedNpc(nameOrId);
		if (npc == null)
		{
			int npcId = 0;
			try { npcId = Integer.parseInt(nameOrId); }
			catch (NumberFormatException ex) { }
			npc = super.getSettings().getProtectedNpc(npcId);
		}
		
		if (npc == null) sender.sendFailure("No such NPC with name or ID '$1'.", nameOrId);
		return npc;
	}

	private boolean doNpcSetLocation(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length != 3) return false;
		final NpcData npc = this.getNpcByNameOrId(sender, args[1]);
		if (npc == null) return true;
		
		Location location = null;
		switch (args[2].toLowerCase())
		{
		case "here":
		case "me":
		case ".":
			location = sender.getLocation();
			if (location == null) return true;
			break;
			
		case "none":
			break;
			
		default: return false;
		}
		
		super.getSettings().updateNpcLocation(npc, location);
		sender.sendSuccess("Set location of $1 to $2.", npc.getName(), (location == null ? "none" : location));
		
		return true;
	}

	private boolean doNpcRelocate(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length < 1 || args.length > 2) return false;
		
		String npcName = "";
		if (args.length == 2) npcName = args[1];
		
		final List<NpcData> npcs = super.getSettings().getNpcList();
		if (npcs.size() == 0) sender.sendSuccess("No NPCs have been protected.");
		else
		{
			int relocatedCount = 0;
			for (final NpcData npc: npcs)
			{
				if (npcName.length() > 0)
				{
					if (npcName.equalsIgnoreCase(npc.getName())) npcName = npc.getName();
					else continue;
				}
				
				final List<Villager> villagers = super.getSettings().getNpcVillagers(npc);
				Villager villager = null;
				if (villagers.size() >= 1) villager = villagers.remove(0);
				
				if (villagers.size() > 0)
				{
					sender.sendNotification("Removing $1 clone$2 of $3.", villagers, Helpers.getPlural(villagers), villager);
					for (final Villager clone: villagers) clone.remove();
				}
				
				if (villager == null) sender.sendInfo("Could not find $1!", npc.getName());
				else
				{
					final Location location = npc.getLocation();
					if (location == null) sender.sendInfo("$1 has no start location.", villager);
					else
					{
						if (villager.teleport(location)) ++relocatedCount;
						else sender.sendFailure("Unable to move $1 to $2.", villager, location);
					}
				}
			}
			
			if (npcName.length() > 0)
			{
				if (relocatedCount == 1) sender.sendSuccess("$1 relocated.", npcName);
				else sender.sendFailure("Unable to relocate $1.", npcName);
			}
			else sender.sendSuccess("$1/$2 NPC$3 relocated.", relocatedCount, npcs, Helpers.getPlural(npcs));
		}
		return true;
	}

	private boolean doNpcUnprotect(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length != 2) return false;
		
		final NpcData npc = this.getNpcByNameOrId(sender, args[1]);
		if (npc == null) return true;

		super.getSettings().removeNpcProtection(npc);
		sender.sendSuccess("Removed protection for $1.", npc.getName());
		
		return true;
	}

	private boolean doNpcProtect(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length != 2) return false;
		final String name = args[1];
		NpcData npc = super.getSettings().getProtectedNpc(name);
		if (npc == null)
		{
			npc = super.getSettings().createNpcProtection(name, null);
			sender.sendSuccess("Protected [$1] $2.", npc.getId(), npc.getName());
		}
		else sender.sendFailure("The NPC [$1] $2 is already protected.", npc.getId(), npc.getName());
		
		return true;
	}
	
	private boolean doNpcList(final PagedOutputCommandSender sender, final String[] args)
	{
		final List<NpcData> npcs = super.getSettings().getNpcList();
		for (final NpcData npc: npcs)
		{
			final List<Villager> villagers = super.getSettings().getNpcVillagers(npc);
			Location location = null;
			if (villagers.size() > 0)
			{
				final Villager villager = villagers.get(0);
				location = villager.getLocation();
			}
			
			sender.sendInfoListItemWithId(
					npc.getId(),
					"$1 $2",
					npc.getName(),
					(location == null ? ChatColor.RED + "NOT FOUND" : location));
			if (villagers.size() > 1)
			{
				sender.sendFailure("Multiple instances of $1 were found:", npc.getName());
				for (final Villager villager: villagers)
					sender.sendFailure("-- $1", villager.getLocation());
			}
		}
		
		return true;
	}
}
