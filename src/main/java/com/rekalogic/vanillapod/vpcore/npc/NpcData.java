package com.rekalogic.vanillapod.vpcore.npc;

import org.bukkit.Location;

public final class NpcData
{
	private final int id;
	private final String name;
	
	private Location location = null;
	
	public int getId() { return this.id; }
	public String getName() { return this.name; }
	public Location getLocation() { return this.location; }
	public void setLocation(Location location) { this.location = location; }
	
	public NpcData(final int id, final String name, final Location location)
	{
		this.id = id;
		this.name = name;
		this.location = location;
	}
}
