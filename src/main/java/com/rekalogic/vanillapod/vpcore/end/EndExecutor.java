package com.rekalogic.vanillapod.vpcore.end;

import java.util.Collection;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.entity.EnderDragon;

import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class EndExecutor extends ExecutorBase<EndSettings>
{
	public EndExecutor(final PluginBase plugin, final EndSettings settings)
	{
		super(plugin, settings, "endreset", "killdragon", "queryreset");
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "endreset":
		case "killdragon":
			return new String[] { ExecutorBase.FLAG_BOOL_CONFIRM };
		}
		
		return super.getAllowedFlags(command);
	}
	
	@Override
	protected boolean checkParameters(String command, int argCount)
	{
		switch (command)
		{
		case "endreset":
		case "killdragon":
			return (argCount >= 0 && argCount <= 1);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		switch (command)
		{
		case "queryreset": return this.doQueryReset(sender);
		case "endreset": return this.doEndReset(sender, args);
		case "killdragon": return this.doKillDragon(sender, args);
		}

		return super.onCommand(sender, command, label, args);
	}

	@Override protected LookupSource getParameterLookupCollection(
			String command, int argIndex, String[] args)
	{
		switch (command)
		{
		case "endreset":
		case "killdragon":
			switch (argIndex)
			{
			case 0: return LookupSource.WORLD;
			}
			break;
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}

	private boolean doKillDragon(final PagedOutputCommandSender sender, final String[] args)
	{
		World world = null;
		if (args.length == 1) world = sender.getWorld(args[0]);
		else world = sender.getWorld();
		if (world == null) return true;
		
		final boolean confirm = super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM); 
		
		final Collection<EnderDragon> dragons = world.getEntitiesByClass(EnderDragon.class);
		if (dragons.size() == 0) sender.sendFailure("There is no dragon loaded in $1.", world.getName());
		else
		{
			int killedDragons = 0;
			for (final EnderDragon dragon: dragons)
			{
				if (confirm)
				{
					if (Helpers.Hacks.killDragon(dragon)) ++killedDragons;
					else sender.sendFailure("Unable to kill dragon (health=$1) in $2.", dragon.getHealth(), world);
				}
			}
			sender.sendSuccess("Killed $1 Ender Dragon$2 in $3.", killedDragons, Helpers.getPlural(killedDragons), world);
			
			if (confirm &&
					world.getEnvironment() == Environment.THE_END &&
					killedDragons == dragons.size())
			{
				super.getSettings().enqueueEndReset(world, sender);
			}
		}		

		return true;
	}

	private boolean doEndReset(final PagedOutputCommandSender sender, final String[] args)
	{
		final long now = System.currentTimeMillis();
		
		World world = null;
		if (args.length == 1) world = sender.getWorld(args[0]);
		else world = sender.getWorld();
		if (world == null) return true;

		if (world.getEnvironment() != Environment.THE_END)
		{
			sender.sendFailure("World $1 is not an End world.", world.getName());
			final World[] possibleWorlds = Bukkit.getWorlds().toArray(new World[0]);
			boolean endWorldsExist = false;
			sender.sendInfo("Valid worlds:");
			for (int i = 0; i < possibleWorlds.length; ++i)
			{
				if (possibleWorlds[i].getEnvironment() == Environment.THE_END)
				{
					sender.sendInfoListItem(i, "$1", possibleWorlds[i]);
					endWorldsExist = true;
				}
			}
			if (!endWorldsExist) sender.sendInfo("None");
		}

		final boolean confirm = super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM);
		
		if (confirm) super.getSettings().setWorldResetTime(world, 0);
		
		final EndResetTask task = new EndResetTask(super.getSettings(), world);
		if (task.resetEndWorld((System.currentTimeMillis() - now) + 1, confirm)) sender.sendSuccess("Reset world $1.", world);
		else sender.sendFailure("Failed to reset world $1.", world);

		return true;
	}

	private boolean doQueryReset(final PagedOutputCommandSender sender)
	{
		int scheduledCount = 0;
		final long now = System.currentTimeMillis();
		final World[] worlds = Bukkit.getWorlds().toArray(new World[0]);

		sender.sendInfo(ChatColor.LIGHT_PURPLE + "Worlds to be reset:");
		for (int i = 0; i < worlds.length; ++i)
		{
			final World world = worlds[i];
			if (world.getEnvironment() == Environment.THE_END)
			{
				final long resetTime = super.getSettings().getWorldResetTime(world);
				if (resetTime > 0)
				{
					sender.sendInfoListItem(
							i,
							"$1 (in $2).",
							world,
							Helpers.DateTime.getHumanReadableFromMillis(resetTime - now));
					++scheduledCount;
				}
			}
		}

		if (scheduledCount == 0) sender.sendNotification("No End worlds are scheduled for reset.");
		else sender.sendNotification(
				"There $1 $2 End world$3 scheduled for reset.",
				scheduledCount,
				Helpers.getIsAre(scheduledCount),
				Helpers.getPlural(scheduledCount));
		
		return true;
	}
}
