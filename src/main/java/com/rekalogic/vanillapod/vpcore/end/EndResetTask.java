package com.rekalogic.vanillapod.vpcore.end;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.World.Environment;

import com.rekalogic.vanillapod.vpcore.command.CommandExecutor;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class EndResetTask extends TaskBase
{
	private final World world;
	private final EndSettings settings;

	public EndResetTask(final EndSettings settings, final World world)
	{
		super(settings);
		this.settings = settings;
		this.world = world;
	}

	@Override
	public void runTask()
	{
		if (Bukkit.getOnlinePlayers().size() > 0)
		{
			super.info("Players online; will reset $1 on last player logout.", this.world);
			return;
		}
		
		final long resetTime = this.settings.getWorldResetTime(this.world);
		this.resetEndWorld(resetTime);
	}

	public boolean resetEndWorld(final long resetTime) { return this.resetEndWorld(resetTime, true); }
	public boolean resetEndWorld(final long resetTime, final boolean confirm)
	{
		boolean success = false;
		if (this.world.getEnvironment() == Environment.THE_END)
		{
			final long now = System.currentTimeMillis();
			
			if (resetTime == 0)
			{
				super.warning("World $1 does not appear to have been scheduled for reset.", this.world.getName());
				super.warning("The world may have been reset manually.");
				super.warning("The task will not reset this world.");
			}
			else
			{
				final long delay = now - resetTime;
				if (delay > 0) super.info(
						"World $1 has been scheduled for reset (task delayed by $2).",
						this.world.getName(),
						Helpers.DateTime.getHumanReadableFromMillis(delay));

				if (confirm)
				{
					CommandExecutor.ejectFromWorld(PagedOutputCommandSender.ConsoleInstance, this.world, this.settings.getEjectMessage());
	
					final int generation = this.settings.getGeneration(this.world) + 1;
					this.settings.recordGeneration(this.world, generation);
	
					final boolean originalKeepSpawnInMemory = this.world.getKeepSpawnInMemory();
					this.world.setKeepSpawnInMemory(false);
					for (final Chunk chunk: this.world.getLoadedChunks()) chunk.unload(true, false);
					this.world.setKeepSpawnInMemory(originalKeepSpawnInMemory);
	
					for (final Chunk chunk: this.world.getLoadedChunks())
					{
						this.settings.recordGeneration(chunk, generation);
						if (!this.world.regenerateChunk(chunk.getX(), chunk.getX()))
							super.severe("Could not regenerate chunk (" + chunk.getX() + "," + chunk.getZ() + ").");
					}
	
					final String message = "End world $1 was reset.";
					Helpers.Broadcasts.sendAll(this.settings.getResetMessage());
					Helpers.Broadcasts.sendPerms("vpcore.moderator", message, this.world.getName());
					super.info(Helpers.Parameters.replace(message, this.world.getName()));
	
					this.settings.removeWorldResetTime(this.world);
					this.world.setSpawnFlags(true, true);
				}
				
				success = true;
			}
		}
		else super.severe(Helpers.Parameters.replace("Cannot reset $1 because it is not an End world.", this.world.getName()));

		return success;
	}
}
