package com.rekalogic.vanillapod.vpcore.end;

import java.util.HashMap;

import org.bukkit.Chunk;
import org.bukkit.World;

public final class GenerationMap extends HashMap<String, Integer>
{
	private static final long serialVersionUID = 1L;

	private static String getSignature(final World world) { return world.getName(); }
	private static String getSignature(final Chunk chunk) { return GenerationMap.getSignature(chunk.getWorld()) + "/" + chunk.getX() + "/" + chunk.getZ(); }

	public void put(final World world, final Integer generation) { super.put(GenerationMap.getSignature(world), generation); }
	public void put(final Chunk chunk, final Integer generation) { super.put(GenerationMap.getSignature(chunk), generation); }

	public Integer get(final World world) { return super.get(GenerationMap.getSignature(world)); }
	public Integer get(final Chunk chunk) { return super.get(GenerationMap.getSignature(chunk)); }

	public boolean containsKey(final World world) { return super.containsKey(GenerationMap.getSignature(world)); }
	public boolean containsKey(final Chunk chunk) { return super.containsKey(GenerationMap.getSignature(chunk)); }
}
