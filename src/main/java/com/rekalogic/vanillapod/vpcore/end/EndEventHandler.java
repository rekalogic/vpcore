package com.rekalogic.vanillapod.vpcore.end;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.ChunkLoadEvent;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class EndEventHandler extends EventHandlerBase<EndSettings>
{
	public EndEventHandler(final PluginBase plugin, final EndSettings settings)
	{
		super(plugin, settings);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onEntityDeath(final EntityDeathEvent event)
	{
		if (event.getEntityType() == EntityType.ENDER_DRAGON)
		{
			final World world = event.getEntity().getWorld();
			if (world.getEnvironment() == Environment.THE_END)
			{
				super.getSettings().enqueueEndReset(world, event.getEntity().getKiller());
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onChunkLoad(final ChunkLoadEvent event)
	{
		final Chunk chunk = event.getChunk();
		if (super.getSettings().isEnabled())
		{
			final World world = event.getWorld();
			if (world.getEnvironment() == Environment.THE_END)
			{
				if (!event.isNewChunk())
				{
					final int generation = super.getSettings().getGeneration(world);

					if (generation > 0)
					{
						final int chunkGeneration = super.getSettings().getGeneration(chunk);
						if (chunkGeneration < generation)
						{
							super.getSettings().recordGeneration(chunk, generation);
							if (!event.getWorld().regenerateChunk(chunk.getX(), chunk.getZ()))
								super.severe("Could not regenerate chunk $1 on load.", chunk);
						}
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onPlayerQuit(final PlayerQuitEvent event)
	{
		if (Bukkit.getOnlinePlayers().size() <= 1)			
		{
			for (final World world: Bukkit.getWorlds())
			{
				if (world.getEnvironment() == Environment.THE_END)
				{
					final long resetTime = super.getSettings().getWorldResetTime(world);
					final long now = System.currentTimeMillis();
					if (resetTime > 0 && resetTime < now)
					{
						EndResetTask task = new EndResetTask(super.getSettings(), world);
						task.runTaskLater(super.getPlugin(), Helpers.DateTime.TicksInASecond);
					}
				}
			}
		}
	}
}
