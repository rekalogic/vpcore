package com.rekalogic.vanillapod.vpcore.end;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Enderman;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class EndSettings extends SettingsBase
{
	private long resetDelay = 60 * 60;
	private String resetMessage = "";
	private String ejectMessage = "";
	private long endermanDespawnTime = 5 * 60;
	
	public long getResetDelay() { return this.resetDelay; }
	public String getResetMessage() { return this.resetMessage; }
	public String getEjectMessage() { return this.ejectMessage; }
	public long getEndermanDespawnTime() { return this.endermanDespawnTime; }

	private final static String CHUNK_GENERATION_FILENAME = "end.chunk";
	private final static String WORLD_GENERATION_FILENAME = "end.world";
	private final static String RESET_TIME_FILENAME = "end.reset";
	private GenerationMap worldGenerationMap = new GenerationMap();
	private GenerationMap chunkGenerationMap = new GenerationMap();
	private Map<String, Long> nextResetTimeMap = new ConcurrentHashMap<String, Long>();

	public EndSettings(final VPPlugin plugin)
	{
		super(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.resetDelay = Helpers.DateTime.getMillis(super.getString("delay"));
		this.resetMessage = super.getString("reset-message");
		this.ejectMessage = super.getString("eject-message");
		this.endermanDespawnTime = Helpers.DateTime.getMillis(super.getString("enderman-despawn-time"));
	}

	@Override
	protected void onSave()
	{
		super.set("delay", Helpers.DateTime.getHumanReadableFromMillis(this.resetDelay));
		super.set("reset-message", this.resetMessage);
		super.set("eject-message", this.ejectMessage);
		super.set("enderman-despawn-time", Helpers.DateTime.getHumanReadableFromMillis(this.endermanDespawnTime));
	}

	@Override
	protected void onEmitInfo()
	{
		super.info("End worlds will be reset $1 after dragon death.", Helpers.DateTime.getHumanReadableFromMillis(this.resetDelay));
		super.info("Message broadcast on End resets: $1", this.resetMessage);
		super.info("End eject message: $1", this.ejectMessage);
		super.info("Endermen will leave The End $1 after dragon death.", Helpers.DateTime.getHumanReadableFromMillis(this.endermanDespawnTime));
	}

	@Override
	protected void onLoadDataStore()
	{
		this.worldGenerationMap = null;
		this.chunkGenerationMap = null;
		this.nextResetTimeMap = null;
		
		super.info("Loading End reset data...");
		super.loadDataStore(
				EndSettings.WORLD_GENERATION_FILENAME,
				new IDataStoreLoader()
				{
					@Override
					public void load(ConfigurationSection section)
					{
						worldGenerationMap = Helpers.Serialisation.getGenerationMap(section);
					}
				});

		super.loadDataStore(
				EndSettings.CHUNK_GENERATION_FILENAME,
				new IDataStoreLoader()
				{
					@Override
					public void load(ConfigurationSection section)
					{
						chunkGenerationMap = Helpers.Serialisation.getGenerationMap(section);
					}
				});

		super.loadDataStore(
				EndSettings.RESET_TIME_FILENAME,
				new IDataStoreLoader()
				{
					@Override
					public void load(ConfigurationSection section)
					{
						nextResetTimeMap = Helpers.Serialisation.getStringLongMap(section);
					}
				});

		if (this.worldGenerationMap == null) this.worldGenerationMap = new GenerationMap();
		if (this.chunkGenerationMap == null) this.chunkGenerationMap = new GenerationMap();
		if (this.nextResetTimeMap == null) this.nextResetTimeMap = new ConcurrentHashMap<String, Long>();

		for (final World world: Bukkit.getWorlds())
		{
			if (world.getEnvironment() == Environment.THE_END)
			{
				if (!this.worldGenerationMap.containsKey(world))
				{
					super.debug("World $1 is generation 0.", world);
					this.worldGenerationMap.put(world, 0);
				}
				else super.debug("World $1 is generation $2.", world, this.worldGenerationMap.get(world));

				long resetTime = this.getWorldResetTime(world);
				if (resetTime > 0)
				{
					resetTime -= System.currentTimeMillis();
					super.debug("World $1 will reset in $2.", world, Helpers.DateTime.getHumanReadableFromMillis(resetTime));
				}
			}
		}

		super.debug(
				"Created generation info for $1 world$2 and $3 chunk$4.",
				this.worldGenerationMap.size(),
				Helpers.getPlural(this.worldGenerationMap.size()),
				this.chunkGenerationMap.size(),
				Helpers.getPlural(this.chunkGenerationMap.size()));
		
		super.onLoadDataStore();
	}

	@Override
	protected void onSaveDataStore()
	{
		super.saveDataStore(
				EndSettings.WORLD_GENERATION_FILENAME,
				new IDataStoreSaver()
				{					
					@Override
					public YamlConfiguration getConfigToSave()
					{
						return Helpers.Serialisation.toConfig(worldGenerationMap);
					}
				});
		
		super.saveDataStore(
				EndSettings.CHUNK_GENERATION_FILENAME,
				new IDataStoreSaver()
				{					
					@Override
					public YamlConfiguration getConfigToSave()
					{
						return Helpers.Serialisation.toConfig(chunkGenerationMap);
					}
				});
		
		super.saveDataStore(
				EndSettings.RESET_TIME_FILENAME,
				new IDataStoreSaver()
				{					
					@Override
					public YamlConfiguration getConfigToSave()
					{
						return Helpers.Serialisation.toConfig(nextResetTimeMap);
					}
				});
	}

	public void setWorldResetTime(final World world, final long delay)
	{
		final long resetTime = System.currentTimeMillis() + delay;
		this.nextResetTimeMap.put(world.getName(), resetTime);
		super.saveDataStore();
	}

	public long getWorldResetTime(final World world)
	{
		long countdown = 0;
		if (this.nextResetTimeMap.containsKey(world.getName()))
			countdown = this.nextResetTimeMap.get(world.getName());
		return countdown;
	}

	public void removeWorldResetTime(final World world)
	{
		if (this.nextResetTimeMap.containsKey(world.getName()))
		{
			this.nextResetTimeMap.remove(world.getName());
			super.saveDataStore();
		}
	}
	
	public int getGeneration(final World world)
	{
		int generation = 0;
		if (this.worldGenerationMap.containsKey(world))
			generation = this.worldGenerationMap.get(world);
		return generation;
	}
	
	public int getGeneration(final Chunk chunk)
	{
		int generation = 0;
		if (this.chunkGenerationMap.containsKey(chunk))
			generation = this.chunkGenerationMap.get(chunk);
		return generation;
	}
	
	public void recordGeneration(final World world, final int generation)
	{
		this.worldGenerationMap.put(world, generation);
	}

	public void recordGeneration(final Chunk chunk, final int generation)
	{
		this.chunkGenerationMap.put(chunk, generation);
	}
	
	public void enqueueEndReset(final World world, final CommandSender killer)
	{
		if (killer != null) super.info("Ender Dragon in $1 has been killed by $2.", world, killer);
		super.info(
				"Queuing reset for $1 in $2.",
				world,
				Helpers.DateTime.getHumanReadableFromMillis(this.getResetDelay()));

		final EndResetTask task = new EndResetTask(this, world);
		task.runTaskLater(super.getPlugin(), Helpers.DateTime.getTicksFromMillis(this.getResetDelay()));
		this.setWorldResetTime(world, this.getResetDelay());

		if (this.getEndermanDespawnTime() > 0)
		{
			final TaskBase endermanTask = new TaskBase(super.isDebug(), "EndermanDespawn")
			{
				@Override
				public void runTask()
				{
					final long resetTime = getWorldResetTime(world);
					if (resetTime == 0)
					{
						warning("Endermen tried to leave $1 but world is not queued for reset!", world);
						warning("The Endermen will stay.");
					}
					else
					{
						debug(
								"It has been $1 since the dragon died in $2.",
								Helpers.DateTime.getHumanReadableFromMillis(getEndermanDespawnTime()),
								world);
						debug("The Endermen are leaving.");
						world.setSpawnFlags(false, false);
						for (final Enderman enderman: world.getEntitiesByClass(Enderman.class))
							enderman.remove();
					}
				}
			};
			endermanTask.runTaskLater(this.getPlugin(), this.getEndermanDespawnTime() * 20);
		}
	}
}
