package com.rekalogic.vanillapod.vpcore.weather;

import org.bukkit.World;

import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class WeatherTask extends TaskBase
{
	private final World world;
	private final long duration;

	public WeatherTask(final WeatherSettings settings, final World world, final long duration)
	{
		super(settings);
		this.world = world;
		this.duration = duration;
	}

	@Override
	public void runTask()
	{
		final long elapsedTimeSinceDurationSet = System.currentTimeMillis() - super.getCreatesTimeMs();
		final long remainingDuration = this.duration - elapsedTimeSinceDurationSet;

		super.debug(
				"Updating the current weather duration from $1 to $2 - $3 of $4 already elapsed.",
				Helpers.DateTime.getHumanReadableFromMillis(this.world.getWeatherDuration()),
				Helpers.DateTime.getHumanReadableFromMillis(remainingDuration),
				Helpers.DateTime.getHumanReadableFromMillis(elapsedTimeSinceDurationSet),
				Helpers.DateTime.getHumanReadableFromMillis(this.duration));

		this.world.setWeatherDuration((int)Helpers.DateTime.getTicksFromMillis(remainingDuration));
	}
}
