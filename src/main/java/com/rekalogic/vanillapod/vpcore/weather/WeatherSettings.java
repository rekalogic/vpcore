package com.rekalogic.vanillapod.vpcore.weather;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.ChatColor;
import org.bukkit.WeatherType;
import org.bukkit.World;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class WeatherSettings extends SettingsBase
{
	private final Random random;

	private long minClear = 0;
	private long maxClear = 0;
	private long minRainy = 0;
	private long maxRainy = 0;
//	private long minClearMillis = 0;
//	private long maxClearMillis = 0;
//	private long maxRainyMillis = 0;
//	private long minRainyMillis = 0;

	private final Map<String, Long> currentWeatherStart = new ConcurrentHashMap<String, Long>();
	private final Map<String, Integer> totalClearTime = new ConcurrentHashMap<String, Integer>();
	private final Map<String, Integer> totalRainyTime = new ConcurrentHashMap<String, Integer>();

	public WeatherSettings(final VPPlugin plugin)
	{
		super(plugin);

		this.random = plugin.getRandom();
	}

	@Override
	protected void onLoad()
	{
		this.minClear = (int)Helpers.DateTime.getMillis(super.getString("min-clear"));
		this.maxClear = (int)Helpers.DateTime.getMillis(super.getString("max-clear"));
		this.minRainy = (int)Helpers.DateTime.getMillis(super.getString("min-rainy"));
		this.maxRainy = (int)Helpers.DateTime.getMillis(super.getString("max-rainy"));

	}

	@Override
	protected void onSave()
	{
		super.set("min-clear", Helpers.DateTime.getHumanReadableFromMillis(this.minClear));
		super.set("max-clear", Helpers.DateTime.getHumanReadableFromMillis(this.maxClear));
		super.set("min-rainy", Helpers.DateTime.getHumanReadableFromMillis(this.minRainy));
		super.set("max-rainy", Helpers.DateTime.getHumanReadableFromMillis(this.maxRainy));
	}

	@Override
	protected void onEmitInfo()
	{
		super.info(
				"Enforcing CLEAR periods of between $1 and $2.",
				Helpers.DateTime.getHumanReadableFromMillis(this.minClear),
				Helpers.DateTime.getHumanReadableFromMillis(this.maxClear));
		super.info(
				"Enforcing RAINY periods of between $1 and $2.",
				Helpers.DateTime.getHumanReadableFromMillis(this.minRainy),
				Helpers.DateTime.getHumanReadableFromMillis(this.maxRainy));
	}

	public long getWeatherStart(final World world)
	{
		if (!this.currentWeatherStart.containsKey(world.getName())) this.currentWeatherStart.put(world.getName(), super.getStartMillis());
		return this.currentWeatherStart.get(world.getName());
	}

	public int getWeatherDuration(final World world)
	{
		return (int)(System.currentTimeMillis() - this.getWeatherStart(world));
	}

	public void markNewPeriod(final World world, final WeatherType weather)
	{
		Map<String, Integer> totalWeatherTime = null;
		if (weather == WeatherType.DOWNFALL) totalWeatherTime = this.totalClearTime;
		else totalWeatherTime = this.totalRainyTime;

		int duration = this.getWeatherDuration(world);
		if (totalWeatherTime.containsKey(world.getName())) duration += totalWeatherTime.get(world.getName());
		totalWeatherTime.put(world.getName(), duration);

		this.currentWeatherStart.put(world.getName(), System.currentTimeMillis());
	}

	public long getMinWeatherDuration(final WeatherType weather)
	{
		if (weather == WeatherType.CLEAR) return this.minClear;
		else return this.minRainy;
	}

	public long getMaxWeatherDuration(final WeatherType weather)
	{
		if (weather == WeatherType.CLEAR) return this.maxClear;
		else return this.maxRainy;
	}

	public static ChatColor getWeatherColor(final WeatherType weather)
	{
		if (weather == WeatherType.CLEAR) return ChatColor.YELLOW;
		else return ChatColor.BLUE;
	}

	public static String getWeatherName(final WeatherType weather)
	{
		if (weather == WeatherType.CLEAR) return "CLEAR";
		else return "RAINY";
	}

	public static String getWeatherNameWithColor(final WeatherType weather)
	{
		return WeatherSettings.getWeatherColor(weather)
				+ WeatherSettings.getWeatherName(weather)
				+ ChatColor.RESET;
	}

	public static WeatherType getNextWeather(final World world)
	{
		return WeatherSettings.getNextWeather(!world.hasStorm());
	}

	public static WeatherType getNextWeather(final boolean toWeather)
	{
		if (toWeather) return WeatherType.DOWNFALL;
		else return WeatherType.CLEAR;
	}

	public static WeatherType getThisWeather(final World world)
	{
		return WeatherSettings.getThisWeather(!world.hasStorm());
	}

	public static WeatherType getThisWeather(final boolean toWeather)
	{
		if (toWeather) return WeatherType.CLEAR;
		else return WeatherType.DOWNFALL;
	}

	public long getRandomDurationForWeather(final WeatherType weather)
	{
		long minDuration = 0;
		long maxDuration = 0;
		if (weather == WeatherType.CLEAR)
		{
			minDuration = this.minClear;
			maxDuration = this.maxClear;
		}
		else
		{
			minDuration = this.minRainy;
			maxDuration = this.maxRainy;
		}

		final int delta = (int)(maxDuration - minDuration);
		return (this.random.nextInt(delta) + minDuration);
	}

	public int getTotalWeatherTime(final World world, final WeatherType weather)
	{
		int duration = 0;

		Map<String, Integer> totalWeatherTime = null;
		if (weather == WeatherType.CLEAR) totalWeatherTime = this.totalClearTime;
		else totalWeatherTime = this.totalRainyTime;

		if (totalWeatherTime.containsKey(world.getName())) duration = totalWeatherTime.get(world.getName());
		return duration;
	}
}
