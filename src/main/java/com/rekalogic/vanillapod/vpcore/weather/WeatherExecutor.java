package com.rekalogic.vanillapod.vpcore.weather;

import org.bukkit.Bukkit;
import org.bukkit.WeatherType;
import org.bukkit.World;
import org.bukkit.World.Environment;

import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class WeatherExecutor extends ExecutorBase<WeatherSettings>
{
	public WeatherExecutor(final PluginBase plugin, final WeatherSettings settings)
	{
		super(plugin, settings, "weather");
	}


	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "weather": return (argCount <= 1);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "weather": return LookupSource.WORLD;
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String shortcut, final String[] args)
	{
		World world = null;
		if (args.length == 1)
		{
			switch (args[0])
			{
			case "*":
			case "all": break;
			default:
				world = sender.getWorld(args[0]);
				if (world == null) return true;
			}
		}
		else
		{
			world = super.getWorld(sender);
			if (world == null) return false;
		}

		World[] worlds = null;
		if (world == null) worlds = Bukkit.getWorlds().toArray(new World[0]);
		else
		{
			if (world.getEnvironment() == Environment.NORMAL) worlds = new World[] { world };
			else
			{
				sender.sendFailure("World $1 is not a NORMAL world.", world.getName());
				return true;
			}
		}

		this.listWeather(sender, worlds);

		return true;
	}

	private void listWeather(final PagedOutputCommandSender sender, final World[] worlds)
	{
		if (worlds.length == 1)
		{
			final World world = worlds[0];
			sender.sendNotification("Weather forecast for $1:", world.getName());
			sender.sendInfo(
					"$1 for the next $2, then $3.",
					WeatherSettings.getWeatherNameWithColor(WeatherSettings.getThisWeather(world)),
					Helpers.DateTime.getHumanReadableFromTicks(world.getWeatherDuration()),
					WeatherSettings.getWeatherNameWithColor(WeatherSettings.getNextWeather(world)));

			long clearTime = super.getSettings().getTotalWeatherTime(world, WeatherType.CLEAR);
			long rainyTime = super.getSettings().getTotalWeatherTime(world, WeatherType.DOWNFALL);

			final long thisWeatherDuration = super.getSettings().getWeatherDuration(world);

			if (WeatherSettings.getThisWeather(world) == WeatherType.CLEAR) clearTime += thisWeatherDuration;
			else rainyTime += thisWeatherDuration;
			sender.sendNotification(
					"Total $1 $2, $3 $4.",
					Helpers.DateTime.getHumanReadableFromMillis(clearTime),
					WeatherSettings.getWeatherName(WeatherType.CLEAR),
					Helpers.DateTime.getHumanReadableFromMillis(rainyTime),
					WeatherSettings.getWeatherName(WeatherType.DOWNFALL));
		}
		else
		{
			sender.sendNotification("Weather forecast:");
			for (int i = 0; i < worlds.length; ++i)
			{
				if (worlds[i].getEnvironment() == Environment.NORMAL)
					sender.sendInfoListItem(
							i,
							Helpers.Parameters.replace(
									"$1: $2 for the next $3, then $4.",
									worlds[i].getName(),
									WeatherSettings.getWeatherNameWithColor(WeatherSettings.getThisWeather(worlds[i])),
									Helpers.DateTime.getHumanReadableFromTicks(worlds[i].getWeatherDuration()),
									WeatherSettings.getWeatherNameWithColor(WeatherSettings.getNextWeather(worlds[i]))));
			}
		}
	}
}
