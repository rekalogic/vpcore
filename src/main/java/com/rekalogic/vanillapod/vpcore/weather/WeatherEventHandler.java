package com.rekalogic.vanillapod.vpcore.weather;

import org.bukkit.WeatherType;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class WeatherEventHandler extends EventHandlerBase<WeatherSettings>
{
	public WeatherEventHandler(final PluginBase plugin, final WeatherSettings settings)
	{
		super(plugin, settings);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onWeatherChange(final WeatherChangeEvent event)
	{
		if (event.isCancelled()) return;

		final World world = event.getWorld();
		final WeatherType thisWeather = WeatherSettings.getThisWeather(event.toWeatherState());
		final WeatherType nextWeather = WeatherSettings.getNextWeather(event.toWeatherState());
		final long currentWeatherStart = super.getSettings().getWeatherStart(event.getWorld());
		final long minimumWeatherPeriod = super.getSettings().getMinWeatherDuration(thisWeather);
		final long currentWeatherDuration = System.currentTimeMillis() - currentWeatherStart;
		if (currentWeatherDuration <= minimumWeatherPeriod)
		{
			final long remainingDuration = minimumWeatherPeriod - currentWeatherDuration;
			world.setWeatherDuration((int)Helpers.DateTime.getTicksFromMillis(remainingDuration + 3));
			event.setCancelled(true);

			final String message = "Weather ($1) postponed for $2: $3 -> $4.";
			Helpers.Broadcasts.sendPerms(
					"vpcore.moderator",
					message,
					event.getWorld(),
					Helpers.DateTime.getHumanReadableFromMillis(remainingDuration),
					WeatherSettings.getWeatherName(thisWeather),
					WeatherSettings.getWeatherName(nextWeather));
			super.debug(
					message,
					event.getWorld(),
					Helpers.DateTime.getHumanReadableFromMillis(remainingDuration),
					WeatherSettings.getWeatherName(thisWeather),
					WeatherSettings.getWeatherName(nextWeather));
		}
		else
		{
			final long nextDuration = super.getSettings().getRandomDurationForWeather(nextWeather);
			final BukkitRunnable task = new WeatherTask(super.getSettings(), world, nextDuration);
			task.runTaskLater(super.getPlugin(), (int)(0.5 * 20));

			super.getSettings().markNewPeriod(event.getWorld(), nextWeather);

			final String message = "Weather ($1) changed: $2 -> $3 for $4.";
			Helpers.Broadcasts.sendPerms(
					"vpcore.moderator",
					message,
					event.getWorld(),
					WeatherSettings.getWeatherName(thisWeather),
					WeatherSettings.getWeatherName(nextWeather),
					Helpers.DateTime.getHumanReadableFromMillis(nextDuration));
			super.debug(
					message,
					event.getWorld(),
					WeatherSettings.getWeatherName(thisWeather),
					WeatherSettings.getWeatherName(nextWeather),
					Helpers.DateTime.getHumanReadableFromMillis(nextDuration));
		}
	}
}
