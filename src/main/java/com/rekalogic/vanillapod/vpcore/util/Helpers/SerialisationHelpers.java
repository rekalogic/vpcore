package com.rekalogic.vanillapod.vpcore.util.Helpers;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import com.rekalogic.vanillapod.vpcore.end.GenerationMap;

public final class SerialisationHelpers
{
	protected static final SerialisationHelpers Instance = new SerialisationHelpers();

	private SerialisationHelpers() { }

	public GenerationMap getGenerationMap(final ConfigurationSection section)
	{
		final GenerationMap map = new GenerationMap();
		if (section.contains("items"))
		{
			final ConfigurationSection itemsSection = section.getConfigurationSection("items");
			final Map<String, Object> values = itemsSection.getValues(false);
			for (final String key: values.keySet())
			{
				final Object valueObject = values.get(key);
				if (valueObject != null)
				{
					try
					{
						final int value = Integer.parseInt(valueObject.toString());
						map.put(key, value);
					}
					catch (Exception ex) { }
				}
			}
		}
		
		return map;
	}
	
	public List<String> getStringList(final ConfigurationSection section)
	{
		return section.getStringList("items");
	}
	
	public Map<String, Long> getStringLongMap(final ConfigurationSection section)
	{
		final Map<String, Long> map = new ConcurrentHashMap<String, Long>();

		if (section.contains("items"))
		{
			final ConfigurationSection itemsSection = section.getConfigurationSection("items");
			final Map<String, Object> values = itemsSection.getValues(false);
			for (final String key: values.keySet())
			{
				final Object valueObject = values.get(key);
				if (valueObject != null)
				{
					try
					{
						final long value = Long.parseLong(valueObject.toString());
						map.put(key, value);
					}
					catch (Exception ex) { }
				}
			}
		}

		return map;
	}

	public YamlConfiguration toConfig(final GenerationMap map)
	{
		YamlConfiguration config = new YamlConfiguration();
		for (final String key: map.keySet())
			config.set("items." + key, map.get(key).toString());
		return config;
	}
	
	public YamlConfiguration toConfig(final List<String> list)
	{
		YamlConfiguration config = new YamlConfiguration();
		config.set("items", list);
		return config;
	}

	public YamlConfiguration toConfig(final Map<String, Long> map)
	{
		YamlConfiguration config = new YamlConfiguration();
		for (final String key: map.keySet())
			config.set("items." + key, map.get(key).toString());
		return config;
	}
}
