package com.rekalogic.vanillapod.vpcore.util.Helpers;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.bukkit.Bukkit;

public final class MailHelpers
{
	protected static final MailHelpers Instance = new MailHelpers();

	private MailHelpers() { }
	
	public void sendMail(
			final String relayHost,
			final int port,
			final String toEmail,
			final String fromEmail,
			final String subject,
			final String body)
	{
		this.sendMail(relayHost, port, toEmail, fromEmail, subject, body, "text/plain");
	}
	
	public void sendMail(
			final String relayHost,
			final int port,
			final String toEmail,
			final String fromEmail,
			final String subject,
			final String body,
			final String mimeType)
	{
		final Properties props = System.getProperties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.smtp.host", relayHost);
		props.setProperty("mail.smtp.port", Integer.toString(port));
		Session session = Session.getDefaultInstance(props);

		try
		{
			final MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
			message.setSubject(subject);
			message.setContent(body, mimeType);
			
			final Transport transport = session.getTransport();
			transport.connect();
			transport.sendMessage(
					message,
					message.getRecipients(Message.RecipientType.TO));
			transport.close();
		}
		catch (Exception ex)
		{
			Bukkit.getLogger().severe(ex.toString());
			Bukkit.getLogger().severe(ex.getStackTrace().toString());
		}
	}
}
