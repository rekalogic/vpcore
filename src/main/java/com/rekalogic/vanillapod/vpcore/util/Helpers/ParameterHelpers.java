package com.rekalogic.vanillapod.vpcore.util.Helpers;

import java.util.Collection;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.plugin.Plugin;

import com.rekalogic.vanillapod.vpcore.player.Group;
import com.rekalogic.vanillapod.vpcore.plot.PlotData;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;

public final class ParameterHelpers
{
	protected static final ParameterHelpers Instance = new ParameterHelpers();

	private ParameterHelpers() { }

	public String replace(final String string, final Object... parameters)
	{
		String resultString = string.replace("$$", "$Þ$");
		if (parameters != null)
		{
			for (int i = 0; i < parameters.length; ++i)
			{
				final String parameter = ParameterHelpers.getParameterString(parameters[i], null, null);
				resultString = resultString.replace("$" + (i + 1), parameter);
			}
		}

		return resultString.replace("$Þ", "$");
	}

	public String replace(final String string, final ChatColor messageColor, final ChatColor paramColor, final Object... parameters)
	{
		String resultString = string;
		if (parameters != null)
		{
			for (int i = 0; i < parameters.length; ++i)
			{
				final String coloredParameter = ParameterHelpers.getParameterString(parameters[i], messageColor, paramColor);
				resultString = resultString.replace("$" + (i + 1), coloredParameter);
			}
		}

		return resultString;
	}

	@SuppressWarnings("rawtypes")
	private static String getParameterString(final Object parameter, final ChatColor messageColor, final ChatColor paramColor)
	{
		String resultString = null;
		if (parameter == null) resultString = ParameterHelpers.wrapInColor("null", ChatColor.DARK_GRAY, messageColor);
		else if (parameter instanceof Object[]) resultString = ParameterHelpers.wrapInColor(((Object[])parameter).length, paramColor, messageColor);
		else if (parameter instanceof Collection) resultString = ParameterHelpers.wrapInColor(((Collection)parameter).size(), paramColor, messageColor);
		else if (parameter instanceof Map) resultString = ParameterHelpers.wrapInColor(((Map)parameter).size(), paramColor, messageColor);
		else if (parameter instanceof World) resultString = ParameterHelpers.wrapInColor(((World)parameter).getName(), paramColor, messageColor);
		else if (parameter instanceof PlotData) resultString = ParameterHelpers.wrapInColor(((PlotData)parameter).getName(), paramColor, messageColor);
		else if (parameter instanceof Player) resultString = ParameterHelpers.wrapInColor(((Player)parameter).getDisplayName(), ChatColor.WHITE, messageColor);
		else if (parameter instanceof OfflinePlayer)
		{
			OfflinePlayer player = (OfflinePlayer)parameter;
			if (player.isOnline()) resultString = ParameterHelpers.wrapInColor(player.getPlayer().getDisplayName(), ChatColor.WHITE, messageColor);
			else resultString = ParameterHelpers.wrapInColor(player.getName(), paramColor, messageColor);
		}
		else if (parameter instanceof AnimalTamer) resultString = ParameterHelpers.wrapInColor(((AnimalTamer)parameter).getName(), ChatColor.WHITE, messageColor);
		else if (parameter instanceof Group)
		{
			final Group group = (Group)parameter;
			resultString = ParameterHelpers.wrapInColor(group.getDisplayName(), group.getColor(), messageColor);
		}
		else if (parameter instanceof Villager)
		{
			if (((Villager)parameter).getCustomName() == null) resultString = ParameterHelpers.wrapInColor("villager", paramColor, messageColor);
			else resultString = ParameterHelpers.wrapInColor(((Villager)parameter).getCustomName(), paramColor, messageColor);
		}
		else if (parameter instanceof Location)
		{
			String worldName = "";
			if (((Location)parameter).getWorld() == null) worldName = "UNKNOWN";
			else worldName = ParameterHelpers.wrapInColor(((Location)parameter).getWorld().getName(), paramColor, messageColor);
			resultString = "(";
			resultString += worldName;
			resultString += ":";
			resultString += ParameterHelpers.wrapInColor(((Location)parameter).getBlockX(), paramColor, messageColor);
			resultString += ",";
			resultString += ParameterHelpers.wrapInColor(((Location)parameter).getBlockY(), paramColor, messageColor);
			resultString += ",";
			resultString += ParameterHelpers.wrapInColor(((Location)parameter).getBlockZ(), paramColor, messageColor);
			resultString += ")";
		}
		else if (parameter instanceof Chunk)
		{
			resultString = "(";
			resultString += ParameterHelpers.wrapInColor(((Chunk)parameter).getWorld().getName(), paramColor, messageColor);
			resultString += ":";
			resultString += ParameterHelpers.wrapInColor(((Chunk)parameter).getX(), paramColor, messageColor);
			resultString += ",";
			resultString += ParameterHelpers.wrapInColor(((Chunk)parameter).getZ(), paramColor, messageColor);
			resultString += ")";
		}
		else if (parameter instanceof Animals)
		{
			final Animals animal = (Animals)parameter;
			resultString = ParameterHelpers.wrapInColor(Helpers.Entity.getDisplayName((Animals)parameter), paramColor, messageColor);
			if (animal.getCustomName() != null)
			{
				resultString += "(";
				resultString += ParameterHelpers.wrapInColor(((Animals)parameter).getCustomName(), paramColor, messageColor);
				resultString += ")";
			}
		}
		else if (parameter instanceof Entity)
		{
			resultString = ParameterHelpers.wrapInColor(Helpers.Entity.getDisplayName((Entity)parameter), paramColor, messageColor);
			resultString += ":";
			resultString += ParameterHelpers.wrapInColor(((Entity)parameter).getEntityId(), paramColor, messageColor);
		}
		else if (parameter instanceof EntityType)
		{
			resultString = ParameterHelpers.wrapInColor(Helpers.Entity.getDisplayName((EntityType)parameter), paramColor, messageColor);
		}
		else if (parameter instanceof Plugin)
		{
			resultString = ParameterHelpers.wrapInColor(((Plugin)parameter).getName(), paramColor, messageColor);
			resultString += " v";
			resultString += ParameterHelpers.wrapInColor(((Plugin)parameter).getDescription().getVersion(), paramColor, messageColor);
		}
		else if (parameter instanceof PagedOutputCommandSender)
		{
			final PagedOutputCommandSender sender = (PagedOutputCommandSender)parameter;
			if (sender.isConsole()) resultString = ParameterHelpers.wrapInColor("CONSOLE", paramColor, messageColor);
			else if (sender.isPlayer()) resultString = ParameterHelpers.getParameterString(sender.getPlayer(), messageColor, paramColor);
			else resultString = ParameterHelpers.wrapInColor("ANONYMOUS", paramColor, messageColor);
		}
		else
		{
			if (parameter instanceof String
					&& ((String)parameter).length() > 1
					&& ((String)parameter).charAt(0) == '#') resultString = ((String)parameter).substring(1);
			else resultString = ParameterHelpers.wrapInColor(parameter.toString(), paramColor, messageColor);
		}
		
		if (resultString == null) resultString = "null";

		return resultString;
	}

	private static String wrapInColor(final int number, final ChatColor startColor, final ChatColor endColor)
	{ return ParameterHelpers.wrapInColor(String.valueOf(number), startColor, endColor); }
	private static String wrapInColor(final String string, final ChatColor startColor, final ChatColor endColor)
	{
		String resultString = string;
		if (startColor != null)
		{
			resultString = startColor + resultString;
			if (endColor == null) resultString = resultString + ChatColor.RESET;
		}
		if (endColor != null) resultString = resultString + endColor;		 
		return resultString;
	}
}
