package com.rekalogic.vanillapod.vpcore.util.Helpers;

import java.util.ArrayList;
import java.util.List;

public final class ArgsHelpers
{
	protected static final ArgsHelpers Instance = new ArgsHelpers();

	private ArgsHelpers() { }
	
	public String[] getStandardisedArgs(final String[] args)
	{
		if (args == null) return null;
		else if (args.length == 0) return args;
		else
		{
			final String concat = this.concatenate(args);
			final List<String> split = this.split(concat);
			if (args[args.length - 1].length() == 0) split.add("");
			return split.toArray(new String[split.size()]);
		}
	}

	public String concatenate(final String[] args) { return this.concatenate(args, 0); }
	public String concatenate(final String[] args, final int startIndex) { return this.concatenate(args, startIndex, args.length - 1); }
	public String concatenate(final String[] args, final int startIndex, int endIndex)
	{
		String concatenated = "";
		if (args != null)
		{
			if (endIndex >= args.length) endIndex = args.length - 1;
			for (int i = startIndex; i <= endIndex; ++i)
			{
				if (concatenated.length() > 0) concatenated += " ";
				concatenated += args[i];
			}
		}
		return concatenated;
	}
	
	private List<String> split(final String string)
	{
		final List<String> strings = new ArrayList<String>();
		
		int jsonLevel = 0;
		char previousChar = ' ';
		boolean inDelimited = false;
		boolean isWholePhraseDelimited = false;
		String phrase = "";
		for (int i = 0; i < string.length(); ++i)
		{
			boolean finishedPhrase = false;
			final char c = string.charAt(i);
			
			boolean addCharacter = true;
			switch (c)
			{
			case ' ':
				if (!inDelimited && jsonLevel == 0)
				{
					finishedPhrase = true;
					addCharacter = false;
				}
				break;
			case '"':
				if (jsonLevel == 0)
				{
					if (previousChar == ' ' && !inDelimited)
						isWholePhraseDelimited = true;
					inDelimited = !inDelimited;
				}
				break;
			case '{': ++jsonLevel; break;
			case '}': --jsonLevel; if (jsonLevel < 0) jsonLevel = 0; break;
			}
			
			if (addCharacter) phrase += c;
			
			if (finishedPhrase || (i + 1 == string.length()))
			{
				if (isWholePhraseDelimited)
				{
					int beginIndex = 0;
					if (phrase.charAt(beginIndex) == '"') beginIndex += 1;
					int endIndex = phrase.length() - 1;
					if (phrase.charAt(endIndex) == '"') endIndex -= 1;
					phrase = phrase.substring(beginIndex, endIndex + 1);
					isWholePhraseDelimited = false;
				}
				strings.add(phrase);
				phrase = "";
			}
		}

		return strings;
	}
}
