package com.rekalogic.vanillapod.vpcore.util.Helpers;

import java.util.Collection;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;

public final class Helpers
{
	public static final ParameterHelpers Parameters = ParameterHelpers.Instance;
	public static final BroadcastHelpers Broadcasts = BroadcastHelpers.Instance;
	public static final MessageHelpers Messages = MessageHelpers.Instance;
	public static final DateTimeHelpers DateTime = DateTimeHelpers.Instance;
	public static final CommandHelpers Commands = CommandHelpers.Instance;
	public static final ArgsHelpers Args = ArgsHelpers.Instance;
	public static final SpawnHelpers Spawn = SpawnHelpers.Instance;
	public static final ItemHelpers Item = ItemHelpers.Instance;
	public static final EntityHelpers Entity = EntityHelpers.Instance;
	public static final BlockHelpers Block = BlockHelpers.Instance;
	public static final MailHelpers Mail = MailHelpers.Instance;
	public static final SerialisationHelpers Serialisation = SerialisationHelpers.Instance;
	public static final HacksHelpers Hacks = HacksHelpers.Instance;
	public static final MoneyHelpers Money = MoneyHelpers.Instance;
	public static final BukkitHelpers Bukkit = BukkitHelpers.Instance;

	public static final ChatColor ColorBroadcast = ChatColor.LIGHT_PURPLE;
	public static final ChatColor ColorNotification = ChatColor.AQUA;
	public static final ChatColor ColorSuccess = ChatColor.GREEN;
	public static final ChatColor ColorFailure = ChatColor.RED;
	public static final ChatColor ColorError = ChatColor.DARK_RED;
	public static final ChatColor ColorWarning = ChatColor.GOLD;

	public static final ChatColor ColorParameter = ChatColor.WHITE;
	public static final ChatColor ColorParameterBroadcast = Helpers.ColorParameter;
	public static final ChatColor ColorParameterNotification = ChatColor.YELLOW;
	public static final ChatColor ColorParameterSuccess = Helpers.ColorParameter;
	public static final ChatColor ColorParameterFailure = Helpers.ColorParameter;
	public static final ChatColor ColorParameterError = Helpers.ColorParameter;
	public static final ChatColor ColorParameterWarning = Helpers.ColorParameter;
	
	private static ChatColor ColorDebug = ChatColor.DARK_GRAY;
	private static ChatColor ColorParameterDebug = ChatColor.GRAY;
	
	public static ChatColor getDebugColor() { return Helpers.ColorDebug; }
	public static void setDebugColor(final ChatColor color) { Helpers.ColorDebug = color; }
	public static ChatColor getDebugParameterColor() { return Helpers.ColorParameterDebug; }
	public static void setDebugParameterColor(final ChatColor color) { Helpers.ColorParameterDebug = color; }

	private Helpers() { }

	public static String formatMessage(final String message)
	{
		return message
				.replace("&0", "§0")
				.replace("&1", "§1")
				.replace("&2", "§2")
				.replace("&3", "§3")
				.replace("&4", "§4")
				.replace("&5", "§5")
				.replace("&6", "§6")
				.replace("&7", "§7")
				.replace("&8", "§8")
				.replace("&9", "§9")
				.replace("&a", "§a")
				.replace("&b", "§b")
				.replace("&c", "§c")
				.replace("&d", "§d")
				.replace("&e", "§e")
				.replace("&f", "§f")
				.replace("&k", "§k")
				.replace("&l", "§l")
				.replace("&m", "§m")
				.replace("&n", "§n")
				.replace("&o", "§o")
				.replace("&r", "§r");
	}

	public static String getCompassDirection(final double degrees)
	{
		final double sector = 22.5;
		final double absDegrees = Math.abs(degrees);
		if (absDegrees <= sector) return "S";
		else if (absDegrees >= (180 - sector)) return "N";
		else
		{
			String sideOfCompass = "W";
			if (degrees > 0) sideOfCompass= "E";
			if (absDegrees <= (45 + sector)) return "S" + sideOfCompass;
			else if (absDegrees >= (90 + sector)) return "N" + sideOfCompass;
			else return sideOfCompass;
		}
	}
	
	public static World getWorld(final PagedOutputCommandSender sender, final String name)
	{
		World world = null;
		for (World testWorld: org.bukkit.Bukkit.getWorlds())
		{
			if (testWorld.getName().equalsIgnoreCase(name))
			{
				world = testWorld;
				break;
			}
		}
		
		if (world == null) sender.sendFailure("No such world $1.", name);
		
		return world;
	}

	public static String getPlural(final Object[] array) { return Helpers.getPlural(array == null ? 0 : array.length); }
	public static <E> String getPlural(final Collection<E> collection) { return Helpers.getPlural(collection == null ? 0 : collection.size()); }
	public static <E, F> String getPlural(final Map<E, F> map) { return Helpers.getPlural(map == null ? 0 : map.size()); }
	public static String getPlural(final int count) { return (count == 1 ? "" : "#s"); }
	public static String getPluralInverse(final int count) { return (count == 1 ? "#s" : ""); }
	public static <E> String getIsAre(final Collection<E> collection) { return Helpers.getIsAre(collection == null ? 0 : collection.size()); }
	public static String getIsAre(final int count) { return (count == 1 ? "#is" : "#are"); }

	public static Block getTargetBlock(final Player player) { return Helpers.getTargetBlock(player, 4); }
	public static Block getTargetBlock(final Player player, final int distance)
	{
		final Location eyeLocation = player.getEyeLocation();
		Block block = eyeLocation.getBlock();
		final Vector direction = eyeLocation.getDirection().normalize();
		for (int i = 0; i <= distance; ++i)
		{
			if (block != null && block.getType() != Material.AIR) break;
			block = eyeLocation.add(direction).getBlock();
		}
		return block;
	}
}
