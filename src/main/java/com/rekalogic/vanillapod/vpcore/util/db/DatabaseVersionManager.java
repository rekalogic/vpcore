package com.rekalogic.vanillapod.vpcore.util.db;

import org.bukkit.plugin.Plugin;

import com.rekalogic.vanillapod.vpcore.player.PlayerDbInterface;
import com.rekalogic.vanillapod.vpcore.util.LoggingBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class DatabaseVersionManager extends LoggingBase
{
	public DatabaseVersionManager(final boolean isDebug)
	{
		super(isDebug);
	}

	public void check(final Plugin plugin, final MySqlConnection connection)
	{
		super.info("Checking database version...");

		final String[] versionComponents = plugin.getDescription().getVersion().split("\\.");
		final int pluginVersion = Integer.parseInt(versionComponents[versionComponents.length - 2]);
		int dbVersion = 0;
		if (!connection.checkTableExists("vp_version"))
		{
			super.info("Creating table vp_version");
			connection.createTable("vp_version", "version INT NOT NULL", "PRIMARY KEY(version)");
			connection.insertInto("vp_version", "version", pluginVersion);
		}
		else
		{
			final MySqlResult result = connection.execute("SELECT * FROM vp_version");
			if (result.moveNext()) dbVersion = result.getInt("version");
		}

		if (dbVersion == pluginVersion) super.info("No database updates necessary.");
		else if (dbVersion > pluginVersion) super.severe("Database version $1 is ahead of plugin version $2!", dbVersion, pluginVersion);
		else
		{
			for (int targetVersion = dbVersion + 1; targetVersion <= pluginVersion; ++targetVersion)
				this.update(connection, dbVersion, targetVersion);
			
			super.info("Database updates complete.");
		}
	}

	private void update(final MySqlConnection connection, final int currentVersion, final int targetVersion)
	{
		if (currentVersion >= targetVersion) return;

		super.info("Updating from v$1 to v$2...", currentVersion, targetVersion);

		switch (targetVersion)
		{
		case 466: this.update466(connection); break;
		case 467: this.update467(connection); break;
		case 468: this.update468(connection); break;
		case 469: this.update469(connection); break;
		case 470: this.update470(connection); break;
		case 471: this.update471(connection); break;
		case 472: this.update472(connection); break;
		case 473: this.update473(connection); break;
		case 477: this.update477(connection); break;
		case 478: this.update478(connection); break;
		case 479: this.update479(connection); break;
		case 480: this.update480(connection); break;
		case 481: this.update481(connection); break;
		case 482: this.update482(connection); break;
		case 483: this.update483(connection); break;
		case 484: this.update484(connection); break;
		case 485: this.update485(connection); break;
		case 486: this.update486(connection); break;
		case 487: this.update487(connection); break;
		case 488: this.update488(connection); break;
		case 489: this.update489(connection); break;
		case 490: this.update490(connection); break;
		case 491: this.update491(connection); break;
		case 492: this.update492(connection); break;
		case 493: this.update493(connection); break;
		case 494: this.update494(connection); break;
		case 495: this.update495(connection); break;
		case 496: this.update496(connection); break;
		case 497: this.update497(connection); break;
		case 498: this.update498(connection); break;
		case 499: this.update499(connection); break;
		case 500: this.update500(connection); break;
		case 501: this.update501(connection); break;
		case 502: this.update502(connection); break;
		case 503: this.update503(connection); break;
		case 504: this.update504(connection); break;

		default:
			super.info("Nothing to do to update database to v$1.", targetVersion);
			return;
		}

		connection.updateAll("vp_version", "version = ?", targetVersion);
	}
	
	private void update504(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_censor_replacement",
				"id INT NOT NULL AUTO_INCREMENT",
				"word VARCHAR(255) NOT NULL",
				"PRIMARY KEY (id)",
				"UNIQUE INDEX (word)");
		
		connection.createTable(
				"vp_censor_behaviour",
				"id INT NOT NULL AUTO_INCREMENT",
				"behaviour VARCHAR(32) NOT NULL",
				"description VARCHAR(255) NOT NULL",
				"PRIMARY KEY (id)",
				"UNIQUE INDEX (behaviour)");
		
		final String replacementWords[] = {
				"unicorn", "rainbow", "stardrop", "princess", "angel", "fluffy",
				"sparkles", "sunshine", "panda", "birthday", "sunbeam", "candybar",
				"bunnyrabbit", "smiles", "candyfloss", "cuddle", "daisy", "dreamy",
				"lollipop", "jelly", "icecream", "lullaby", "pixie", "puppydog" };
		
		for (final String word: replacementWords)
			connection.insertInto("vp_censor_replacement", "word", word);

		connection.insertInto("vp_censor_behaviour", "behaviour, description", "ban", "Bans the player");
		connection.insertInto("vp_censor_behaviour", "behaviour, description", "censor", "Uses built-in censorship logic");
		connection.insertInto("vp_censor_behaviour", "behaviour, description", "disconnect", "Silently disconnects the player");
		connection.insertInto("vp_censor_behaviour", "behaviour, description", "kick", "Disconnects the player with a kick message");
		
		final MySqlResult result = connection.execute("SELECT id FROM vp_censor_behaviour WHERE behaviour = ?", "censor");
		int id = 0;
		if (result.moveNext())
			id = result.getInt("id");

		connection.alterTable("vp_censor", "ADD COLUMN behaviourid INT NOT NULL DEFAULT " + id);
	}
	
	private void update503(final MySqlConnection connection)
	{
		connection.alterTable("vp_sign", "ADD COLUMN metadata VARCHAR(255) AFTER item");
	}
	
	private void update502(final MySqlConnection connection)
	{
		connection.alterTable("vp_baninfo", "CHANGE COLUMN bannedby old_bannedby CHAR(16)");
		connection.alterTable("vp_baninfo", "CHANGE COLUMN unbannedby old_unbannedby CHAR(16)");
		connection.alterTable("vp_baninfo", "ADD COLUMN bannedby CHAR(36) AFTER old_bannedby");
		connection.alterTable("vp_baninfo", "ADD COLUMN unbannedby CHAR(36) AFTER old_unbannedby");
		
		final MySqlResult result =  connection.execute("SELECT id, old_bannedby, old_unbannedby FROM vp_baninfo");
		while (result.moveNext())
		{
			final int id = result.getInt("id");
			final String bannedBy = this.getBannedBy(result.getString("old_bannedby"));
			
			String unbannedBy = result.getString("old_unbannedby");
			if (!result.wasNull()) unbannedBy = this.getBannedBy(unbannedBy);
			
			connection.update("vp_baninfo", "bannedby = ?, unbannedby = ?", "id = ?", bannedBy, unbannedBy, id);
		}
		
		connection.alterTable("vp_baninfo", "DROP COLUMN old_bannedby");
		connection.alterTable("vp_baninfo", "DROP COLUMN old_unbannedby");
		
		connection.createIndex("uuid", "vp_baninfo", "uuid");
		connection.createIndex("active", "vp_baninfo", "active");
	}
	
	private String getBannedBy(final String name)
	{
		final String bannedBy;
		switch (name.toLowerCase())
		{
		case "fyfi13": bannedBy = "1eda6a2a-128d-4512-b5c6-877ee4b74729"; break;
		case "inkbeard":
		case "nikrivers": bannedBy = "67a06b9c-df35-499b-91fa-7a367ffc299a"; break;
		case "valiante": bannedBy = "7ea384e1-dc8f-49ff-a086-85c0192d2e88"; break;
		case "console": bannedBy = null; break;
		default: bannedBy = "";
		}
		return bannedBy;
	}
	
	private void update501(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_book",
				"id INT NOT NULL AUTO_INCREMENT",
				"name VARCHAR(255) NOT NULL",
				"mindelay BIGINT NOT NULL DEFAULT 0",
				"maxcopies INT NOT NULL DEFAULT 1",
				"onfirstjoin INT NOT NULL DEFAULT 0",
				"PRIMARY KEY (id)",
				"UNIQUE INDEX (name)");
		connection.createTable(
				"vp_book_receipt",
				"id INT NOT NULL AUTO_INCREMENT",
				"uuid CHAR(36) NOT NULL",
				"bookid INT NOT NULL",
				"lastgiven BIGINT NOT NULL",
				"totalgiven INT NOT NULL DEFAULT 1",
				"PRIMARY KEY (id)",
				"UNIQUE INDEX (uuid, bookid)");
		final int bookId = connection.insertInto(
				"vp_book",
				"name, mindelay, maxcopies, onfirstjoin",
				"handbook",
				Helpers.DateTime.MillisecondsInAMinute * 15,
				5,
				1);
		connection.executeVoid(
				"INSERT INTO vp_book_receipt (uuid, bookid, lastgiven, totalgiven) " +
				"SELECT uuid, ?, firstlogin, 1 " +
				"FROM vp_online " +
				"WHERE lastusername IS NOT NULL",
				bookId);
	}
	
	private void update500(final MySqlConnection connection)
	{
		connection.alterTable("vp_sign", "DROP PRIMARY KEY");
		connection.alterTable("vp_sign", "ADD COLUMN id INT NOT NULL PRIMARY KEY AUTO_INCREMENT FIRST");
		connection.alterTable("vp_sign", "ADD COLUMN created BIGINT");
		connection.alterTable("vp_sign", "ADD COLUMN destroyed BIGINT");
		connection.createIndex("world_x_y_z", "vp_sign", "world", "x", "y", "z");
		connection.createIndex("destroyed", "vp_sign", "destroyed");
		connection.updateAll("vp_sign", "created = ?", System.currentTimeMillis());
		connection.alterTable("vp_sign", "MODIFY COLUMN created BIGINT NOT NULL");
	}
	
	private void update499(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_alias",
				"id INT NOT NULL AUTO_INCREMENT",				
				"uuid CHAR(36) NOT NULL",
				"firstseen BIGINT NOT NULL",
				"name VARCHAR(64)",
				"PRIMARY KEY(id)");
		connection.executeVoid(
				"INSERT INTO vp_alias (uuid, firstseen, name) " +
				"SELECT uuid, firstlogin, lastusername " +
				"FROM vp_online " +
				"WHERE lastusername IS NOT NULL");
		connection.createIndex("uuid", "vp_alias", "uuid");
		connection.createIndex("name", "vp_alias", "name");
	}
	
	private void update498(final MySqlConnection connection)
	{
		connection.alterTable("vp_item", "DROP COLUMN name");
		connection.alterTable("vp_item", "DROP COLUMN enchants");
	}
	
	private void update497(final MySqlConnection connection)
	{
		final MySqlResult result = connection.execute(
				"SELECT id, name, enchants " +
				"FROM vp_item " +
				"WHERE name IS NOT NULL OR enchants IS NOT NULL");
		while (result.moveNext())
		{
			String metadata = "";
			
			final String name = result.getString("name");
			if (!result.wasNull()) metadata += "name:" + name;
			
			final String enchants = result.getString("enchants");
			if (!result.wasNull()) 
			{
				String enchantmentsJson = "";
				final String[] enchantments = enchants.split(",");
				for (final String enchantment: enchantments)
				{
					final String[] parts = enchantment.split(":");
					if (parts.length == 2)
					{
						if (enchantmentsJson.length() > 0) enchantmentsJson += ",";
						enchantmentsJson +=  Helpers.Parameters.replace("{name:$1,level:$2}", parts[0], parts[1]);
					}
				}
				
				if (enchantmentsJson.length() > 0)
				{
					if (metadata.length() > 0) metadata += ",";
					metadata += Helpers.Parameters.replace("enchantments:[$1]", enchantmentsJson);
				}
			}
			
			if (metadata.length() > 0)
			{
				connection.update(
						"vp_item",
						"metadata = ?",
						"id = ?",
						Helpers.Parameters.replace("{$1}", metadata),
						result.getInt("id"));
			}
		}
	}
		
	private void update496(final MySqlConnection connection)
	{
		connection.alterTable("vp_item", "ADD COLUMN metadata VARCHAR(255)");
	}
	
	private void update495(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_npc",
				"id INT NOT NULL AUTO_INCREMENT",				
				"name VARCHAR(30)",
				"locationid INT",
				"PRIMARY KEY(id)");
	}
	
	private void update494(final MySqlConnection connection)
	{
		connection.executeVoid("DELETE FROM vp_location WHERE id IN (SELECT locationid FROM vp_death)");
		connection.executeVoid("DELETE FROM vp_item");
		connection.executeVoid("DELETE FROM vp_inventory_item");
		connection.executeVoid("DELETE FROM vp_inventory");
		connection.executeVoid("DELETE FROM vp_death");
		connection.alterTable("vp_death", "DROP COLUMN message");
		connection.alterTable("vp_death", "ADD COLUMN cause VARCHAR(32) NOT NULL");
	}
	
	private void update493(final MySqlConnection connection)
	{
		connection.alterTable("vp_motd", "ADD COLUMN active TINYINT NOT NULL DEFAULT 0");
		connection.executeVoid("UPDATE vp_motd SET active = 1");
	}

	private void update492(final MySqlConnection connection)
	{
		connection.createIndex("uuid", "vp_death", "uuid");
		connection.createIndex("occurred", "vp_death", "occurred");
	}
	
	private void update491(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_item",
				"id INT NOT NULL AUTO_INCREMENT",				
				"item VARCHAR(50) NOT NULL",
				"quantity INT DEFAULT 1 NOT NULL",
				"enchants VARCHAR(255)",
				"name VARCHAR(30)",
				"PRIMARY KEY(id)");
		connection.createTable(
				"vp_inventory",				
				"id INT NOT NULL AUTO_INCREMENT",
				"items INT NOT NULL",
				"PRIMARY KEY(id)");
		connection.createTable(
				"vp_inventory_item",
				"inventoryid INT NOT NULL",
				"itemid INT NOT NULL",
				"PRIMARY KEY(inventoryid, itemid)");		
		connection.createTable(
				"vp_death",
				"id INT NOT NULL AUTO_INCREMENT",
				"uuid CHAR(36) NOT NULL",
				"occurred BIGINT NOT NULL",
				"message VARCHAR(256) NOT NULL",
				"locationid INT NOT NULL",
				"lostitems INT DEFAULT 0 NOT NULL",
				"inventoryid INT DEFAULT NULL",
				"PRIMARY KEY(id)");
	}
	
	private void update490(final MySqlConnection connection)
	{
		PlayerDbInterface.fixLoginData(connection);
	}
	
	private void update489(final MySqlConnection connection)
	{
		connection.createIndex("status", "vp_absence_info", "status");
		connection.createIndex("exempt", "vp_group", "exempt");
		connection.createIndex("phpbb_groupid", "vp_group", "phpbb_groupid");
	}
	
	private void update488(final MySqlConnection connection)
	{
		connection.alterTable("vp_player", "DROP COLUMN email");
		connection.executeVoid("DROP TABLE vp_group_map");		
	}
	
	private void update487(final MySqlConnection connection)
	{
		connection.alterTable("vp_online", "ADD COLUMN accrued BIGINT NOT NULL DEFAULT 0");
	}
	
	private void update486(final MySqlConnection connection)
	{
		connection.alterTable("vp_group", "ADD COLUMN phpbb_groupid MEDIUMINT");
		connection.executeVoid(
				"UPDATE vp_group g " +
				"SET g.phpbb_groupid = (SELECT gm.phpbb_groupid FROM vp_group_map gm WHERE gm.groupname = g.groupname)");
		connection.executeVoid(
				"UPDATE vp_group g " +
				"SET g.phpbb_groupid = 0 " +
				"WHERE g.phpbb_groupid IS NULL");
		connection.alterTable("vp_group", "MODIFY COLUMN phpbb_groupid MEDIUMINT NOT NULL");
	}
	
	private void update485(final MySqlConnection connection)
	{
		connection.insertInto("vp_bundle", "bundle, permission", "vpcore_registered", "vpcore.command.holiday");
	}

	private void update484(final MySqlConnection connection)
	{
		connection.alterTable("vp_group", "ADD COLUMN exempt TINYINT(1) NOT NULL DEFAULT 0");
		connection.update("vp_group", "exempt = 1", "groupname = ?", "Regular");
		connection.update("vp_group", "exempt = 1", "groupname = ?", "Veteran");
		connection.update("vp_group", "exempt = 1", "groupname = ?", "Moderator");
		connection.update("vp_group", "exempt = 1", "groupname = ?", "Admin");
	}

	private void update483(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_absence_info",
				"uuid CHAR(36) NOT NULL",
				"updated BIGINT NOT NULL",
				"status VARCHAR(20) NOT NULL",
				"PRIMARY KEY(uuid)");
		connection.createIndex("updated", "vp_absence_info", "updated");
		connection.createIndex("online", "vp_online", "online");
	}
	
	private void update482(final MySqlConnection connection)
	{
		connection.alterTable("vp_online", "ADD COLUMN lastusername VARCHAR(64) DEFAULT NULL");
		connection.alterTable("vp_player", "ADD COLUMN phpbb_userid INT DEFAULT NULL");
		connection.alterTable("vp_player", "ADD COLUMN email VARCHAR(255) DEFAULT NULL");
		connection.updateAll(
				"vp_player",
				"phpbb_userid = (SELECT user_id FROM mcforum.phpbb_profile_fields_data WHERE pf_mc_uuid = uuid)");
		connection.updateAll(
				"vp_player",
				"email = (" +
						"SELECT user_email " +
						"FROM mcforum.phpbb_users u, mcforum.phpbb_profile_fields_data pfd " +
						"WHERE u.user_id = pfd.user_id AND pfd.pf_mc_uuid = uuid)");
	}
	
	private void update481(final MySqlConnection connection)
	{
		connection.alterTable("vp_group", "ADD COLUMN isregistered INT DEFAULT 0");
		connection.update("vp_group", "isregistered = 1", "lower(groupname) = ?", "registered");
	}

	private void update480(final MySqlConnection connection)
	{
		connection.alterTable("vp_plot", "ADD COLUMN groundlevel INT DEFAULT 0");
	}

	private void update479(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_transaction",
				"id INT NOT NULL AUTO_INCREMENT",
				"uuid CHAR(36) NOT NULL",
				"execution BIGINT NOT NULL",
				"type VARCHAR(20) NOT NULL",
				"otheruuid CHAR(36)",
				"item VARCHAR(50)",
				"quantity INT NOT NULL",
				"price DOUBLE NOT NULL",
				"total DOUBLE NOT NULL",
				"PRIMARY KEY(id)");
	}

	private void update478(final MySqlConnection connection)
	{
		connection.createIndex("uuid", "vp_plot", "uuid");
	}

	private void update477(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_plot",
				"name VARCHAR(50) NOT NULL",
				"ispremium TINYINT(1) NOT NULL",
				"markup DOUBLE NOT NULL",
				"buyprice DOUBLE NOT NULL",
				"sellprice DOUBLE NOT NULL",
				"uuid VARCHAR(36)",
				"PRIMARY KEY (name)");
	}

	private void update473(final MySqlConnection connection)
	{
		connection.alterTable("vp_online", "ADD COLUMN exempt TINYINT(1) NOT NULL DEFAULT 0");
	}

	private void update472(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_sign",
				"world VARCHAR(50) NOT NULL",
				"x INT NOT NULL",
				"y INT NOT NULL",
				"z INT NOT NULL",
				"type VARCHAR(20) NOT NULL",
				"isserverowned TINYINT(1) NOT NULL",
				"uuid CHAR(36)",
				"item VARCHAR(50)",
				"quantity INT NOT NULL",
				"price DOUBLE NOT NULL",
				"itemlevel INT NOT NULL",
				"moneylevel DOUBLE NOT NULL",
				"PRIMARY KEY (world, x, y, z)");
	}

	private void update471(final MySqlConnection connection)
	{
		connection.alterTable("vp_player", "ADD COLUMN ip CHAR(15) NULL");
		connection.createTable(
				"vp_iphistory",
				"id INT NOT NULL AUTO_INCREMENT",
				"uuid CHAR(36) NOT NULL",
				"ip CHAR(15) NOT NULL",
				"seen BIGINT NOT NULL",
				"PRIMARY KEY(id)",
				"INDEX (uuid)",
				"INDEX (ip)");
	}

	private void update470(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_mail",
				"id INT NOT NULL AUTO_INCREMENT",
				"sent BIGINT NOT NULL",
				"sender CHAR(36) NOT NULL",
				"recipient CHAR(36) NOT NULL",
				"subject VARCHAR(100)",
				"body VARCHAR (1000)",
				"unread TINYINT(1) NOT NULL DEFAULT 1",
				"deleted TINYINT(1) NOT NULL DEFAULT 0",
				"PRIMARY KEY(id)");
	}

	private void update469(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_motd",
				"id INT NOT NULL AUTO_INCREMENT",
				"motd VARCHAR(100) NOT NULL",
				"PRIMARY KEY(id)");
		connection.createTable(
				"vp_censor",
				"id INT NOT NULL AUTO_INCREMENT",
				"phrase VARCHAR(500) NOT NULL",
				"PRIMARY KEY(id)");
	}

	private void update468(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_home",
				"uuid CHAR(36) NOT NULL",
				"name VARCHAR(50) NOT NULL",
				"locationid INT NOT NULL",
				"PRIMARY KEY(uuid,name)");
		connection.alterTable(
				"vp_group",
				"ADD COLUMN maxhomes INT NOT NULL DEFAULT 1");

		connection.update("vp_group", "maxhomes = ?", "groupname = ?", 3, "Member");
		connection.update("vp_group", "maxhomes = ?", "groupname = ?", 6, "Veteran");
		connection.update("vp_group", "maxhomes = ?", "groupname = ?", 9999, "Moderator");
		connection.update("vp_group", "maxhomes = ?", "groupname = ?", 9999, "Admin");
	}

	private void update467(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_player",
				"uuid CHAR(36) NOT NULL",
				"balance DOUBLE NOT NULL",
				"PRIMARY KEY(uuid)");
	}

	private void update466(final MySqlConnection connection)
	{
		connection.createTable(
				"vp_baninfo",
				"id INT NOT NULL AUTO_INCREMENT",
				"uuid CHAR(36) NOT NULL",
				"name CHAR(16) NOT NULL",
				"bannedby CHAR(16) NOT NULL",
				"bannedon BIGINT NOT NULL",
				"unbannedby CHAR(16) NULL",
				"unbannedon BIGINT NOT NULL DEFAULT 0",
				"expireon BIGINT NOT NULL DEFAULT 0",
				"reason VARCHAR(255) NOT NULL",
				"active TINYINT(1) NOT NULL DEFAULT 1",
				"groupid MEDIUMINT NOT NULL",
				"PRIMARY KEY(id)");
		connection.createTable(
				"vp_online",
				"uuid CHAR(36) NOT NULL",
				"online TINYINT(1) NOT NULL DEFAULT 0",
				"firstlogin BIGINT NOT NULL",
				"lastlogin BIGINT NOT NULL",
				"lastlogout BIGINT NOT NULL",
				"playtime BIGINT NOT NULL",
				"PRIMARY KEY(uuid)");
		connection.createTable(
				"vp_group",
				"groupname VARCHAR(50) NOT NULL",
				"chatcolor VARCHAR(20) NOT NULL",
				"prefix VARCHAR(20) NOT NULL",
				"restrictbuild BIT(1) NOT NULL",
				"isdefault BIT(1) NOT NULL",
				"PRIMARY KEY(groupname)");
		connection.createTable(
				"vp_group_map",
				"groupname VARCHAR(50) NOT NULL",
				"phpbb_groupid MEDIUMINT NOT NULL",
				"PRIMARY KEY(groupname)");
		connection.createTable(
				"vp_group_directpermission",
				"groupname VARCHAR(50) NOT NULL",
				"world VARCHAR(50) NOT NULL",
				"permission VARCHAR(100) NOT NULL",
				"PRIMARY KEY (groupname, world)");
		connection.createTable(
				"vp_group_inheritance",
				"groupname VARCHAR(50) NOT NULL",
				"world VARCHAR(50) NOT NULL",
				"inherits VARCHAR(50) NOT NULL",
				"PRIMARY KEY (groupname, world)");
		connection.createTable(
				"vp_bundle",
				"bundle VARCHAR(50) NOT NULL",
				"permission VARCHAR(100) NOT NULL",
				"KEY(bundle)");
		connection.createTable(
				"vp_group_bundle",
				"groupname VARCHAR(50) NOT NULL",
				"world VARCHAR(50) NOT NULL",
				"bundle VARCHAR(50) NOT NULL",
				"KEY (groupname, world)");
		connection.createTable(
				"vp_location",
				"id INT NOT NULL AUTO_INCREMENT",
				"world CHAR(50) NOT NULL",
				"x DOUBLE NOT NULL",
				"y DOUBLE NOT NULL",
				"z DOUBLE NOT NULL",
				"yaw FLOAT NOT NULL",
				"pitch FLOAT NOT NULL",
				"PRIMARY KEY(id)");
		connection.createTable(
				"vp_spawn",
				"groupname VARCHAR(50) NOT NULL",
				"locationid INT NOT NULL",
				"PRIMARY KEY(groupname)");
		connection.createTable(
				"vp_warp",
				"name VARCHAR(50) NOT NULL",
				"locationid INT NOT NULL",
				"PRIMARY KEY(name)");

		connection.insertInto("vp_group_map", "groupname, phpbb_groupid", "Registered", 3);
		connection.insertInto("vp_group_map", "groupname, phpbb_groupid", "Member", 8);
		connection.insertInto("vp_group_map", "groupname, phpbb_groupid", "Regular", 15);
		connection.insertInto("vp_group_map", "groupname, phpbb_groupid", "Veteran", 9);
		connection.insertInto("vp_group_map", "groupname, phpbb_groupid", "Moderator", 4);
		connection.insertInto("vp_group_map", "groupname, phpbb_groupid", "Admin", 5);

		int locationId = 0;
		locationId = connection.insertInto(
				"vp_location",
				"world, x, y, z, yaw, pitch",
				"VanillaSMP", 17.5, 64, 165.5, -89.699974, 1.2000104);
		connection.insertInto("vp_spawn", "groupname, locationid", "Guest", locationId);
		locationId = connection.insertInto(
				"vp_location",
				"world, x, y, z, yaw, pitch",
				"VanillaSMP", -32.5, 66.5, 68.5, -271.05008, -1.4999981);
		connection.insertInto("vp_spawn", "groupname, locationid", "Registered", locationId);
		locationId = connection.insertInto(
				"vp_location",
				"world, x, y, z, yaw, pitch",
				"VanillaSMP", -32.5, 66.5, 68.5, -271.05008, -1.4999981);
		connection.insertInto("vp_spawn", "groupname, locationid", "Member", locationId);
		locationId = connection.insertInto(
				"vp_location",
				"world, x, y, z, yaw, pitch",
				"VanillaSMP", -32.5, 66.5, 68.5, -271.05008, -1.4999981);
		connection.insertInto("vp_spawn", "groupname, locationid", "Regular", locationId);
		locationId = connection.insertInto(
				"vp_location",
				"world, x, y, z, yaw, pitch",
				"VanillaSMP", -32.5, 66.5, 68.5, -271.05008, -1.4999981);
		connection.insertInto("vp_spawn", "groupname, locationid", "Veteran", locationId);
		locationId = connection.insertInto(
				"vp_location",
				"world, x, y, z, yaw, pitch",
				"VanillaSMP", -32.5, 66.5, 68.5, -271.05008, -1.4999981);
		connection.insertInto("vp_spawn", "groupname, locationid", "Moderator", locationId);
		locationId = connection.insertInto(
				"vp_location",
				"world, x, y, z, yaw, pitch",
				"VanillaSMP", -32.5, 66.5, 68.5, -271.05008, -1.4999981);
		connection.insertInto("vp_spawn", "groupname, locationid", "Admin", locationId);
	}
}
