package com.rekalogic.vanillapod.vpcore.util.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.UUID;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.player.Group;
import com.rekalogic.vanillapod.vpcore.util.LoggingBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class MySqlConnection extends LoggingBase
{
	private Connection connection = null;
	private final String connectionString;
	private final String database;
	
	public MySqlConnection(final boolean debug, final String connectionString, final String database)
	{
		super(debug);

		this.connectionString = connectionString;
		this.database = database;
	}

	public void open(final String username, final String password)
	{
		super.debug("JDBC: Connecting: $1 as $2", this.connectionString, username);
		try
		{
			this.connection = DriverManager.getConnection(this.connectionString, username, password);
			super.debug("JDBC: Connected OK");
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Unable to open connection ($1): $2", this.connectionString, e.getMessage());
			e.printStackTrace();
			this.connection = null;
		}
	}

	public boolean close()
	{
		if (this.connection != null)
		{
			try
			{
				this.connection.close();
				super.debug("JDBC: Connection closed: $1", this.connectionString);
				return true;
			}
			catch (final SQLException e)
			{
				super.severe("JDBC: Unable to close connection gracefully: $1", e.getMessage());
				e.printStackTrace();
			}
			finally
			{
				this.connection = null;
			}
			return false;
		}
		else return true;
	}

	public boolean isOpen()
	{
		return (this.connection != null);
	}

	public void executeVoid(final String sql, final Object... parameters)
	{
		try
		{
			final PreparedStatement statement = this.prepareStatement(sql, parameters);
			statement.executeUpdate();
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Unable to execute statement ($1): $2", sql, e.getMessage());
			e.printStackTrace();
		}
	}

	public MySqlResult execute(final String sql, final Object... parameters)
	{
		super.debug("JDBC: Executing $1", sql);

		try
		{
			final PreparedStatement statement = this.prepareStatement(sql, parameters);
			return new MySqlResult(super.isDebug(), statement.executeQuery());
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Unable to execute query ($1): $2", sql, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	private PreparedStatement prepareStatement(final String sql, final Object... parameters) throws SQLException
	{
		final PreparedStatement statement = this.connection.prepareStatement(sql);

		super.debug("JDBC: Preparing statement: $1", sql);

		if (parameters != null)
		{
			for (int i = 0; i < parameters.length; ++i)
			{
				final int parameterIndex = i + 1;
				Object parameter = parameters[i];
				
				if (parameter == null) statement.setNull(parameterIndex, Types.INTEGER);
				// Morph some high-level types into primitives
				else if (parameter instanceof Player) parameter = ((Player)parameter).getUniqueId();
				else if (parameter instanceof OfflinePlayer) parameter = ((OfflinePlayer)parameter).getUniqueId();
				else if (parameter instanceof Group) parameter = ((Group)parameter).getDisplayName();
				
				this.setParameter(statement, parameterIndex, parameter);
				
				super.debug("JDBC: -- Parameter $1: $2", parameterIndex, parameter);
			}
		}

		return statement;
	}

	private void setParameter(PreparedStatement statement, int index, Object parameter) throws SQLException
	{
		if (parameter == null) statement.setNull(index, Types.INTEGER);
		else
		{
			switch (parameter.getClass().getName())
			{
			case "java.lang.Boolean": statement.setBoolean(index, (boolean)parameter); break;
			case "java.lang.Double": statement.setDouble(index, (double)parameter); break;
			case "java.lang.Float": statement.setFloat(index, (float)parameter); break;
			case "java.lang.Integer": statement.setInt(index, (int)parameter); break;
			case "java.lang.Long": statement.setLong(index, (long)parameter); break;
			case "java.lang.String": statement.setString(index, (String)parameter); break;
			case "java.util.UUID": statement.setString(index, ((UUID)parameter).toString()); break;
			default:
				super.severe("JDBC: Prepared statement argument type $1 not supported.", parameter.getClass().getName());
				break;
			}
		}
	}

	public boolean checkTableExists(final String tableName)
	{
		boolean result = false;

		super.debug("JDBC: Checking whether table $1 exists.", tableName);

		try
		{
			final Statement statement = this.connection.createStatement();
			if (statement.execute(
					Helpers.Parameters.replace(
							"SELECT count(*) FROM information_schema.tables WHERE table_schema = '$1' AND table_name = '$2'",
							database,
							tableName)))
			{
				final ResultSet resultSet = statement.getResultSet();
				if (resultSet.next())
					result = (resultSet.getInt(1) == 1);
			}

			if (result) super.debug("JDBC: Table $1 exists.", tableName);
			else super.debug("JDBC: Table $1 does not exist.", tableName);
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Unable to check if table $1 exists: $2", tableName, e.getMessage());
			e.printStackTrace();
		}

		return result;
	}

	public void createTable(final String tableName, final String... columnDefinitions)
	{
		super.debug("JDBC: Creating table $1", tableName);

		String columnDefinitionLine = "";
		for (final String definition: columnDefinitions)
		{
			if (columnDefinitionLine.length() > 0) columnDefinitionLine += ", ";
			columnDefinitionLine += definition;
		}

		this.executeVoid(
				Helpers.Parameters.replace(
						"CREATE TABLE $1 ($2)",
						tableName,
						columnDefinitionLine));
	}

	public void alterTable(final String tableName, final String... specifications)
	{
		super.debug("JDBC: Altering table $1", tableName);

		String specificationLine = "";
		for (final String specification: specifications)
		{
			if (specificationLine.length() > 0) specificationLine += ", ";
			specificationLine += specification;
		}

		this.executeVoid(
				Helpers.Parameters.replace(
						"ALTER TABLE $1 $2",
						tableName,
						specificationLine));
	}

	public void createIndex(final String indexName, final String tableName, final String... columns)
	{
		super.debug("JDBC: Adding index $1 to $2", indexName, tableName);

		String columnLine = "";
		for (final String column: columns)
		{
			if (columnLine.length() > 0) columnLine += ", ";
			columnLine += column;
		}

		this.executeVoid(
				Helpers.Parameters.replace(
						"CREATE INDEX $1 ON $2 ($3)",
						indexName,
						tableName,
						columnLine));
	}

	public void deleteAllFrom(final String tableName) { this.deleteFrom(tableName, null); }
	public void deleteFrom(final String tableName, final String where, final Object... parameters)
	{
		super.debug("JDBC: Deleting from table $1", tableName);

		if (where == null) this.executeVoid(Helpers.Parameters.replace("DELETE FROM $1", tableName));
		else this.executeVoid(Helpers.Parameters.replace("DELETE FROM $1 WHERE $2", tableName, where), parameters);
	}

	public int insertInto(final String tableName, final String columns, final Object... parameters)
	{
		super.debug("JDBC: Inserting into table $1", tableName);

		int id = 0;
		String placeHolders = "";
		for (int i = 0; i < parameters.length; ++i)
		{
			if (placeHolders.length() > 0) placeHolders += ", ";
			placeHolders += "?";
		}
		final String sql = Helpers.Parameters.replace(
				"INSERT INTO $1 ($2) VALUES ($3)",
				tableName,
				columns,
				placeHolders);
		this.executeVoid(sql, parameters);
		final MySqlResult result = this.execute("SELECT last_insert_id() AS id");
		if (result.moveNext()) id = result.getInt("id");
		return id;
	}

	public void updateAll(final String tableName, final String set, final Object... parameters) { this.update(tableName, set, null, parameters); }
	public void update(final String tableName, final String set, final String where, final Object... parameters)
	{
		super.debug("JDBC: Updating table $1", tableName);

		if (where == null) this.executeVoid(Helpers.Parameters.replace("UPDATE $1 SET $2", tableName, set), parameters);
		else this.executeVoid(Helpers.Parameters.replace("UPDATE $1 SET $2 WHERE $3", tableName, set, where), parameters);
	}
}