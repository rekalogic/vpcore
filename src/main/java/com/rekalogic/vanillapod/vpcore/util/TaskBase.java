package com.rekalogic.vanillapod.vpcore.util;

import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public abstract class TaskBase extends BukkitRunnable
{
	private final boolean isDebug;
	private final String taskName;
	
	private final long createdTimeMs;
	private long startTimeMs = 0;
	
	protected abstract void runTask();
	protected void cancelTask() {}
	
	protected long getCreatesTimeMs() { return this.createdTimeMs; }
	protected long getStartTimeMs() { return this.startTimeMs; }
	
	public TaskBase(final LoggingBase loggingBase)
	{
		this.isDebug = loggingBase.isDebug();
		this.taskName = this.getClass().getSimpleName();
		this.createdTimeMs = System.currentTimeMillis();
	}

	public TaskBase(final boolean isDebug, final String taskName)
	{
		this.isDebug = isDebug;
		this.taskName = taskName;
		this.createdTimeMs = System.currentTimeMillis();
	}
	
	protected final void debug(final String message, final Object... parameters)
	{
		if (this.isDebug) LoggingBase.emitDebug(
				Helpers.Parameters.replace("[TASK=$1] ", this.taskName) + message,
				parameters);
	}

	protected final void info(final String message, final Object... parameters)
	{
		LoggingBase.emitInfo(
				Helpers.Parameters.replace(
						"$1[TASK=$2]$3 ",
						Helpers.getDebugColor(),
						this.taskName,
						ChatColor.RESET) + message,
				parameters);
	}

	protected final void warning(final String message, final Object... parameters)
	{
		LoggingBase.emitWarning(Helpers.Parameters.replace("[TASK=$1] ", this.taskName) + message, parameters);
	}

	protected final void severe(final String message, final Object... parameters)
	{
		LoggingBase.emitSevere(Helpers.Parameters.replace("[TASK=$1] ", this.taskName) + message, parameters);
	}

	@Override
	public final void run()
	{
		try
		{
			this.startTimeMs = System.currentTimeMillis();
			this.debug("Running task (set up $1 ago).", Helpers.DateTime.getHumanReadableFromMillis(this.startTimeMs - this.createdTimeMs));
			this.runTask();
			this.debug("Task complete (took $1).", Helpers.DateTime.getHumanReadableFromMillis(System.currentTimeMillis() - this.startTimeMs));
		}
		catch (Exception e)
		{
			this.severe("An exception occurred while running task $1:", this.taskName);
			e.printStackTrace();
		}
	}
	
	@Override
	public final void cancel()
	{
		try
		{
			this.debug("Cancelling task.");
			super.cancel();
			this.cancelTask();
			this.debug("Cancelled task.");
		}
		catch (Exception e)
		{
			this.severe("An exception occurred while cancelling task $1:", this.taskName);
			e.printStackTrace();
		}
	}
}
