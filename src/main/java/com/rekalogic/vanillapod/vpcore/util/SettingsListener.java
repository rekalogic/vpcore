package com.rekalogic.vanillapod.vpcore.util;

public interface SettingsListener
{
	public void onSettingsSaved();
	public void loadAllSettings();
}
