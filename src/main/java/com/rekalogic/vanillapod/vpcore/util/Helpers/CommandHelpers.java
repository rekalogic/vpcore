package com.rekalogic.vanillapod.vpcore.util.Helpers;

public final class CommandHelpers
{
	protected static final CommandHelpers Instance = new CommandHelpers();

	private CommandHelpers() { }

	public String[] popFlagAndValueArgs(final String[] args, final String flag)
	{
		String[] finalArgs = args;
		final int index = this.getIndexOfFlagArg(finalArgs, flag);
		finalArgs = this.popNthArg(finalArgs, index);
		finalArgs = this.popNthArg(finalArgs, index);
		return finalArgs;
	}

	public String[] popFlagArg(final String[] args, final String flag)
	{
		String[] finalArgs = args;
		final int index = this.getIndexOfFlagArg(finalArgs, flag);
		finalArgs = this.popNthArg(finalArgs, index);
		return finalArgs;
	}
	
	public boolean argsContainsFlag(final String[] args, final String flag)
	{
		return (this.getIndexOfFlagArg(args, flag) != -1);
	}

	public boolean argsContainsFlagValue(final String[] args, final String flag)
	{
		return ((this.getIndexOfFlagArg(args, flag) + 1) < args.length);
	}

	public String getFlagValue(final String[] args, final String flag)
	{
		final int index = this.getIndexOfFlagArg(args, flag);
		if (index >= 0 && index + 1 < args.length) return args[index + 1];
		return "";
	}

	private String[] popNthArg(final String[] args, final int index)
	{
		if (args == null) return null;
		if (args.length <= 1) return new String[0];

		final String[] newArgs = new String[args.length - 1];
		for (int i = 0; i < index; ++i) newArgs[i] = args[i];
		for (int i = index; i < newArgs.length; ++i) newArgs[i] = args[i + 1];
		return newArgs;
	}

	public int getIndexOfFlagArg(final String[] args, String flag)
	{
		if (!flag.startsWith("-")) flag = "-" + flag;
		for (int i = 0; i < args.length; ++i)
			if (args[i].equals(flag)) return i;
		return -1;
	}
}
