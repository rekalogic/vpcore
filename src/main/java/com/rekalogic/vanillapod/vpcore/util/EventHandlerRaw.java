package com.rekalogic.vanillapod.vpcore.util;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;

public abstract class EventHandlerRaw extends LoggingBase implements Listener
{
	private final PluginBase plugin;

	protected boolean isEnabled() { return true; }
	protected void onRegister() { };

	protected final PluginBase getPlugin() { return this.plugin; }

	public EventHandlerRaw(final PluginBase plugin)
	{
		super(plugin.isDebug());

		this.plugin = plugin;
	}

	public final void register()
	{
		this.unregister();
		this.onRegister();

		if (this.isEnabled())
		{
			final PluginManager pluginManager = Bukkit.getPluginManager();
			pluginManager.registerEvents(this, this.plugin);
		}
	}
	
	public final void unregister()
	{
		HandlerList.unregisterAll(this);
	}
}