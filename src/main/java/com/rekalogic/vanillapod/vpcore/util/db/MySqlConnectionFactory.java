package com.rekalogic.vanillapod.vpcore.util.db;

import com.rekalogic.vanillapod.vpcore.util.LoggingBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class MySqlConnectionFactory extends LoggingBase
{
	private String database = "";
	private String username = "";
	private String password = "";

	private String connectionString = "";	

	public String getConnectionString() { return this.connectionString; }
	public String getDatabase() { return this.database; }

	public MySqlConnectionFactory(final boolean debug)
	{
		super(debug);

		try { Class.forName("com.mysql.jdbc.Driver"); }
		catch (final Exception e)
		{
			super.severe("MySQL JDBC driver not available: $1", e.getMessage());
			e.printStackTrace();
		}
	}

	public void initialise(
			final boolean isDebug,
			final String hostname,
			final int port,
			final String database,
			final String username,
			final String password)
	{
		super.setDebug(isDebug);

		this.connectionString = Helpers.Parameters.replace(
				"jdbc:mysql://$1:$2/$3",
				hostname,
				port,
				database);

		this.database = database;
		this.username = username;
		this.password = password;
	}

	public MySqlConnection getConnection()
	{
		MySqlConnection connection = new MySqlConnection(
				super.isDebug(),
				this.getConnectionString(),
				this.getDatabase());
		connection.open(this.username, this.password);
		return connection;
	}
}