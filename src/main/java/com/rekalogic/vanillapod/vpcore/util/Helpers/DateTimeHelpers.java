package com.rekalogic.vanillapod.vpcore.util.Helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateTimeHelpers
{
	protected static final DateTimeHelpers Instance = new DateTimeHelpers();

	public final int TicksInASecond = 20;

	public final int MillisecondsInASecond = 1000;
	public final int SecondsInAMinute = 60;
	public final int MinutesInAnHour = 60;
	public final int HoursInADay = 24;
	public final int DaysInAWeek = 7;

	public final int MillisecondsInATick = this.MillisecondsInASecond / this.TicksInASecond;
	public final int MillisecondsInAMinute = this.MillisecondsInASecond * this.SecondsInAMinute;
	public final int MillisecondsInAnHour = this.MillisecondsInAMinute * this.MinutesInAnHour;
	public final int MillisecondsInADay = this.MillisecondsInAnHour * this.HoursInADay;

	public final int SecondsInAnHour = this.SecondsInAMinute * this.MinutesInAnHour;
	public final int SecondsInADay = this.SecondsInAnHour * this.HoursInADay;
	public final int SecondsInAWeek = this.SecondsInADay * this.DaysInAWeek;

	public final int MinutesInADay = this.MinutesInAnHour * this.HoursInADay;
	public final int MinutesInAWeek = this.MinutesInADay * this.DaysInAWeek;

	public final int HoursInAWeek = this.HoursInADay * this.DaysInAWeek;

	private DateTimeHelpers() {	}

	public String getHumanReadableFromTicks(final int ticks)
	{
		return this.getHumanReadableFromMillis(ticks * Helpers.DateTime.MillisecondsInATick);
	}
	
	public String getHumanReadableFromMillis(final long millis) { return this.getHumanReadableFromMillis(millis, false); }
	public String getHumanReadableFromMillis(final long millis, final boolean suppressMillis)
	{
		if (millis <= 0) return "0s";
		
		long milliseconds = millis;

		String result = "";
		final long days = milliseconds / Helpers.DateTime.MillisecondsInADay;
		milliseconds %= Helpers.DateTime.MillisecondsInADay;
		final int hours = (int)(milliseconds / Helpers.DateTime.MillisecondsInAnHour);
		milliseconds %= Helpers.DateTime.MillisecondsInAnHour;
		final int minutes = (int)(milliseconds / Helpers.DateTime.MillisecondsInAMinute);
		milliseconds %= Helpers.DateTime.MillisecondsInAMinute;
		final int seconds = (int)(milliseconds / Helpers.DateTime.MillisecondsInASecond);
		milliseconds %= Helpers.DateTime.MillisecondsInASecond;

		if (days > 0) result += days + "d ";
		if (hours > 0) result += hours + "h ";
		if (minutes > 0) result += minutes + "m ";
		if (seconds > 0) result += seconds + "s ";
		if (milliseconds > 0 && !suppressMillis) result += milliseconds + "ms";
		else if (millis < Helpers.DateTime.MillisecondsInASecond) result = milliseconds + "ms";
		else result = result.trim();
		
		if (result.length() == 0) result = "0s";

		return result;
	}

	public String getHumanReadableFromGameTime(final long ticks)
	{
		final int hourOffset = 6;
		int hours = (int)(ticks / 1000);
		final int minutes = Math.round(((((float)ticks) / 1000) - hours) * 60);
		hours += hourOffset;
		if (hours >= 24) hours -= 24;

		String timeString = hours + ":";
		if (minutes < 10) timeString += "0";
		timeString += minutes;
		if (hours < 12) timeString += " AM";
		return timeString;
	}

	public String getHumanReadableFromGameFullTime(long fullTicks)
	{
		fullTicks += 6000;
		int days = (int)(fullTicks / 24000) % 365;

		String month = "";
		if (days <= 31) month = "January";
		else if (days <= 59) { month = "February"; days -= 31; }
		else if (days <= 90) { month = "March"; days -= 59; }
		else if (days <= 120) { month = "April"; days -= 90; }
		else if (days <= 151) { month = "May"; days -= 120; }
		else if (days <= 181) { month = "June"; days -= 151; }
		else if (days <= 212) { month = "July"; days -= 181; }
		else if (days <= 243) { month = "August"; days -= 212; }
		else if (days <= 273) { month = "September"; days -= 243; }
		else if (days <= 304) { month = "October"; days -= 273; }
		else if (days <= 334) { month = "November"; days -= 304; }
		else { month = "December"; days -= 334; }

		String suffix = "th";
		switch (days)
		{
		case 1:
		case 21:
		case 31: suffix = "st"; break;
		case 2:
		case 22: suffix = "nd"; break;
		case 3:
		case 23: suffix = "rd"; break;
		}

		return Helpers.Parameters.replace("$1 $2$3", month, days, suffix);
	}
	
	public long getMillis(final String value)
	{
		long result = 0;

		final String[] parts = value.trim()
				.toLowerCase()
				.replace("ms", "MS ")
				.replace("d", "d ")
				.replace("h", "h ")
				.replace("m", "m ")
				.replace("s", "s ")
				.replace("  ", " ")
				.toLowerCase()
				.split(" ");
		int significance = 0;
		int lastSignificance = 0;

		for (final String part: parts)
		{
			if (part == null) continue;
			final String unit = part.substring(part.length() - 1, part.length());
			final int valueLength = part.length() - 1;
			switch (unit)
			{
			case "d":
				significance = 5;
				try { result += Helpers.DateTime.MillisecondsInADay * Long.parseLong(part.substring(0, valueLength)); }
				catch (final NumberFormatException ex) { return -1; }
				break;
			case "h":
				significance = 4;
				try { result += Helpers.DateTime.MillisecondsInAnHour * Long.parseLong(part.substring(0, valueLength)); }
				catch (final NumberFormatException ex) { return -1; }
				break;
			case "m":
				significance = 3;
				try { result += Helpers.DateTime.MillisecondsInAMinute * Long.parseLong(part.substring(0, valueLength)); }
				catch (final NumberFormatException ex) { return -1; }
				break;
			case "s":
				if (part.length() >= 2 && part.charAt(part.length() - 2) == 'm')
				{
					significance = 1;
					try { result += Long.parseLong(part.substring(0, valueLength - 1)); }
					catch (final NumberFormatException ex) { return -1; }
				}
				else
				{
					significance = 2;
					try { result += Helpers.DateTime.MillisecondsInASecond * Long.parseLong(part.substring(0, valueLength)); }
					catch (final NumberFormatException ex) { return -1; }
				}
				break;
			default:
				if (parts.length == 1 && "0123456789".contains(unit))
				{
					try { result = Long.parseLong(part); } catch (final NumberFormatException ex) { }
					return result;
				}
				else return -1;
			}

			if (lastSignificance != 0 && significance > lastSignificance) return -1;
			else lastSignificance = significance;
		}

		return result;
	}

	public long getGameTime(final String time)
	{
		switch (time.toLowerCase())
		{
		case "night": return 14000;
		case "day": return 2000;
		case "dawn": return 0;
		case "dusk": return 12000;
		default:
			try { return Long.parseLong(time); }
			catch (final Exception ex) { }
			break;
		}

		return -1;
	}
	
	public String getDateFromMillis(final long millis)
	{
		return new SimpleDateFormat("dd MMM yy HH:mm:ss").format(new Date(millis));
	}
	
	public String getTruncatedAtHourTimeDifference(final long when)
	{
		return this.getTruncatedAtHourTimeDifference(when, System.currentTimeMillis());
	}
	
	public String getTruncatedAtMinuteTimeDifference(final long when)
	{
		return this.getTruncatedAtMinuteTimeDifference(when, System.currentTimeMillis());
	}

	public String getTruncatedAtHourTimeDifference(final long fromMs, final long toMs)
	{
		final long difference = toMs - fromMs;
		if (difference < Helpers.DateTime.MillisecondsInAnHour) return this.getHumanReadableFromMillis(difference, true);
		else return this.getHumanReadableFromMillis(this.getTruncatedMillis(difference, Helpers.DateTime.MillisecondsInAnHour), true);
	}

	public String getTruncatedAtMinuteTimeDifference(final long fromMs, final long toMs)
	{
		final long difference = toMs - fromMs;
		if (difference < Helpers.DateTime.MillisecondsInAMinute) return this.getHumanReadableFromMillis(difference, true);
		else return this.getHumanReadableFromMillis(this.getTruncatedMillis(difference, Helpers.DateTime.MillisecondsInAMinute), true);
	}
	
	private long getTruncatedMillis(final long milliseconds, final long truncationLimit)
	{
		return (milliseconds / truncationLimit) * truncationLimit;
	}
	
	public long getTicksFromSeconds(int seconds) { return seconds * Helpers.DateTime.TicksInASecond; };
	public long getTicksFromMillis(long millis) { return millis / Helpers.DateTime.MillisecondsInATick; }
}
