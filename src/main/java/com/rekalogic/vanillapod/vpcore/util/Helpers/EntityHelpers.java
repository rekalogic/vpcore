package com.rekalogic.vanillapod.vpcore.util.Helpers;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public final class EntityHelpers
{
	protected static final EntityHelpers Instance = new EntityHelpers();

	private EntityHelpers() { }

	public EntityType getMobType(final String name)
	{
		switch (name.toLowerCase())
		{
		case "bat": return EntityType.BAT;
		case "blaze": return EntityType.BLAZE;
		case "cave_spider":
		case "cavespider": return EntityType.CAVE_SPIDER;
		case "chicken": return EntityType.CHICKEN;
		case "cow": return EntityType.COW;
		case "creeper": return EntityType.CREEPER;
		case "ender_dragon":
		case "enderdragon": return EntityType.ENDER_DRAGON;
		case "enderman": return EntityType.ENDERMAN;
		case "endermite": return EntityType.ENDERMITE;
		case "ghast": return EntityType.GHAST;
		case "giant": return EntityType.GIANT;
		case "guardian": return EntityType.GUARDIAN;
		case "horse": return EntityType.HORSE;
		case "iron_golem":
		case "irongolem": return EntityType.IRON_GOLEM;
		case "magma_cube":
		case "magmacube": return EntityType.MAGMA_CUBE;
		case "mushroom_cow":
		case "mushroomcow": return EntityType.MUSHROOM_COW;
		case "cat":
		case "ocelot": return EntityType.OCELOT;
		case "pig": return EntityType.PIG;
		case "zombiepig":
		case "zombiepigman":
		case "pig_zombie":
		case "pigzombie": return EntityType.PIG_ZOMBIE;
		case "rabbit": return EntityType.RABBIT;
		case "sheep": return EntityType.SHEEP;
		case "silverfish": return EntityType.SILVERFISH;
		case "skeleton": return EntityType.SKELETON;
		case "slime": return EntityType.SLIME;
		case "snowgolem":
		case "snowman": return EntityType.SNOWMAN;
		case "spider": return EntityType.SPIDER;
		case "squid": return EntityType.SQUID;
		case "villager": return EntityType.VILLAGER;
		case "witch": return EntityType.WITCH;
		case "wither": return EntityType.WITHER;
		case "dog":
		case "wolf": return EntityType.WOLF;
		case "zombie": return EntityType.ZOMBIE;
		default: return EntityType.UNKNOWN;
		}
	}

	public String getMobFormalName(final Entity entity) { return this.getDisplayName(entity.getType()); }
	public String getMobFormalName(final EntityType type)
	{
		if (this.isAMob(type)) return type.toString();
		return "NOT_A_MOB";
	}

	public String getDisplayName(final Entity entity) { return this.getDisplayName(entity.getType()); }
	public String getDisplayName(final EntityType type)
	{
		switch (type)
		{
		case PLAYER : return "Player";
		case VILLAGER: return "Villager";

		case ARROW: return "Arrow";
		case BOAT: return "Boat";
		case EGG: return "Egg";
		case DROPPED_ITEM: return "Dropped Item";
		case EXPERIENCE_ORB: return "Experience Orb";
		case ITEM_FRAME: return "Item Frame";
		case PAINTING: return "Painting";
		case PRIMED_TNT: return "Primed TNT";
		case SNOWBALL: return "Snowball";

		case BAT: return "Bat";
		case CHICKEN: return "Chicken";
		case COW: return "Cow";
		case HORSE: return "Horse";
		case MUSHROOM_COW: return "Mushroom Cow";
		case OCELOT: return "Ocelot";
		case PIG: return "Pig";
		case RABBIT: return "Rabbit";
		case SHEEP: return "Sheep";
		case SQUID: return "Squid";
		case WOLF: return "Wolf";

		case IRON_GOLEM: return "Iron Golem";
		case SNOWMAN: return "Snow Golem";

		case BLAZE: return "Blaze";
		case CAVE_SPIDER: return "Cave Spider";
		case CREEPER: return "Creeper";
		case ENDER_DRAGON: return "Ender Dragon";
		case ENDERMAN: return "Enderman";
		case ENDERMITE: return "EnderMite";
		case GHAST: return "Ghast";
		case GIANT: return "Giant";
		case GUARDIAN: return "Guardian";
		case MAGMA_CUBE: return "Magma Cube";
		case PIG_ZOMBIE: return "Pig Zombie";
		case SILVERFISH: return "Silverfish";
		case SKELETON: return "Skeleton";
		case SLIME: return "Slime";
		case SPIDER: return "Spider";
		case WITCH: return "Witch";
		case WITHER: return "Wither";
		case ZOMBIE: return "Zombie";

		default: break;
		}

		return type.toString();
	}

	public boolean isAMob(final EntityType type)
	{
		boolean isAMob = false;
		switch (type)
		{
		case BAT:
		case BLAZE:
		case CAVE_SPIDER:
		case CHICKEN:
		case COW:
		case CREEPER:
		case ENDER_DRAGON:
		case ENDERMAN:
		case ENDERMITE:
		case GHAST:
		case GIANT:
		case GUARDIAN:
		case HORSE:
		case IRON_GOLEM:
		case MAGMA_CUBE:
		case MUSHROOM_COW:
		case OCELOT:
		case PIG:
		case PIG_ZOMBIE:
		case RABBIT:
		case SHEEP:
		case SILVERFISH:
		case SKELETON:
		case SLIME:
		case SNOWMAN:
		case SPIDER:
		case SQUID:
		case VILLAGER:
		case WITCH:
		case WITHER:
		case WOLF:
		case ZOMBIE:
			isAMob = true;
			break;

		default: break;
		}

		return isAMob;
	}
	
	public boolean isAHostileMob(final EntityType type)
	{
		boolean isAHostileMob = false;
		switch (type)
		{
		case BLAZE:
		case CAVE_SPIDER:
		case CREEPER:
		case ENDER_DRAGON:
		case ENDERMITE:
		case GHAST:
		case GIANT:
		case GUARDIAN:
		case MAGMA_CUBE:
		case PIG_ZOMBIE:
		case SILVERFISH:
		case SKELETON:
		case SLIME:
		case SPIDER:
		case WITCH:
		case WITHER:
		case ZOMBIE:
			isAHostileMob = true;
			break;

		default: break;
		}

		return isAHostileMob;
	}
	
	public String formatMessage(final String message, final Entity entity) { return this.formatMessage(message, entity.getType()); }
	public String formatMessage(final String message, final EntityType entityType)
	{
		final String name = Helpers.Entity.getMobFormalName(entityType).toLowerCase();
		String indefiniteArticle = "a";

		switch (name.charAt(0))
		{
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
			indefiniteArticle = "an";
			break;
		}

		return message.replace(
				"$e",
				indefiniteArticle + " " + name).replace(
						"$E",
						"the " + name);
	}	
}
