package com.rekalogic.vanillapod.vpcore.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public abstract class SettingsBase extends LoggingBase
{
	protected abstract void onLoad();
	protected abstract void onSave();
	protected void onEmitSettings() { }
	protected void onEmitInfo() { super.info("No settings to display."); }
	protected void onLoadDataStore() { }
	protected void onSaveDataStore() { }

	private final List<SettingsListener> settingsListeners = new ArrayList<SettingsListener>();

	private final VPPlugin plugin;
	private final String sectionName;
	private final String componentName;
	private boolean isGlobalDebug = false;
	private FileConfiguration config = null;

	private boolean isEnabled = false;
	private final long startMillis = System.currentTimeMillis();
	
	private final static String namespacePrefix = "com.rekalogic.vanillapod.vpcore.";

	protected final VPPlugin getPlugin() { return this.plugin; }
	protected final String getComponentName() { return this.componentName; }

	public SettingsBase(final VPPlugin plugin)
	{
		super(false);
		this.plugin = plugin;
				
		final String packageName = this.getClass().getPackage().getName();
		if (packageName.contains(SettingsBase.namespacePrefix))
		{
			final String className = this.getClass().getName();
			final int settingsIndex = className.indexOf("Settings");
			this.componentName = className.substring(packageName.length() + 1, settingsIndex);		

			final int nameIndex = SettingsBase.namespacePrefix.length();
			this.sectionName = packageName.substring(nameIndex);
		}		
		else
		{
			this.componentName = "UNKNOWN";
			this.sectionName = "UNKNOWN";
		}
	}

	public boolean isEnabled() { return this.isEnabled; }
	public void setEnabled(final boolean isEnabled) { this.isEnabled = isEnabled; }
	public long getStartMillis() { return this.startMillis; }

	public final void addListener(final SettingsListener listener)
	{
		this.settingsListeners.add(listener);
	}

	public final void load(final boolean isDebug, final FileConfiguration config)
	{
		this.isGlobalDebug = isDebug;
		this.config = config;

		this.reload();
	}

	public final void reload()
	{
		this.isEnabled = this.getBoolean("enabled");
		super.setDebug(this.isGlobalDebug || this.getBoolean("debug"));
		if (this.isEnabled())
		{
			super.debug("Loading config and data store for $1 component.", this.componentName);
			this.onLoad();
			this.onLoadDataStore();
		}
	}

	public final void reloadAll()
	{
		for (final SettingsListener listener: this.settingsListeners) listener.loadAllSettings();
	}

	public final void save()
	{
		this.set("enabled", this.isEnabled());
		if (this.isEnabled())
		{
			super.debug("Saving config and data for $1 component.", this.componentName);
			this.onSave();
			this.onSaveDataStore();
			for (final SettingsListener listener: this.settingsListeners) listener.onSettingsSaved();
		}
	}

	public final void saveDataStore()
	{
		if (this.isEnabled())
		{
			super.debug("Saving data store for $1 component.", this.componentName);
			this.onSaveDataStore();
		}
	}

	public final void emitSettings()
	{
		this.emitSetting(this.componentName, this.isEnabled());
		if (this.isEnabled()) this.onEmitSettings();
	}

	public final void emitInfo()
	{
		if (this.isEnabled())
		{
			super.info("$1== $2 Settings ==", ChatColor.YELLOW, this.componentName);
			this.debug("Debugging is enabled!");
			this.onEmitInfo();
		}
	}

	protected final void emitSetting(final String name, final boolean state) { this.emitSetting(name, state, true); }
	protected final void emitSetting(final String name, final boolean state, final boolean fullyEnabled)
	{
		String option = ChatColor.RED + "OFF";
		if (state)
		{
			if (fullyEnabled) option = ChatColor.GREEN + "ON";
			else option = ChatColor.YELLOW + "INACTIVE";
		}
		this.emitSetting(name, option);
	}

	private final void emitSetting(final String name, String option)
	{
		if (option.length() >= 1 && ChatColor.getByChar(option.charAt(0)) == null)
			option = ChatColor.AQUA + option;

		final int paddingLength = 25 - name.length();
		String padding = "";
		for (int i = 0; i < paddingLength; ++i) padding += " ";
		Bukkit.getConsoleSender().sendMessage(
				Helpers.Parameters.replace(
						"[VPCore] $1:$2$3",
						name,
						padding,
						option));
	}

	protected final Set<String> getSubKeys(final String settingName, final boolean deep)
	{
		final ConfigurationSection section = this.config.getConfigurationSection(this.sectionName + "." + settingName);
		return section.getKeys(deep);
	}

	protected final boolean isSet(final String settingName)
	{
		return this.config.isSet(this.sectionName + "." + settingName);
	}

	protected final boolean getBoolean(final String settingName)
	{
		return this.config.getBoolean(this.sectionName + "." + settingName);
	}

	protected final String getString(final String settingName)
	{
		return this.config.getString(this.sectionName + "." + settingName);
	}

	protected final List<String> getStringList(final String settingName)
	{
		return this.config.getStringList(this.sectionName + "." + settingName);
	}

	protected final long getLong(final String settingName)
	{
		return this.config.getLong(this.sectionName + "." + settingName);
	}

	protected final int getInt(final String settingName)
	{
		return this.config.getInt(this.sectionName + "." + settingName);
	}

	protected final double getDouble(final String settingName)
	{
		return this.config.getDouble(this.sectionName + "." + settingName);
	}

	protected final void set(final String settingName, final Object value)
	{
		this.config.set(this.sectionName + "." + settingName, value);
	}

	public interface IDataStoreLoader
	{
		public void load(final ConfigurationSection section);
	}

	public interface IDataStoreSaver
	{
		public YamlConfiguration getConfigToSave();
	}

	protected final void loadDataStore(final String fileName, final IDataStoreLoader loader)
	{
		if (this.plugin.getDataFolder().exists())
		{
			final String path = this.getPlugin().getDataFolder().getAbsolutePath() + File.separatorChar + fileName;
			final File dataFile = new File(path);
			if (dataFile.exists())
			{
				final YamlConfiguration config = new YamlConfiguration();
				try { config.load(path); }
				catch (final Exception ex)
				{
					super.severe("Could not load auxiliary file $1.", path);
					ex.printStackTrace();
					return;
				}
				
				loader.load(config);
			}
			else super.warning("Auxiliary file $1 does not exist.", path);
		}
		else super.severe("Data folder $1 does not exist.", this.getPlugin().getDataFolder().getAbsolutePath());
	}

	protected final void saveDataStore(final String fileName, final IDataStoreSaver saver)
	{
		final String path = this.getPlugin().getDataFolder().getAbsolutePath() + File.separatorChar + fileName;
		final YamlConfiguration config = saver.getConfigToSave();
		try { config.save(path); }
		catch (final Exception ex)
		{
			ex.printStackTrace();
			super.severe("Unable to save auxiliary file $1", path);
		}
	}
}