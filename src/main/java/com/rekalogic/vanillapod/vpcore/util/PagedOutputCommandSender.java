package com.rekalogic.vanillapod.vpcore.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;

import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class PagedOutputCommandSender implements CommandSender
{
	public final static PagedOutputCommandSender ConsoleInstance = new PagedOutputCommandSender(null, null, Bukkit.getConsoleSender(), false, true);
	public final static PagedOutputCommandSender NullInstance = new PagedOutputCommandSender(null, null, null, false, true);
	public final static PagedOutputCommandSender DebugInstance = new PagedOutputCommandSender(null, null, null, false, true, true);
	
	private final PluginBase plugin;
	private final ExecutorBaseRaw executor;
	private final CommandSender sender;
	private final Player player;
	private final List<String> messages;
	private final List<String> errors = new ArrayList<String>();
	private boolean isPaging = false;
	private int page = 1;

	private final boolean isConsole;
	private final boolean isPlayer;
	private final boolean isDebug;

	public boolean isConsole() { return this.isConsole; }
	public boolean isPlayer() { return this.isPlayer; }
	
	public PagedOutputCommandSender(
			final PluginBase plugin,
			final ExecutorBaseRaw executor,
			final CommandSender sender,
			final boolean purge,
			final boolean forceNoPaging)
	{
		this(plugin, executor, sender, purge, forceNoPaging, (executor == null ? false : executor.isDebug()));
	}

	@SuppressWarnings("unchecked")
	private PagedOutputCommandSender(
			final PluginBase plugin,
			final ExecutorBaseRaw executor,
			final CommandSender sender,
			final boolean purge,
			final boolean forceNoPaging,
			final boolean isDebug)
	{
		this.plugin = plugin;
		this.executor = executor;
		this.sender = sender;
		this.isDebug = isDebug;
		
		List<String> messages = null;
		
		if (this.sender instanceof Player)
		{
			this.isPaging = true;
			this.player = (Player)this.sender;
			if (purge) this.player.removeMetadata("vpcore-messages", this.plugin);
			if (this.plugin != null && this.executor != null)
			{
				if (this.player.hasMetadata("vpcore-messages"))
				{
					final List<MetadataValue> metadata = this.player.getMetadata("vpcore-messages");
					for (final MetadataValue value: metadata)
					{
						if (value.getOwningPlugin() == this.plugin)
						{
							messages = (ArrayList<String>)value.value();
							break;
						}
					}
				}
			}
		}
		else this.player = null;

		this.isPlayer = (this.player != null);
		this.isConsole = (!this.isPlayer);
		if (forceNoPaging) this.isPaging = false;
		if (messages == null) messages = new ArrayList<String>();
		else if (!this.isPaging) messages.clear();
		this.messages = messages;
	}

	public boolean is(final OfflinePlayer player)
	{
		return (this.isPlayer && this.getPlayer().equals(player));
	}

	public void suppressPaging()
	{
		this.isPaging = false;
	}

	public void setPage(final String pageNumber)
	{
		this.page = -1;
		try { this.page = Integer.parseInt(pageNumber); }
		catch (final Exception ex) { }
	}

	public boolean canPage()
	{
		return (this.player != null && this.messages.size() > 0);
	}

	public void finish()
	{
		if (this.player != null && this.messages.size() > 0)
		{
			this.showPage(this.page);
			this.player.setMetadata("vpcore-messages", new FixedMetadataValue(this.plugin, this.messages));
		}
		this.errors.clear();
	}

	public Location getLocation()
	{
		if (this.isPlayer) return this.player.getLocation();
		else Helpers.Messages.sendFailure(this.sender, "Cannot automatically get location for CONSOLE/REMOTE.");
		return null;
	}

	public String getDisplayName() { return this.getDisplayName(ChatColor.RESET); }
	public String getDisplayName(final ChatColor resumeColor)
	{
		if (this.isPlayer) return ((Player)this.sender).getDisplayName() + resumeColor;
		else return ChatColor.WHITE + "CONSOLE" + resumeColor;
	}

	public World getWorld()
	{
		if (this.isPlayer) return this.player.getWorld();
		else Helpers.Messages.sendFailure(this.sender, "Cannot automatically get world for CONSOLE/REMOTE.");
		return null;
	}

	public Player getPlayer()
	{
		if (this.isPlayer) return this.player;
		else Helpers.Messages.sendFailure(this.sender, "Cannot automatically get player for CONSOLE/REMOTE.");
		return null;
	}

	public World getWorld(final String name)
	{
		World world = Bukkit.getWorld(name);
		if (world == null)
		{
			try
			{
				final int worldIndex = Integer.parseInt(name);
				if (worldIndex > 0 && worldIndex <= Bukkit.getWorlds().size())
					world = Bukkit.getWorlds().get(worldIndex - 1);
			}
			catch (final NumberFormatException ex) { }
		}
		if (world == null) this.sendFailure("World $1 not found.", name);

		return world;
	}
	
	public void sendInfo(final String message, final Object... parameters)
	{
		Helpers.Messages.sendInfo(this, message, parameters);
		if (this.isDebug) LoggingBase.emitDebug(
				"[$1]$2",
				this.getName(),
				Helpers.Parameters.replace(message, parameters));
	}
	
	public void sendInfo(final ChatColor color, final String message, final Object... parameters)
	{
		Helpers.Messages.sendInfo(this, color, message, parameters);
		if (this.isDebug) LoggingBase.emitDebug(
				"[$1]$2",
				this.getName(),
				Helpers.Parameters.replace(message, parameters));
	}
	
	public void sendInfoListItemWithId(final int id, final String item, final Object... parameters)
	{
		final String message = this.decorateMessageWithGotoIndex(item, parameters);
		Helpers.Messages.sendInfoListItemWithId(
				this,
				id,
				message,
				parameters);
		if (this.isDebug) LoggingBase.emitDebug(
				"[$1]$2",
				this.getName(),
				Helpers.Parameters.replace(message, parameters));
	}
	
	public void sendInfoListItem(final int index, final String item, final Object... parameters)
	{
		final String message = this.decorateMessageWithGotoIndex(item, parameters);
		Helpers.Messages.sendInfoListItem(
				this,
				index,
				message,
				parameters);
		if (this.isDebug) LoggingBase.emitDebug(
				"[$1]$2",
				this.getName(),
				Helpers.Parameters.replace(message, parameters));
	}
	
	private String decorateMessageWithGotoIndex(final String item, final Object... parameters)
	{
		String newItem = item;
		if (parameters != null && this.hasPermission("vpcore.command.goto"))
		{
			for (final Object parameter: parameters)
			{
				final String tag = this.plugin.recordItem(parameter);
				if (tag.length() > 0)
				{
					newItem += " " + tag;
					break;
				}
			}
		}
		return newItem;
	}
	
	public void sendSuccess(final String message, final Object... parameters)
	{
		Helpers.Messages.sendSuccess(this, message, parameters);
		if (this.isDebug) LoggingBase.emitDebug(
				"[$1]$2",
				this.getName(),
				Helpers.Parameters.replace(message, parameters));
	}
	
	public void sendFailure(final String message, final Object... parameters)
	{
		Helpers.Messages.sendFailure(this, message, parameters);
		if (this.isDebug) LoggingBase.emitDebug(
				"[$1]$2",
				this.getName(),
				Helpers.Parameters.replace(message, parameters));
	}
	
	public void sendError(final String message, final Object... parameters)
	{
		if (this.isPaging) this.errors.add(Helpers.Parameters.replace(message, Helpers.ColorError, Helpers.ColorParameterError, parameters));
		Helpers.Messages.sendError(this, message, parameters);
		if (this.isDebug) LoggingBase.emitDebug(
				"[$1]$2",
				this.getName(),
				Helpers.Parameters.replace(message, parameters));
	}
	
	public void sendNotification(final String message, final Object... parameters)
	{
		Helpers.Messages.sendNotification(this, message, parameters);
		if (this.isDebug) LoggingBase.emitDebug(
				"[$1]$2",
				this.getName(),
				Helpers.Parameters.replace(message, parameters));
	}

	public boolean showPage(final int pageNumber)
	{
		if (this.isPaging)
		{
			if (this.messages.size() == 0) Helpers.Messages.sendFailure(this.sender, "No pages available.");
			else if (this.messages.size() <= this.plugin.getPageSize()) this.flush();
			else
			{
				int pageSize = this.plugin.getPageSize();
				final int messageCount = this.messages.size();
				final int maxPage = ((messageCount - 1)/ pageSize) + 1;
				if (pageNumber <= 0 || pageNumber > maxPage) Helpers.Messages.sendFailure(this.sender, "Select a page from $1 to $2 inclusive.", 1, maxPage);
				else
				{
					final int startIndex = (pageNumber - 1) * pageSize;
					int endIndex = startIndex + pageSize;

					if (endIndex >= messageCount) endIndex = messageCount;
					if (messageCount < pageSize) pageSize = messageCount;

					this.sender.sendMessage(ChatColor.DARK_GRAY + "========================================");
					this.sender.sendMessage(
							ChatColor.DARK_GRAY + Helpers.Parameters.replace(
									"Page $1 of $2.",
									ChatColor.DARK_GRAY,
									ChatColor.GRAY,
									pageNumber,
									maxPage));
					this.sender.sendMessage(ChatColor.DARK_GRAY + "----------------------------------------");
					this.sender.sendMessage(this.messages.subList(startIndex, endIndex).toArray(new String[0]));
					this.sender.sendMessage(ChatColor.DARK_GRAY + "----------------------------------------");
					if (pageNumber == maxPage) this.sender.sendMessage(ChatColor.DARK_GRAY + "End of results.");
					else this.sender.sendMessage(
							ChatColor.DARK_GRAY + Helpers.Parameters.replace(
									"Use $1 $2 for more results.",
									ChatColor.DARK_GRAY,
									ChatColor.GRAY,
									"/page",
									pageNumber + 1));
					this.sender.sendMessage(ChatColor.DARK_GRAY + "----------------------------------------");
					if (page == 1) this.outputErrors();
				}
			}
		}
		return true;
	}
	
	private void flush()
	{
		if (this.messages.size() > 0)
		{
			this.sender.sendMessage(this.messages.toArray(new String[0]));
			this.messages.clear();
		}
	}

	private void outputErrors()
	{
		if (this.errors.size() > 0)
		{
			Helpers.Messages.sendError(this.sender, "The following errors occurred:");
			for (final String error: this.errors) Helpers.Messages.sendError(this.sender, "-  " + error);
		}
	}

	@Override
	public void sendMessage(final String message)
	{
		if (this.isPaging) this.messages.add(message);
		else if (this.sender != null) this.sender.sendMessage(message);
	}

	@Override
	public void sendMessage(final String[] messages)
	{
		if (this.isPaging) for (final String message: messages) this.messages.add(message);
		else if (this.sender != null) this.sender.sendMessage(messages);
	}

	@Override
	public boolean isPermissionSet(final String name) { return this.sender.isPermissionSet(name); }

	@Override
	public boolean isPermissionSet(final Permission perm) { return this.sender.isPermissionSet(perm); }

	@Override
	public boolean hasPermission(final String name) { return this.sender.hasPermission(name); }

	@Override
	public boolean hasPermission(final Permission perm) { return this.sender.hasPermission(perm); }

	@Override
	public PermissionAttachment addAttachment(final Plugin plugin, final String name, final boolean value) { return this.sender.addAttachment(plugin, name, value); }

	@Override
	public PermissionAttachment addAttachment(final Plugin plugin) { return this.sender.addAttachment(plugin); }

	@Override
	public PermissionAttachment addAttachment(final Plugin plugin, final String name, final boolean value, final int ticks) { return this.sender.addAttachment(plugin, name, value, ticks); }

	@Override
	public PermissionAttachment addAttachment(final Plugin plugin, final int ticks) { return this.sender.addAttachment(plugin, ticks); }

	@Override
	public void removeAttachment(final PermissionAttachment attachment) { this.sender.removeAttachment(attachment); }

	@Override
	public void recalculatePermissions() { this.sender.recalculatePermissions(); }

	@Override
	public Set<PermissionAttachmentInfo> getEffectivePermissions() { return sender.getEffectivePermissions(); }

	@Override
	public boolean isOp() { return this.sender.isOp(); }

	@Override
	public void setOp(final boolean value) { this.sender.setOp(value); }

	@Override
	public Server getServer() { return this.sender.getServer(); }

	@Override
	public String getName()
	{
		if (this.isConsole) return "CONSOLE";
		else return this.sender.getName();
	}
}
