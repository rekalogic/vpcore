package com.rekalogic.vanillapod.vpcore.util;

import org.bukkit.Bukkit;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public abstract class LoggingBase
{
	private boolean isDebug = false;

	public final boolean isDebug() { return this.isDebug; }
	public final void setDebug(final boolean debug) { this.isDebug = debug; }

	public LoggingBase(final boolean isDebug)
	{
		this.isDebug = isDebug;
	}

	public final static void emitDebug(final String message, final Object... parameters)
	{
		Bukkit.getConsoleSender().sendMessage(
				Helpers.getDebugColor() +
				"[VPCore][DEBUG] " + Helpers.Parameters.replace(
						message,
						Helpers.getDebugColor(),
						Helpers.getDebugParameterColor(),
						parameters));			
	}
	
	protected final void debug(final String message, final Object... parameters)
	{
		if (this.isDebug) LoggingBase.emitDebug(message, parameters);
	}

	public final static void emitInfo(final String message, final Object... parameters)
	{
		Bukkit.getConsoleSender().sendMessage(
				"[VPCore] " + Helpers.Parameters.replace(
						message,
						parameters));
	}
	
	protected final void info(final String message, final Object... parameters)
	{
		LoggingBase.emitInfo(message, parameters);
	}

	public final static void emitWarning(final String message, final Object... parameters)
	{
		Bukkit.getConsoleSender().sendMessage(
				Helpers.ColorWarning +
				"[VPCore][WARN] " + Helpers.Parameters.replace(
						message,
						Helpers.ColorWarning,
						Helpers.ColorParameterWarning,
						parameters));
	}
	
	protected final void warning(final String message, final Object... parameters)
	{
		LoggingBase.emitWarning(message, parameters);
	}

	public final static void emitSevere(final String message, final Object... parameters)
	{
		Bukkit.getConsoleSender().sendMessage(
				Helpers.ColorError +
				"[VPCore][SEVERE] " + Helpers.Parameters.replace(
						message,
						Helpers.ColorError,
						Helpers.ColorParameterError,
						parameters));
	}
	
	protected final void severe(final String message, final Object... parameters)
	{
		LoggingBase.emitSevere(message, parameters);
	}
}
