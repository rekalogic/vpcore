package com.rekalogic.vanillapod.vpcore.util.Helpers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.rekalogic.vanillapod.vpcore.util.LoggingBase;

@SuppressWarnings("unused") public final class MessageHelpers
{
	protected static final MessageHelpers Instance = new MessageHelpers();

	private MessageHelpers() { }

	public void sendInfo(final CommandSender sender, final String message, final Object... parameters)
	{
		sender.sendMessage(
				ParameterHelpers.Instance.replace(
						message,
						parameters));
	}

	public void sendInfo(final CommandSender sender, final ChatColor color, final String message, final Object... parameters)
	{
		sender.sendMessage(
				color +
				ParameterHelpers.Instance.replace(
						message,
						color,
						color,
						parameters));
	}

	public void sendNotification(final CommandSender sender, final String message, final Object... parameters)
	{
		sender.sendMessage(
				Helpers.ColorNotification +
				ParameterHelpers.Instance.replace(
						message,
						Helpers.ColorNotification,
						Helpers.ColorParameterNotification,
						parameters));
	}

	public void sendSuccess(final CommandSender sender, final String message, final Object... parameters)
	{
		sender.sendMessage(
				Helpers.ColorSuccess +
				ParameterHelpers.Instance.replace(
						message,
						Helpers.ColorSuccess,
						Helpers.ColorParameterSuccess,
						parameters));
	}

	public void sendFailure(final CommandSender sender, final String message, final Object... parameters)
	{
		sender.sendMessage(
				Helpers.ColorFailure +
				ParameterHelpers.Instance.replace(
						message,
						Helpers.ColorFailure,
						Helpers.ColorParameterFailure,
						parameters));
	}

	public void sendError(final CommandSender sender, final String message, final Object... parameters)
	{
		final String finalMessage = Helpers.ColorError + ParameterHelpers.Instance.replace(
				"[ERROR] " + message,
				Helpers.ColorError,
				Helpers.ColorParameterError,
				parameters);
 
		sender.sendMessage(
				Helpers.ColorError +
				ParameterHelpers.Instance.replace(
						"[ERROR] " + message,
						Helpers.ColorError,
						Helpers.ColorParameterError,
						parameters));
		this.sendInfo(sender, "-- Please report this at http://mc.rv.rs/link/help");

		LoggingBase.emitSevere(
				Helpers.ColorError +
				ParameterHelpers.Instance.replace(
						message,
						Helpers.ColorError,
						Helpers.ColorParameterError,
						parameters));
	}

	public void sendInfoListItem(final CommandSender sender, final int index, final String item, final Object... parameters)
	{
		this.sendInfoListItemInternal(sender, index + 1, item, parameters);
	}
	
	public void sendInfoListItemWithId(final CommandSender sender, final int id, final String item, final Object... parameters)
	{
		this.sendInfoListItemInternal(sender, id, item, parameters);
	}
	
	private void sendInfoListItemInternal(final CommandSender sender, final int id, final String item, final Object... parameters)
	{
		final String itemString = ParameterHelpers.Instance.replace(item, parameters);
		this.sendInfo(sender, "    $1$2$3: $4", ChatColor.GOLD, id, ChatColor.RESET, itemString);
	}
}
