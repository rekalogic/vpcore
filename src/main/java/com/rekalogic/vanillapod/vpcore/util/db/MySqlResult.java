package com.rekalogic.vanillapod.vpcore.util.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.UUID;

import com.rekalogic.vanillapod.vpcore.util.LoggingBase;

public final class MySqlResult extends LoggingBase
{
	private final ResultSet resultSet;
	private boolean beforeStart = true;

	public MySqlResult(final boolean debug, final ResultSet resultSet)
	{
		super(debug);

		this.resultSet = resultSet;
	}

	public String getString(final String columnName)
	{
		if (this.beforeStart) super.warning("JDBC: Before start of result set.");

		String value = "";
		try { value = this.resultSet.getString(columnName); }
		catch (final SQLException e)
		{
			super.severe("JDBC: Unable to retrieve STRING '$1' from result: $2", columnName, e.getMessage());
			e.printStackTrace();
		}
		return value;
	}

	public int getInt(final String columnName) { return this.getInt(columnName, 0); }
	public int getInt(final String columnName, final int defaultValue)
	{
		if (this.beforeStart) super.warning("JDBC: Before start of result set.");

		int value = defaultValue;
		try
		{
			value = this.resultSet.getInt(columnName);
			if (this.resultSet.wasNull()) value = defaultValue;
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Unable to retrieve INT '$1' from result: $2", columnName, e.getMessage());
			e.printStackTrace();
		}
		return value;
	}

	public float getFloat(final String columnName) { return this.getFloat(columnName, 0); }
	public float getFloat(final String columnName, final float defaultValue)
	{
		if (this.beforeStart) super.warning("JDBC: Before start of result set.");

		float value = defaultValue;
		try
		{
			value = this.resultSet.getFloat(columnName);
			if (this.resultSet.wasNull()) value = defaultValue;
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Unable to retrieve FLOAT '$1' from result: $2", columnName, e.getMessage());
			e.printStackTrace();
		}
		return value;
	}

	public double getDouble(final String columnName) { return this.getDouble(columnName, 0); }
	public double getDouble(final String columnName, final double defaultValue)
	{
		if (this.beforeStart) super.warning("JDBC: Before start of result set.");

		double value = defaultValue;
		try
		{
			value = this.resultSet.getDouble(columnName);
			if (this.resultSet.wasNull()) value = defaultValue;
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Unable to retrieve DOUBLE '$1' from result: $2", columnName, e.getMessage());
			e.printStackTrace();
		}
		return value;
	}

	public long getLong(final String columnName) { return this.getLong(columnName, 0); }
	public long getLong(final String columnName, final long defaultValue)
	{
		if (this.beforeStart) super.warning("JDBC: Before start of result set.");

		long value = defaultValue;
		try
		{
			value = this.resultSet.getLong(columnName);
			if (this.resultSet.wasNull()) value = defaultValue;
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Unable to retrieve INT '$1' from result: $2", columnName, e.getMessage());
			e.printStackTrace();
		}
		return value;
	}

	public boolean getBoolean(final String columnName)
	{
		final int value = this.getInt(columnName, 0);
		switch (value)
		{
		case 0: return false;
		case 1: return true;

		default:
			super.warning("JDBC: Value for '$1' was unexpected BOOLEAN ($2).  Assuming TRUE.", value, columnName);
			break;
		}

		return true;
	}

	public UUID getUuid(final String columnName)
	{
		final String value = this.getString(columnName);
		if (value == null || value.length() == 0) return null;
		return UUID.fromString(value);
	}

	public boolean moveNext()
	{
		boolean moveNext = false;
		try
		{
			moveNext = this.resultSet.next();
			this.beforeStart = false;
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Failed to move to next row in result set: $1", e.getMessage());
			e.printStackTrace();
		}

		return moveNext;
	}

	public boolean wasNull()
	{
		boolean wasNull = false;
		try
		{
			wasNull = this.resultSet.wasNull();
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Could not determine whether last column was null: $1", e.getMessage());
			e.printStackTrace();
		}

		return wasNull;
	}
	
	public boolean hasColumn(final String name) { return this.hasColumns(name); }
	public boolean hasColumns(final String... names)
	{
		int existCount = 0;
		try
		{
			final ResultSetMetaData metaData = this.resultSet.getMetaData();
			for (int i = 1; i <= metaData.getColumnCount(); ++i)
			{
				for (int j = 0; j < names.length; ++j)
				{
					if (names[j].equals(metaData.getColumnLabel(i)))
					{
						++existCount;
						break;
					}
				}
			}
		}
		catch (final SQLException e)
		{
			super.severe("JDBC: Could not determine whether column(s) exist: $1", e.getMessage());
			e.printStackTrace();
		}
		
		return (existCount == names.length);
	}
}
