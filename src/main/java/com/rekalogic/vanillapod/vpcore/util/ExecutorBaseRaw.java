package com.rekalogic.vanillapod.vpcore.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.player.PermissionManager;
import com.rekalogic.vanillapod.vpcore.player.Group;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public abstract class ExecutorBaseRaw extends LoggingBase implements CommandExecutor, TabExecutor
{
	private static final List<String> emptyTabCompleteList = new ArrayList<String>();
	
	protected abstract boolean onCommandRaw(
			final PagedOutputCommandSender sender,
			final String command,
			final String label,
			final String[] args);

	protected boolean checkCommandAllowed(final String command, final boolean isConsole) { return true; }
	protected boolean checkParameters(final String command, final int argCount) { return (argCount == 0); }
	protected String[] getAllowedFlags(final String command) { return null; }	
	protected boolean outputCustomUsage(final CommandSender sender, final String command, final String label) { return false; }
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args) { return ExecutorBaseRaw.emptyTabCompleteList; }
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args) { return LookupSource.NONE; }
	
	protected boolean shouldCommandPurgePagedOutput(final String command) { return true; }
	protected abstract boolean getSettingsDebug();

	private final PluginBase plugin;
	private final String[] myCommands;
	private final Random random = new Random();

	private final Map<String, String> flagValues = new ConcurrentHashMap<String, String>();
	private final Map<String, Boolean> flagStates = new ConcurrentHashMap<String, Boolean>();

	public ExecutorBaseRaw(final PluginBase plugin, final boolean isDebug, final String... myCommands)
	{
		super(isDebug);
		this.plugin = plugin;
		this.myCommands = myCommands;

		for (final String command: this.myCommands)
		{
			final PluginCommand pluginCommand = plugin.getCommand(command);
			if (pluginCommand != null) pluginCommand.setExecutor(this);
			else super.severe("Could not hook command /$1.", command);
		}
	}

	protected final PluginBase getPlugin() { return this.plugin; }
	
	public final void updateDebug() { super.setDebug(this.getSettingsDebug()); }

	private List<String> getAllAllowedFlags(final String command)
	{
		final List<String> flags = new ArrayList<String>(1);
		flags.add(ExecutorBase.FLAG_BOOL_PAGEDOUTPUT);

		final String[] commandFlags = this.getAllowedFlags(command);
		if (commandFlags != null)
			for (final String flag: commandFlags)
				if (!flags.contains(flag)) flags.add(flag);

		return flags;
	}

	@Override
	public final List<String> onTabComplete(final CommandSender sender, final Command command, final String alias, final String[] originalArgs)
	{
		final String[] args = Helpers.Args.getStandardisedArgs(originalArgs);		
		final String token = args[args.length - 1];
		final List<String> results = new ArrayList<String>();
		final List<String> flags = this.getAllAllowedFlags(command.getName());

		if (token.length() > 0 && token.charAt(0) == '-')
		{			
			for (final String flag: flags)
			{
				final String fullFlag = "-" + flag;
				if (token.length() == 1 || fullFlag.startsWith(token))
					results.add(fullFlag);
			}
			return results;
		}
		
		boolean handled = false;
		if (args.length >= 2)
		{
			String flag = args[args.length - 2];
			if (flag == null || flag.length() == 0) return results;			
			else if (flag.charAt(0) == '-')
			{
				flag = flag.substring(1);
				
				if (flags.contains(flag))
				{
					switch (flag)
					{
					case ExecutorBase.FLAG_GROUP:
						this.addGroupMatches(results, token);
						break;
		
					case ExecutorBase.FLAG_PLAYER:
						final boolean includeVanished = sender.isPermissionSet("vanish.see");
						this.addPlayerMatches(results, token, true, includeVanished);
						break;
		
					case ExecutorBase.FLAG_WORLD:
						this.addWorldMatches(results, token);
						break;
		
					case ExecutorBase.FLAG_BOOL_ALLWORLDS:
					case ExecutorBase.FLAG_BOOL_CONFIRM:
					case ExecutorBase.FLAG_BOOL_LOAD:
					case ExecutorBase.FLAG_BOOL_PAGEDOUTPUT:
					case ExecutorBase.FLAG_BOOL_SILENT:
					case ExecutorBase.FLAG_BOOL_VERBOSE:
						Helpers.Messages.sendFailure(sender, "Flag $1 does not take an argument.", flag);
						break;
		
					case ExecutorBase.FLAG_TOP:
						Helpers.Messages.sendFailure(sender, "Flag $1 takes an integer argument.", flag);
						break;
		
					case ExecutorBase.FLAG_TIME:
						final int fiveDays = Helpers.DateTime.MillisecondsInADay * 5;
						final int oneDay = Helpers.DateTime.MillisecondsInADay;
						Helpers.Messages.sendFailure(
								sender,
								"Flag $1 takes an argument of the form $2.",
								flag,
								Helpers.DateTime.getHumanReadableFromMillis(this.random.nextInt((int)fiveDays) + oneDay), true);
						break;
		
					default:
						Helpers.Messages.sendFailure(sender, "Flag $1 does not take an argument.", flag);
						break;
					}
				}
				else Helpers.Messages.sendFailure(sender, "Flag $1 is not valid for $2.", flag, command.getName());
				
				handled = true;
			}
			else handled = false;
		}
		
		final int argIndex = args.length - 1;
		if (!handled)
		{
			final LookupSource lookup = this.getParameterLookupCollection(command.getName(), argIndex, args);
			switch (lookup)
			{
			case PLAYER:
			{
				final boolean includeVanished = sender.isPermissionSet("vanish.see");
				this.addPlayerMatches(results, token, false, includeVanished);
			} break;
			case OFFLINE_PLAYER:
			{
				if (token.length() < 3)
				{
					Helpers.Messages.sendNotification(sender, "Type 3 or more characters to include offline players.");
					final boolean includeVanished = sender.isPermissionSet("vanish.see");
					this.addPlayerMatches(results, token, false, includeVanished);
				}
				else this.addPlayerMatches(results, token, true);
			} break;
			case GROUP: this.addGroupMatches(results, token); break;
			case WORLD: this.addWorldMatches(results, token); break;
			
			case FROM_PLUGIN:
			{
				List<String> customResults = this.onTabComplete(sender, command.getName(), argIndex, args);
				final String lowerToken = token.toLowerCase();
				if (customResults == null) super.severe("Unable to tab-complete parameter #$1 for '$2'.", argIndex, command.getName());
				else
				{
					for (final String matchToken: customResults)
					{
						if (matchToken.toLowerCase().startsWith(lowerToken))
							results.add(matchToken);
					}
				}
			}
			
			case NONE:
			default: break;
			}
			
			if (lookup != LookupSource.NONE &&
					results.size() == 0)
				Helpers.Messages.sendFailure(sender, "There are no matches.");
		}
		
		final int resultCount = results.size();
		if (resultCount > 0)
		{
			final String[] resultArray = results.toArray(new String[resultCount]);
			Arrays.sort(resultArray, new Comparator<String>()
			{
				@Override
				public int compare(final String o1, final String o2)
				{
					return o1.compareToIgnoreCase(o2);
				}
			});
			results.clear();
			for (final String result: resultArray) results.add(result);
		}
		
		return results;
	}
	
	private void addWorldMatches(List<String> results, String token)
	{
		final String lowerToken = token.toLowerCase();
		final List<World> worlds = Bukkit.getWorlds();
		for (final World world: worlds)
			if (world.getName().toLowerCase().startsWith(lowerToken))
				results.add(world.getName());
	}
	
	private void addPlayerMatches(final List<String> results, final String token, final boolean includeOffline) { this.addPlayerMatches(results, token, includeOffline, true); }
	private void addPlayerMatches(final List<String> results, final String token, final boolean includeOffline, final boolean includeVanished)
	{
		final String lowerToken = token.toLowerCase();
		if (includeOffline)
		{
			final OfflinePlayer[] players = Helpers.Bukkit.getOfflinePlayers();
			for (final OfflinePlayer player: players)
				if (player.getName().toLowerCase().startsWith(lowerToken))
					results.add(player.getName());
			
			for (final String alias: this.plugin.getAliases())
				if (alias.toLowerCase().startsWith(lowerToken) &&
						!results.contains(alias))
					results.add(alias);
		}
		else
		{
			final Collection<? extends Player> players = Bukkit.getOnlinePlayers();
			for (final Player player: players)
			{
				if (includeVanished ||
						!this.plugin.isVanished(player))
				{
					final String name = player.getName();
					if (lowerToken.length() == 0 ||
							player.getName().toLowerCase().startsWith(lowerToken)) results.add(name);
				}
			}
		}
	}
	
	private void addGroupMatches(final List<String> results, final String token)
	{
		final PermissionManager permissionManager = this.plugin.getPermissionManager();
		if (permissionManager == null) return;

		final String lowerToken = token.toLowerCase();
		for (final Group group: permissionManager.getGroups())
		{
			if (group.getDisplayName().toLowerCase().startsWith(lowerToken))
				results.add(group.getDisplayName());
		}
	}
	
	@Override
	public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] originalArgs)
	{
		String[] args = Helpers.Args.getStandardisedArgs(originalArgs);
		
		final String commandName = command.getName().toLowerCase();
		if (Helpers.Commands.argsContainsFlag(args, ExecutorBase.FLAG_BOOL_HELP))
		{
			Helpers.Messages.sendFailure(sender, "Usage: ");
			return this.outputCustomUsage(sender, commandName, label);
		}

		if (!this.checkCommandAllowed(commandName, ExecutorBase.isSenderConsole(sender)))
		{
			if (!ExecutorBase.isSenderConsole(sender, true))
				Helpers.Messages.sendFailure(sender, "You must be CONSOLE to use that command.");
			return true;
		}

		this.flagValues.clear();
		this.flagStates.clear();

		boolean allFlagsOk = true;
		for (final String flag: this.getAllAllowedFlags(commandName))
		{
			if (!allFlagsOk) break;

			if (Helpers.Commands.argsContainsFlag(args, flag))
			{
				switch (flag)
				{
				//  When provided, some flags are set to TRUE
				case ExecutorBase.FLAG_BOOL_ALLWORLDS:
				case ExecutorBase.FLAG_BOOL_CONFIRM:
				case ExecutorBase.FLAG_BOOL_LOAD:
				case ExecutorBase.FLAG_BOOL_SILENT:
				case ExecutorBase.FLAG_BOOL_VERBOSE:
					this.flagStates.put(flag, true);
					args = Helpers.Commands.popFlagArg(args, flag);
					break;

				//  When provided, some flags are set to FALSE
				case ExecutorBase.FLAG_BOOL_PAGEDOUTPUT:
					this.flagStates.put(flag, false);
					args = Helpers.Commands.popFlagArg(args, flag);
					break;

				//  All other flags are assumed to be STRING flags, and the subsequent argument is the value
				default:
					if (Helpers.Commands.argsContainsFlagValue(args, flag))
					{
						String value = Helpers.Commands.getFlagValue(args, flag);
						if (flag.equals(ExecutorBase.FLAG_TIME))
						{
							final long millis = Helpers.DateTime.getMillis(value);
							if (millis == -1)
							{
								Helpers.Messages.sendFailure(sender, "Invalid value $1 specified for flag $2.", value, flag);
								allFlagsOk = false;
							}
							else value = String.valueOf(millis);
						}
						else if (flag.equals(ExecutorBase.FLAG_GROUP))
						{
							final PermissionManager permissionManager = this.plugin.getPermissionManager();
							if (permissionManager != null)
							{
								for (final Group group: permissionManager.getGroups())
								{
									if (group.getDisplayName().equalsIgnoreCase(value))
									{
										allFlagsOk = true;
										break;
									}
								}
								if (!allFlagsOk) Helpers.Messages.sendFailure(sender, "No such group $1 specified for flag $2.", value, flag);
							}
							else
							{
								Helpers.Messages.sendFailure(
										sender,
										"Flag $1 is not valid when $1 is not enabled.",
										flag,
										"VPCore Permissions");
								allFlagsOk = false;
							}
						}
						this.flagValues.put(flag, value);
						args = Helpers.Commands.popFlagAndValueArgs(args, flag);
					}
					else
					{
						Helpers.Messages.sendFailure(sender, "Flag $1 specified but no value provided.", flag);
						allFlagsOk = false;
					}
					break;
				}
				
				if (allFlagsOk && Helpers.Commands.argsContainsFlag(args, flag))
				{
					Helpers.Messages.sendFailure(sender, "Flag $1 was specified more than once.", flag);
					return true;
				}
			}
			else
			{
				//  Set the default values for absent flags
				switch (flag)
				{
				case ExecutorBase.FLAG_BOOL_LOAD:
				case ExecutorBase.FLAG_BOOL_CONFIRM:
				case ExecutorBase.FLAG_BOOL_ALLWORLDS:
					this.flagStates.put(flag, false);
					break;
				case ExecutorBase.FLAG_BOOL_PAGEDOUTPUT:
					this.flagStates.put(flag, true);
					break;
				}
			}
		}
		if (!allFlagsOk) return true;

		for (final String arg: args)
		{
			if (arg.length() > 1 && arg.charAt(0) == '-')
			{
				boolean isValidNumber = true;
				boolean seenPeriod = false;
				for (int i = 1; i < arg.length(); ++i)
				{
					switch (arg.charAt(i))
					{
					case '0': case '1': case '2':
					case '3': case '4': case '5':
					case '6': case '7': case '8':
					case '9':
						break;

					case '.':
						if (seenPeriod) isValidNumber = false;
						seenPeriod = true;
						break;

					default: isValidNumber = false;
					}

					if (!isValidNumber) break;
				}

				if (!isValidNumber)
				{
					Helpers.Messages.sendFailure(sender, "Flag $1 is not valid for command $2.", arg.substring(1), commandName);
					return true;
				}
			}
		}

		if (this.getFlagState(ExecutorBase.FLAG_WORLD) && this.getFlagState(ExecutorBase.FLAG_BOOL_ALLWORLDS))
		{
			Helpers.Messages.sendFailure(
					sender,
					"The $1 and $2 flags are mutually exclusive.",
					ExecutorBase.FLAG_WORLD,
					ExecutorBase.FLAG_BOOL_ALLWORLDS);
			return true;
		}

		final PagedOutputCommandSender pagedSender = new PagedOutputCommandSender(
				this.plugin,
				this,
				sender,
				this.shouldCommandPurgePagedOutput(commandName),
				!this.getFlagState(ExecutorBase.FLAG_BOOL_PAGEDOUTPUT));

		if (this.checkParameters(commandName, args.length))
		{
			final boolean result = this.onCommandRaw(pagedSender, commandName, label.toLowerCase(), args);
			pagedSender.finish();
			if (result)
			{
				if (this.hasFlag(ExecutorBase.FLAG_BOOL_CONFIRM) &&
						!this.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM)) Helpers.Messages.sendNotification(
								sender,
								"Action not completed: specify $1$2 flag to override.",
								"-",
								ExecutorBase.FLAG_BOOL_CONFIRM);
				return true;
			}
		}
		
		Helpers.Messages.sendFailure(sender, "Invalid parameters in command $1:", commandName);
		return this.outputCustomUsage(sender, commandName, label);
	}

	protected final static boolean isSenderConsole(final CommandSender sender)
	{
		return (sender instanceof ConsoleCommandSender);
	}

	protected final static boolean isSenderConsole(final CommandSender sender, final boolean emitError)
	{
		boolean isSenderConsole = false;
		if (ExecutorBase.isSenderConsole(sender))
		{
			isSenderConsole = true;
			if (emitError) Helpers.Messages.sendFailure(sender, "This command cannot be executed by CONSOLE.");
		}
		return isSenderConsole;
	}

	protected final boolean hasFlag(final String flag) { return this.hasFlag(flag, false, null); }
	protected final boolean hasFlag(final String flag, final boolean emitError, final CommandSender sender)
	{
		if (this.flagStates.containsKey(flag) || this.flagValues.containsKey(flag))
		{
			if (emitError) Helpers.Messages.sendFailure(sender, "Flag $1 is not valid for this command.", flag);
			return true;
		}
		return false;
	}

	protected final boolean getFlagState(final String flag)
	{
		if (this.flagStates.containsKey(flag)) return this.flagStates.get(flag);
		else return this.flagValues.containsKey(flag);
	}

	protected final String getFlagValue(final String flag) { return this.getFlagValue(flag, null); }
	protected final String getFlagValue(final String flag, final String defaultValue)
	{
		if (this.flagValues.containsKey(flag)) return this.flagValues.get(flag);
		else if (this.flagStates.containsKey(flag)) return this.flagStates.get(flag).toString();
		else return defaultValue;
	}

	protected final long getFlagLong(final String flag) { return this.getFlagLong(flag, 0); }
	protected final long getFlagLong(final String flag, final long defaultValue)
	{
		long value = defaultValue;
		if (this.flagValues.containsKey(flag))
		{
			try { value = Long.parseLong(this.flagValues.get(flag)); }
			catch (final Exception ex) { }
		}
		return value;
	}

	protected final int getFlagInt(final String flag) { return this.getFlagInt(flag, 0); }
	protected final int getFlagInt(final String flag, final int defaultValue)
	{
		int value = defaultValue;
		if (this.flagValues.containsKey(flag))
		{
			try { value = Integer.parseInt(this.flagValues.get(flag)); }
			catch (final Exception ex) { }
		}
		return value;
	}

	protected final World getWorld(final PagedOutputCommandSender sender)
	{
		if (this.hasFlag(ExecutorBase.FLAG_WORLD)) return sender.getWorld(this.getFlagValue(ExecutorBase.FLAG_WORLD));
		else return sender.getWorld();
	}

	protected final OfflinePlayer getOfflinePlayer(final PagedOutputCommandSender sender)
	{
		if (this.hasFlag(ExecutorBase.FLAG_PLAYER)) return this.plugin.getOfflinePlayer(sender, this.getFlagValue(ExecutorBase.FLAG_PLAYER));
		else return sender.getPlayer();
	}

	protected final Player getPlayer(final PagedOutputCommandSender sender)
	{
		if (this.hasFlag(ExecutorBase.FLAG_PLAYER)) return this.plugin.getPlayer(sender, this.getFlagValue(ExecutorBase.FLAG_PLAYER));
		else return sender.getPlayer();
	}

	protected final String getSortOrder(final PagedOutputCommandSender sender, final String defaultOption, final String... validOptions)
	{
		String orderBy = this.getFlagValue(ExecutorBase.FLAG_ORDERBY, defaultOption);
		switch (orderBy.toLowerCase())
		{
		case "qty":
		case "number":
		case "num":
		case "amount":
			orderBy = "quantity";
			break;
		}

		String optionList = "";
		for (final String validOption: validOptions)
		{
			if (optionList.length() > 0) optionList += " ";
			optionList += validOptions;
			if (validOption.equalsIgnoreCase(orderBy)) return validOption;
		}

		sender.sendFailure("Invalid sort option $1; valid options are:", orderBy);
		sender.sendInfo(optionList);

		return "";
	}

	protected final void outputCustomUsageLine(final CommandSender sender, final String label, final String format, final String description)
	{
		Helpers.Messages.sendInfo(
				sender,
				"/$1 $2- $3",
				Helpers.Parameters.replace(format, label),
				ChatColor.GRAY,
				description);
	}
	
	protected final void outputCustomUsageDivider(final CommandSender sender, final String label)
	{
		Helpers.Messages.sendInfo(
				sender,
				"$1-- $2 --------",
				ChatColor.GRAY,
				label);
	}
	
	protected final void setConfirmFlag()
	{
		this.flagStates.put(ExecutorBase.FLAG_BOOL_CONFIRM, true);
	}
}