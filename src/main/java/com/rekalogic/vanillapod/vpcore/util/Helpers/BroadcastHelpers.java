package com.rekalogic.vanillapod.vpcore.util.Helpers;

import org.bukkit.Bukkit;
import org.bukkit.Server;

public final class BroadcastHelpers
{
	protected static final BroadcastHelpers Instance = new BroadcastHelpers();

	private BroadcastHelpers() { }

	public void sendAll(final String message)
	{
		final Server server = Bukkit.getServer();
		final String colouredMessage = Helpers.ColorBroadcast + message;
		server.broadcastMessage(colouredMessage);
	}

	public void sendAll(final String message, final Object... parameters)
	{
		this.sendAll(ParameterHelpers.Instance.replace(message, Helpers.ColorBroadcast, Helpers.ColorParameterBroadcast, parameters));
	}

	public void sendPerms(final String permission, String message)
	{
		final Server server = Bukkit.getServer();
		message = Helpers.ColorBroadcast + message;
		server.broadcast(message, permission);
	}

	public void sendPerms(final String permission, final String message, final Object... parameters)
	{
		this.sendPerms(permission, ParameterHelpers.Instance.replace(message, Helpers.ColorBroadcast, Helpers.ColorParameterBroadcast, parameters));
	}
}
