package com.rekalogic.vanillapod.vpcore.util.db;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.LoggingBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public abstract class DatabaseInterfaceBase extends LoggingBase
{
	private final VPPlugin plugin;
	
	protected final PluginBase getPlugin() { return this.plugin; }

	public DatabaseInterfaceBase(final VPPlugin plugin)
	{
		super(plugin.isDebug());

		this.plugin = plugin;
	}

	protected final MySqlConnection getConnection()
	{
		return this.plugin.getConnection();
	}
	
	public final void changeUserPhpbbGroup(final UUID uuid, final int groupId) { this.changeUserPhpbbGroup(uuid, groupId, false); }
	public final void changeUserPhpbbGroup(final UUID uuid, final int groupId, final boolean ignoreMissingForumAccounts)
	{
		final MySqlConnection connection = this.getConnection();
		this.changeUserPhpbbGroup(connection, uuid, groupId, ignoreMissingForumAccounts);
		connection.close();
	}
	
	public final void changeUserPhpbbGroup(final UUID uuid, final String groupName) { this.changeUserPhpbbGroup(uuid, groupName, false); }
	public final void changeUserPhpbbGroup(final UUID uuid, final String groupName, final boolean ignoreMissingForumAccounts)
	{
		final MySqlConnection connection = this.getConnection();
		MySqlResult result = connection.execute(
				"SELECT phpbb_groupid FROM vp_group " +
				"WHERE lower(groupname) = ?",
				groupName.toLowerCase());
		
		if (result.moveNext())
		{
			final int groupId = result.getInt("phpbb_groupid");
			this.changeUserPhpbbGroup(connection, uuid, groupId, ignoreMissingForumAccounts);
		}
		else super.severe("No such group $1.", groupName);

		connection.close();
	}	

	public final void changeUserPhpbbGroup(final MySqlConnection connection, final UUID uuid, final int groupId) { this.changeUserPhpbbGroup(connection, uuid, groupId, false); }
	public final void changeUserPhpbbGroup(final MySqlConnection connection, final UUID uuid, final int groupId, final boolean ignoreMissingForumAccounts)
	{
		super.debug("Moving user $1 to group $2", uuid, groupId);
		if (groupId != 0)
		{
			MySqlResult result = connection.execute("SELECT phpbb_userid FROM vp_player WHERE uuid = ?", uuid);
	
			if (!result.moveNext())
			{
				if (!ignoreMissingForumAccounts) super.severe("UUID $1 not associated with a forum profile.", uuid);
				return;
			}
			final int userId = result.getInt("phpbb_userid");
	
			result = connection.execute("SELECT group_colour, group_rank FROM mcforum.phpbb_groups WHERE group_id = ?", groupId);
			if (!result.moveNext())
			{
				super.severe("PHPBB group $1 does not exist.", groupId);
				return;
			}
			final String groupColour = result.getString("group_colour");
			final int groupRank = result.getInt("group_rank");
	
			connection.deleteFrom("mcforum.phpbb_user_group", "user_id = ?", userId);
			connection.insertInto(
					"mcforum.phpbb_user_group",
					"user_id, group_id, group_leader, user_pending",
					userId,
					groupId,
					0,
					0);
			
			connection.update(
					"mcforum.phpbb_users",
					"group_id = ?, user_colour = ?, user_rank = ?",
					"user_id = ?",
					groupId,
					groupColour,
					groupRank,
					userId);
		}
	}

	protected final Location getLocationFromMySqlResult(final MySqlConnection connection, final MySqlResult result)
	{
		Location location = null;
		World world = null;
		double x = 0;
		double y = 0;
		double z = 0;
		float yaw = 0;
		float pitch = 0;

		if (result.hasColumns("world", "x", "y", "z"))
		{
			world = Bukkit.getWorld(result.getString("world"));
			x = result.getDouble("x");
			y = result.getDouble("y");
			z = result.getDouble("z");

			if (result.hasColumns("yaw", "pitch"))
			{
				yaw = result.getFloat("yaw");
				pitch = result.getFloat("pitch");
			}
			
			location = new Location(world, x, y, z, yaw, pitch);
		}
		else if (result.hasColumn("locationid"))
		{
			final int locationid = result.getInt("locationid");
			if (result.wasNull()) return null;
			
			final MySqlResult result2 = connection.execute(
					"SELECT * FROM vp_location WHERE id = ?",
					locationid);
			
			if (result2.moveNext())
			{
				world = Bukkit.getWorld(result2.getString("world"));
				x = result2.getDouble("x");
				y = result2.getDouble("y");
				z = result2.getDouble("z");
				yaw = result2.getFloat("yaw");
				pitch = result2.getFloat("pitch");

				location = new Location(world, x, y, z, yaw, pitch);				
			}
			else super.severe("Could not retrieve location $1 from vp_location.", locationid);
		}
		else super.severe("Could not retrieve location without adequate data.");
		
		return location;
	}
	
	protected final int createLocation(final MySqlConnection connection, final Location location)
	{
		final int locationId = connection.insertInto(
				"vp_location",
				"world, x, y, z, yaw, pitch",
				location.getWorld().getName(),
				location.getX(),
				location.getY(),
				location.getZ(),
				location.getYaw(),
				location.getPitch());
		return locationId;
	}

	protected final void updateNamedLocation(final String nameTable, final String nameColumn, final String name, final Location location)
	{
		final MySqlConnection connection = this.getConnection();
		connection.update(
				"vp_location",
				"world = ?, x = ?, y = ?, z = ?, yaw = ?, pitch = ?",
				Helpers.Parameters.replace(
						"id = (SELECT locationid FROM $1 WHERE $2 = ?)",
						nameTable,
						nameColumn),
						location.getWorld().getName(),
						location.getX(),
						location.getY(),
						location.getZ(),
						location.getYaw(),
						location.getPitch(),
						name);
		connection.close();
	}	
}
