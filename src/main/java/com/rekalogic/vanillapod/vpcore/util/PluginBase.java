package com.rekalogic.vanillapod.vpcore.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import net.milkbowl.vault.economy.Economy;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.kitteh.vanish.VanishManager;
import org.kitteh.vanish.VanishPlugin;

import com.rekalogic.vanillapod.vpcore.player.EconomyManager;
import com.rekalogic.vanillapod.vpcore.player.PermissionManager;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseVersionManager;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnectionFactory;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public abstract class PluginBase extends JavaPlugin
{
	private boolean isDebug = false;
	private int pageSize = 0;
	private boolean isWorldGuardEnabled = false;
	private boolean isVanishNoPacketEnabled = false;
	private boolean isVaultEnabled = false;
	private PermissionManager permissionManager = null;
	private EconomyManager economyManager = null;
	private WorldGuardPlugin worldGuard = null;
	private VanishManager vanishManager = null;
	private String hostName = "";
	private boolean usePlayerCache = false;

	private final Random random;
	private final MySqlConnectionFactory connectionFactory;
	
	private final ConcurrentHashMap<String, List<String>> aliasLookup = new ConcurrentHashMap<String, List<String>>();
	private final ConcurrentHashMap<String, List<String>> aliasReverseLookup = new ConcurrentHashMap<String, List<String>>();
	private final ConcurrentHashMap<String, UUID> playerCache = new ConcurrentHashMap<String, UUID>();
	
	public final int getPageSize() { return this.pageSize; }
	protected final void setPageSize(final int pageSize) { this.pageSize = pageSize; }
	
	public String getHostName() { return this.hostName; }
	protected final void setHostname(final String hostName) { this.hostName = hostName; }

	public PluginBase()
	{
		this.random = new Random(System.currentTimeMillis());
		this.connectionFactory = new MySqlConnectionFactory(this.isDebug);

		super.getServer().getServicesManager().unregisterAll(this);
	}
	
	protected final void initialisePlayerCache(final boolean usePlayerCache)
	{
		this.playerCache.clear();
		this.usePlayerCache = usePlayerCache;
		if (this.usePlayerCache)
		{
			this.info("Initialising player cache...");
			for (final OfflinePlayer player: Helpers.Bukkit.getOfflinePlayers())
				this.playerCache.put(player.getName().toLowerCase(), player.getUniqueId());
		}
	}

	protected final void setupFunctionalityProviders()
	{
		this.info("Setting up functionality providers...");
		final PluginManager pluginManager = Bukkit.getPluginManager();
		if (pluginManager.isPluginEnabled("WorldGuard"))
		{
			this.worldGuard = (WorldGuardPlugin)pluginManager.getPlugin("WorldGuard");
			this.info("Functionality provider detected: $1.", this.worldGuard);
			this.isWorldGuardEnabled = true;
		}

		if (pluginManager.isPluginEnabled("VanishNoPacket"))
		{
			final VanishPlugin vanishNoPacket = (VanishPlugin)pluginManager.getPlugin("VanishNoPacket"); 
			this.vanishManager = vanishNoPacket.getManager();
			this.info("Functionality provider detected: $1.", vanishNoPacket);
			this.isVanishNoPacketEnabled = true;
		}
		
		if (pluginManager.isPluginEnabled("Vault"))
		{
			this.isVaultEnabled = true;
		}
	}

	public final void setPermissionManager(final PermissionManager permissionManager) { this.permissionManager = permissionManager; }
	public final PermissionManager getPermissionManager() { return this.permissionManager; }

	public final void setEconomyManager(final EconomyManager economyManager)
	{
		if (this.isVaultEnabled)
		{
			this.economyManager = economyManager;
			super.getServer().getServicesManager().register(
					Economy.class,
					this.economyManager,
					this,
					ServicePriority.Highest);
		}
	}
	public final EconomyManager getEconomyManager() { return this.economyManager; }

	public final boolean isWorldGuardEnabled() { return this.isWorldGuardEnabled; }
	public final WorldGuardPlugin getWorldGuard() { return this.worldGuard; }
	public final boolean isVanishNoPacketEnabled() { return this.isVanishNoPacketEnabled; }
	public final boolean isVaultEnabled() { return this.isVaultEnabled; }
	public final Random getRandom() { return this.random; }
	public final boolean isDebug() { return this.isDebug; }
	public final void setDebug(final boolean isDebug) { this.isDebug = isDebug; }

	public final boolean isVanished(final OfflinePlayer player)
	{
		if (!player.isOnline()) return false;
		if (!this.isVanishNoPacketEnabled) return false;
		return this.vanishManager.isVanished(player.getPlayer());
	}
	
	public String[] getAliases()
	{
		return this.aliasReverseLookup.keySet().toArray(new String[this.aliasReverseLookup.size()]);
	}
	
	public final String[] getAliases(final String name)
	{
		String[] aliases = null;
		final String lowerName = name.toLowerCase();
		if (this.aliasLookup.containsKey(lowerName)) aliases = this.aliasLookup.get(lowerName).toArray(new String[0]);
		else aliases = new String[0];
		return aliases;
	}
	
	public final boolean isAnAlias(final String name)
	{
		boolean isAnAlias = false;
		for (final String alias: this.aliasReverseLookup.keySet())
		{
			if (alias.equalsIgnoreCase(name))
			{
				isAnAlias = true;
				break;
			}
		}
		return isAnAlias;
	}
	
	public final String[] getNamesForAlias(final String alias)
	{
		String[] names = null;
		for (final String candidate: this.aliasReverseLookup.keySet())
		{
			if (candidate.equalsIgnoreCase(alias))
			{
				names = this.aliasReverseLookup.get(candidate).toArray(new String[0]);
				break;
			}
		}
		return names;
	}
	
	public final void addAlias(final String name, final String alias)
	{
		List<String> aliases = null;
		List<String> aliasesReverse = null;
		final String lowerName = name.toLowerCase();
		
		if (this.aliasLookup.containsKey(lowerName)) aliases = this.aliasLookup.get(lowerName);
		else
		{
			aliases = new ArrayList<String>();
			this.aliasLookup.put(lowerName, aliases);
		}
		
		if (this.aliasReverseLookup.containsKey(alias)) aliasesReverse = this.aliasReverseLookup.get(alias);
		else
		{
			aliasesReverse = new ArrayList<String>();
			this.aliasReverseLookup.put(alias, aliasesReverse);
		}
		
		if (!aliases.contains(alias)) aliases.add(alias);
		if (!aliasesReverse.contains(name)) aliasesReverse.add(name);
	}
	
	public final void clearAliases()
	{
		this.aliasLookup.clear();
		this.aliasReverseLookup.clear();
	}

	public final void InitialiseConnectionFactory(
			final boolean debug,
			final String hostname,
			final int port,
			final String database,
			final String username,
			final String password)
	{
		this.connectionFactory.initialise(debug, hostname, port, database, username, password);
	}

	public final MySqlConnection getConnection()
	{
		return this.connectionFactory.getConnection();
	}

	public final void checkDatabase()
	{
		this.info("Testing MySQL connection...");
		final MySqlConnection connection = this.connectionFactory.getConnection();
		if (connection.isOpen()) this.info("Connection looks OK.");

		final DatabaseVersionManager dbManager = new DatabaseVersionManager(this.isDebug);
		dbManager.check(this, connection);

		connection.close();
	}

	protected final void debug(final String message, final Object... parameters)
	{
		if (this.isDebug) LoggingBase.emitDebug(message, parameters);
	}

	protected final void info(final String message, final Object... parameters)
	{
		LoggingBase.emitInfo(message, parameters);
	}

	protected final void warning(final String message, final Object... parameters)
	{
		LoggingBase.emitWarning(message, parameters);
	}

	protected final void severe(final String message, final Object... parameters)
	{
		LoggingBase.emitSevere(message, parameters);
	}

	private int itemIndex = 0;
	private final Map<Integer, Object> knownItems = new ConcurrentHashMap<Integer, Object>();
	public final String recordItem(final Object item)
	{
		if (item instanceof Location ||
				item instanceof Chunk ||
				item instanceof World ||
				item instanceof Player ||
				item instanceof OfflinePlayer ||
				item instanceof Entity)
		{
			this.knownItems.put(itemIndex, item);
			++this.itemIndex;
			return Helpers.Parameters.replace("[@$1]", this.itemIndex);
		}
		else return "";
	}

	public final Object getItem(final int number)
	{
		final int index = number - 1;
		if (index < 0 || index >= this.knownItems.size()) return null;
		else return this.knownItems.get(index);
	}

	public final int clearItems()
	{
		final int size = this.knownItems.size();
		this.debug("Cleared $1 registered items.", this.knownItems.size());
		this.knownItems.clear();
		this.itemIndex = 0;
		return size;
	}
	
	public final OfflinePlayer getOfflinePlayer(final PagedOutputCommandSender sender, final String name)
	{
		return this.getPlayer(sender, name, true);
	}
	
	public final Player getPlayer(final PagedOutputCommandSender sender, final String name)
	{
		final OfflinePlayer player = this.getPlayer(sender, name, false);
		if (player == null) return null;
		else return player.getPlayer();
	}
	
	private OfflinePlayer getPlayer(final PagedOutputCommandSender sender, final String name, final boolean includeOffline)
	{
		if (this.isAnAlias(name))
		{
			final String[] aliases = this.getNamesForAlias(name);
			if (aliases.length == 1) sender.sendFailure("Player $1 is now known as $2.", name, aliases[0]);
			else
			{
				sender.sendFailure("The following players have been known as $1:", name);
				for (final String alias: aliases)
					sender.sendFailure("--> $1", alias);
			}
			
			return null;
		}
		
		OfflinePlayer player = null;
		boolean foundInCache = false;

		final String lowerName = name.toLowerCase();
		if (this.usePlayerCache)
		{
			if (this.playerCache.containsKey(lowerName))
			{
				final UUID uuid = this.playerCache.get(lowerName);
				final OfflinePlayer testPlayer = Helpers.Bukkit.getOfflinePlayer(uuid);
				if (testPlayer != null && testPlayer.getName().equalsIgnoreCase(name))
				{
					player = testPlayer;
					foundInCache = true;
				}
				else
				{
					this.playerCache.remove(lowerName);
					player = null;
				}
			}
		}

		if (player == null)
		{
			for (final Player testPlayer: Bukkit.getOnlinePlayers())
			{
				if (testPlayer.getName().equalsIgnoreCase(name))
				{
					player = testPlayer;
					break;
				}
			}
		}

		OfflinePlayer bestMatch = null;
		double certainty = 0;
		if (player == null)
		{
			int substringMatchCount = 0;
			int shortestDistance = Integer.MAX_VALUE;

			for (final OfflinePlayer testPlayer: Helpers.Bukkit.getOfflinePlayers())
			{
				if (!testPlayer.hasPlayedBefore()) continue;
				
				final String testName = testPlayer.getName().toLowerCase();
				if (testName.equals(lowerName))
				{
					player = testPlayer;
					break;
				}
				
				final boolean isASubstringMatch = testName.contains(lowerName); 
				if (isASubstringMatch)
				{
					if (substringMatchCount == 0) shortestDistance = Integer.MAX_VALUE;
					++substringMatchCount;
				}

				if (substringMatchCount == 0 || isASubstringMatch)
				{
					int distance = StringUtils.getLevenshteinDistance(lowerName, testName);
					if (distance < shortestDistance)
					{
						shortestDistance = distance;
						bestMatch = testPlayer;
					}
				}
			}
			
			if (bestMatch != null)
			{
				int denominator = bestMatch.getName().length() + name.length();
				certainty = (1 - (((double)shortestDistance) / denominator)) * 100;
			}
		}

		OfflinePlayer foundPlayer = null;
		if (player == null)
		{
			sender.sendFailure("Player $1 is not known to the server.", name);
						
			if (bestMatch != null) sender.sendFailure(
					"Did you mean $1? (certainty $2%)",
					bestMatch,
					"#" + String.valueOf((int)certainty));
		}
		else
		{
			boolean isOnline = player.isOnline();
			if (isOnline && sender.isPlayer())
				isOnline = (sender.getPlayer().canSee(player.getPlayer()));
			if (!includeOffline && !isOnline) sender.sendFailure("Player $1 is not online.", name);
			else foundPlayer = player;
		}
		
		if (this.usePlayerCache && foundPlayer != null && !foundInCache)
			this.playerCache.put(lowerName, foundPlayer.getUniqueId());

		return foundPlayer;
	}
}
