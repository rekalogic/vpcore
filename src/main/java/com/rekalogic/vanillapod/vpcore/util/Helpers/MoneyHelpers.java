package com.rekalogic.vanillapod.vpcore.util.Helpers;

import java.text.DecimalFormat;

public final class MoneyHelpers
{
	protected static final MoneyHelpers Instance = new MoneyHelpers();

	private MoneyHelpers() { }

	public String formatString(final double amount)
	{
		final DecimalFormat decimalFormat = new DecimalFormat("#0.00");
		return decimalFormat.format(amount);
	}
}
