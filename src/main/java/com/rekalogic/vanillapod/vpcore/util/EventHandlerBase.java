package com.rekalogic.vanillapod.vpcore.util;

public abstract class EventHandlerBase<T extends SettingsBase> extends EventHandlerRaw
{
	private final T settings;

	protected boolean canEnable() { return true; }

	public EventHandlerBase(final PluginBase plugin, final T settings)
	{
		super(plugin);

		this.settings = settings;
	}

	protected final T getSettings() { return this.settings; }

	@Override
	protected final boolean isEnabled()
	{
		return this.settings.isEnabled() && this.canEnable();
	}

	@Override
	protected final void onRegister()
	{
		super.setDebug(this.settings.isDebug());
		super.onRegister();
	}
}
