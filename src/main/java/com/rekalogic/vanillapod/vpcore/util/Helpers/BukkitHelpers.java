package com.rekalogic.vanillapod.vpcore.util.Helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public final class BukkitHelpers
{
	protected static final BukkitHelpers Instance = new BukkitHelpers();

	private BukkitHelpers() { }

	public final OfflinePlayer[] getOfflinePlayers()
	{
		final OfflinePlayer[] offlinePlayers = Bukkit.getOfflinePlayers();
		final Map<UUID, OfflinePlayer> players = new HashMap<UUID, OfflinePlayer>();
		for (final OfflinePlayer player: offlinePlayers) players.put(player.getUniqueId(), player);
		for (final Player player: Bukkit.getOnlinePlayers()) players.put(player.getUniqueId(), player);
		return players.values().toArray(new OfflinePlayer[players.size()]);
	}

	public OfflinePlayer getOfflinePlayer(UUID uuid)
	{
		OfflinePlayer player = null;
		for (final Player onlinePlayer: Bukkit.getOnlinePlayers())
		{
			if (onlinePlayer.getUniqueId().equals(uuid))
			{
				player = onlinePlayer;
				break;
			}
		}
		if (player == null) player = Bukkit.getOfflinePlayer(uuid);
		return player;
	}
}
