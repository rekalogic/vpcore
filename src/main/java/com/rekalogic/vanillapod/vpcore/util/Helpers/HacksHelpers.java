package com.rekalogic.vanillapod.vpcore.util.Helpers;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Tree;
import org.bukkit.material.WoodenStep;

import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;

public final class HacksHelpers
{
	protected static final HacksHelpers Instance = new HacksHelpers();

	private HacksHelpers() { }
	
	public void updateInventory(final PluginBase plugin, final Player player)
	{
		final TaskBase task = new TaskBase(plugin.isDebug(), "Hacks.UpdateInventory")
		{
			@Override
			public void runTask() { player.updateInventory(); }
		};
		task.runTaskLater(plugin, 5);
	}

	public void setStackData(final ItemStack stack, final MaterialData data)
	{
		stack.setData(data);

		//  https://bukkit.atlassian.net/browse/BUKKIT-3380
		@SuppressWarnings("deprecation")
		final byte dataByte = data.getData();
		stack.setDurability(dataByte);
	}

	@SuppressWarnings("deprecation")
	public boolean areEqual(ItemStack inventoryStack, ItemStack stack)
	{
		return (inventoryStack.getType() == stack.getType() &&
				inventoryStack.getData().getData() == stack.getData().getData());
	}
	
	@SuppressWarnings("deprecation")
	public byte getUnsupportedData(ItemStack stack)
	{
		MaterialData data = stack.getData();
		return data.getData();
	}
	
	@SuppressWarnings("deprecation")
	public MaterialData getUnsupportedData(Material type, byte data)
	{
		return new MaterialData(type, data);
	}

	public TreeSpecies getTreeSpeciesFromData(final ItemStack stack, final TreeSpecies defaultSpecies)
	{
		TreeSpecies species = defaultSpecies;
		boolean checkDurability = false;
		switch (stack.getType())
		{
		case LEAVES_2:
		case LOG_2:
			//  First hacky bit - these Materials don't have a Tree-based MaterialData.
			switch (stack.getDurability())
			{
			case 0: species = TreeSpecies.ACACIA; break;
			case 1: species = TreeSpecies.DARK_OAK; break;
			}
			break;
			
		case LEAVES:
		case LOG:
		case SAPLING:
		case WOOD:
		{
			final Tree tree = (Tree)stack.getData();
			species = tree.getSpecies();
			checkDurability= true;
		} break;
			
		case WOOD_STEP:
		{
			final WoodenStep step = (WoodenStep)stack.getData();
			species = step.getSpecies();
			checkDurability = true;
		} break;

			
		default: break;
		}
		
		if (checkDurability)
		{
			//  Second hacky bit - some Materials support all wood types, but the
			//  species is wrong for all but the first four species.
			switch (stack.getDurability())
			{
			case 4: species = TreeSpecies.ACACIA; break;
			case 5: species = TreeSpecies.DARK_OAK; break;
			}
		}

		return species;
	}
	
	@SuppressWarnings("deprecation")
	public MaterialData getMaterialData(final Material material, final TreeSpecies species)
	{
		MaterialData data = null;
		switch (material)
		{
		case LEAVES_2:
		case LOG_2:
			byte byteData = 0;
			switch (species)
			{
			case ACACIA: byteData = 0; break;
			case DARK_OAK: byteData = 1; break;
			default: byteData = 127;
			}
			data = new MaterialData(material, byteData);
			break;

		case LEAVES:
		case LOG:
		case SAPLING:
		case WOOD:
			data = new Tree(species);
			break;
			
		case WOOD_STEP:
			data = new WoodenStep(species);
			break;
			
		default: break;
		}
		return data;
	}

	public boolean killDragon(final EnderDragon dragon)
	{
		boolean isDragonDead = false;
		if (dragon != null)
		{
			final double damage = dragon.getHealth() * 10;			
			dragon.damage(damage);

			//  Bukkit bug? dragon.damage() does nothing.
			if (!dragon.isDead()) dragon.setHealth(0);
			
			isDragonDead = dragon.isDead();
		}
		
		return isDragonDead;
	}
	
	@SuppressWarnings("deprecation")
	public int getMaterialId(final Material material)
	{
		return material.getId();
	}
	
	@SuppressWarnings("deprecation")
	public Material getMaterialFromId(final int id)
	{
		return Material.getMaterial(id);
	}

	@SuppressWarnings("deprecation")
	public DyeColor getDyeByData(final byte data)
	{
		return DyeColor.getByDyeData(data);
	}
	
	@SuppressWarnings("deprecation")
	public byte getDataFromDye(final DyeColor colour)
	{
		return colour.getDyeData();
	}
}
