package com.rekalogic.vanillapod.vpcore.util;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.TabExecutor;

import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public abstract class ExecutorBase<T extends SettingsBase> extends ExecutorBaseRaw implements CommandExecutor, TabExecutor
{
	private final T settings;

	public static final String FLAG_GROUP = "g";
	public static final String FLAG_ORDERBY = "o";
	public static final String FLAG_PLAYER = "p";
	public static final String FLAG_TIME = "t";
	public static final String FLAG_TOP = "T";
	public static final String FLAG_WORLD = "w";
	public static final String FLAG_BOOL_ALLWORLDS = "ALLWORLDS";
	public static final String FLAG_BOOL_CONFIRM = "Y";
	public static final String FLAG_BOOL_HELP = "?";
	public static final String FLAG_BOOL_LOAD = "L";
	public static final String FLAG_BOOL_PAGEDOUTPUT = "P";
	public static final String FLAG_BOOL_SILENT = "s";
	public static final String FLAG_BOOL_VERBOSE = "V";
		
	public ExecutorBase(final PluginBase plugin, final T settings, final String... myCommands)
	{
		super(plugin, settings.isDebug(), myCommands);
		this.settings = settings;
	}
	
	@Override
	protected final boolean getSettingsDebug() { return this.settings.isDebug(); }

	protected final T getSettings() { return this.settings; }
	
	@Override
	protected final boolean onCommandRaw(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		if (this.settings != null && !this.settings.isEnabled())
		{
			Helpers.Messages.sendFailure(sender, "The $1 module is not enabled in VPCore.", this.settings.getComponentName());
			Helpers.Messages.sendFailure(sender, "Cannot execute command $1.", command);
			return true;
		}
		
		boolean success = true;
		try
		{
			success = this.onCommand(sender, command, label, args);
		}
		catch (Exception ex)
		{
			Helpers.Messages.sendError(sender, "An error has occurred while trying to execute the command.");
			super.severe("Exception executing command $1 for $2.", command, sender);
			ex.printStackTrace();
		}
		
		return success;
	}
	
	protected boolean onCommand(
			final PagedOutputCommandSender sender,
			final String command,
			final String label,
			final String[] args)
	{
		Helpers.Messages.sendFailure(sender, "Handler for $1 in $2 is not implemented.", command, this.settings.getComponentName());

		return true;
	}	
}