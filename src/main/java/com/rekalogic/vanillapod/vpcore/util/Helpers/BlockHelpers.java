package com.rekalogic.vanillapod.vpcore.util.Helpers;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

public final class BlockHelpers
{
	protected static final BlockHelpers Instance = new BlockHelpers();

	private BlockHelpers() { }
	
	public boolean isBedUnobstructed(Location location)
	{
		final Block bedBlock = location.getBlock(); 
		final Block belowBedBlock = bedBlock.getRelative(BlockFace.DOWN);
		final Block aboveBedBlock = bedBlock.getRelative(BlockFace.UP);
		return (belowBedBlock.getType() == Material.BED_BLOCK) &&
				Helpers.Block.isPassable(aboveBedBlock) &&
				Helpers.Block.isPassable(bedBlock);		
	}
	
	public boolean isPassable(Block block)
	{
		switch (block.getType())
		{
		case FENCE_GATE:
		case IRON_TRAPDOOR:
		case TRAP_DOOR:
			break;
		
		case ACACIA_DOOR:
		case BIRCH_DOOR:
		case DARK_OAK_DOOR:
		case IRON_DOOR:
		case SPRUCE_DOOR:
		case WOODEN_DOOR:
			return true;
		
		case DIODE_BLOCK_OFF:
		case DIODE_BLOCK_ON:
		case REDSTONE_COMPARATOR_OFF:
		case REDSTONE_COMPARATOR_ON:
		case REDSTONE_TORCH_OFF:
		case REDSTONE_TORCH_ON:
		case REDSTONE_WIRE:
		case TORCH:
			return true;
			
		case GOLD_PLATE:
		case IRON_PLATE:
		case LEVER:
		case STONE_BUTTON:
		case STONE_PLATE:
		case TRIPWIRE:
		case TRIPWIRE_HOOK:
		case WOOD_BUTTON:
		case WOOD_PLATE:
			return true;
						
		case BROWN_MUSHROOM:
		case DEAD_BUSH:
		case DOUBLE_PLANT:
		case LONG_GRASS:
		case RED_MUSHROOM:
		case RED_ROSE:
		case SAPLING:
		case VINE:
		case WATER_LILY:
		case YELLOW_FLOWER:
			return true;
			
		case ACTIVATOR_RAIL:
		case DETECTOR_RAIL:
		case POWERED_RAIL:
		case RAILS:
			return true;
			
		case CARPET:
		case FLOWER_POT:
		case LADDER:
		case SIGN_POST:
		case SKULL:
		case SNOW:
		case STANDING_BANNER:
		case WALL_BANNER:
		case WALL_SIGN:
		case WEB:
			return true;
			
		case AIR:
			return true;			

		default: break;
		}
		
		return false;
	}
	
	public boolean isHalfHeight(Block block)
	{
		switch (block.getType())
		{
		case DAYLIGHT_DETECTOR:
		case STEP:
		case STONE_SLAB2:
		case WOOD_STEP:
			return true;
		
		default: break;
		}
		
		return false;		
	}
}
