package com.rekalogic.vanillapod.vpcore.util.Helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

public final class SpawnHelpers
{
	protected static final SpawnHelpers Instance = new SpawnHelpers();
	private final Random random = new Random();

	private static final int MaximumAllowedEnchants = 5;
	private static final Material[] ZombieHeldItems = new Material[]
			{
		Material.APPLE, Material.ARROW, Material.BAKED_POTATO, Material.BONE, Material.BOW,
		Material.BREAD, Material.BUCKET, Material.CARROT_ITEM, Material.COOKED_FISH, Material.DIAMOND,
		Material.DIRT, Material.EGG, Material.FEATHER, Material.FLINT, Material.FLOWER_POT_ITEM,
		Material.GOLD_INGOT, Material.IRON_INGOT, Material.LEASH, Material.LEATHER, Material.POISONOUS_POTATO,
		Material.POTATO_ITEM, Material.RED_ROSE, Material.STICK, Material.STRING, Material.TNT,
		Material.TORCH, Material.WEB
			};

	private SpawnHelpers() { }

	public void spawnMobAt(final EntityType type, final Location location, final int quantity, final String... meta)
	{
		for (int i = 0; i < quantity; ++i)
		{
			final Entity entity = location.getWorld().spawnEntity(location, type);
			if (meta != null)
			{
				for (int j = 0; j < meta.length; ++j) this.setMetaData(type, entity, meta[j].toLowerCase());
			}
		}
	}

	private void setMetaData(final EntityType type, final Entity entity, final String meta)
	{
		if (meta.length() == 0 || meta.equalsIgnoreCase("none")) return;
		if (meta.contains(","))
		{
			for (final String metaPart: meta.split(",")) this.setMetaData(type, entity, metaPart);
			return;
		}

		switch (type)
		{
		case HORSE: this.setMetaData((Horse)entity, meta); break;
		case SHEEP: this.setMetaData((Sheep)entity, meta); break;
		case SKELETON: this.setMetaData((Skeleton)entity, meta); break;
		case VILLAGER: this.setMetaData((Villager)entity, meta); break;
		case ZOMBIE: this.setMetaData((Zombie)entity, meta); break;
		default: break;
		}
	}

	private void setMetaData(final Sheep sheep, final String meta)
	{
		switch (meta)
		{
		case "black": sheep.setColor(DyeColor.BLACK); break;
		case "blue": sheep.setColor(DyeColor.BLUE); break;
		case "brown": sheep.setColor(DyeColor.BROWN); break;
		case "cyan": sheep.setColor(DyeColor.CYAN); break;
		case "grey":
		case "gray": sheep.setColor(DyeColor.GRAY); break;
		case "green": sheep.setColor(DyeColor.GREEN); break;
		case "lblue":
		case "lightblue":
		case "light-blue":
		case "light_blue": sheep.setColor(DyeColor.LIGHT_BLUE); break;
		case "lime": sheep.setColor(DyeColor.LIME); break;
		case "magenta": sheep.setColor(DyeColor.MAGENTA); break;
		case "orange": sheep.setColor(DyeColor.ORANGE); break;
		case "pink": sheep.setColor(DyeColor.PINK); break;
		case "purple": sheep.setColor(DyeColor.PURPLE); break;
		case "red": sheep.setColor(DyeColor.RED); break;
		case "silver": sheep.setColor(DyeColor.SILVER); break;
		case "yellow": sheep.setColor(DyeColor.YELLOW); break;
		case "white": sheep.setColor(DyeColor.WHITE); break;

		case "sheared":
		case "shorn": sheep.setSheared(true);

		case "random":
			if (this.random.nextInt(100) == 0)
			{
				switch (this.random.nextInt(14))
				{
				case 0: sheep.setColor(DyeColor.BLUE); break;
				case 1: sheep.setColor(DyeColor.BROWN); break;
				case 2: sheep.setColor(DyeColor.CYAN); break;
				case 3: sheep.setColor(DyeColor.GRAY); break;
				case 4: sheep.setColor(DyeColor.GREEN); break;
				case 5: sheep.setColor(DyeColor.LIGHT_BLUE); break;
				case 6: sheep.setColor(DyeColor.LIME); break;
				case 7: sheep.setColor(DyeColor.MAGENTA); break;
				case 8: sheep.setColor(DyeColor.ORANGE); break;
				case 9: sheep.setColor(DyeColor.PINK); break;
				case 10: sheep.setColor(DyeColor.PURPLE); break;
				case 11: sheep.setColor(DyeColor.RED); break;
				case 12: sheep.setColor(DyeColor.SILVER); break;
				case 13: sheep.setColor(DyeColor.YELLOW); break;
				}
			}
			else if (this.random.nextInt(5) == 0) sheep.setColor(DyeColor.BLACK);
			else sheep.setColor(DyeColor.WHITE);
			sheep.setSheared(this.random.nextInt(10) == 0);
			break;
		}
	}

	private void setMetaData(final Horse horse, final String meta)
	{
		switch (meta)
		{
		case "undead": horse.setVariant(Variant.UNDEAD_HORSE); break;
		case "skeleton": horse.setVariant(Variant.SKELETON_HORSE); break;
		default:
			switch (this.random.nextInt(3))
			{
			case 0:
				horse.setVariant(Variant.HORSE);
				switch (this.random.nextInt(7))
				{
				case 0: horse.setColor(Horse.Color.BLACK); break;
				case 1: horse.setColor(Horse.Color.BROWN); break;
				case 2: horse.setColor(Horse.Color.CHESTNUT); break;
				case 3: horse.setColor(Horse.Color.CREAMY); break;
				case 4: horse.setColor(Horse.Color.DARK_BROWN); break;
				case 5: horse.setColor(Horse.Color.GRAY); break;
				case 6: horse.setColor(Horse.Color.WHITE); break;
				}
				break;
			case 1: horse.setVariant(Variant.DONKEY); break;
			case 2: horse.setVariant(Variant.MULE); break;
			}
			break;
		}

		horse.setMaxHealth((this.random.nextDouble() * 15) + 15);

		switch (horse.getVariant())
		{
		case HORSE:
		case MULE:
		case SKELETON_HORSE:
		case UNDEAD_HORSE:
			horse.setJumpStrength((this.random.nextDouble() * 0.4) + 0.6);
			break;

		case DONKEY:
			horse.setJumpStrength(0.5);
			break;
		}
	}	
	
	private void setMetaData(final Villager villager, final String meta)
	{
		switch (meta)
		{
		case "child":
		case "baby": villager.setBaby(); break;

		case "villager":
		case "farmer":
			villager.setAdult();
			villager.setProfession(Profession.FARMER);
			break;
		case "blacksmith":
			villager.setAdult();
			villager.setProfession(Profession.BLACKSMITH);
			break;
		case "butcher":
			villager.setAdult();
			villager.setProfession(Profession.BUTCHER);
			break;
		case "librarian":
			villager.setAdult();
			villager.setProfession(Profession.LIBRARIAN);
			break;
		case "priest":
			villager.setAdult();
			villager.setProfession(Profession.PRIEST);
			break;

		case "random":
		default:
			if (this.random.nextInt(10) == 0) villager.setBaby();
			else
			{
				villager.setAdult();
				final int professionChance = this.random.nextInt(100);
				if (professionChance < 60) villager.setProfession(Profession.FARMER);
				else if (professionChance < 70) villager.setProfession(Profession.BLACKSMITH);
				else if (professionChance < 80) villager.setProfession(Profession.BUTCHER);
				else if (professionChance < 90) villager.setProfession(Profession.LIBRARIAN);
				else if (professionChance < 100) villager.setProfession(Profession.PRIEST);
			}
			break;
		}
	}

	private void setMetaData(final Zombie zombie, final String meta)
	{
		switch (meta)
		{
		case "random":
			final int randomChance = this.random.nextInt(100);
			if (randomChance < 5) zombie.setVillager(true);
			else if (randomChance < 10) zombie.setBaby(true);
			final int handChance = this.random.nextInt(100);
			if (handChance >= 95) this.setMetaData(zombie, "sword");
			else if (handChance >= 75) this.setMetaData(zombie, "item");
			break;
		case "villager":
			zombie.setVillager(true);
			if (this.random.nextInt(10) == 0) this.setMetaData(zombie, "sword");
			break;
		case "child":
		case "baby":
			if (this.random.nextInt(10) == 0) this.setMetaData(zombie, "sword");
			zombie.setBaby(true);
			break;

		case "enchant":
		case "enchanted":
			this.setEquipmentEnchantment(zombie.getEquipment());
			break;

		case "sword":
			final int swordChance = this.random.nextInt(100);
			if (swordChance >= 90) zombie.getEquipment().setItemInHand(new ItemStack(Material.WOOD_SWORD, 1));
			else if (swordChance >= 60) zombie.getEquipment().setItemInHand(new ItemStack(Material.STONE_SWORD, 1));
			else if (swordChance >= 10) zombie.getEquipment().setItemInHand(new ItemStack(Material.IRON_SWORD, 1));
			else if (swordChance >= 5) zombie.getEquipment().setItemInHand(new ItemStack(Material.GOLD_SWORD, 1));
			else if (swordChance >= 0) zombie.getEquipment().setItemInHand(new ItemStack(Material.DIAMOND_SWORD, 1));
			this.setMetaData(zombie, "enchant");
			break;

		case "item":
			final int itemChance = this.random.nextInt(SpawnHelpers.ZombieHeldItems.length);
			zombie.getEquipment().setItemInHand(new ItemStack(SpawnHelpers.ZombieHeldItems[itemChance], 1));
			break;
		}

		this.setArmourFromMetadata(zombie.getEquipment(), meta);
	}

	private void setMetaData(final Skeleton skeleton, final String meta)
	{
		switch (meta)
		{
		case "wither":
			skeleton.setSkeletonType(SkeletonType.WITHER);
			skeleton.getEquipment().setItemInHand(new ItemStack(Material.STONE_SWORD, 1));
			this.setMetaData(skeleton, "enchant");
			break;
		case "bow":
			skeleton.getEquipment().setItemInHand(new ItemStack(Material.BOW, 1));
			this.setMetaData(skeleton, "enchant");
			break;
		case "sword":
			final int chance = this.random.nextInt(100);
			if (chance > 10) skeleton.getEquipment().setItemInHand(new ItemStack(Material.WOOD_SWORD, 1));
			else if (chance < 30) skeleton.getEquipment().setItemInHand(new ItemStack(Material.STONE_SWORD, 1));
			else if (chance < 90) skeleton.getEquipment().setItemInHand(new ItemStack(Material.IRON_SWORD, 1));
			else if (chance < 95) skeleton.getEquipment().setItemInHand(new ItemStack(Material.GOLD_SWORD, 1));
			else if (chance < 100) skeleton.getEquipment().setItemInHand(new ItemStack(Material.DIAMOND_SWORD, 1));
			this.setMetaData(skeleton, "enchant");
			break;

		case "random":
			skeleton.setSkeletonType(SkeletonType.NORMAL);
			if (this.random.nextInt(8) == 0) this.setMetaData(skeleton, "sword");
			else this.setMetaData(skeleton, "bow");
			break;

		case "enchant":
		case "enchanted":
			this.setEquipmentEnchantment(skeleton.getEquipment());
			break;

		case "normal":
		case "skeleton":
		default:
			skeleton.setSkeletonType(SkeletonType.NORMAL);
			break;
		}

		this.setArmourFromMetadata(skeleton.getEquipment(), meta);
	}

	private void setEquipmentEnchantment(final EntityEquipment equipment)
	{
		equipment.setItemInHand(this.trySetEquipmentEnchantment(equipment.getItemInHand()));
		equipment.setHelmet(this.trySetEquipmentEnchantment(equipment.getHelmet()));
		equipment.setLeggings(this.trySetEquipmentEnchantment(equipment.getLeggings()));
		equipment.setBoots(this.trySetEquipmentEnchantment(equipment.getBoots()));
		equipment.setChestplate(this.trySetEquipmentEnchantment(equipment.getChestplate()));
	}

	private ItemStack trySetEquipmentEnchantment(ItemStack itemStack)
	{
		if (itemStack == null) itemStack = new ItemStack(Material.AIR);
		if (itemStack.getType() != Material.AIR && this.random.nextInt(10) == 0)
			itemStack = this.randomlyEnchant(itemStack);
		return itemStack;
	}

	private void setArmourFromMetadata(final EntityEquipment equipment, final String meta)
	{
		switch (meta)
		{
		case "leather":
			this.setArmourFromMetadata(equipment, "leather-helm");
			this.setArmourFromMetadata(equipment, "leather-chest");
			this.setArmourFromMetadata(equipment, "leather-legs");
			this.setArmourFromMetadata(equipment, "leather-boots");
			break;
		case "iron":
			this.setArmourFromMetadata(equipment, "iron-helm");
			this.setArmourFromMetadata(equipment, "iron-chest");
			this.setArmourFromMetadata(equipment, "iron-legs");
			this.setArmourFromMetadata(equipment, "iron-boots");
			break;
		case "diamond":
			this.setArmourFromMetadata(equipment, "diamond-helm");
			this.setArmourFromMetadata(equipment, "diamond-chest");
			this.setArmourFromMetadata(equipment, "diamond-legs");
			this.setArmourFromMetadata(equipment, "diamond-boots");
			break;
		case "gold":
			this.setArmourFromMetadata(equipment, "gold-helm");
			this.setArmourFromMetadata(equipment, "gold-chest");
			this.setArmourFromMetadata(equipment, "gold-legs");
			this.setArmourFromMetadata(equipment, "gold-boots");
			break;
		case "chain":
			this.setArmourFromMetadata(equipment, "chain-helm");
			this.setArmourFromMetadata(equipment, "chain-chest");
			this.setArmourFromMetadata(equipment, "chain-legs");
			this.setArmourFromMetadata(equipment, "chain-boots");
			break;
		case "leather-helm":
		case "leather-helmet":
		case "leather_helm":
		case "leather_helmet":
		case "leatherhelm":
		case "leatherhelmet": equipment.setHelmet(new ItemStack(Material.LEATHER_HELMET, 1)); break;
		case "leather-leg":
		case "leather-legs":
		case "leather-leggings":
		case "leather_leg":
		case "leather_legs":
		case "leather_leggings":
		case "leatherleg":
		case "leatherlegs":
		case "leatherleggings": equipment.setLeggings(new ItemStack(Material.LEATHER_LEGGINGS, 1)); break;
		case "leather-boots":
		case "leather_boots":
		case "leatherboots": equipment.setBoots(new ItemStack(Material.LEATHER_BOOTS, 1)); break;
		case "leather-chest":
		case "leather-chestplate":
		case "leather_chest":
		case "leather_chestplate":
		case "leatherchest":
		case "leatherchestplate": equipment.setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE, 1)); break;

		case "iron-helm":
		case "iron-helmet":
		case "iron_helm":
		case "iron_helmet":
		case "ironhelm":
		case "ironhelmet": equipment.setHelmet(new ItemStack(Material.IRON_HELMET, 1)); break;
		case "iron-leg":
		case "iron-legs":
		case "iron-leggings":
		case "iron_leg":
		case "iron_legs":
		case "iron_leggings":
		case "ironleg":
		case "ironlegs":
		case "ironleggings": equipment.setLeggings(new ItemStack(Material.IRON_LEGGINGS, 1)); break;
		case "iron-boots":
		case "iron_boots":
		case "ironboots": equipment.setBoots(new ItemStack(Material.IRON_BOOTS, 1)); break;
		case "iron-chest":
		case "iron-chestplate":
		case "iron_chest":
		case "iron_chestplate":
		case "ironchest":
		case "ironchestplate": equipment.setChestplate(new ItemStack(Material.IRON_CHESTPLATE, 1)); break;

		case "diamond-helm":
		case "diamond-helmet":
		case "diamond_helm":
		case "diamond_helmet":
		case "diamondhelm":
		case "diamondhelmet": equipment.setHelmet(new ItemStack(Material.DIAMOND_HELMET, 1)); break;
		case "diamond-leg":
		case "diamond-legs":
		case "diamond-leggings":
		case "diamond_leg":
		case "diamond_legs":
		case "diamond_leggings":
		case "diamondleg":
		case "diamondlegs":
		case "diamondleggings": equipment.setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS, 1)); break;
		case "diamond-boots":
		case "diamond_boots":
		case "diamondboots": equipment.setBoots(new ItemStack(Material.DIAMOND_BOOTS, 1)); break;
		case "diamond-chest":
		case "diamond-chestplate":
		case "diamond_chest":
		case "diamond_chestplate":
		case "diamondchest":
		case "diamondchestplate": equipment.setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE, 1)); break;

		case "gold-helm":
		case "gold-helmet":
		case "gold_helm":
		case "gold_helmet":
		case "goldhelm":
		case "goldhelmet": equipment.setHelmet(new ItemStack(Material.GOLD_HELMET, 1)); break;
		case "gold-leg":
		case "gold-legs":
		case "gold-leggings":
		case "gold_leg":
		case "gold_legs":
		case "gold_leggings":
		case "goldleg":
		case "goldlegs":
		case "goldleggings": equipment.setLeggings(new ItemStack(Material.GOLD_LEGGINGS, 1)); break;
		case "gold-boots":
		case "gold_boots":
		case "goldboots": equipment.setBoots(new ItemStack(Material.GOLD_BOOTS, 1)); break;
		case "gold-chest":
		case "gold-chestplate":
		case "gold_chest":
		case "gold_chestplate":
		case "goldchest":
		case "goldchestplate": equipment.setChestplate(new ItemStack(Material.GOLD_CHESTPLATE, 1)); break;

		case "chain-helm":
		case "chain-helmet":
		case "chain_helm":
		case "chain_helmet":
		case "chainhelm":
		case "chainhelmet":
		case "chainmail-helm":
		case "chainmail-helmet":
		case "chainmail_helm":
		case "chainmail_helmet":
		case "chainmailhelm":
		case "chainmailhelmet": equipment.setHelmet(new ItemStack(Material.CHAINMAIL_HELMET, 1)); break;
		case "chain-leg":
		case "chain-legs":
		case "chain-leggings":
		case "chain_leg":
		case "chain_legs":
		case "chain_leggings":
		case "chainleg":
		case "chainlegs":
		case "chainleggings":
		case "chainmail-leg":
		case "chainmail-legs":
		case "chainmail-leggings":
		case "chainmail_leg":
		case "chainmail_legs":
		case "chainmail_leggings":
		case "chainmailleg":
		case "chainmaillegs":
		case "chainmailleggings": equipment.setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS, 1)); break;
		case "chain-boots":
		case "chain_boots":
		case "chainboots":
		case "chainmail-boots":
		case "chainmail_boots":
		case "chainmailboots": equipment.setBoots(new ItemStack(Material.CHAINMAIL_BOOTS, 1)); break;
		case "chain-chest":
		case "chain-chestplate":
		case "chain_chest":
		case "chain_chestplate":
		case "chainchest":
		case "chainchestplate":
		case "chainmail-chest":
		case "chainmail-chestplate":
		case "chainmail_chest":
		case "chainmail_chestplate":
		case "chainmailchest":
		case "chainmailchestplate": equipment.setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1)); break;

		case "random":
			final int helmetChance = this.random.nextInt(100);
			if (helmetChance >= 98) equipment.setHelmet(new ItemStack(Material.CHAINMAIL_HELMET, 1));
			else if (helmetChance >= 96) equipment.setHelmet(new ItemStack(Material.GOLD_HELMET, 1));
			else if (helmetChance >= 94) equipment.setHelmet(new ItemStack(Material.DIAMOND_HELMET, 1));
			else if (helmetChance >= 90) equipment.setHelmet(new ItemStack(Material.IRON_HELMET, 1));
			else if (helmetChance >= 85) equipment.setHelmet(new ItemStack(Material.LEATHER_HELMET, 1));

			final int leggingsChance = this.random.nextInt(100);
			if (leggingsChance >= 98) equipment.setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS, 1));
			else if (leggingsChance >= 96) equipment.setLeggings(new ItemStack(Material.GOLD_LEGGINGS, 1));
			else if (leggingsChance >= 94) equipment.setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS, 1));
			else if (leggingsChance >= 90) equipment.setLeggings(new ItemStack(Material.IRON_LEGGINGS, 1));
			else if (leggingsChance >= 85) equipment.setLeggings(new ItemStack(Material.LEATHER_LEGGINGS, 1));

			final int bootsChance = this.random.nextInt(100);
			if (bootsChance >= 98) equipment.setBoots(new ItemStack(Material.CHAINMAIL_BOOTS, 1));
			else if (bootsChance >= 96) equipment.setBoots(new ItemStack(Material.GOLD_BOOTS, 1));
			else if (bootsChance >= 94) equipment.setBoots(new ItemStack(Material.DIAMOND_BOOTS, 1));
			else if (bootsChance >= 90) equipment.setBoots(new ItemStack(Material.IRON_BOOTS, 1));
			else if (bootsChance >= 85) equipment.setBoots(new ItemStack(Material.LEATHER_BOOTS, 1));

			final int chestChance = this.random.nextInt(100);
			if (chestChance >= 98) equipment.setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1));
			else if (chestChance >= 96) equipment.setChestplate(new ItemStack(Material.GOLD_CHESTPLATE, 1));
			else if (chestChance >= 94) equipment.setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE, 1));
			else if (chestChance >= 90) equipment.setChestplate(new ItemStack(Material.IRON_CHESTPLATE, 1));
			else if (chestChance >= 85) equipment.setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE, 1));

			break;
		}
	}

	private ItemStack randomlyEnchant(final ItemStack itemStack) { return this.randomlyEnchant(itemStack, SpawnHelpers.MaximumAllowedEnchants); }
	private ItemStack randomlyEnchant(final ItemStack itemStack, int maxEnchantments)
	{
		final List<Enchantment> validEnchantments = new ArrayList<Enchantment>();
		for (final Enchantment enchantment: Enchantment.values())
			if (enchantment.canEnchantItem(itemStack)) validEnchantments.add(enchantment);

		if (maxEnchantments > validEnchantments.size()) maxEnchantments = validEnchantments.size();
		if (maxEnchantments > 0)
		{
			int numberOfEnchantments = this.random.nextInt(maxEnchantments) + 1;
			while (numberOfEnchantments > 0 && validEnchantments.size() > 0)
			{
				final int enchantmentIndex = this.random.nextInt(validEnchantments.size());
				final Enchantment enchantment = validEnchantments.get(enchantmentIndex);
				final int level = this.random.nextInt(enchantment.getMaxLevel()) + 1;
				itemStack.addEnchantment(enchantment, level);
				validEnchantments.remove(enchantmentIndex);
				--numberOfEnchantments;
			}
		}

		return itemStack;
	}
}
