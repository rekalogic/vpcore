package com.rekalogic.vanillapod.vpcore.util;

public enum LookupSource
{
	NONE(0),
	PLAYER(1),
	OFFLINE_PLAYER(2),
	GROUP(3),
	WORLD(4),
	FROM_PLUGIN(99);

	private final int id;
	
	public int getId() { return this.id; }
	
	private LookupSource(int id)
	{
		this.id = id;
	}
}
