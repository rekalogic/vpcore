package com.rekalogic.vanillapod.vpcore.util.Helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.WordUtils;
import org.bukkit.CoalType;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.GrassSpecies;
import org.bukkit.Material;
import org.bukkit.SandstoneType;
import org.bukkit.TreeSpecies;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.Coal;
import org.bukkit.material.Dye;
import org.bukkit.material.LongGrass;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Sandstone;
import org.bukkit.material.SmoothBrick;
import org.bukkit.material.SpawnEgg;
import org.bukkit.material.Step;
import org.bukkit.material.WoodenStep;
import org.bukkit.material.Wool;
import org.json.JSONArray;
import org.json.JSONObject;

public final class ItemHelpers
{
	protected static final ItemHelpers Instance = new ItemHelpers();

	private final Map<Material, String[]> aliases = new ConcurrentHashMap<Material, String[]>();
	private final Map<String, Material> aliasesReverse = new ConcurrentHashMap<String, Material>();

	private final Map<Enchantment, String[]> enchantments = new ConcurrentHashMap<Enchantment, String[]>();
	private final Map<String, Enchantment> enchantmentsReverse = new ConcurrentHashMap<String, Enchantment>();
	
	private static final int DefaultLeatherColour = 10511680;
	private static final int MaxBannerPatterns = 6;

	private ItemHelpers()
	{
		this.initialiseItemAliases();
		this.initialiseEnchantmentAliases();
	}
	
	private void initialiseItemAliases()
	{
		this.aliases.put(Material.ACACIA_DOOR_ITEM, new String[] { "AcaciaDoor" });
		this.aliases.put(Material.BIRCH_DOOR_ITEM, new String[] { "BirchDoor" });
		this.aliases.put(Material.CHAINMAIL_CHESTPLATE, new String[] { "ChainChest" });
		this.aliases.put(Material.CHAINMAIL_LEGGINGS, new String[] { "ChainLegs" });
		this.aliases.put(Material.COBBLESTONE, new String[] { "Cobble" });
		this.aliases.put(Material.DARK_OAK_DOOR, new String[] { "DarkOakDoor" });
		this.aliases.put(Material.DAYLIGHT_DETECTOR, new String[] { "LightSensor" });
		this.aliases.put(Material.DIAMOND_CHESTPLATE, new String[] { "DiamondChest" });
		this.aliases.put(Material.DIAMOND_LEGGINGS, new String[] { "DiamondLegs" });
		this.aliases.put(Material.DIODE, new String[] { "Repeater" });
		this.aliases.put(Material.FENCE, new String[] { "Fence", "OakFence" });
		this.aliases.put(Material.FLINT_AND_STEEL, new String[] { "Lighter", "FlintSteel" });
		this.aliases.put(Material.GOLD_CHESTPLATE, new String[] { "GoldChest" });
		this.aliases.put(Material.GOLD_LEGGINGS, new String[] { "GoldLegs" });
		this.aliases.put(Material.JUNGLE_DOOR_ITEM, new String[] { "JungleDoor" });
		this.aliases.put(Material.LEASH, new String[] { "Lead" });
		this.aliases.put(Material.LONG_GRASS, new String[] { "TallGrass" });
		this.aliases.put(Material.IRON_CHESTPLATE, new String[] { "IronChest" });
		this.aliases.put(Material.IRON_LEGGINGS, new String[] { "IronLegs" });
		this.aliases.put(Material.LEATHER_CHESTPLATE, new String[] { "LeatherChest" });
		this.aliases.put(Material.LEATHER_LEGGINGS, new String[] { "LeatherLegs" });
		this.aliases.put(Material.MOSSY_COBBLESTONE, new String[] { "MossyCobble" });
		this.aliases.put(Material.PISTON_BASE, new String[] { "Piston" });
		this.aliases.put(Material.PISTON_STICKY_BASE, new String[] { "StickyPiston" });
		this.aliases.put(Material.PRISMARINE_CRYSTALS, new String[] { "PrismarineCryst" });
		this.aliases.put(Material.PRISMARINE_SHARD, new String[] { "PrismarineShard" });
		this.aliases.put(Material.RED_ROSE, new String[] { "Rose" });
		this.aliases.put(Material.RED_SANDSTONE_STAIRS, new String[] { "RedSandStairs" });
		this.aliases.put(Material.REDSTONE, new String[] { "RedstoneDust" });
		this.aliases.put(Material.REDSTONE_COMPARATOR, new String[] { "Comparator" });
		this.aliases.put(Material.REDSTONE_LAMP_OFF, new String[] { "RedstoneLamp" });
		this.aliases.put(Material.REDSTONE_TORCH_ON, new String[] { "RedstoneTorch" });
		this.aliases.put(Material.SPRUCE_DOOR_ITEM, new String[] { "SpruceDoor" });
		this.aliases.put(Material.STONE_SLAB2, new String[] { "RedSandSlab" });
		this.aliases.put(Material.SULPHUR, new String[] { "Gunpowder" });
		this.aliases.put(Material.YELLOW_FLOWER, new String[] { "Flower" });
		
		this.aliases.put(Material.WOOD_AXE, new String[] { "WoodenAxe" });
		this.aliases.put(Material.WOOD_HOE, new String[] { "WoodenHoe" });
		this.aliases.put(Material.WOOD_PICKAXE, new String[] { "WoodPick", "WoodenPick", "WoodenPickaxe" });
		this.aliases.put(Material.WOOD_SPADE, new String[] { "WoodenSpade" });
		this.aliases.put(Material.WOOD_SWORD, new String[] { "WoodenSword" });
		
		this.aliases.put(Material.STONE_PICKAXE, new String[] { "StonePick" });
		this.aliases.put(Material.IRON_PICKAXE, new String[] { "IronPick" });
		this.aliases.put(Material.GOLD_PICKAXE, new String[] { "GoldPick" });
		this.aliases.put(Material.DIAMOND_PICKAXE, new String[] { "DiamondPick" });

		for (final Material material: this.aliases.keySet())
			for (final String alias: this.aliases.get(material))
				this.aliasesReverse.put(alias.toLowerCase(), material);
	}
	
	private void initialiseEnchantmentAliases()
	{
		this.enchantments.put(Enchantment.ARROW_DAMAGE, new String[] { "arrowdamage" });
		this.enchantments.put(Enchantment.ARROW_FIRE, new String[] { "firedamage" });
		this.enchantments.put(Enchantment.ARROW_INFINITE, new String[] { "infinity", "infinite" });
		this.enchantments.put(Enchantment.ARROW_KNOCKBACK, new String[] { "arrowknockback" });
		this.enchantments.put(Enchantment.DAMAGE_ALL, new String[] { "damage", "damageall" });
		this.enchantments.put(Enchantment.DAMAGE_ARTHROPODS, new String[] { "arthropods", "arth" });
		this.enchantments.put(Enchantment.DAMAGE_UNDEAD, new String[] { "undead" });
		this.enchantments.put(Enchantment.DIG_SPEED, new String[] { "efficiency", "speed" });
		this.enchantments.put(Enchantment.DURABILITY, new String[] { "durability" });
		this.enchantments.put(Enchantment.FIRE_ASPECT, new String[] { "fire", "fireaspect" });
		this.enchantments.put(Enchantment.KNOCKBACK, new String[] { "knockback" });
		this.enchantments.put(Enchantment.LOOT_BONUS_BLOCKS, new String[] { "blockloot", "lootblock" });
		this.enchantments.put(Enchantment.LOOT_BONUS_MOBS, new String[] { "mobloot", "lootmob" });
		this.enchantments.put(Enchantment.LUCK, new String[] { "luck" });
		this.enchantments.put(Enchantment.LURE, new String[] { "lure" });
		this.enchantments.put(Enchantment.OXYGEN, new String[] { "breath", "breathing" });
		this.enchantments.put(Enchantment.PROTECTION_ENVIRONMENTAL, new String[] { "environment" });
		this.enchantments.put(Enchantment.PROTECTION_EXPLOSIONS, new String[] { "explosion" });
		this.enchantments.put(Enchantment.PROTECTION_FALL, new String[] { "feather", "fall" });
		this.enchantments.put(Enchantment.PROTECTION_FIRE, new String[] { "fireprot" });
		this.enchantments.put(Enchantment.PROTECTION_PROJECTILE, new String[] { "projectile" });
		this.enchantments.put(Enchantment.SILK_TOUCH, new String[] { "silk", "silktouch" });
		this.enchantments.put(Enchantment.THORNS, new String[] { "thorns" });
		this.enchantments.put(Enchantment.WATER_WORKER, new String[] { "water", "waterworker" });
		
		for (final Enchantment enchantment: this.enchantments.keySet())
			for (final String alias: this.enchantments.get(enchantment))
				this.enchantmentsReverse.put(alias, enchantment);
	}
	
	public boolean isShortNameCompatible(final ItemStack stack) { return this.isShortNameCompatible(stack.getType()); }
	public boolean isShortNameCompatible(final Material material)
	{
		boolean isShortNameCompatible = true;
		switch (material)
		{
		default:
			isShortNameCompatible = this.isSupported(material);
			break;
		}
		return isShortNameCompatible;
	}
	
	public boolean isSupported(final ItemStack stack) { return this.isSupported(stack.getType()); }
	public boolean isSupported(final Material material)
	{
		boolean isSupported = true;
		switch (material)
		{
		case FIREWORK:
		case WRITTEN_BOOK:
			isSupported = false;
			break;
			
		default: break;
		}
		return isSupported;
	}
	
	public boolean isSupported(final Enchantment enchantment)
	{
		return true;
	}

	public ItemStack getItemStack(final String name) { return this.getItemStack(name, -1); }
	public ItemStack getItemStack(final String name, final int quantity)
	{
		String lowerName = name.toLowerCase();
		ItemStack stack = null;

		Material material = null;
		TreeSpecies treeSpecies = TreeSpecies.GENERIC;
		DyeColor colour = DyeColor.WHITE;
		GrassSpecies grassSpecies = GrassSpecies.NORMAL;
		Material subMaterial = Material.STONE;
		EntityType entityType = null;
		SandstoneType sandstoneType = SandstoneType.CRACKED;
		short durability = -1;
		byte unsupportedData = -1;
		String metadata = null;
		
		if (this.hasMetadata(name))
		{
			lowerName = this.stripMetadata(lowerName);
			metadata = this.getMetadata(name);
		}

		switch (lowerName)
		{
		case "bonemeal": lowerName = "whitedye"; break;
		case "cobblestoneslab": lowerName = "cobbleslab"; break;
		case "cocoabean": lowerName = "browndye"; break;
		case "greybanner": lowerName = "graybanner"; break;
		case "greycarpet": lowerName = "graycarpet"; break;
		case "greyclay": lowerName = "grayclay"; break;
		case "greydye": lowerName = "graydye"; break;
		case "greyglass": lowerName = "grayglass"; break;
		case "greypane": lowerName = "graypane"; break;
		case "greywool": lowerName = "graywool"; break;
		case "inksac": lowerName = "blackdye"; break;
		case "lightgreycarpet": lowerName = "lightgraycarpet"; break;
		case "lightgreyclay": lowerName = "lightgrayclay"; break;
		case "lightgraydye": lowerName = "silverdye"; break;
		case "lightgreydye": lowerName = "silverdye"; break;
		case "lightgreyglass": lowerName = "lightgrayglass"; break;
		case "lightgreypane": lowerName = "lightgraypane"; break;
		case "lightgraywool": lowerName = "silverwool"; break;
		case "lightgreywool": lowerName = "silverwool"; break;
		case "sandstoneslab": lowerName = "sandslab"; break;
		case "stonebrick": lowerName = "smoothbrick"; break;
		case "wool": lowerName = "whitewool"; break;

		case "leaves":
		case "log":
		case "sapling":
		case "wood":
		case "slab":
			lowerName = "oak" + lowerName;
			break;
		}
		
		if (lowerName.contains(":"))
		{
			String parts[] = lowerName.split(":");
			if (parts.length != 2) return null;
			lowerName = parts[0];
			long longDurability = 0;
			try { longDurability = Long.parseLong(parts[1]); }
			catch (NumberFormatException ex) { return null; }
			if (longDurability > Short.MAX_VALUE) return null;
			else if (longDurability <= 0) return null;
			durability = (short)longDurability;
		}

		switch (lowerName)
		{
		case "acacialeaves": material = Material.LEAVES_2; treeSpecies = TreeSpecies.ACACIA; break;
		case "acacialog": material = Material.LOG_2; treeSpecies = TreeSpecies.ACACIA; break;
		case "acaciasapling": material = Material.SAPLING; treeSpecies = TreeSpecies.ACACIA; break;
		case "acaciawood": material = Material.WOOD; treeSpecies = TreeSpecies.ACACIA; break;
		case "acaciaslab": material = Material.WOOD_STEP; treeSpecies = TreeSpecies.ACACIA; break;
		case "birchleaves": material = Material.LEAVES; treeSpecies = TreeSpecies.BIRCH; break;
		case "birchlog": material = Material.LOG; treeSpecies = TreeSpecies.BIRCH; break;
		case "birchsapling": material = Material.SAPLING; treeSpecies = TreeSpecies.BIRCH; break;
		case "birchwood": material = Material.WOOD; treeSpecies = TreeSpecies.BIRCH; break;
		case "birchslab": material = Material.WOOD_STEP; treeSpecies = TreeSpecies.BIRCH; break;
		case "darkoakleaves": material = Material.LEAVES_2; treeSpecies = TreeSpecies.DARK_OAK; break;
		case "darkoaklog": material = Material.LOG_2; treeSpecies = TreeSpecies.DARK_OAK; break;
		case "darkoaksapling": material = Material.SAPLING; treeSpecies = TreeSpecies.DARK_OAK; break;
		case "darkoakwood": material = Material.WOOD; treeSpecies = TreeSpecies.DARK_OAK; break;
		case "darkoakslab": material = Material.WOOD_STEP; treeSpecies = TreeSpecies.DARK_OAK; break;
		case "oakleaves": material = Material.LEAVES; treeSpecies = TreeSpecies.GENERIC; break;
		case "oaklog": material = Material.LOG; treeSpecies = TreeSpecies.GENERIC; break;
		case "oaksapling": material = Material.SAPLING; treeSpecies = TreeSpecies.GENERIC; break;
		case "oakwood": material = Material.WOOD; treeSpecies = TreeSpecies.GENERIC; break;
		case "oakslab": material = Material.WOOD_STEP; treeSpecies = TreeSpecies.GENERIC; break;
		case "jungleleaves": material = Material.LEAVES; treeSpecies = TreeSpecies.JUNGLE; break;
		case "junglelog": material = Material.LOG; treeSpecies = TreeSpecies.JUNGLE; break;
		case "junglesapling": material = Material.SAPLING; treeSpecies = TreeSpecies.JUNGLE; break;
		case "junglewood": material = Material.WOOD; treeSpecies = TreeSpecies.JUNGLE; break;
		case "jungleslab": material = Material.WOOD_STEP; treeSpecies = TreeSpecies.JUNGLE; break;
		case "spruceleaves": material = Material.LEAVES; treeSpecies = TreeSpecies.REDWOOD; break;
		case "sprucelog": material = Material.LOG; treeSpecies = TreeSpecies.REDWOOD; break;
		case "sprucesapling": material = Material.SAPLING; treeSpecies = TreeSpecies.REDWOOD; break;
		case "sprucewood": material = Material.WOOD; treeSpecies = TreeSpecies.REDWOOD; break;
		case "spruceslab": material = Material.WOOD_STEP; treeSpecies = TreeSpecies.REDWOOD; break;
		case "blackdye": material = Material.INK_SACK; colour = DyeColor.BLACK; break;
		case "bluedye": material = Material.INK_SACK; colour = DyeColor.BLUE; break;
		case "browndye": material = Material.INK_SACK; colour = DyeColor.BROWN; break;
		case "cyandye": material = Material.INK_SACK; colour = DyeColor.CYAN; break;
		case "graydye": material = Material.INK_SACK; colour = DyeColor.GRAY; break;
		case "greendye": material = Material.INK_SACK; colour = DyeColor.GREEN; break;
		case "lightbluedye": material = Material.INK_SACK; colour = DyeColor.LIGHT_BLUE; break;
		case "limedye": material = Material.INK_SACK; colour = DyeColor.LIME; break;
		case "magentadye": material = Material.INK_SACK; colour = DyeColor.MAGENTA; break;
		case "orangedye": material = Material.INK_SACK; colour = DyeColor.ORANGE; break;
		case "pinkdye": material = Material.INK_SACK; colour = DyeColor.PINK; break;
		case "purpledye": material = Material.INK_SACK; colour = DyeColor.PURPLE; break;
		case "reddye": material = Material.INK_SACK; colour = DyeColor.RED; break;
		case "silverdye": material = Material.INK_SACK; colour = DyeColor.SILVER; break;
		case "whitedye": material = Material.INK_SACK; colour = DyeColor.WHITE; break;
		case "yellowdye": material = Material.INK_SACK; colour = DyeColor.YELLOW; break;
		case "blackwool": material = Material.WOOL; colour = DyeColor.BLACK; break;
		case "bluewool": material = Material.WOOL; colour = DyeColor.BLUE; break;
		case "brownwool": material = Material.WOOL; colour = DyeColor.BROWN; break;
		case "cyanwool": material = Material.WOOL; colour = DyeColor.CYAN; break;
		case "graywool": material = Material.WOOL; colour = DyeColor.GRAY; break;
		case "greenwool": material = Material.WOOL; colour = DyeColor.GREEN; break;
		case "lightbluewool": material = Material.WOOL; colour = DyeColor.LIGHT_BLUE; break;
		case "limewool": material = Material.WOOL; colour = DyeColor.LIME; break;
		case "magentawool": material = Material.WOOL; colour = DyeColor.MAGENTA; break;
		case "orangewool": material = Material.WOOL; colour = DyeColor.ORANGE; break;
		case "pinkwool": material = Material.WOOL; colour = DyeColor.PINK; break;
		case "purplewool": material = Material.WOOL; colour = DyeColor.PURPLE; break;
		case "redwool": material = Material.WOOL; colour = DyeColor.RED; break;
		case "silverwool": material = Material.WOOL; colour = DyeColor.SILVER; break;
		case "whitewool": material = Material.WOOL; colour = DyeColor.WHITE; break;
		case "yellowwool": material = Material.WOOL; colour = DyeColor.YELLOW; break;
		case "longgrass": material = Material.LONG_GRASS; grassSpecies = GrassSpecies.NORMAL; break;
		case "deadgrass": material = Material.LONG_GRASS; grassSpecies = GrassSpecies.DEAD; break;
		case "fern": material = Material.LONG_GRASS; grassSpecies = GrassSpecies.FERN_LIKE; break;
		case "slab": material = Material.STEP; subMaterial = Material.STONE; break;
		case "brickslab": material = Material.STEP; subMaterial = Material.BRICK; break;
		case "netherslab": material = Material.STEP; subMaterial = Material.NETHER_BRICK; break;
		case "quartzslab": material = Material.STEP; subMaterial = Material.QUARTZ_BLOCK; break;
		case "smoothslab": material = Material.STEP; subMaterial = Material.SMOOTH_BRICK; break;
		case "stoneslab": material = Material.STEP; subMaterial = Material.STONE; break;
		case "cobbleslab": material = Material.STEP; subMaterial = Material.COBBLESTONE; break;
		case "sandslab": material = Material.STEP; subMaterial = Material.SANDSTONE; break;
		case "bategg": material = Material.MONSTER_EGG; entityType = EntityType.BAT; break;
		case "blazeegg": material = Material.MONSTER_EGG; entityType = EntityType.BLAZE; break;
		case "cavespideregg": material = Material.MONSTER_EGG; entityType = EntityType.CAVE_SPIDER; break;
		case "chickenegg": material = Material.MONSTER_EGG; entityType = EntityType.CHICKEN; break;
		case "cowegg": material = Material.MONSTER_EGG; entityType = EntityType.COW; break;
		case "creeperegg": material = Material.MONSTER_EGG; entityType = EntityType.CREEPER; break;
		case "endermanegg": material = Material.MONSTER_EGG; entityType = EntityType.ENDERMAN; break;
		case "endermiteegg": material = Material.MONSTER_EGG; entityType = EntityType.ENDERMITE; break;
		case "ghastegg": material = Material.MONSTER_EGG; entityType = EntityType.GHAST; break;
		case "giant": material = Material.MONSTER_EGG; entityType = EntityType.GIANT; break;
		case "guardianegg": material = Material.MONSTER_EGG; entityType = EntityType.GUARDIAN; break;
		case "horseegg": material = Material.MONSTER_EGG; entityType = EntityType.HORSE; break;
		case "irongolemegg": material = Material.MONSTER_EGG; entityType = EntityType.IRON_GOLEM; break;
		case "magmacubeegg": material = Material.MONSTER_EGG; entityType = EntityType.MAGMA_CUBE; break;
		case "mooshroomegg": material = Material.MONSTER_EGG; entityType = EntityType.MUSHROOM_COW; break;
		case "ocelotegg": material = Material.MONSTER_EGG; entityType = EntityType.OCELOT; break;
		case "pigegg": material = Material.MONSTER_EGG; entityType = EntityType.PIG; break;
		case "pigzombieegg": material = Material.MONSTER_EGG; entityType = EntityType.PIG_ZOMBIE; break;
		case "rabbitegg": material = Material.MONSTER_EGG; entityType = EntityType.RABBIT; break;
		case "sheepegg": material = Material.MONSTER_EGG; entityType = EntityType.SHEEP; break;
		case "silverfishegg": material = Material.MONSTER_EGG; entityType = EntityType.SILVERFISH; break;
		case "skeletonegg": material = Material.MONSTER_EGG; entityType = EntityType.SKELETON; break;
		case "slimeegg": material = Material.MONSTER_EGG; entityType = EntityType.SLIME; break;
		case "snowgolemegg": material = Material.MONSTER_EGG; entityType = EntityType.SNOWMAN; break;
		case "spideregg": material = Material.MONSTER_EGG; entityType = EntityType.SPIDER; break;
		case "squidegg": material = Material.MONSTER_EGG; entityType = EntityType.SQUID; break;
		case "villageregg": material = Material.MONSTER_EGG; entityType = EntityType.VILLAGER; break;
		case "witchegg": material = Material.MONSTER_EGG; entityType = EntityType.WITCH; break;
		case "witheregg": material = Material.MONSTER_EGG; entityType = EntityType.WITHER; break;
		case "wolfegg": material = Material.MONSTER_EGG; entityType = EntityType.WOLF; break;
		case "zombieegg": material = Material.MONSTER_EGG; entityType = EntityType.ZOMBIE; break;
		case "smoothbrick": material = Material.SMOOTH_BRICK; subMaterial = Material.STONE; break;
		case "mossybrick": material = Material.SMOOTH_BRICK; subMaterial = Material.MOSSY_COBBLESTONE; break;
		case "crackedbrick": material = Material.SMOOTH_BRICK; subMaterial = Material.COBBLESTONE; break;
		case "glyphbrick": material = Material.SMOOTH_BRICK; subMaterial = Material.SMOOTH_BRICK; break;
		case "sandstone": material = Material.SANDSTONE; sandstoneType = SandstoneType.CRACKED; break;
		case "glyphsandstone": material = Material.SANDSTONE; sandstoneType = SandstoneType.GLYPHED; break;
		case "smoothsandstone": material = Material.SANDSTONE; sandstoneType = SandstoneType.SMOOTH; break;
		case "quartzblock": material = Material.QUARTZ_BLOCK; unsupportedData = 0; break;
		case "glyphquartz": material = Material.QUARTZ_BLOCK; unsupportedData = 1; break;
		case "quartzpillar": material = Material.QUARTZ_BLOCK; unsupportedData = 2; break;
		case "poppy": material = Material.RED_ROSE; unsupportedData = 0; break;
		case "blueorchid": material = Material.RED_ROSE; unsupportedData = 1; break;
		case "allium": material = Material.RED_ROSE; unsupportedData = 2; break;
		case "azurebluet": material = Material.RED_ROSE; unsupportedData = 3; break;
		case "redtulip": material = Material.RED_ROSE; unsupportedData = 4; break;
		case "orangetulip": material = Material.RED_ROSE; unsupportedData = 5; break;
		case "whitetulip": material = Material.RED_ROSE; unsupportedData = 6; break;
		case "pinktulip": material = Material.RED_ROSE; unsupportedData = 7; break;
		case "oxeyedaisy": material = Material.RED_ROSE; unsupportedData = 8; break;
		case "coarsedirt": material = Material.DIRT; unsupportedData = 1; break;
		case "podzol": material = Material.DIRT; unsupportedData = 2; break;
		case "redsand": material = Material.SAND; unsupportedData = 1; break;
		case "wetsponge": material = Material.SPONGE; unsupportedData = 1; break;
		case "redsandstone": material = Material.RED_SANDSTONE; unsupportedData = 0; break;
		case "glyphredsand": material = Material.RED_SANDSTONE; unsupportedData = 1; break;
		case "smoothredsand": material = Material.RED_SANDSTONE; unsupportedData = 2; break;		
		case "whitecarpet": material = Material.CARPET; unsupportedData = 0; break;
		case "orangecarpet": material = Material.CARPET; unsupportedData = 1; break;
		case "magentacarpet": material = Material.CARPET; unsupportedData = 2; break;
		case "lightbluecarpet": material = Material.CARPET; unsupportedData = 3; break;
		case "yellowcarpet": material = Material.CARPET; unsupportedData = 4; break;
		case "limecarpet": material = Material.CARPET; unsupportedData = 5; break;
		case "pinkcarpet": material = Material.CARPET; unsupportedData = 6; break;
		case "graycarpet": material = Material.CARPET; unsupportedData = 7; break;
		case "lightgraycarpet": material = Material.CARPET; unsupportedData = 8; break;
		case "cyancarpet": material = Material.CARPET; unsupportedData = 9; break;
		case "purplecarpet": material = Material.CARPET; unsupportedData = 10; break;
		case "bluecarpet": material = Material.CARPET; unsupportedData = 11; break;
		case "browncarpet": material = Material.CARPET; unsupportedData = 12; break;
		case "greencarpet": material = Material.CARPET; unsupportedData = 13; break;
		case "redcarpet": material = Material.CARPET; unsupportedData = 14; break;
		case "blackcarpet": material = Material.CARPET; unsupportedData = 15; break;		
		case "whiteclay": material = Material.STAINED_CLAY; unsupportedData = 0; break;
		case "orangeclay": material = Material.STAINED_CLAY; unsupportedData = 1; break;
		case "magentaclay": material = Material.STAINED_CLAY; unsupportedData = 2; break;
		case "lightblueclay": material = Material.STAINED_CLAY; unsupportedData = 3; break;
		case "yellowclay": material = Material.STAINED_CLAY; unsupportedData = 4; break;
		case "limeclay": material = Material.STAINED_CLAY; unsupportedData = 5; break;
		case "pinkclay": material = Material.STAINED_CLAY; unsupportedData = 6; break;
		case "grayclay": material = Material.STAINED_CLAY; unsupportedData = 7; break;
		case "lightgrayclay": material = Material.STAINED_CLAY; unsupportedData = 8; break;
		case "cyanclay": material = Material.STAINED_CLAY; unsupportedData = 9; break;
		case "purpleclay": material = Material.STAINED_CLAY; unsupportedData = 10; break;
		case "blueclay": material = Material.STAINED_CLAY; unsupportedData = 11; break;
		case "brownclay": material = Material.STAINED_CLAY; unsupportedData = 12; break;
		case "greenclay": material = Material.STAINED_CLAY; unsupportedData = 13; break;
		case "redclay": material = Material.STAINED_CLAY; unsupportedData = 14; break;
		case "blackclay": material = Material.STAINED_CLAY; unsupportedData = 15; break;		
		case "whiteglass": material = Material.STAINED_GLASS; unsupportedData = 0; break;
		case "orangeglass": material = Material.STAINED_GLASS; unsupportedData = 1; break;
		case "magentaglass": material = Material.STAINED_GLASS; unsupportedData = 2; break;
		case "lightblueglass": material = Material.STAINED_GLASS; unsupportedData = 3; break;
		case "yellowglass": material = Material.STAINED_GLASS; unsupportedData = 4; break;
		case "limeglass": material = Material.STAINED_GLASS; unsupportedData = 5; break;
		case "pinkglass": material = Material.STAINED_GLASS; unsupportedData = 6; break;
		case "grayglass": material = Material.STAINED_GLASS; unsupportedData = 7; break;
		case "lightgrayglass": material = Material.STAINED_GLASS; unsupportedData = 8; break;
		case "cyanglass": material = Material.STAINED_GLASS; unsupportedData = 9; break;
		case "purpleglass": material = Material.STAINED_GLASS; unsupportedData = 10; break;
		case "blueglass": material = Material.STAINED_GLASS; unsupportedData = 11; break;
		case "brownglass": material = Material.STAINED_GLASS; unsupportedData = 12; break;
		case "greenglass": material = Material.STAINED_GLASS; unsupportedData = 13; break;
		case "redglass": material = Material.STAINED_GLASS; unsupportedData = 14; break;
		case "blackglass": material = Material.STAINED_GLASS; unsupportedData = 15; break;		
		case "whitepane": material = Material.STAINED_GLASS_PANE; unsupportedData = 0; break;
		case "orangepane": material = Material.STAINED_GLASS_PANE; unsupportedData = 1; break;
		case "magentapane": material = Material.STAINED_GLASS_PANE; unsupportedData = 2; break;
		case "lightbluepane": material = Material.STAINED_GLASS_PANE; unsupportedData = 3; break;
		case "yellowpane": material = Material.STAINED_GLASS_PANE; unsupportedData = 4; break;
		case "limepane": material = Material.STAINED_GLASS_PANE; unsupportedData = 5; break;
		case "pinkpane": material = Material.STAINED_GLASS_PANE; unsupportedData = 6; break;
		case "graypane": material = Material.STAINED_GLASS_PANE; unsupportedData = 7; break;
		case "lightgraypane": material = Material.STAINED_GLASS_PANE; unsupportedData = 8; break;
		case "cyanpane": material = Material.STAINED_GLASS_PANE; unsupportedData = 9; break;
		case "purplepane": material = Material.STAINED_GLASS_PANE; unsupportedData = 10; break;
		case "bluepane": material = Material.STAINED_GLASS_PANE; unsupportedData = 11; break;
		case "brownpane": material = Material.STAINED_GLASS_PANE; unsupportedData = 12; break;
		case "greenpane": material = Material.STAINED_GLASS_PANE; unsupportedData = 13; break;
		case "redpane": material = Material.STAINED_GLASS_PANE; unsupportedData = 14; break;
		case "blackpane": material = Material.STAINED_GLASS_PANE; unsupportedData = 15; break;
		case "sunflower": material = Material.DOUBLE_PLANT; unsupportedData = 0; break;
		case "lilac": material = Material.DOUBLE_PLANT; unsupportedData = 1; break;
		case "doubletallgrass": material = Material.DOUBLE_PLANT; unsupportedData = 2; break;
		case "largefern": material = Material.DOUBLE_PLANT; unsupportedData = 3; break;
		case "rosebush": material = Material.DOUBLE_PLANT; unsupportedData = 4; break;
		case "peony": material = Material.DOUBLE_PLANT; unsupportedData = 5; break;
		case "regenpot": material = Material.POTION; durability = this.getPotionDurability(1, 0, true, false); break;
		case "swiftnesspot": material = Material.POTION; durability = this.getPotionDurability(2, 0, true, false); break;
		case "fireresistpot": material = Material.POTION; durability = this.getPotionDurability(3, 0, true, false); break;
		case "poisonpot": material = Material.POTION; durability = this.getPotionDurability(4, 0, true, false); break;
		case "healthpot": material = Material.POTION; durability = this.getPotionDurability(5, 0, true, false); break;
		case "nightvispot": material = Material.POTION; durability = this.getPotionDurability(6, 0, true, false); break;
		case "weaknesspot": material = Material.POTION; durability = this.getPotionDurability(8, 0, true, false); break;
		case "strengthpot": material = Material.POTION; durability = this.getPotionDurability(9, 0, true, false); break;
		case "slownesspot": material = Material.POTION; durability = this.getPotionDurability(10, 0, true, false); break;
		case "leapingpot": material = Material.POTION; durability = this.getPotionDurability(11, 0, true, false); break;
		case "harmingpot": material = Material.POTION; durability = this.getPotionDurability(12, 0, true, false); break;
		case "waterbrpot": material = Material.POTION; durability = this.getPotionDurability(13, 0, true, false); break;
		case "invispot": material = Material.POTION; durability = this.getPotionDurability(14, 0, true, false); break;
		case "regenpot2": material = Material.POTION; durability = this.getPotionDurability(1, 1, true, false); break;
		case "swiftnesspot2": material = Material.POTION; durability = this.getPotionDurability(2, 1, true, false); break;
		case "fireresistpot2": material = Material.POTION; durability = this.getPotionDurability(3, 1, true, false); break;
		case "poisonpot2": material = Material.POTION; durability = this.getPotionDurability(4, 1, true, false); break;
		case "healthpot2": material = Material.POTION; durability = this.getPotionDurability(5, 1, true, false); break;
		case "nightvispot2": material = Material.POTION; durability = this.getPotionDurability(6, 1, true, false); break;
		case "weaknesspot2": material = Material.POTION; durability = this.getPotionDurability(8, 1, true, false); break;
		case "strengthpot2": material = Material.POTION; durability = this.getPotionDurability(9, 1, true, false); break;
		case "slownesspot2": material = Material.POTION; durability = this.getPotionDurability(11, 1, true, false); break;
		case "leapingpot2": material = Material.POTION; durability = this.getPotionDurability(11, 1, true, false); break;
		case "harmingpot2": material = Material.POTION; durability = this.getPotionDurability(12, 1, true, false); break;
		case "waterbrpot2": material = Material.POTION; durability = this.getPotionDurability(13, 1, true, false); break;
		case "invispot2": material = Material.POTION; durability = this.getPotionDurability(14, 1, true, false); break;
		case "regenpot3": material = Material.POTION; durability = this.getPotionDurability(1, 3, true, false); break;
		case "swiftnesspot3": material = Material.POTION; durability = this.getPotionDurability(2, 3, true, false); break;
		case "fireresistpot3": material = Material.POTION; durability = this.getPotionDurability(3, 3, true, false); break;
		case "poisonpot3": material = Material.POTION; durability = this.getPotionDurability(4, 3, true, false); break;
		case "healthpot3": material = Material.POTION; durability = this.getPotionDurability(5, 3, true, false); break;
		case "nightvispot3": material = Material.POTION; durability = this.getPotionDurability(6, 3, true, false); break;
		case "weaknesspot3": material = Material.POTION; durability = this.getPotionDurability(8, 3, true, false); break;
		case "strengthpot3": material = Material.POTION; durability = this.getPotionDurability(9, 3, true, false); break;
		case "slownesspot3": material = Material.POTION; durability = this.getPotionDurability(13, 3, true, false); break;
		case "leapingpot3": material = Material.POTION; durability = this.getPotionDurability(11, 3, true, false); break;
		case "harmingpot3": material = Material.POTION; durability = this.getPotionDurability(12, 3, true, false); break;
		case "waterbrpot3": material = Material.POTION; durability = this.getPotionDurability(13, 3, true, false); break;
		case "invispot3": material = Material.POTION; durability = this.getPotionDurability(14, 3, true, false); break;
		case "regenspot": material = Material.POTION; durability = this.getPotionDurability(1, 0, false, true); break;
		case "swiftnessspot": material = Material.POTION; durability = this.getPotionDurability(2, 0, false, true); break;
		case "fireresistspot": material = Material.POTION; durability = this.getPotionDurability(3, 0, false, true); break;
		case "poisonspot": material = Material.POTION; durability = this.getPotionDurability(4, 0, false, true); break;
		case "healthspot": material = Material.POTION; durability = this.getPotionDurability(5, 0, false, true); break;
		case "nightvisspot": material = Material.POTION; durability = this.getPotionDurability(6, 0, false, true); break;
		case "weaknessspot": material = Material.POTION; durability = this.getPotionDurability(8, 0, false, true); break;
		case "strengthspot": material = Material.POTION; durability = this.getPotionDurability(9, 0, false, true); break;
		case "slownessspot": material = Material.POTION; durability = this.getPotionDurability(10, 0, false, true); break;
		case "leapingspot": material = Material.POTION; durability = this.getPotionDurability(11, 0, false, true); break;
		case "harmingspot": material = Material.POTION; durability = this.getPotionDurability(12, 0, false, true); break;
		case "waterbrspot": material = Material.POTION; durability = this.getPotionDurability(13, 0, false, true); break;
		case "invisspot": material = Material.POTION; durability = this.getPotionDurability(14, 0, false, true); break;
		case "regenspot2": material = Material.POTION; durability = this.getPotionDurability(1, 1, false, true); break;
		case "swiftnessspot2": material = Material.POTION; durability = this.getPotionDurability(2, 1, false, true); break;
		case "fireresistspot2": material = Material.POTION; durability = this.getPotionDurability(3, 1, false, true); break;
		case "poisonspot2": material = Material.POTION; durability = this.getPotionDurability(4, 1, false, true); break;
		case "healthspot2": material = Material.POTION; durability = this.getPotionDurability(5, 1, false, true); break;
		case "nightvisspot2": material = Material.POTION; durability = this.getPotionDurability(6, 1, false, true); break;
		case "weaknessspot2": material = Material.POTION; durability = this.getPotionDurability(8, 1, false, true); break;
		case "strengthspot2": material = Material.POTION; durability = this.getPotionDurability(9, 1, false, true); break;
		case "slownessspot2": material = Material.POTION; durability = this.getPotionDurability(11, 1, false, true); break;
		case "leapingspot2": material = Material.POTION; durability = this.getPotionDurability(11, 1, false, true); break;
		case "harmingspot2": material = Material.POTION; durability = this.getPotionDurability(12, 1, false, true); break;
		case "waterbrspot2": material = Material.POTION; durability = this.getPotionDurability(13, 1, false, true); break;
		case "invisspot2": material = Material.POTION; durability = this.getPotionDurability(14, 1, false, true); break;
		case "regenspot3": material = Material.POTION; durability = this.getPotionDurability(1, 3, false, true); break;
		case "swiftnessspot3": material = Material.POTION; durability = this.getPotionDurability(2, 3, false, true); break;
		case "fireresistspot3": material = Material.POTION; durability = this.getPotionDurability(3, 3, false, true); break;
		case "poisonspot3": material = Material.POTION; durability = this.getPotionDurability(4, 3, false, true); break;
		case "healthspot3": material = Material.POTION; durability = this.getPotionDurability(5, 3, false, true); break;
		case "nightvisspot3": material = Material.POTION; durability = this.getPotionDurability(6, 3, false, true); break;
		case "weaknessspot3": material = Material.POTION; durability = this.getPotionDurability(8, 3, false, true); break;
		case "strengthspot3": material = Material.POTION; durability = this.getPotionDurability(9, 3, false, true); break;
		case "slownessspot3": material = Material.POTION; durability = this.getPotionDurability(13, 3, false, true); break;
		case "leapingspot3": material = Material.POTION; durability = this.getPotionDurability(11, 3, false, true); break;
		case "harmingspot3": material = Material.POTION; durability = this.getPotionDurability(12, 3, false, true); break;
		case "waterbrspot3": material = Material.POTION; durability = this.getPotionDurability(13, 3, false, true); break;
		case "invisspot3": material = Material.POTION; durability = this.getPotionDurability(14, 3, false, true); break;		
		case "thickpot": material = Material.POTION; durability = this.getPotionDurability(0, 2, false, false); break;
		case "charmingpot": material = Material.POTION; durability = this.getPotionDurability(6, 2, false, false); break;
		case "charmingpot2": material = Material.POTION; durability = this.getPotionDurability(7, 2, false, false); break;
		case "refinedpot": material = Material.POTION; durability = this.getPotionDurability(12, 2, false, false); break;
		case "cordialpot": material = Material.POTION; durability = this.getPotionDurability(13, 2, false, false); break;
		case "sparklingpot": material = Material.POTION; durability = this.getPotionDurability(14, 2, false, false); break;
		case "sparklingpot2": material = Material.POTION; durability = this.getPotionDurability(15, 2, false, false); break;
		case "potentpot": material = Material.POTION; durability = this.getPotionDurability(16, 2, false, false); break;
		case "rankpot": material = Material.POTION; durability = this.getPotionDurability(22, 2, false, false); break;
		case "rankpot2": material = Material.POTION; durability = this.getPotionDurability(23, 2, false, false); break;
		case "acridpot": material = Material.POTION; durability = this.getPotionDurability(27, 2, false, false); break;
		case "grosspot": material = Material.POTION; durability = this.getPotionDurability(29, 2, false, false); break;
		case "stinkypot": material = Material.POTION; durability = this.getPotionDurability(30, 2, false, false); break;
		case "stinkypot2": material = Material.POTION; durability = this.getPotionDurability(32, 2, false, false); break;
		case "waterbottle": material = Material.POTION; durability = this.getPotionDurability(0, 1, false, false); break;
		case "mundanepot": material = Material.POTION; durability = this.getPotionDurability(0, 1, true, false); break;
		case "mundanepot2": material = Material.POTION; durability = this.getPotionDurability(0, 3, false, false); break;
		case "clearpot": material = Material.POTION; durability = this.getPotionDurability(6, 1, false, false); break;
		case "clearpot2": material = Material.POTION; durability = this.getPotionDurability(7, 1, false, false); break;
		case "diffusepot": material = Material.POTION; durability = this.getPotionDurability(11, 1, false, false); break;
		case "artlesspot": material = Material.POTION; durability = this.getPotionDurability(13, 1, false, false); break;
		case "thinpot": material = Material.POTION; durability = this.getPotionDurability(14, 1, false, false); break;
		case "thinpot2": material = Material.POTION; durability = this.getPotionDurability(15, 1, false, false); break;
		case "awkwardpot": material = Material.POTION; durability = this.getPotionDurability(16, 1, false, false); break;
		case "bunglingpot": material = Material.POTION; durability = this.getPotionDurability(22, 1, false, false); break;
		case "bunglingpot2": material = Material.POTION; durability = this.getPotionDurability(23, 1, false, false); break;
		case "smoothpot": material = Material.POTION; durability = this.getPotionDurability(27, 1, false, false); break;
		case "suavepot": material = Material.POTION; durability = this.getPotionDurability(29, 1, false, false); break;
		case "debonairpot": material = Material.POTION; durability = this.getPotionDurability(30, 1, false, false); break;
		case "debonairpot2": material = Material.POTION; durability = this.getPotionDurability(31, 1, false, false); break;
		case "skeletonskull": material = Material.SKULL_ITEM; unsupportedData = 0; break;
		case "wskeletonskull": material = Material.SKULL_ITEM; unsupportedData = 1; break;
		case "zombiehead": material = Material.SKULL_ITEM; unsupportedData = 2; break;
		case "head": material = Material.SKULL_ITEM; unsupportedData = 3; break;
		case "playerhead": material = Material.SKULL_ITEM; unsupportedData = 3; break;
		case "creeperhead": material = Material.SKULL_ITEM; unsupportedData = 4; break;
		case "blackbanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.BLACK); break;
		case "bluebanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.BLUE); break;
		case "brown": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.BROWN); break;
		case "cyanbanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.CYAN); break;		
		case "graybanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.GRAY); break;
		case "greenbanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.GREEN); break;
		case "lightbluebanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.LIGHT_BLUE); break;
		case "limebanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.LIME); break;
		case "magentabanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.MAGENTA); break;
		case "orangebanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.ORANGE); break;
		case "pinkbanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.PINK); break;
		case "purplebanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.PURPLE); break;
		case "redbanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.RED); break;
		case "silverbanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.SILVER); break;
		case "whitebanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.WHITE); break;
		case "yellowbanner": material = Material.BANNER; unsupportedData = Helpers.Hacks.getDataFromDye(DyeColor.YELLOW); break;
		case "granite": material = Material.STONE; unsupportedData = 1; break;
		case "polishedgranite": material = Material.STONE; unsupportedData = 2; break;
		case "diorite": material = Material.STONE; unsupportedData = 3; break;
		case "polisheddiorite": material = Material.STONE; unsupportedData = 4; break;
		case "andesite": material = Material.STONE; unsupportedData = 5; break;
		case "polishedandesite": material = Material.STONE; unsupportedData = 6; break;
		case "prismarinebrick": material = Material.PRISMARINE; unsupportedData = 1; break;
		case "darkprismarine": material = Material.PRISMARINE; unsupportedData = 2; break;
		
		default:
			if (this.aliasesReverse.containsKey(lowerName)) material = this.aliasesReverse.get(lowerName);
			else
			{
				try
				{
					final int materialId = Integer.parseInt(lowerName);
					material = Helpers.Hacks.getMaterialFromId(materialId);
				}
				catch (NumberFormatException ex) { }
			}
			break;
		}

		if (material == null)
		{
			try { material = Material.valueOf(lowerName.toUpperCase()); }
			catch (final IllegalArgumentException e) { material = null; }
		}
		if (material == null)
		{
			for (final Material testMaterial: Material.values())
			{
				String testMaterialName = testMaterial.toString();
				if (testMaterialName.contains("_"))
				{
					testMaterialName = testMaterialName.replace("_", "");
					if (testMaterialName.equalsIgnoreCase(lowerName))
					{
						material = testMaterial;
						break;
					}
				}
			}
		}
		if (material == null) return null;

		stack = new ItemStack(material, quantity);
		if (durability >= 0)
		{
			switch (material)
			{
			/*
			 * Keep this for backward compatibility with setting map id by durability
			 */
			case MAP: break;  
				
			default:
				if (!this.hasDurability(material)) return null;
				break;
			}
			stack.setDurability(durability);
		}
		
		MaterialData data = null;
		switch (material)
		{
		case WOOD_STEP: data = new WoodenStep(treeSpecies); break;
		case WOOL: data = new Wool(colour); break;
		case STEP: data = new Step(subMaterial); break;
		case LONG_GRASS: data = new LongGrass(grassSpecies); break;
		case SMOOTH_BRICK: data = new SmoothBrick(subMaterial); break;
		case SANDSTONE: data = new Sandstone(sandstoneType); break;
		case MONSTER_EGG:
			if (entityType == null) return null;
			data = new SpawnEgg(entityType);
			break;

		case LEAVES:
		case LEAVES_2:
		case LOG:
		case LOG_2:
		case SAPLING:
		case WOOD:
			data = Helpers.Hacks.getMaterialData(material, treeSpecies);
			break;

		case INK_SACK:
			final Dye dye = new Dye(material);
			dye.setColor(colour);
			data = dye;
			break;

		default:
			if (unsupportedData >= 0)
				data = Helpers.Hacks.getUnsupportedData(material, unsupportedData);
			break;
		}
		
		if (data != null) Helpers.Hacks.setStackData(stack, data);
		
		if (!this.applyMetadata(stack, metadata)) return null;
		
		int actualQuantity = quantity;
		if (actualQuantity == -1) actualQuantity = stack.getMaxStackSize();
		stack.setAmount(actualQuantity);
		return stack;
	}
	
	public Enchantment getEnchantment(final String name)
	{
		final String lowerName = name.toLowerCase();
		Enchantment enchantment = null;
		for (final Enchantment candidate: Enchantment.values())
		{
			if (candidate.getName().equalsIgnoreCase(lowerName))
			{
				enchantment = candidate;
				break;
			}
		}
		if (enchantment == null)
		{
			if (this.enchantmentsReverse.containsKey(lowerName))
			enchantment = this.enchantmentsReverse.get(lowerName);
		}
		return enchantment;
	}
	
	public Color getColour(final String name)
	{
		Color colour = null;
		
		switch (name.toLowerCase())
		{
		case "aqua": colour = Color.AQUA; break;
		case "black": colour = Color.BLACK; break;
		case "blue": colour = Color.BLUE; break;
		case "fuchsia": colour = Color.FUCHSIA; break;
		case "gray":
		case "grey": colour = Color.GRAY; break;
		case "green": colour = Color.GREEN; break;
		case "lime": colour = Color.LIME; break;
		case "maroon": colour = Color.MAROON; break;
		case "navy": colour = Color.NAVY; break;
		case "olive": colour = Color.OLIVE; break;
		case "orange": colour = Color.ORANGE; break;
		case "purple": colour = Color.PURPLE; break;
		case "red": colour = Color.RED; break;
		case "silver": colour = Color.SILVER; break;
		case "teal": colour = Color.TEAL; break;
		case "white": colour = Color.WHITE; break;
		case "yellow": colour = Color.YELLOW; break;
		
		default:
			int rgb = -1;
			try { rgb = Integer.parseInt(name); }
			catch (Exception ex) { }
			if (rgb != -1) colour = Color.fromRGB(rgb);
			break;
		}
		
		return colour;
	}
	
	public DyeColor getDyeColour(final String name)
	{
		DyeColor colour = null;
		
		switch (name.toLowerCase())
		{		
		case "black": colour = DyeColor.BLACK; break;
		case "blue": colour = DyeColor.BLUE; break;
		case "brown": colour = DyeColor.BROWN; break;
		case "cyan": colour = DyeColor.CYAN; break;		
		case "gray":
		case "grey": colour = DyeColor.GRAY; break;
		case "green": colour = DyeColor.GREEN; break;
		case "lightblue": colour = DyeColor.LIGHT_BLUE; break;
		case "lime": colour = DyeColor.LIME; break;
		case "magenta": colour = DyeColor.MAGENTA; break;
		case "orange": colour = DyeColor.ORANGE; break;
		case "pink": colour = DyeColor.PINK; break;
		case "purple": colour = DyeColor.PURPLE; break;
		case "red": colour = DyeColor.RED; break;
		case "silver": colour = DyeColor.SILVER; break;
		case "white": colour = DyeColor.WHITE; break;
		case "yellow": colour = DyeColor.YELLOW; break;
		}
		
		return colour;
	}
	
	public String[] getFriendlyNames(final ItemStack stack) { return this.getFriendlyNames(stack, false); }
	public String[] getFriendlyNames(final ItemStack stack, final boolean includeMetadata)
	{
		List<String> names = new ArrayList<String>();
		final Material material = stack.getType();

		switch (material)
		{
		case COAL:
		{
			final Coal coal = (Coal)stack.getData();
			if (coal.getType() == CoalType.COAL) names.add("Coal");
			else names.add("Charcoal");
		} break;

		case LEAVES:
		case LEAVES_2:
		case LOG:
		case LOG_2:
		case SAPLING:
		case WOOD:
		case WOOD_STEP:
		{
			Material typeMaterial = material;
			switch (material)
			{
			case LEAVES_2: typeMaterial = Material.LEAVES; break;
			case LOG_2: typeMaterial = Material.LOG; break;
			default: break;
			}
			
			String typeName = "";
			if (typeMaterial == Material.LEAVES) typeName = "Leaves";
			else if (typeMaterial == Material.LOG) typeName = "Log";
			else if (typeMaterial == Material.SAPLING) typeName = "Sapling";
			else if (typeMaterial == Material.WOOD) typeName = "Wood";
			else if (typeMaterial == Material.WOOD_STEP) typeName = "Slab";

			final TreeSpecies species = Helpers.Hacks.getTreeSpeciesFromData(stack, TreeSpecies.GENERIC);
			switch (species)
			{
			case ACACIA: names.add("Acacia" + typeName); break;
			case BIRCH: names.add("Birch" + typeName); break;
			case DARK_OAK: names.add("DarkOak" + typeName); break;
			case JUNGLE: names.add("Jungle" + typeName); break;
			case REDWOOD: names.add("Spruce" + typeName); break;

			case GENERIC:
				names.add("Oak" + typeName);
				names.add(typeName);
				break;
			}
		} break;

		case INK_SACK:
		case WOOL:
		{
			DyeColor colour = null;
			String typeName = "";
			if (material == Material.INK_SACK)
			{
				final Dye dye = (Dye)stack.getData();
				colour = dye.getColor();
				typeName = "Dye";
				
				switch (colour)
				{
				case BLACK: names.add("InkSac"); break;
				case BROWN: names.add("CocoaBean"); break;
				case WHITE: names.add("BoneMeal"); break;
				default: break;
				}
			}
			else if (material == Material.WOOL)
			{
				final Wool wool = (Wool)stack.getData();
				colour = wool.getColor();
				typeName = "Wool";
			}
			if (colour == null) colour = DyeColor.WHITE;
			
			for (final String colourName: Helpers.Item.getFriendlyNames(colour))
				names.add(colourName + typeName);
		} break;
		
		case CARPET:
		case STAINED_CLAY:
		case STAINED_GLASS:
		case STAINED_GLASS_PANE:
		{
			String typeName = "";
			if (material == Material.CARPET) typeName = "Carpet";
			else if (material == Material.STAINED_CLAY) typeName = "Clay";
			else if (material == Material.STAINED_GLASS) typeName = "Glass";
			else if (material == Material.STAINED_GLASS_PANE) typeName = "Pane";
			
			String colours = "";
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 0: colours = "White"; break;
			case 1: colours = "Orange"; break;
			case 2: colours = "Magenta"; break;
			case 3: colours = "LightBlue"; break;
			case 4: colours = "Yellow"; break;
			case 5: colours = "Lime"; break;
			case 6: colours = "Pink"; break;
			case 7: colours = "Gray/Grey"; break;
			case 8: colours = "LightGray/LightGrey"; break;
			case 9: colours = "Cyan"; break;
			case 10: colours = "Purple"; break;
			case 11: colours = "Blue"; break;
			case 12: colours = "Brown"; break;
			case 13: colours = "Green"; break;
			case 14: colours = "Red"; break;
			case 15: colours = "Black"; break;
			}
			
			for (final String colour: colours.split("/"))
				names.add(colour + typeName);
		} break;

		case STEP:
		{
			final Step step = (Step)stack.getData();
			switch (step.getMaterial())
			{
			case BRICK: names.add("BrickSlab"); break;
			case NETHER_BRICK: names.add("NetherSlab"); break;
			case QUARTZ_BLOCK: names.add("QuartzSlab"); break;
			case SMOOTH_BRICK: names.add("SmoothSlab"); break;
			case STONE: names.add("StoneSlab"); break;

			case COBBLESTONE:
				names.add("CobbleSlab");
				names.add("CobblestoneSlab");
				break;
			case SANDSTONE:
				names.add("SandstoneSlab");
				names.add("SandSlab"); break;
			default: break;
			}
		} break;

		case LONG_GRASS:
		{
			final LongGrass grass = (LongGrass)stack.getData();
			switch (grass.getSpecies())
			{
			case DEAD: names.add("DeadGrass"); break;
			case FERN_LIKE: names.add("Fern"); break;
			case NORMAL: names.add("LongGrass"); break;
			}
		} break;

		case MONSTER_EGG:
		{
			final SpawnEgg egg = (SpawnEgg)stack.getData();
			switch (egg.getSpawnedType())
			{
			case BAT: names.add("BatEgg"); break;
			case BLAZE: names.add("BlazeEgg"); break;
			case CAVE_SPIDER: names.add("CaveSpiderEgg"); break;
			case CHICKEN: names.add("ChickenEgg"); break;
			case COW: names.add("CowEgg"); break;
			case CREEPER: names.add("CreeperEgg"); break;
			case ENDERMAN: names.add("EndermanEgg"); break;
			case ENDERMITE: names.add("EndermiteEgg"); break;			
			case GHAST: names.add("GhastEgg"); break;
			case GIANT: names.add("GiantEgg"); break;
			case GUARDIAN: names.add("GuardianEgg"); break;
			case HORSE: names.add("HorseEgg"); break;
			case IRON_GOLEM: names.add("IronGolemEgg"); break;
			case MAGMA_CUBE: names.add("MagmaCubeEgg"); break;
			case MUSHROOM_COW: names.add("MooshroomEgg"); break;
			case OCELOT: names.add("OcelotEgg"); break;
			case PIG: names.add("PigEgg"); break;
			case PIG_ZOMBIE: names.add("PigZombieEgg"); break;
			case RABBIT: names.add("RabbitEgg"); break;
			case SHEEP: names.add("SheepEgg"); break;
			case SILVERFISH: names.add("SilverfishEgg"); break;
			case SKELETON: names.add("SkeletonEgg"); break;
			case SLIME: names.add("SlimeEgg"); break;
			case SNOWMAN: names.add("SnowGolemEgg"); break;
			case SPIDER: names.add("SpiderEgg"); break;
			case SQUID: names.add("SquidEgg"); break;
			case VILLAGER: names.add("VillagerEgg"); break;
			case WITCH: names.add("WitchEgg"); break;
			case WITHER: names.add("WitherEgg"); break;
			case WOLF: names.add("WolfEgg"); break;
			case ZOMBIE: names.add("ZombieEgg"); break;
			default: break;
			}
		} break;

		case SMOOTH_BRICK:
		{
			final SmoothBrick brick = (SmoothBrick)stack.getData();
			switch (brick.getMaterial())
			{
			case STONE:
				names.add("SmoothBrick");
				names.add("Stonebrick");
				break;
			case MOSSY_COBBLESTONE: names.add("MossyBrick"); break;
			case COBBLESTONE: names.add("CrackedBrick"); break;
			case SMOOTH_BRICK: names.add("GlyphBrick"); break;
			default: break;
			}
		} break;

		case SANDSTONE:
		{
			final Sandstone stone = (Sandstone)stack.getData();
			switch (stone.getType())
			{
			case CRACKED: names.add("Sandstone"); break;
			case GLYPHED: names.add("GlyphSandstone"); break;
			case SMOOTH: names.add("SmoothSandstone"); break;
			}
		} break;
		
		case QUARTZ_BLOCK:
		{
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 0: names.add("QuartzBlock"); break;
			case 1: names.add("GlyphQuartz"); break;
			case 2: names.add("QuartzPillar"); break;
			}
		} break;
		
		case RED_ROSE:
		{
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 0: names.add("Poppy"); break;
			case 1: names.add("BlueOrchid"); break;
			case 2: names.add("Allium"); break;
			case 3: names.add("AzureBluet"); break;
			case 4: names.add("RedTulip"); break;
			case 5: names.add("OrangeTulip"); break;
			case 6: names.add("WhiteTulip"); break;
			case 7: names.add("PinkTulip"); break;
			case 8: names.add("OxeyeDaisy"); break;
			}
		} break;
		
		case DIRT:
		{
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 1: names.add("CoarseDirt"); break;
			case 2: names.add("Podzol"); break;
			}
		} break;

		case SAND:
		{
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 1: names.add("RedSand"); break;
			}
		} break;
		
		case RED_SANDSTONE:
		{
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 0: names.add("RedSandstone"); break;
			case 1: names.add("GlyphRedSand"); break;
			case 2: names.add("SmoothRedSand"); break;
			}
		} break;
		
		case SPONGE:
		{
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 1: names.add("WetSponge"); break;
			}
		} break;
		
		case DOUBLE_PLANT:
		{
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 0: names.add("Sunflower"); break;
			case 1: names.add("Lilac"); break;
			case 2: names.add("DoubleTallgrass"); break;
			case 3: names.add("LargeFern"); break;
			case 4: names.add("RoseBush"); break;
			case 5: names.add("Peony"); break;
			}
		} break;
		
		case POTION:
		{
			final short durability = stack.getDurability(); 
			final int type = (durability & 31);
			final boolean isLevel2 = ((durability & 32) == 32);
			final boolean isExtended = ((durability & 64) == 64);
			final boolean isDrinkable = ((durability & 8192) == 8192);
			final boolean isSplash = ((durability & 16384) == 16384);
			
			if (type > 0 && (isDrinkable || isSplash))
			{
				String name = "";
				switch (type)
				{
				case 1: name = "Regen"; break;
				case 2: name = "Swiftness"; break;
				case 3: name = "FireResist"; break;
				case 4: name = "Poison"; break;
				case 5: name = "Health"; break;
				case 6: name = "NightVis"; break;
				case 8: name = "Weakness"; break;
				case 9: name = "Strength"; break;
				case 10: name = "Slowness"; break;
				case 11: name = "Leaping"; break;
				case 12: name = "Harming"; break;
				case 13: name = "WaterBr"; break;
				case 14: name = "Invis"; break;
				}
				
				if (name.length() > 0)
				{
					if (isSplash) name += "S";
					name += "Pot";
					if (isLevel2) name += "2";
					else if (isExtended) name += "3";
					
					names.add(name);
				}
			}
			else
			{
				if (isLevel2)
				{
					switch (type)
					{					
					case 0: names.add("ThickPot"); break;
					case 6: names.add("CharmingPot"); break;
					case 7: names.add("CharmingPot2"); break;
					case 11: names.add("RefinedPot"); break;
					case 13: names.add("CordialPot"); break;
					case 14: names.add("SparklingPot"); break;
					case 15: names.add("SparklingPot2"); break;
					case 16: names.add("PotentPot"); break;
					case 22: names.add("RankPot"); break;
					case 23: names.add("RankPot2"); break;
					case 27: names.add("AcridPot"); break;
					case 29: names.add("GrossPot"); break;
					case 30: names.add("StinkyPot"); break;
					case 31: names.add("StinkyPot2"); break;
					}
				}
				else
				{
					switch (type)
					{
					case 0:
						if (isExtended) names.add("MundanePot2");
						else if (isDrinkable) names.add("MundanePot");					
						else names.add("WaterBottle");
						break;					
					case 6: names.add("ClearPot"); break;
					case 7: names.add("ClearPot2"); break;
					case 11: names.add("DiffusePot"); break;
					case 13: names.add("ArtlessPot"); break;
					case 14: names.add("ThinPot"); break;
					case 15: names.add("ThinPot2"); break;
					case 16: names.add("AwkwardPot"); break;
					case 22: names.add("BunglingPot"); break;
					case 23: names.add("BunglingPot2"); break;
					case 27: names.add("SmoothPot"); break;
					case 29: names.add("SuavePot"); break;
					case 30: names.add("DebonairPot"); break;
					case 31: names.add("DebonairPot2"); break;
					}
				}
			}
		} break;
		
		case SKULL_ITEM:
		{
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 0: names.add("SkeletonSkull"); break;
			case 1: names.add("WSkeletonSkull"); break;
			case 2: names.add("ZombieHead"); break;
			case 3:
				names.add("Head");
				names.add("PlayerHead");
				break;
			case 4: names.add("CreeperHead"); break;
			}
		} break;
		
		case BANNER:
		{
			final DyeColor colour = Helpers.Hacks.getDyeByData(Helpers.Hacks.getUnsupportedData(stack));
			for (final String colourName: Helpers.Item.getFriendlyNames(colour))
					names.add(colourName + "Banner");
		} break;

		case STONE:
		{
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 1: names.add("Granite"); break;
			case 2: names.add("PolishedGranite"); break;
			case 3: names.add("Diorite"); break;
			case 4: names.add("PolishedDiorite"); break;
			case 5: names.add("Andesite"); break;
			case 6: names.add("PolishedAndesite"); break;
			}
		} break;
		
		case PRISMARINE:
		{
			switch (Helpers.Hacks.getUnsupportedData(stack))
			{
			case 1: names.add("PrismarineBrick"); break;
			case 2: names.add("DarkPrismarine"); break;
			}
		} break;

		default: break;
		}

		if (this.aliases.containsKey(material))
			for (final String alias: this.aliases.get(material)) names.add(alias);

		if (names.size() == 0)
			names.add(WordUtils.capitalizeFully(material.toString(), new char[] { '_' }).replace("_", ""));
		
		if (includeMetadata)
		{
			final String metadata = this.getFullMetadata(stack);
			if (metadata.length() > 0)
			{
				List<String> namesWithMetadata = new ArrayList<String>(names.size());
				for (final String name: names)
					namesWithMetadata.add(Helpers.Parameters.replace("$1$2", name, metadata));
				names = namesWithMetadata;
			}
		}

		return names.toArray(new String[names.size()]);
	}
	
	public String[] getFriendlyNames(final Color colour)
	{
		final List<String> names = new ArrayList<String>();
		
		if (colour.equals(Color.AQUA)) names.add("Aqua");
		else if (colour.equals(Color.BLACK)) names.add("Black");
		else if (colour.equals(Color.BLUE)) names.add("Blue");
		else if (colour.equals(Color.FUCHSIA)) names.add("Fuchsia");
		else if (colour.equals(Color.GRAY))
		{
			names.add("Gray");
			names.add("Grey");
		}
		else if (colour.equals(Color.GREEN)) names.add("Green");
		else if (colour.equals(Color.LIME)) names.add("Lime");
		else if (colour.equals(Color.MAROON)) names.add("Maroon");
		else if (colour.equals(Color.NAVY)) names.add("Navy");
		else if (colour.equals(Color.OLIVE)) names.add("Olive");
		else if (colour.equals(Color.ORANGE)) names.add("Orange");
		else if (colour.equals(Color.PURPLE)) names.add("Purple");
		else if (colour.equals(Color.RED)) names.add("Red");
		else if (colour.equals(Color.SILVER)) names.add("Silver");		
		else if (colour.equals(Color.TEAL)) names.add("Teal");
		else if (colour.equals(Color.WHITE)) names.add("White");
		else if (colour.equals(Color.YELLOW)) names.add("Yellow");
		else names.add(String.valueOf(colour.asRGB()));
		
		return names.toArray(new String[names.size()]);
	}
	
	public String[] getFriendlyNames(final DyeColor colour)
	{
		final List<String> names = new ArrayList<String>();

		if (colour.equals(DyeColor.BLACK)) names.add("Black");
		else if (colour.equals(DyeColor.BLUE)) names.add("Blue");
		else if (colour.equals(DyeColor.BROWN)) names.add("Brown");
		else if (colour.equals(DyeColor.CYAN)) names.add("Cyan");
		else if (colour.equals(DyeColor.GRAY))
		{
			names.add("Gray");
			names.add("Grey");
		}
		else if (colour.equals(DyeColor.GREEN)) names.add("Green");
		else if (colour.equals(DyeColor.LIGHT_BLUE)) names.add("LightBlue");
		else if (colour.equals(DyeColor.LIME)) names.add("Lime");
		else if (colour.equals(DyeColor.MAGENTA)) names.add("Magenta");
		else if (colour.equals(DyeColor.ORANGE)) names.add("Orange");
		else if (colour.equals(DyeColor.PINK)) names.add("Pink");
		else if (colour.equals(DyeColor.PURPLE)) names.add("Purple");
		else if (colour.equals(DyeColor.RED)) names.add("Red");
		else if (colour.equals(DyeColor.SILVER))
		{			
			names.add("Silver");
			names.add("LightGray");
			names.add("LightGrey");
		}
		else if (colour.equals(DyeColor.WHITE)) names.add("White");
		else if (colour.equals(DyeColor.YELLOW)) names.add("Yellow");
		
		return names.toArray(new String[names.size()]);
	}
	
	private short getPotionDurability(
			final int type,
			final int level,
			final boolean isDrinkable,
			final boolean isSplash)
	{
		short potionDurability = (short)(type & Short.MAX_VALUE);
		
		switch (level)
		{
		case 1: break;
		case 2: potionDurability += 32; break;
		case 3: potionDurability += 64; break;
		}
		if (isDrinkable) potionDurability += 8192;
		if (isSplash) potionDurability += 16384;
		
		return potionDurability;
	}
	
	public String[] getFriendlyNames(final Enchantment enchantment)
	{
		final String[] friendlyNames = this.enchantments.get(enchantment);
		final List<String> allNames = new ArrayList<String>();
		if (friendlyNames != null)
			for (final String name: friendlyNames) allNames.add(name.toUpperCase());
		if (allNames.size() == 0)
			allNames.add(enchantment.getName());
		return allNames.toArray(new String[0]);
	}
	
	public boolean hasMetadata(final String name)
	{
		return name.contains("{") || name.contains("}");
	}
	
	public boolean hasDurability(final ItemStack stack) { return this.hasDurability(stack.getType()); }
	public boolean hasDurability(final Material material)
	{
		switch (material)
		{
		case CHAINMAIL_BOOTS:
		case CHAINMAIL_CHESTPLATE:
		case CHAINMAIL_HELMET:
		case CHAINMAIL_LEGGINGS:
			
		case DIAMOND_BOOTS:
		case DIAMOND_CHESTPLATE:
		case DIAMOND_HELMET:
		case DIAMOND_LEGGINGS:
		
		case GOLD_BOOTS:
		case GOLD_CHESTPLATE:
		case GOLD_HELMET:
		case GOLD_LEGGINGS:
			
		case IRON_BOOTS:
		case IRON_CHESTPLATE:
		case IRON_HELMET:
		case IRON_LEGGINGS:
			
		case LEATHER_BOOTS:
		case LEATHER_CHESTPLATE:
		case LEATHER_HELMET:
		case LEATHER_LEGGINGS:
			
		case DIAMOND_AXE:
		case DIAMOND_HOE:
		case DIAMOND_PICKAXE:
		case DIAMOND_SPADE:
		case DIAMOND_SWORD:			
			
		case GOLD_AXE:
		case GOLD_HOE:
		case GOLD_PICKAXE:
		case GOLD_SPADE:
		case GOLD_SWORD:

		case IRON_AXE:
		case IRON_HOE:
		case IRON_PICKAXE:
		case IRON_SPADE:
		case IRON_SWORD:

		case STONE_AXE:
		case STONE_HOE:
		case STONE_PICKAXE:
		case STONE_SPADE:
		case STONE_SWORD:

		case WOOD_AXE:
		case WOOD_HOE:
		case WOOD_PICKAXE:
		case WOOD_SPADE:
		case WOOD_SWORD:
			
		case BOW:
		case CARROT_STICK:
		case FISHING_ROD:
		case FLINT_AND_STEEL:
		case SHEARS:

			return true;
			
		default: return false;
		}
	}
	
	public boolean isATool(final ItemStack stack) { return this.isATool(stack.getType()); }
	public boolean isATool(final Material material)
	{
		switch (material)
		{			
		case DIAMOND_AXE:
		case DIAMOND_HOE:
		case DIAMOND_PICKAXE:
		case DIAMOND_SPADE:
			
		case GOLD_AXE:
		case GOLD_HOE:
		case GOLD_PICKAXE:
		case GOLD_SPADE:

		case IRON_AXE:
		case IRON_HOE:
		case IRON_PICKAXE:
		case IRON_SPADE:

		case STONE_AXE:
		case STONE_HOE:
		case STONE_PICKAXE:
		case STONE_SPADE:

		case WOOD_AXE:
		case WOOD_HOE:
		case WOOD_PICKAXE:
		case WOOD_SPADE:
			
		case CARROT_STICK:
		case FISHING_ROD:
		case FLINT_AND_STEEL:
		case SHEARS:

			return true;
			
		default: return false;
		}
	}
	
	public boolean isAWeapon(final ItemStack stack) { return this.isAWeapon(stack.getType()); }
	public boolean isAWeapon(final Material material)
	{
		switch (material)
		{
		case BOW:
		case DIAMOND_SWORD:			
		case GOLD_SWORD:
		case IRON_SWORD:
		case STONE_SWORD:
		case WOOD_SWORD:

			return true;
			
		default: return false;
		}
	}
	
	public boolean isArmour(final ItemStack stack) { return this.isArmour(stack.getType()); }
	public boolean isArmour(final Material material)
	{
		switch (material)
		{
		case CHAINMAIL_BOOTS:
		case CHAINMAIL_CHESTPLATE:
		case CHAINMAIL_HELMET:
		case CHAINMAIL_LEGGINGS:
			
		case DIAMOND_BOOTS:
		case DIAMOND_CHESTPLATE:
		case DIAMOND_HELMET:
		case DIAMOND_LEGGINGS:
		
		case GOLD_BOOTS:
		case GOLD_CHESTPLATE:
		case GOLD_HELMET:
		case GOLD_LEGGINGS:
			
		case IRON_BOOTS:
		case IRON_CHESTPLATE:
		case IRON_HELMET:
		case IRON_LEGGINGS:
			
		case LEATHER_BOOTS:
		case LEATHER_CHESTPLATE:
		case LEATHER_HELMET:
		case LEATHER_LEGGINGS:
			
			return true;
			
		default: return false;
		}
	}	

	public boolean canCarry(final Inventory inventory, final ItemStack stack)
	{
		int remaining = stack.getAmount();
		int maxStackSize = stack.getMaxStackSize();
		if (maxStackSize > inventory.getMaxStackSize()) maxStackSize = inventory.getMaxStackSize();

		for (final ItemStack inventoryStack: inventory.getContents())
		{
			if (inventoryStack == null) remaining -= maxStackSize;
			else if (Helpers.Hacks.areEqual(inventoryStack, stack))
			{
				if (inventoryStack.getAmount() < maxStackSize)
					remaining -= (maxStackSize - inventoryStack.getAmount());
			}

			if (remaining <= 0) break;
		}

		return (remaining <= 0);
	}
	
	public void putInInventory(final Inventory inventory, final ItemStack stack)
	{		
		int remaining = stack.getAmount();
		int maxStackSize = stack.getMaxStackSize();
		if (maxStackSize > inventory.getMaxStackSize()) maxStackSize = inventory.getMaxStackSize();
		
		while (remaining > 0)
		{
			int amount = remaining;
			if (amount > maxStackSize) amount = maxStackSize;
			final ItemStack depositedStack = stack.clone();
			depositedStack.setAmount(amount);
			
			if (!Helpers.Item.canCarry(inventory, depositedStack)) break;
			
			inventory.addItem(depositedStack);
			remaining -= amount;
		}
		
		stack.setAmount(remaining);
		
		return;
	}

	public boolean repair(final ItemStack stack)
	{
		boolean success = false;
		if (this.hasDurability(stack) &&
				stack.getDurability() > 0)
		{
			stack.setDurability((short)0);
			success = true;
		}
		return success;
	}

	public boolean enchantItem(final ItemStack stack, final Enchantment enchantment, final int level)
	{
		boolean success = false;
		if (enchantment.canEnchantItem(stack))
		{
			if (stack.getEnchantmentLevel(enchantment) > 0)
				stack.removeEnchantment(enchantment);
			stack.addEnchantment(enchantment, level);
			success = true;
		}
		return success;
	}
	
	public void unenchantItem(final ItemStack stack)
	{
		final Set<Enchantment> enchantments = stack.getEnchantments().keySet();
		for (final Enchantment enchantment: enchantments)
			stack.removeEnchantment(enchantment);
	}
	
	public String getFullMetadata(final ItemStack stack)
	{
		final JSONObject json = new JSONObject();
		boolean hasMetadata = false;
		
		if (this.hasDurability(stack))
		{
			json.put("durability", stack.getDurability());
			hasMetadata = true;
		}
		
		if (stack.getType() == Material.MAP)
		{
			json.put("map", stack.getDurability());
			hasMetadata = true;
		}
		
		if (stack.hasItemMeta())
		{
			final ItemMeta metadata = stack.getItemMeta();
			if (metadata.hasDisplayName())
			{
				json.put("name", metadata.getDisplayName());
				hasMetadata = true;				
			}
			
			if (metadata instanceof LeatherArmorMeta)
			{
				final LeatherArmorMeta leatherArmorMetadata = (LeatherArmorMeta)metadata;
				final Color colour = leatherArmorMetadata.getColor();
				if (colour.asRGB() != ItemHelpers.DefaultLeatherColour)
					json.put("colour", this.getFriendlyNames(leatherArmorMetadata.getColor())[0]);
				hasMetadata = true;
			}
			else if (metadata instanceof EnchantmentStorageMeta)
			{
				final EnchantmentStorageMeta enchantmentStorageMeta = (EnchantmentStorageMeta)metadata;
				if (enchantmentStorageMeta.hasStoredEnchants())
				{
					json.put("storedenchantments", this.getEnchantmentJson(enchantmentStorageMeta.getStoredEnchants()));
					hasMetadata = true;
				}
			}
			else if (metadata instanceof BannerMeta)
			{
				final BannerMeta bannerMeta = (BannerMeta)metadata;
				final JSONArray patternsJson = new JSONArray();
				for (final Pattern pattern: bannerMeta.getPatterns())
				{
					final String code = pattern.getPattern().getIdentifier();
					final DyeColor colour = pattern.getColor();
					final JSONObject patternJson = this.getPatternJson(code, colour);
					patternsJson.put(patternJson);
					hasMetadata = true;
				}
				json.put("patterns", patternsJson);
			}
			else if (metadata instanceof SkullMeta)
			{
				final SkullMeta skullMeta = (SkullMeta)metadata;
				json.put("owner", skullMeta.getOwner());
				hasMetadata = true;
			}
			
			if (metadata.hasEnchants())
			{
				json.put("enchantments", this.getEnchantmentJson(metadata.getEnchants()));
				hasMetadata = true;
			}			
		}
		
		if (hasMetadata) return json.toString();
		else return "";
	}
	
	private JSONObject getPatternJson(final String code, final DyeColor colour)
	{
		final JSONObject patternJson = new JSONObject();
		patternJson.put("code", code);
		patternJson.put("colour", Helpers.Item.getFriendlyNames(colour)[0]);
		return patternJson;
	}

	private JSONArray getEnchantmentJson(final Map<Enchantment, Integer> enchantments)
	{
		final JSONArray enchantmentsJson = new JSONArray();
		for (final Enchantment enchantment: enchantments.keySet())
		{			
			final JSONObject enchantmentJson = new JSONObject();
			enchantmentJson.put("name", Helpers.Item.getFriendlyNames(enchantment)[0]);
			enchantmentJson.put("level", enchantments.get(enchantment));
			enchantmentsJson.put(enchantmentJson);
		}
		return enchantmentsJson;
	}

	public boolean applyMetadata(final ItemStack stack, final String metadata)
	{
		if (metadata == null || metadata.length() == 0) return true;

		final ItemMeta meta = stack.getItemMeta();
		if (meta == null) return false;
		
		JSONObject json = null;
		
		try { json = new JSONObject(metadata); }
		catch (Exception ex) { ex.printStackTrace(); }
		if (json == null) return false;
		
		if (json.has("name"))
		{
			final String name = json.getString("name");
			meta.setDisplayName(name);
			stack.setItemMeta(meta);
		}
		
		if (json.has("durability") && this.hasDurability(stack))
		{
			final short durability = (short)json.getInt("durability");
			stack.setDurability(durability);
		}
		
		if (json.has("map") && stack.getType() == Material.MAP)
		{
			final short durability = (short)json.getInt("map");
			stack.setDurability(durability);
		}
		
		if (json.has("durability"))
		{
			final short durability = (short)json.getInt("durability");
			stack.setDurability(durability);
		}

		if (json.has("colour"))
		{
			if (meta instanceof LeatherArmorMeta)
			{
				final LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta)meta;
				final String colourName = json.getString("colour");
				final Color colour = this.getColour(colourName); 
				if (colour != null)
				{
					leatherArmorMeta.setColor(colour);
					stack.setItemMeta(leatherArmorMeta);
				}
			}
		}
		
		if (json.has("storedenchantments"))
		{
			if (meta instanceof EnchantmentStorageMeta)
			{
				final EnchantmentStorageMeta enchantmentStorageMeta = (EnchantmentStorageMeta)meta;
				final JSONArray enchantments = json.getJSONArray("storedenchantments");
				for (int i = 0 ; i < enchantments.length(); ++i)
				{
					JSONObject enchantment = enchantments.getJSONObject(i);
					final String name = enchantment.getString("name");
					final int level = enchantment.getInt("level");
					enchantmentStorageMeta.addStoredEnchant(this.getEnchantment(name), level, true);
				}
				stack.setItemMeta(enchantmentStorageMeta);
			}
		}

		if (json.has("enchantments"))
		{
			final JSONArray enchantments = json.getJSONArray("enchantments");
			for (int i = 0 ; i < enchantments.length(); ++i)
			{
				JSONObject enchantment = enchantments.getJSONObject(i);
				final String name = enchantment.getString("name");
				final int level = enchantment.getInt("level");
				this.enchantItem(stack, this.getEnchantment(name), level);
			}
		}
		
		if (json.has("patterns"))
		{
			if (meta instanceof BannerMeta)
			{
				final BannerMeta bannerMeta = (BannerMeta)meta;
				final JSONArray patterns = json.getJSONArray("patterns");
				for (int i = 0 ; i < patterns.length() && i < ItemHelpers.MaxBannerPatterns; ++i)
				{
					JSONObject pattern = patterns.getJSONObject(i);
					final String code = pattern.getString("code");
					final PatternType patternType = PatternType.getByIdentifier(code.toLowerCase());
					final String colourName = pattern.getString("colour");
					DyeColor dyeColour = this.getDyeColour(colourName);					
					if (dyeColour == null)
					{
						final Color colour = this.getColour(colourName);
						if (colour != null) dyeColour = DyeColor.getByColor(colour);
					}
					if (patternType == null || dyeColour == null) continue;
					bannerMeta.addPattern(new Pattern(dyeColour, patternType));
				}
				stack.setItemMeta(bannerMeta);
			}
		}
		
		if (json.has("owner"))
		{
			if (meta instanceof SkullMeta)
			{
				final SkullMeta skullMeta = (SkullMeta)meta;
				final String owner = json.getString("owner");
				skullMeta.setOwner(owner);
				stack.setItemMeta(skullMeta);
			}
		}

		return true;
	}
	
	public boolean isMetadataValid(final String name)
	{
		boolean valid = false;
		
		if (this.hasMetadata(name))
		{
			final String metadata = this.getMetadata(name);			
			try
			{
				new JSONObject(metadata);
				valid = true;
			}
			catch (Exception ex) { }
		}
		else valid = true;
		
		return valid;
	}
	
	public String getMetadata(final String name)
	{
		String metadata = "";
		
		if (this.hasMetadata(name))
		{
			final int braceIndex = name.indexOf('{');
			if (braceIndex >= 0)
				metadata = name.substring(braceIndex);
		}
		
		return metadata;
	}

	public String stripMetadata(final String name)
	{
		String strippedName = "";
		
		if (this.hasMetadata(name))
		{
			final int braceIndex = name.indexOf('{');
			if (braceIndex >= 0)
				strippedName = name.substring(0, braceIndex);
		}
		
		return strippedName;
	}
}
