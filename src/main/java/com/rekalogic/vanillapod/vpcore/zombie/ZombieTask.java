package com.rekalogic.vanillapod.vpcore.zombie;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Zombie;

import com.rekalogic.vanillapod.vpcore.util.TaskBase;

public final class ZombieTask extends TaskBase
{
	private static boolean isZombieDetected = false;
	public static boolean getZombieDetected() { return ZombieTask.isZombieDetected; }
	public static void setZombieDetected(final boolean value) { ZombieTask.isZombieDetected = value; }

	public ZombieTask(final ZombieSettings settings)
	{
		super(settings);
	}

	@Override
	public void runTask()
	{
		if (ZombieTask.getZombieDetected())
		{
			int zombieCount = 0;
			for (final World world: Bukkit.getWorlds())
			{
				if (world.getEnvironment() == Environment.NORMAL)
				{
					final Collection<Zombie> zombies = world.getEntitiesByClass(Zombie.class);
					zombieCount += zombies.size();
					int reason = 0;

					for (final Zombie zombie: zombies)
					{
						final LivingEntity target = zombie.getTarget();
						reason = 0;
						if (target != null) reason = ZombieTask.getZombieCancelTargetReason(zombie, target);
						if (reason != 0) zombie.setTarget(null);
					}
				}
			}

			ZombieTask.isZombieDetected = (zombieCount > 0);
		}
	}

	public final static int getZombieCancelTargetReason(final Zombie zombie, final Entity target)
	{
		int reason = 0;
		if (!zombie.hasLineOfSight(target)) reason = 1;
		else if (target.getType() == EntityType.VILLAGER)
		{
			if (!((Villager)target).hasLineOfSight(zombie)) reason = 2;
			else if (zombie.getLocation().distance(target.getLocation()) > 20) reason = 3;
		}

		return reason;
	}
}
