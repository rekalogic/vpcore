package com.rekalogic.vanillapod.vpcore.zombie;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;

public final class ZombieEventHandler extends EventHandlerBase<ZombieSettings>
{
	public ZombieEventHandler(final PluginBase plugin, final ZombieSettings settings)
	{
		super(plugin, settings);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onCreatureSpawn(final CreatureSpawnEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getEntityType() == EntityType.ZOMBIE)
		{
			ZombieTask.setZombieDetected(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onEntityTargetLivingEntity(final EntityTargetLivingEntityEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getEntityType() == EntityType.ZOMBIE)
		{
			final Zombie zombie = (Zombie)event.getEntity();
			final Entity target = event.getTarget();
			if (target != null)
			{
				final int reason = ZombieTask.getZombieCancelTargetReason(zombie, target);
				if (reason != 0) event.setTarget(null);
			}
		}
	}
}
