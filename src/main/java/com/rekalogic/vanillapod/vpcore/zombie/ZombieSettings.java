package com.rekalogic.vanillapod.vpcore.zombie;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;

public final class ZombieSettings extends SettingsBase
{
	public ZombieSettings(final VPPlugin plugin)
	{
		super(plugin);
	}

	@Override
	protected void onLoad() { }

	@Override
	protected void onSave() { }
}
