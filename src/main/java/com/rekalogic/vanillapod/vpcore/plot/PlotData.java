package com.rekalogic.vanillapod.vpcore.plot;

import java.util.UUID;

public final class PlotData
{
	private final String name;
	private final boolean isPremium;
	private final double markup;
	private final double buyPrice;
	private final double sellPrice;

	private UUID ownerUuid;
	private int groundLevel;

	public String getName() { return this.name; }
	public boolean getIsPremium() { return this.isPremium; }
	public double getMarkup() { return this.markup; }
	public double getBuyPrice() { return this.buyPrice; }
	public double getSellPrice() { return this.sellPrice; }

	public UUID getOwnerUuid() { return this.ownerUuid; }
	public void setOwnerUuid(final UUID uuid) { this.ownerUuid = uuid; }
	public int getGroundLevel() { return this.groundLevel; }
	public void setGroundLevel(final int groundLevel) { this.groundLevel = groundLevel; }

	public boolean isOwned() { return (this.ownerUuid != null); }

	public PlotData(
			final String name,
			final boolean isPremium,
			final double markup,
			final double buyPrice,
			final double sellPrice,
			final int groundLevel)
	{
		this.name = name;
		this.isPremium = isPremium;
		this.markup = markup;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
		this.groundLevel = groundLevel;
		this.ownerUuid = null;
	}
}
