package com.rekalogic.vanillapod.vpcore.plot;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class PlotDbInterface extends DatabaseInterfaceBase
{
	public PlotDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}

	public void updatePlotOwner(final PlotData plotData)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update(
				"vp_plot",
				"uuid = ?",
				"lower(name) = ?",
				plotData.getOwnerUuid(),
				plotData.getName().toLowerCase());
		connection.close();
	}

	public void createPlot(final PlotData plotData)
	{
		final MySqlConnection connection = super.getConnection();
		connection.insertInto(
				"vp_plot",
				"name, ispremium, markup, buyprice, sellprice",
				plotData.getName(),
				plotData.getIsPremium(),
				plotData.getMarkup(),
				plotData.getBuyPrice(),
				plotData.getSellPrice());
		connection.close();
	}

	public Map<String, PlotData> loadPlotData()
	{
		final Map<String, PlotData> plotDataMap = new ConcurrentHashMap<String, PlotData>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT * FROM vp_plot");
		while (result.moveNext())
		{
			final PlotData plotData = new PlotData(
					result.getString("name"),
					result.getBoolean("ispremium"),
					result.getDouble("markup"),
					result.getDouble("buyprice"),
					result.getDouble("sellprice"),
					result.getInt("groundlevel"));
			plotData.setOwnerUuid(result.getUuid("uuid"));
			plotDataMap.put(plotData.getName(), plotData);
		}
		connection.close();
		return plotDataMap;
	}

	public void recordPlotPurchase(final PlotData plotData)
	{
		final MySqlConnection connection = super.getConnection();
		connection.insertInto("vp_transaction",
				"uuid, execution, type, otheruuid, item, quantity, price, total",
				plotData.getOwnerUuid(),
				System.currentTimeMillis(),
				"plotpurchase",
				null,
				plotData.getName(),
				1,
				plotData.getBuyPrice(),
				plotData.getBuyPrice());
		connection.close();
	}

	public void recordPlotSale(final PlotData plotData)
	{
		final MySqlConnection connection = super.getConnection();
		connection.insertInto("vp_transaction",
				"uuid, execution, type, otheruuid, item, quantity, price, total",
				plotData.getOwnerUuid(),
				System.currentTimeMillis(),
				"plotsale",
				null,
				plotData.getName(),
				1,
				plotData.getSellPrice(),
				plotData.getSellPrice());
		connection.close();
	}

	public void recordPlotReset(final PlotData plotData, final UUID lastOwner)
	{
		final MySqlConnection connection = super.getConnection();
		connection.insertInto("vp_transaction",
				"uuid, execution, type, otheruuid, item, quantity, price, total",
				lastOwner,
				System.currentTimeMillis(),
				"plotreset",
				null,
				plotData.getName(),
				1,
				0,
				0);
		connection.close();
	}

	public void updatePlotGroundLevel(final PlotData plotData)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update(
				"vp_plot",
				"groundlevel = ?",
				"lower(name) = ?",
				plotData.getGroundLevel(),
				plotData.getName().toLowerCase());
		connection.close();
	}

	public void deletePlot(final PlotData plotData)
	{
		final MySqlConnection connection = super.getConnection();
		connection.deleteFrom("vp_plot", "lower(name) = ?", plotData.getName().toLowerCase());
		connection.close();
	}
}
