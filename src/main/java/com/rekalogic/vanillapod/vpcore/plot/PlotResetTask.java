package com.rekalogic.vanillapod.vpcore.plot;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.InventoryHolder;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.sign.SignSettings;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public final class PlotResetTask extends TaskBase
{
	private final VPPlugin plugin;
	private final PlotSettings plotSettings;
	private final SignSettings signSettings;
	private final PlotData plotData;
	private final World plotWorld;
	private boolean confirm;
	
	public PlotResetTask(
			final VPPlugin plugin,
			final PlotSettings plotSettings,
			final SignSettings signSettings,
			final PlotData plotData,
			final World plotWorld,
			final boolean confirm)
	{
		super(plotSettings);
		
		this.plugin = plugin;
		this.plotSettings = plotSettings;
		this.signSettings = signSettings;
		this.plotData = plotData;
		this.plotWorld = plotWorld;
		this.confirm = confirm;
	}
	
	@Override
	public void runTask()
	{
		if (!this.plugin.isWorldGuardEnabled())
		{
			super.info("WorldGuard support is not enabled.");
			this.cancel();
			return;
		}
		
		if (this.confirm)
		{
			if (!this.plugin.isLiveServer())
			{
				this.confirm = false;
				super.info("NOTLIVE: Reset of $1 will not be confirmed.", this.plotData);
			}
			else super.info("Resetting plot $1.", this.plotData);
		}
		
		final ProtectedRegion region = this.plotSettings.getRegionManager().getRegion(this.plotData.getName());
		if (region == null) super.severe("Could not find region for plot '$1'.", this.plotData);
		else if (this.plotData.getGroundLevel() == 0) super.severe("Could not reset plot $1 (groundlevel=0).", this.plotData);
		else
		{
			final BlockVector minPoint = region.getMinimumPoint();
			final BlockVector maxPoint = region.getMaximumPoint();
			final int minX = minPoint.getBlockX();
			final int minY = minPoint.getBlockY();
			final int minZ = minPoint.getBlockZ();
			final int maxX = maxPoint.getBlockX();
			final int maxY = maxPoint.getBlockY();
			final int maxZ = maxPoint.getBlockZ();

			for (int x = minX; x <= maxX; ++x)
			{
				for (int y = minY; y <= maxY; ++y)
				{
					for (int z = minZ; z <= maxZ; ++z)
					{
						if (!region.contains(x, y, z)) continue;
						final Block block = this.plotWorld.getBlockAt(x, y, z);
						switch (block.getType())
						{
						case BEDROCK:
							if (y <= 5) continue;
							break;

						default:
							break;
						}

						if (this.confirm)
						{
							if (this.signSettings.isControlledSign(block))
								this.signSettings.removeSignData(block.getLocation());
							else if (block.getState() instanceof InventoryHolder)
								((InventoryHolder)block.getState()).getInventory().clear();

							if (y > this.plotData.getGroundLevel()) block.setType(Material.AIR);
							else if (y == this.plotData.getGroundLevel()) block.setType(Material.GRASS);
							else if (y > (this.plotData.getGroundLevel() - 5)) block.setType(Material.DIRT);
							else block.setType(Material.STONE);
						}
					}
				}
			}

			final Location midPoint = this.plotSettings.getPlotMidPoint(region, 128);
			for (final Entity entity: this.plotWorld.getEntities())
			{
				final Location entityLocation = entity.getLocation();
				if (entityLocation.distance(midPoint) <= 128 &&
						region.contains(
								entityLocation.getBlockX(),
								entityLocation.getBlockY(),
								entityLocation.getBlockZ()))
				{
					if (entity.getType() == EntityType.PLAYER) continue;
					if (confirm) entity.remove();
				}
			}

			if (this.confirm)
			{
				this.plotSettings.doPlotReset(this.plotData);
				super.info("Reset of $1 was confirmed.", this.plotData);
			}
			else super.info("Reset of $1 was NOT confirmed.", this.plotData);
		}
	}
}
