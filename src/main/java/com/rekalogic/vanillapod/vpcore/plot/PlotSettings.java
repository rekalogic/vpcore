package com.rekalogic.vanillapod.vpcore.plot;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.SkullType;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.player.EconomyManager;
import com.rekalogic.vanillapod.vpcore.sign.SignData;
import com.rekalogic.vanillapod.vpcore.sign.SignSettings;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public final class PlotSettings extends SettingsBase
{
	private final PlotDbInterface dbInterface;
	private final World plotWorld;
	
	private SignSettings signSettings = null;

	private double basePrice;
	private double buyPrice;
	private double sellPrice;

	private Map<String, PlotData> allPlotData = null;
	
	public void setSignSettings(final SignSettings settings) { this.signSettings = settings; }

	public RegionManager getRegionManager()
	{
		if (super.getPlugin().isWorldGuardEnabled())
			return super.getPlugin().getWorldGuard().getRegionManager(this.plotWorld);
		else return null;
	}

	public PlotSettings(final VPPlugin plugin)
	{
		super(plugin);

		this.dbInterface = new PlotDbInterface(plugin);
		this.plotWorld = Bukkit.getWorlds().get(0);
	}

	@Override
	protected void onLoad()
	{
		this.basePrice = super.getDouble("base-price");
		this.buyPrice = super.getDouble("buy-price");
		this.sellPrice = super.getDouble("sell-price");
	}

	@Override
	protected void onSave()
	{
		super.set("base-price", this.basePrice);
		super.set("buy-price", this.buyPrice);
		super.set("sell-price", this.sellPrice);
	}

	@Override
	protected void onEmitInfo()
	{
		super.info(
				"Prices: BASE=$$1, BUY=$$2, SELL=$$3",
				Helpers.Money.formatString(this.basePrice),
				Helpers.Money.formatString(this.buyPrice),
				Helpers.Money.formatString(this.sellPrice));
	}

	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading plot data...");
		this.allPlotData = this.dbInterface.loadPlotData();
		
		super.onLoadDataStore();
	}

	public void createPlotData(final String name)
	{
		this.createPlotData(name, false, 1, this.basePrice, 0);
	}

	private void createPlotData(final String name, final boolean isPremium, final double markup, final double buyPrice, final double sellPrice)
	{
		final PlotData plotData = new PlotData(
				name,
				isPremium,
				markup,
				buyPrice,
				sellPrice,
				0);
		this.dbInterface.createPlot(plotData);
		this.allPlotData.put(plotData.getName(), plotData);
		this.updatePlotSigns(plotData);
	}

	public void removePlotData(final PlotData plotData)
	{
		this.allPlotData.remove(plotData.getName());
		this.dbInterface.deletePlot(plotData);
		this.signSettings.removePlotSignData(plotData.getName());
		this.updatePlotSigns(plotData);
	}

	public ProtectedRegion getPlotAt(final Location location)
	{
		ProtectedRegion inRegion = null;
		if (super.getPlugin().isWorldGuardEnabled())
		{
			for (final PlotData plot: this.allPlotData.values())
			{
				final ProtectedRegion region = this.getRegionManager().getRegion(plot.getName());
				if (region != null &&
						region.contains(location.getBlockX(), location.getBlockY(), location.getBlockZ()))
				{
					inRegion = region;
					break;
				}
			}
		}
		return inRegion;
	}

	public ProtectedRegion getPlotFor(final OfflinePlayer player)
	{
		ProtectedRegion plot = null;
		if (super.getPlugin().isWorldGuardEnabled())
		{
			final PlotData plotData = this.getPlotDataFor(player);
			if (plotData != null) plot = this.getRegionManager().getRegion(plotData.getName());
		}		
		return plot;
	}
	
	public PlotData getPlotData(final String plotName)
	{
		PlotData plotData = null;
		if (super.getPlugin().isWorldGuardEnabled())
		{
			final ProtectedRegion plot = this.getRegionManager().getRegion(plotName);
			if (plot != null) plotData = this.getPlotData(plot);
		}
		return plotData;
	}

	public PlotData getPlotData(final ProtectedRegion plot)
	{
		PlotData plotData = null;
		if (plot != null &&
				this.allPlotData.containsKey(plot.getId()))
			plotData = this.allPlotData.get(plot.getId());
		return plotData;
	}
	
	public PlotData getPlotDataFor(final OfflinePlayer player)
	{
		PlotData plotData = null;
		for (final PlotData testPlotData: this.allPlotData.values())
		{
			if (testPlotData.getOwnerUuid() != null &&
					testPlotData.getOwnerUuid().equals(player.getUniqueId()))
			{
				plotData = testPlotData;
				break;
			}
		}
		return plotData;
	}	
	
	public void doPlotReset(final PlotData plotData)
	{
		final UUID lastOwner = plotData.getOwnerUuid();
		plotData.setOwnerUuid(null);
		this.dbInterface.updatePlotOwner(plotData);
		if (lastOwner != null) this.dbInterface.recordPlotReset(plotData, lastOwner);
		this.setRegionOwnership(plotData);
	}

	public void doPlotPurchase(final Player player, final PlotData plotData)
	{
		if (super.getPlugin().isVaultEnabled())
		{
			final EconomyManager economyManager = super.getPlugin().getEconomyManager();
			if (economyManager.has(player, plotData.getBuyPrice()))
			{
				plotData.setOwnerUuid(player.getUniqueId());
				this.dbInterface.recordPlotPurchase(plotData);
				this.dbInterface.updatePlotOwner(plotData);
				this.setRegionOwnership(plotData);
				economyManager.debit(player, plotData.getBuyPrice());
				Helpers.Messages.sendSuccess(
						player,
						"You purchased plot $1 for $$2.",
						plotData,
						Helpers.Money.formatString(plotData.getBuyPrice()));
				super.info(
						"$1 has purchased plot $2 for $$3.",
						player,
						plotData,
						Helpers.Money.formatString(plotData.getBuyPrice()));
			}
			else Helpers.Messages.sendFailure(player, "You do not have enough money to buy this plot.");
		}
	}

	public void doPlotSale(final Player player, final PlotData plotData)
	{
		if (super.getPlugin().isVaultEnabled())
		{
			final EconomyManager economyManager = super.getPlugin().getEconomyManager();
			if (economyManager.canReceive(player, plotData.getSellPrice()))
			{
				this.dbInterface.recordPlotSale(plotData);
				economyManager.credit(player, plotData.getSellPrice());
				this.resetPlot(plotData, true);
				Helpers.Messages.sendSuccess(
						player,
						"You sold plot $1 for $$2.",
						plotData,
						Helpers.Money.formatString(plotData.getSellPrice()));
				super.info(
						"$1 has sold plot $2 for $$3.",
						player,
						plotData,
						Helpers.Money.formatString(plotData.getSellPrice()));
			}
			else Helpers.Messages.sendFailure(player, "Your balance is too close to its limit.");
		}
	}

	public Location getPlotMidPoint(final ProtectedRegion plot, final int referenceY)
	{
		final BlockVector minVector = plot.getMinimumPoint();
		final BlockVector maxVector = plot.getMaximumPoint();
		final int midX = (maxVector.getBlockX() + minVector.getBlockX()) / 2;
		final int midZ = (maxVector.getBlockZ() + minVector.getBlockZ()) / 2;

		return new Location(this.plotWorld, midX, referenceY, midZ);
	}

	public void updatePlotSigns(final PlotData plotData)
	{
		final Collection<SignData> signs = this.signSettings.getPlotSigns(plotData.getName());
		for (final SignData signData: signs)
		{
			final Block block = this.plotWorld.getBlockAt(
					signData.getX(),
					signData.getY(),
					signData.getZ());
			if (this.signSettings.isControlledSign(block))
			{
				final Sign signState = (Sign)block.getState();
				this.signSettings.updatePlotSignText(signState, signData, plotData);
				this.updatePlotHead(signState, plotData);
			}
		}
	}
	
	public void updatePlotHead(final Sign signState, final PlotData plotData)
	{
		final org.bukkit.material.Sign signMaterialData = (org.bukkit.material.Sign)signState.getData();
		final Block hedgeBlock = signState.getBlock().getRelative(signMaterialData.getAttachedFace());
		
		switch (hedgeBlock.getType())
		{
		case LEAVES:
		case LEAVES_2:
			break;
			
		default: return;
		}
		
		final Block headBlock = hedgeBlock.getRelative(BlockFace.UP);
		switch (headBlock.getType())
		{
		case AIR:
		case SKULL:
			break;
			
		default: return;
		}
		
		if (plotData.getOwnerUuid() == null) headBlock.setType(Material.AIR);
		else
		{
			headBlock.setType(Material.SKULL);
			final Skull skullState = (Skull)headBlock.getState();
			skullState.setSkullType(SkullType.PLAYER);
			final OfflinePlayer player = Bukkit.getOfflinePlayer(plotData.getOwnerUuid());
			skullState.setOwner(player.getName());
			skullState.setRotation(signMaterialData.getFacing());
			final org.bukkit.material.Skull skullMaterialData = (org.bukkit.material.Skull)skullState.getData();
			skullMaterialData.setFacingDirection(BlockFace.UP);
			skullState.update();
			
			signState.setLine(3, player.getName());
			signState.update();
		}
	}

	public PlotData searchForAssociatedPlotData(final Block block)
	{
		PlotData plotData = null;
		final String plotName = this.searchForAssociatedPlotName(block);
		if (plotName != null) plotData = this.getPlotData(plotName);
		return plotData;
	}

	public String searchForAssociatedPlotName(final Block block)
	{
		String plotName = null;
		final PlotData plotTemplate = this.searchForAssociatedPlotTemplate((Sign)block.getState());
		if (plotTemplate != null) plotName = plotTemplate.getName();
		return plotName;
	}

	private PlotData searchForAssociatedPlotTemplate(final Sign signState)
	{
		final org.bukkit.material.Sign signMaterialData = (org.bukkit.material.Sign)signState.getData();
		final BlockFace plotDirection = signMaterialData.getAttachedFace();
		
		Block testBlock = signState.getBlock();
		PlotData plotTemplate = null;
		for (int i = 0; i < 20; ++i)
		{
			testBlock = testBlock.getRelative(plotDirection);
			final ProtectedRegion plotForSign = this.getPlotAt(testBlock.getLocation());

			if (plotForSign != null)
			{
				int groundLevel = 0;
				Block hedgeBlock = testBlock.getRelative(plotDirection.getOppositeFace());
				for (int j = 0; j < 10; ++j)
				{
					if (hedgeBlock.getType() == Material.LEAVES ||
							hedgeBlock.getType() == Material.LEAVES_2)
					{
						groundLevel = hedgeBlock.getY() - 1;
						break;
					}
					else hedgeBlock = hedgeBlock.getRelative(BlockFace.UP);
				}

				plotTemplate = new PlotData(
						plotForSign.getId(),
						false,
						1,
						this.basePrice,
						0,
						groundLevel);
				break;
			}
		}

		return plotTemplate;
	}

	public PlotData[] getAllPlotData()
	{
		return this.allPlotData.values().toArray(new PlotData[0]);
	}

	public ProtectedRegion getContainingPlot(final Location location)
	{
		ProtectedRegion plot = null;
		if (super.getPlugin().isWorldGuardEnabled())
		{
			for (final ProtectedRegion region: this.getRegionManager().getApplicableRegions(location))
			{
				if (region.getId().toLowerCase().startsWith("ok-plot"))
				{
					plot = region;
					break;
				}
			}
		}
		return plot;
	}

	public void setRegionOwnership(final PlotData plotData)
	{
		final TaskBase task = new TaskBase(super.isDebug(), "SetRegionOwnership")
		{
			@Override
			protected void runTask()
			{
				final RegionManager regionManager = getRegionManager();
				if (regionManager == null) return;
				
				final ProtectedRegion region = regionManager.getRegion(plotData.getName());
				final DefaultDomain owners = region.getOwners();
				owners.clear();
				region.getMembers().clear();
				region.getFlags().clear();

				final UUID ownerId = plotData.getOwnerUuid();
				if (ownerId != null) region.getOwners().addPlayer(ownerId);

				updatePlotSigns(plotData);
			}
		};
		task.runTask(this.getPlugin());
	}

	public void updatePlotGroundLevel(final PlotData plotData, final int groundLevel)
	{
		plotData.setGroundLevel(groundLevel);
		this.dbInterface.updatePlotGroundLevel(plotData);
	}
	
	public void resetPlot(final PlotData plotData, final boolean confirm)
	{
		final PlotResetTask task = new PlotResetTask(super.getPlugin(), this, this.signSettings, plotData, this.plotWorld, confirm);
		task.runTask(this.getPlugin());
	}
}
