package com.rekalogic.vanillapod.vpcore.plot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.rekalogic.vanillapod.vpcore.ban.BanInfo;
import com.rekalogic.vanillapod.vpcore.ban.BanSettings;
import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public final class PlotExecutor extends ExecutorBase<PlotSettings>
{
	private final Map<UUID, Long> lastSellAttempts = new ConcurrentHashMap<UUID, Long>();
	
	private final BanSettings banSettings;

	public PlotExecutor(final PluginBase plugin, final PlotSettings settings, final BanSettings banSettings)
	{
		super(plugin, settings, "plot", "plotadmin", "plotreport", "plotreset");
		
		this.banSettings = banSettings;
	}

	@Override
	protected boolean checkCommandAllowed(final String command, final boolean isConsole)
	{
		switch (command)
		{
		case "plot":
		case "plotadmin":
			return (!isConsole);
		}
		
		return super.checkCommandAllowed(command, isConsole);
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "plot": return (argCount == 1);
		case "plotadmin": return (argCount == 1);
		case "plotreset": return (argCount <= 1);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "plot": return new String[] { ExecutorBase.FLAG_PLAYER };
		case "plotreset": return new String[] { ExecutorBase.FLAG_PLAYER, ExecutorBase.FLAG_BOOL_CONFIRM };
		case "plotreport": return new String[] { ExecutorBase.FLAG_BOOL_VERBOSE };
		}
		
		return super.getAllowedFlags(command);
	}
	
	@Override
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "plot":
			switch (argIndex)
			{
			case 0: return Arrays.asList("buy", "confirm", "find", "sell");
			}
			break;
			
		case "plotadmin":
			switch (argIndex)
			{
			case 0: return Arrays.asList("add", "remove", "setgroundlevel");
			}
			break;
		}
		
		return super.onTabComplete(sender, command, argIndex, args);
	}
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "plot":
		case "plotadmin":
			switch (argIndex)
			{
			case 0: return LookupSource.FROM_PLUGIN;				
			}
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		if (!super.getPlugin().isWorldGuardEnabled())
		{
			sender.sendFailure("WorldGuard is not available.");
			return true;
		}

		switch (command)
		{
		case "plot": return this.doPlot(sender, args);
		case "plotadmin": return this.doPlotAdmin(sender, args);
		case "plotreport": return this.doPlotReport(sender, args);
		case "plotreset": return this.doPlotReset(sender, args);
		}
		
		return super.onCommand(sender, command, label, args);		
	}

	private boolean doPlotAdmin(final PagedOutputCommandSender sender, final String[] args)
	{
		final Player player = sender.getPlayer();
		String subCommand = args[0].toLowerCase();
		
		if (subCommand.equals("sgl")) subCommand = "setgroundlevel"; 
		
		ProtectedRegion plot = null;
		PlotData plotData = null;
		switch (subCommand)
		{
		case "add":
		case "remove":
		case "setgroundlevel":
			plot = super.getSettings().getContainingPlot(player.getLocation());
			if (plot == null)
			{
				sender.sendFailure("You must be standing in an $1 region when using this command.", "ok-plotXXX");
				return true;
			}
			else plotData = super.getSettings().getPlotData(plot);
			break;
			
		default: return false;
		}

		switch (subCommand)
		{
		case "add":
			if (plotData != null)
			{
				sender.sendFailure("Plot $1 is already a managed plot.", plotData);
				return true;
			}
			break;
		case "remove":
			if (plotData == null)
			{
				sender.sendFailure("Plot $1 is not a managed plot.", plot);
				return true;
			}
			break;
		case "setgroundlevel":
			if (plotData == null)
			{
				sender.sendFailure("Plot $1 is not a managed plot.", plot);
				return true;
			}
			break;
		}

		switch (subCommand)
		{
		case "add":
			super.getSettings().createPlotData(plot.getId());
			sender.sendSuccess("Plot $1 is now a managed plot.", plot);
			break;
		case "remove":
			super.getSettings().removePlotData(plotData);
			sender.sendSuccess("Plot $1 is no longer a managed plot.", plotData);
			break;
		case "setgroundlevel":
			super.getSettings().updatePlotGroundLevel(plotData, player.getLocation().getBlockY() - 1);
			sender.sendSuccess("Updated $1 ground level to your feet.", plotData);
			break;
		}

		return true;
	}

	private boolean doPlotReset(final PagedOutputCommandSender sender, final String[] args)
	{
		PlotData plotData = null;
		
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER) && args.length == 1) return false;
		
		if (args.length == 1) plotData = super.getSettings().getPlotData(args[0]);
		else
		{
			OfflinePlayer player = super.getOfflinePlayer(sender);
			if (player == null) return true;
			plotData = super.getSettings().getPlotDataFor(player);
		}
		
		if (plotData == null)
		{
			if (args.length == 1) sender.sendFailure("Plot '$1' not found.", args[0]);
			else sender.sendFailure("Plot not found for $1.", super.getOfflinePlayer(sender));
		}
		else if (plotData.getGroundLevel() == 0) sender.sendError("Plot $1 has no ground level set!", plotData);
		else
		{
			final boolean confirm = super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM);
			super.getSettings().resetPlot(plotData, confirm);
			if (confirm) sender.sendSuccess("Plot $1 was successfully reset.", plotData);
		}

		return true;
	}

	private boolean doPlotReport(final PagedOutputCommandSender sender, final String[] args)
	{
		final PlotData[] data = super.getSettings().getAllPlotData();
		final boolean verbose = super.getFlagState(ExecutorBase.FLAG_BOOL_VERBOSE);
		
		final List<PlotData> reportPlots = new ArrayList<PlotData>();
		int ownedCount = 0;
		for (final PlotData plotData: data)
		{
			final boolean isOwned = plotData.isOwned();
			if (verbose || isOwned) reportPlots.add(plotData);
			if (isOwned) ++ownedCount;			
		}
		
		reportPlots.sort(new Comparator<PlotData>()
		{
			@Override
			public int compare(final PlotData o1, final PlotData o2)
			{
				final String o1Number = o1.getName().substring(7);
				final String o2Number = o2.getName().substring(7);
				return Integer.compare(Integer.parseInt(o1Number), Integer.parseInt(o2Number));
			}
		});
		
		Helpers.Messages.sendInfo(sender, "There are $1 plots ($2 owned, $3 empty).", data.length, ownedCount, data.length - ownedCount);
		
		for (int i = 0; i < reportPlots.size(); ++i)
		{
			final PlotData plotData = reportPlots.get(i);
			if (plotData.isOwned())
			{
				final UUID ownerUuid = plotData.getOwnerUuid();
				final OfflinePlayer owner = Helpers.Bukkit.getOfflinePlayer(ownerUuid);
				String ownerName = ownerUuid.toString();
				if (owner != null && owner.hasPlayedBefore())
				{
					ownerName = owner.getName();
					
					final BanInfo banInfo = this.banSettings.getBanInfo(owner);
					if (banInfo != null &&
							banInfo.isActive() &&
							banInfo.isPermanent())
						ownerName = ChatColor.RED + ownerName;
				}
				sender.sendInfoListItem(i, "$1: $2", plotData.getName(), ownerName);
			}
			else sender.sendInfoListItem(i, "$1 has no owner.", plotData.getName());
		}

		return true;
	}

	private boolean doPlot(final PagedOutputCommandSender sender, final String[] args)
	{
		final Player player = sender.getPlayer();
		final OfflinePlayer targetPlayer = super.getOfflinePlayer(sender);
		final String subCommand = args[0].toLowerCase();
		ProtectedRegion plot = null;

		switch (subCommand)
		{		
		case "buy":
		case "confirm":
		case "sell":			
			if (super.hasFlag(ExecutorBase.FLAG_PLAYER)) return false;
			plot = super.getSettings().getPlotAt(player.getLocation());
			break;
		case "find":
			if (super.hasFlag(ExecutorBase.FLAG_PLAYER) &&
					!sender.hasPermission("vpcore.moderator")) return false;
			plot = super.getSettings().getPlotFor(targetPlayer);
			if (plot == null)
			{
				if (player.equals(targetPlayer)) sender.sendFailure("You do not own a plot in Orkida.");
				else sender.sendFailure("$1 does not own a plot in Orkida.", targetPlayer);
				return true;
			}
			break;
		}

		final UUID uuid = player.getUniqueId();
		final PlotData plotData = super.getSettings().getPlotData(plot);

		switch (subCommand)
		{
		case "buy":
			if (super.getSettings().getPlotFor(player) != null)
			{
				if (plotData != null && targetPlayer.equals(plotData.getOwnerUuid()))
					sender.sendFailure("You already own this plot.");
				else sender.sendFailure("You already own a plot.");
			}
			else if (plot == null) sender.sendFailure("You must be standing in a plot when using this command.");
			else if (plotData == null) sender.sendError("No plot data exists for $1!", plot.getId());
			else if (plotData.getOwnerUuid() == null) super.getSettings().doPlotPurchase(player, plotData);
			else sender.sendFailure("This plot is owned by another player.");
			break;
		case "confirm":
			final long lastSellAttempt = this.lastSellAttempts.get(uuid);
			final long latestValidTime = System.currentTimeMillis() - (30 * Helpers.DateTime.MillisecondsInASecond);
			if (lastSellAttempt == 0) sender.sendFailure("You have no $1 command to confirm.", "/plot");
			else if (plotData != null && uuid.equals(plotData.getOwnerUuid()))
					sender.sendFailure("Leave your plot before confirming the $1 command.", "/plot sell");
			else if (lastSellAttempt < latestValidTime) sender.sendFailure("You waited too long.  Please return to your plot and try again.");
			else
			{
				final PlotData plotSaleData = super.getSettings().getPlotDataFor(player);
				super.getSettings().doPlotSale(player, plotSaleData);
			}
			break;
		case "sell":
			if (super.getSettings().getPlotFor(player) == null) sender.sendFailure("You do not own a plot in Orkida.");
			else if (plot == null) sender.sendFailure("You must be standing in your plot when using this command.");
			else if (plotData == null) sender.sendError("No plot data exists for $1!", plot.getId());
			else if (uuid.equals(plotData.getOwnerUuid()))
			{
				sender.sendSuccess("Now leave the plot, and issue $1 to confirm the sale.", "/plot confirm");
				this.lastSellAttempts.put(uuid, System.currentTimeMillis());
			}
			else sender.sendFailure("You must be standing in your own plot when using this command.");
			break;
		case "find":
		{
			final Location playerLocation = player.getLocation();
			final Location plotLocation = super.getSettings().getPlotMidPoint(plot, playerLocation.getBlockY());
			final Vector delta = plotLocation.toVector().subtract(playerLocation.toVector());

			String format;
			if (player.equals(targetPlayer)) format = "Your Orkida plot is $1 ($2m $3)";
			else format = "Orkida plot for $4 is $1 ($2m $3)";

			sender.sendNotification(
					format,
					plot.getId(),
					(int)plotLocation.distance(playerLocation),
					Helpers.getCompassDirection(
							Math.toDegrees(
									Math.atan2(delta.getX(), delta.getZ()))),
									targetPlayer);
			
			if (player.getItemInHand() != null &&
					player.getItemInHand().getType() == Material.COMPASS)
			{
				player.setCompassTarget(plotLocation);
				sender.sendNotification("Your compass has been set to $1.", plot);
			}
			else player.setCompassTarget(player.getWorld().getSpawnLocation());
		} break;
		default: return false;
		}

		return true;
	}

}