package com.rekalogic.vanillapod.vpcore.plot;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;

public final class PlotEventHandler extends EventHandlerBase<PlotSettings>
{
	public PlotEventHandler(final PluginBase plugin, final PlotSettings settings)
	{
		super(plugin, settings);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	private void onPlayerJoin(final PlayerJoinEvent event)
	{
		final Player player = event.getPlayer();
		final PlotData plotData = super.getSettings().getPlotDataFor(player);
		if (plotData != null)
		{
			super.debug("Updating plot signs for $1 ($2).", player, plotData.getName());
			super.getSettings().updatePlotSigns(plotData);
		}
	}
}
