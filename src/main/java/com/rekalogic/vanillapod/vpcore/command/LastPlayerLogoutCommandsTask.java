package com.rekalogic.vanillapod.vpcore.command;

import org.bukkit.Bukkit;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class LastPlayerLogoutCommandsTask extends TaskBase
{
	private final String[] commands;
	private final VPPlugin plugin;
	private final CommandSettings settings;
	private final long logIns;

	public LastPlayerLogoutCommandsTask(final LastPlayerLogoutCommandsTask instance, final String[] commands, final long logIns)
	{
		this(instance.plugin, instance.settings, commands, logIns);
	}
	
	public LastPlayerLogoutCommandsTask(final VPPlugin plugin, final CommandSettings settings, final String[] commands)
	{
		this(plugin, settings, commands, settings.getLogIns());
	}

	private LastPlayerLogoutCommandsTask(final VPPlugin plugin, final CommandSettings settings, final String[] commands, final long logIns)
	{
		super(settings);
		this.plugin = plugin;
		this.settings = settings;
		this.commands = commands;

		this.logIns = logIns;
	}

	@Override
	public void runTask()
	{
		if (this.settings.getLogIns() > this.logIns)
		{
			super.debug("Player logins occurred since logout command task started; aborting.");
			return;
		}

		for (int i = 0; i < this.commands.length; ++i)
		{
			long delay = 0;
			boolean executeCommand = false;
			String command = this.commands[i];
			if (command.startsWith("WAIT "))
			{
				delay = Helpers.DateTime.getMillis(command.substring(5));
				if (delay == -1) super.severe("Instruction $1 invalid; not waiting.", command);
			}
			else if (command.equals("FLUSH"))
			{
				this.plugin.flushCaches();
			}
			else if (command.startsWith("COMMAND "))
			{
				command = command.substring(8);
				executeCommand = true;
			}
			else
			{
				super.severe("Instruction $1 invalid; ignoring.", command);
				continue;
			}

			if (executeCommand) Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
			else if (delay > 0)
			{
				final String[] delayedCommands = new String[this.commands.length - (i + 1)];
				for (int j = 0; j < delayedCommands.length; ++j) delayedCommands[j] = this.commands[j + i + 1];
				final LastPlayerLogoutCommandsTask task = new LastPlayerLogoutCommandsTask(this, delayedCommands, this.logIns);
				super.info(
						Helpers.Parameters.replace(
								"Waiting $1 to continue executing logout commands.",
								Helpers.DateTime.getHumanReadableFromMillis(delay)));
				task.runTaskLater(this.plugin, Helpers.DateTime.getTicksFromMillis(delay));
				break;
			}
		}
	}
}
