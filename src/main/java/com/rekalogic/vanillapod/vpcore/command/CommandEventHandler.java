package com.rekalogic.vanillapod.vpcore.command;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.server.PluginEnableEvent;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;

public final class CommandEventHandler extends EventHandlerBase<CommandSettings>
{
	private final VPPlugin vpplugin;

	public CommandEventHandler(final VPPlugin plugin, final CommandSettings settings)
	{
		super(plugin, settings);

		this.vpplugin = plugin;
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerQuit(final PlayerQuitEvent event)
	{
		if (Bukkit.getOnlinePlayers().size() <= 1 && super.getSettings().getLastPlayerLogoutCommands().length > 0)
		{
			final LastPlayerLogoutCommandsTask task = new LastPlayerLogoutCommandsTask(
					this.vpplugin,
					super.getSettings(),
					super.getSettings().getLastPlayerLogoutCommands());
			task.runTask(super.getPlugin());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerJoin(final PlayerJoinEvent event)
	{
		final Player player = event.getPlayer();

		if (super.getSettings().hasStoredSpeed(player)) super.getSettings().restoreSpeed(player, true);

		super.getSettings().recordLogIn();
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPluginEnable(final PluginEnableEvent event)
	{
		super.getSettings().modifyPlugin(event.getPlugin());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerChangedWorld(final PlayerChangedWorldEvent event)
	{
		final Player player = event.getPlayer();

		if (super.getSettings().hasStoredSpeed(player)) super.getSettings().restoreSpeed(player);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerRespawn(final PlayerRespawnEvent event)
	{
		final Player player = event.getPlayer();

		if (super.getSettings().hasStoredSpeed(player)) super.getSettings().restoreSpeed(player);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void onInventoryClick(InventoryClickEvent event)
	{
		if (event.isCancelled()) return;
		
		if (event.getInventory().getTitle().startsWith("Recipe for"))
			event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void onInventoryInteract(InventoryInteractEvent event)
	{
		if (event.isCancelled()) return;
		
		if (event.getInventory().getTitle().startsWith("Recipe for"))
			event.setCancelled(true);
	}	
}
