package com.rekalogic.vanillapod.vpcore.command;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.player.PermissionManager;
import com.rekalogic.vanillapod.vpcore.player.Group;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class CommandSettings extends SettingsBase
{
	private final CommandDbInterface dbInterface;

	private long logIns = 0;
	private String[] lastPlayerLogoutCommands = null;
	private final Map<String, Float> speedMap = new ConcurrentHashMap<String, Float>();
	private String permissionMessage = "";
	
	public long getLogIns() { return this.logIns; }
	public void recordLogIn() { ++this.logIns; }
	public String[] getLastPlayerLogoutCommands() { return this.lastPlayerLogoutCommands; }

	public CommandSettings(final VPPlugin plugin)
	{
		super(plugin);

		this.dbInterface = new CommandDbInterface(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.lastPlayerLogoutCommands = super.getStringList("last-player-logout.commands").toArray(new String[0]);
		this.permissionMessage = super.getString("permission-message");

		for (final Plugin existingPlugin: Bukkit.getPluginManager().getPlugins())
			this.modifyPlugin(existingPlugin);
	}

	@Override
	protected void onSave()
	{
		super.set("last-player-logout.commands", this.lastPlayerLogoutCommands);
		super.set("permission-message", this.permissionMessage);
	}

	@Override
	protected void onEmitInfo()
	{
		super.info("Orkida plot manipulation will be restricted.");
		if (this.lastPlayerLogoutCommands.length > 0)
		{
			super.info("Executed on logout of last player: ");
			for (final String command: this.lastPlayerLogoutCommands) super.info(Helpers.Parameters.replace("    $1", command));
		}

		super.info("Permission message: $1", this.permissionMessage);
	}

	public void saveSpeed(final Player player, final float speed)
	{
		this.speedMap.put(player.getName(), speed);
		super.debug("Speed $1 stored for player $2.", speed, player);
	}

	public boolean hasStoredSpeed(final Player player)
	{
		return this.speedMap.containsKey(player.getName());
	}

	public void restoreSpeed(final Player player) { this.restoreSpeed(player, false); }
	public void restoreSpeed(final Player player, final boolean defer)
	{
		if (!player.isOnline()) return;
		if (defer)
		{
			final TaskBase task = new TaskBase(super.isDebug(), "RestoreSpeed")
			{
				@Override
				public void runTask() { restoreSpeed(player); }
			};
			task.runTaskLater(super.getPlugin(), 50);
		}
		else
		{
			final String name = player.getName();
			if (this.speedMap.containsKey(name))
			{
				final float speed = this.speedMap.get(name);
				final float originalSpeed = player.getWalkSpeed();
				player.setWalkSpeed(speed);
				player.setFlySpeed(speed - 0.1F);
				super.debug("Speed $1 restored for player $2.", speed, player);
				if (speed != originalSpeed) Helpers.Messages.sendNotification(player, "Your speed was restored.");
			}
		}
	}

	public List<OfflinePlayer> getPlayersInGroup(final Group group)
	{
		return this.getPlayersInGroup(group.getName());
	}
	
	public List<OfflinePlayer> getPlayersInGroup(final String groupName)
	{
		final List<OfflinePlayer> playersInGroup = new ArrayList<OfflinePlayer>();
		final PermissionManager permissionManager = super.getPlugin().getPermissionManager();
		if (permissionManager != null)
		{
			final List<UUID> playerUuids = new ArrayList<UUID>();
			if (groupName.equalsIgnoreCase("guest")) playerUuids.addAll(this.dbInterface.getUuidsForGroupless());
			else playerUuids.addAll(this.dbInterface.getUuidsForGroup(groupName));
			for (final UUID uuid: playerUuids)
			{
				final OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
				if (player.hasPlayedBefore()) playersInGroup.add(player);
			}
		}
		return playersInGroup;
	}

	public void modifyPlugin(final Plugin plugin)
	{
		final Map<String, Map<String, Object>> commands = plugin.getDescription().getCommands();
		if (commands == null) return;
		for (final String name: commands.keySet())
		{
			final PluginCommand command = Bukkit.getPluginCommand(name);
			if (command != null)
				command.setPermissionMessage(Helpers.formatMessage(this.permissionMessage));
		}
	}
	
	public void deletePlayerData(final OfflinePlayer player)
	{
		for (final World world: Bukkit.getWorlds())
		{
			final File playerDat = new File(
					Helpers.Parameters.replace(
							"$1/playerdata/$2.dat",
							world.getWorldFolder().getAbsolutePath(),
							player.getUniqueId()));

			if (playerDat.exists()) playerDat.delete();
		}
		
		this.dbInterface.deletePlayerData(player.getUniqueId());
	}
	
	public void clearLogoutCommands()
	{
		this.lastPlayerLogoutCommands = new String[0];
	}
	
	public String getForumUsername(final OfflinePlayer player)
	{
		return this.dbInterface.getForumUserName(player.getUniqueId());
	}
}
