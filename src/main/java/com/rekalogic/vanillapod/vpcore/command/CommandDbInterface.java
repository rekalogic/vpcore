package com.rekalogic.vanillapod.vpcore.command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class CommandDbInterface extends DatabaseInterfaceBase
{
	public CommandDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}

	public List<UUID> getUuidsForGroup(final String groupName)
	{
		final List<UUID> uuidsForGroup = new ArrayList<UUID>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT p.uuid " +
				"FROM vp_player p, mcforum.phpbb_users u, vp_group g " +
				"WHERE p.phpbb_userid = u.user_id AND u.group_id = g.phpbb_groupid AND lower(g.groupname) = ?",
				groupName.toLowerCase());
		while (result.moveNext())
			uuidsForGroup.add(result.getUuid("uuid"));
		connection.close();
		return uuidsForGroup;
	}
	
	public List<UUID> getUuidsForGroupless()
	{
		final List<UUID> uuidsForGroup = new ArrayList<UUID>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT p.uuid FROM vp_player p WHERE p.phpbb_userid IS NULL");
		while (result.moveNext())
			uuidsForGroup.add(result.getUuid("uuid"));
		connection.close();
		return uuidsForGroup;
	}

	public void deletePlayerData(final UUID uuid)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update("vp_plot", "uuid = null", "uuid = ?", uuid);
		connection.deleteFrom("vp_online", "uuid = ?", uuid);
		connection.deleteFrom("vp_player", "uuid = ?", uuid);
		connection.deleteFrom("vp_absence_info", "uuid = ?", uuid);		
		connection.deleteFrom("vp_location", "id IN (SELECT locationid FROM vp_home WHERE uuid = ?)", uuid);
		connection.deleteFrom("vp_home", "uuid = ?", uuid);
		connection.deleteFrom("vp_sign", "isserverowned = 0 AND uuid = ?", uuid);
		connection.close();
	}

	public String getForumUserName(final UUID uuid)
	{
		String userName = "";
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"select username from mcforum.phpbb_users u, vp_player p where u.user_id = p.phpbb_userid and p.uuid = ?",
				uuid);
		if (result.moveNext())
			userName = result.getString("username");
		return userName;
	}
}
