package com.rekalogic.vanillapod.vpcore.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Villager;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.util.Vector;

import com.rekalogic.vanillapod.vpcore.ban.BanInfo;
import com.rekalogic.vanillapod.vpcore.ban.BanSettings;
import com.rekalogic.vanillapod.vpcore.chunk.ChunkSettings;
import com.rekalogic.vanillapod.vpcore.npc.NpcSettings;
import com.rekalogic.vanillapod.vpcore.player.Group;
import com.rekalogic.vanillapod.vpcore.player.PlayerSettings;
import com.rekalogic.vanillapod.vpcore.plot.PlotSettings;
import com.rekalogic.vanillapod.vpcore.serverlist.ServerListSettings;
import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public final class CommandExecutor extends ExecutorBase<CommandSettings>
{
	private final ServerListSettings serverListSettings;
	private final ChunkSettings chunkSettings;
	private final PlayerSettings playerSettings;
	private final NpcSettings villagerSettings;
	private final BanSettings banSettings;
	private final PlotSettings plotSettings;
	private final Random random;

	private static final DecimalFormat CurrencyFormat = new DecimalFormat("#0.00", DecimalFormatSymbols.getInstance(Locale.UK));

	public CommandExecutor(
			final PluginBase plugin,
			final CommandSettings settings,
			final ServerListSettings serverListSettings,
			final ChunkSettings chunkSettings,
			final PlayerSettings playerSettings,
			final NpcSettings villagerSettings,
			final BanSettings banSettings,
			final PlotSettings plotSettings)
	{
		super(plugin, settings,

				//  Admin stuff
				"cleargoto", "cull", "disconnect",
				"dropanvil", "fixup", "goto",
				"listplayers", "playerinfo", "playerpurge",
				"setspeed", "spawnmobat", "vpinfo",
				"vpreload", "whowhere",

				//  World stuff
				"eject", "world",

				//  Gameplay stuff
				"clearinventory", "heal", "time",

				//  Item stuff
				"enchant", "give", "name", "recipe", "repair", "unenchant",

				//  Misc
				"echo", "page", "performance"

				);

		this.serverListSettings = serverListSettings;
		this.chunkSettings = chunkSettings;
		this.playerSettings = playerSettings;
		this.villagerSettings = villagerSettings;
		this.banSettings = banSettings;
		this.plotSettings = plotSettings;
		this.random = plugin.getRandom();

		CommandExecutor.CurrencyFormat.setRoundingMode(RoundingMode.HALF_UP);
	}

	@Override
	protected boolean checkCommandAllowed(final String command, final boolean isConsole)
	{
		switch (command)
		{
		case "enchant": return (!isConsole);
		case "goto": return (!isConsole);
		case "north": return (!isConsole);
		case "plot": return (!isConsole);
		case "recipe": return (!isConsole);
		case "repair": return (!isConsole);
		case "unenchant": return (!isConsole);
		case "world": return (!isConsole);
		}
		
		return super.checkCommandAllowed(command, isConsole);
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "clearinventory": return (argCount <= 1);
		case "cull": return (argCount >= 1 && argCount <= 2);
		case "disconnect": return (argCount >= 1);
		case "dropanvil": return (argCount >= 1 && argCount <= 2);
		case "echo": return (argCount >= 1);
		case "eject": return true;
		case "enchant": return (argCount <= 2);
		case "give": return (argCount >= 2 && argCount <= 3);
		case "goto": return (argCount == 1);
		case "heal": return (argCount <= 1);
		case "listplayers": return (argCount <= 1);
		case "name": return (argCount <= 1);
		case "page": return (argCount == 1);
		case "playerinfo": return (argCount <= 1);
		case "playerpurge": return (argCount <= 2);
		case "recipe": return (argCount <= 1);
		case "repair": return (argCount <= 1);
		case "setspeed": return (argCount == 1);
		case "spawnmobat": return (argCount >= 2 && argCount <= 5);
		case "time": return (argCount <= 1);
		case "world": return (argCount == 1);
		}
		
		return super.checkParameters(command, argCount);
	}
	
	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "clearinventory": return new String[] { ExecutorBase.FLAG_BOOL_SILENT };
		case "cull": return new String[] { ExecutorBase.FLAG_BOOL_CONFIRM };
		case "echo": return new String[] { ExecutorBase.FLAG_PLAYER };
		case "eject": return new String[] { ExecutorBase.FLAG_WORLD, ExecutorBase.FLAG_BOOL_CONFIRM };
		case "playerinfo": return new String[] { ExecutorBase.FLAG_BOOL_VERBOSE };
		case "playerpurge": return new String[] { ExecutorBase.FLAG_PLAYER, ExecutorBase.FLAG_GROUP, ExecutorBase.FLAG_ORDERBY, ExecutorBase.FLAG_BOOL_CONFIRM };
		case "plot": return new String[] { ExecutorBase.FLAG_PLAYER };
		case "repair": return new String[] { ExecutorBase.FLAG_PLAYER };
		case "setspeed": return new String[] { ExecutorBase.FLAG_PLAYER };
		case "spawnmobat": return new String[] { ExecutorBase.FLAG_WORLD };
		case "time": return new String[] { ExecutorBase.FLAG_WORLD };
		}
		
		return super.getAllowedFlags(command);
	}

	@Override
	protected boolean shouldCommandPurgePagedOutput(final String command)
	{
		switch (command)
		{
		case "goto": return false;
		case "page": return false;
		}
		return true;
	}
	
	@Override
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "cull": return Arrays.asList("a", "f", "h", "i", "m", "n", "t", "v", "w", "x", "?", "*");
		case "enchant":
		{
			if (sender instanceof Player)
			{
				final Player player = (Player)sender;
				final ItemStack item = player.getItemInHand();
				final List<String> validEnchantments = new ArrayList<String>();
				for (final Enchantment enchantment: Enchantment.values())
					if (enchantment.canEnchantItem(item))
						validEnchantments.add(Helpers.Item.getFriendlyNames(enchantment)[0]);
				
				return validEnchantments;
			}
		} break;
		}
		
		return super.onTabComplete(sender, command, argIndex, args);
	}	
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "enchant": return LookupSource.FROM_PLUGIN;

		case "clearinventory": 
		case "dropanvil": 
		case "heal":
		case "disconnect":
		case "give":
		case "setspeed":
			switch (argIndex)
			{
			case 0: return LookupSource.PLAYER;
			}
			break;
			
		case "cull":
			switch (argIndex)
			{
			case 0: return LookupSource.FROM_PLUGIN;
			}
			break;

		case "listplayers":
			switch (argIndex)
			{
			case 0: return LookupSource.GROUP;
			}
			break;

		case "playerinfo":
			switch (argIndex)
			{
			case 0: return LookupSource.OFFLINE_PLAYER;
			}
			break;
				
		case "spawnmobat":
			switch (argIndex)
			{
			case 1: return LookupSource.PLAYER;
			}
			break;

		case "world":
			switch (argIndex)
			{
			case 0: return LookupSource.WORLD;
			}
			break;
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		switch (command)
		{
		case "clearinventory": return this.doClearInventory(sender, args);
		case "cull": return this.doCull(sender, args);
		case "disconnect": return this.doDisconnect(sender, args);
		case "dropanvil": return this.doDropAnvil(sender, args);
		case "echo": return this.doEcho(sender, args);
		case "eject": return this.doEject(sender, args);
		case "enchant": return this.doEnchant(sender, args);
		case "fixup": return this.doFixup(sender, args);
		case "give": return this.doGive(sender, args);
		case "goto": return this.doGoto(sender, args);
		case "heal": return this.doHeal(sender, args);
		case "listplayers": return this.doListPlayers(sender, args);
		case "name": return this.doName(sender, args);
		case "playerinfo": return this.doPlayerInfo(sender, args);
		case "playerpurge": return this.doPlayerPurge(sender, args);
		case "recipe": return this.doRecipe(sender, args);
		case "repair": return this.doRepair(sender, args);
		case "setspeed": return this.doSetSpeed(sender, args);
		case "spawnmobat": return this.doSpawnMobAt(sender, args);
		case "time": return this.doTime(sender, args);
		case "unenchant": return this.doUnenchant(sender, args);
		case "vpinfo": return this.doInfo(sender, args);
		case "whowhere": return this.doWhoWhere(sender, args);
		case "world": return this.doWorld(sender, args);

		case "cleargoto":
			final int size = super.getPlugin().clearItems();
			if (size == 0) sender.sendFailure("The cache was already empty.");
			else sender.sendSuccess("The cache was cleared of $1 item(s).", size);
			return true;

		case "page":
			if (!sender.canPage()) sender.sendFailure("No paged output available.");
			else sender.setPage(args[0]);
			return true;

		case "performance":
			if (sender.hasPermission("vpcore.moderator")) sender.sendFailure("This command produces dummy output, as follows.");
			sender.sendInfo("$1Ticks per second: $220.0 [100.0%]", ChatColor.YELLOW, ChatColor.GREEN);
			return true;

		case "vpreload":
			sender.sendNotification("Reloading VPCore...");
			final long start = System.currentTimeMillis();
			super.getSettings().reloadAll();
			sender.sendSuccess("Configuration reloaded in $1ms.", System.currentTimeMillis() - start);					
			return true;

		}

		return super.onCommand(sender, command, label, args);
	}

	private boolean doEcho(final PagedOutputCommandSender sender, final String[] args)
	{
		CommandSender target = sender;
		boolean targetIsOtherPlayer = false;
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER))
		{
			final Player player = super.getPlayer(sender);
			if (player == null) return true;
			target = player;
			
			targetIsOtherPlayer = (sender.isConsole() || !player.equals(sender.getPlayer()));
		}
		
		String text = Helpers.Args.concatenate(args);
		if (sender.hasPermission("vpcore.chat.colour"))
			text = Helpers.formatMessage(text);
		Helpers.Messages.sendInfo(target, text);
		
		if (targetIsOtherPlayer)
		{
			sender.sendSuccess("Echoed the following text to $1:", target);
			sender.sendInfo(text);
		}
		
		return true;
	}

	private boolean doRecipe(final PagedOutputCommandSender sender, final String[] args)
	{		
		ItemStack stack = null;
		if (args.length == 0)
		{
			final Player player = sender.getPlayer();
			if (player == null) return true;
			stack = player.getItemInHand();			
		}
		else stack = Helpers.Item.getItemStack(args[0]);

		if (stack == null)
		{
			if (args.length == 0) sender.sendError("Could not get item in hand.");
			else sender.sendFailure("Unknown item $1.", args[0]);
		}
		else if (stack.getType() == Material.AIR) sender.sendFailure("Recipe for air is O2!");
		else
		{
			final String name = Helpers.Item.getFriendlyNames(stack)[0];
			final List<Recipe> recipes = Bukkit.getRecipesFor(stack);
			if (recipes.isEmpty()) sender.sendFailure("No recipe for $1.", name);
			else
			{
				final Recipe recipe = recipes.get(0);
				final List<ItemStack> ingredientList = new ArrayList<ItemStack>();
				
				InventoryType inventoryType = InventoryType.CHEST;
				if (recipe instanceof ShapelessRecipe)
				{
					inventoryType = InventoryType.WORKBENCH;

					final ShapelessRecipe shapeless = (ShapelessRecipe)recipe;
					ingredientList.add(recipe.getResult());
					ingredientList.addAll(shapeless.getIngredientList());
				}
				else if (recipe instanceof ShapedRecipe)
				{
					inventoryType = InventoryType.WORKBENCH;

					final ShapedRecipe shaped = (ShapedRecipe)recipe;
					final Map<Character, ItemStack> ingredients = shaped.getIngredientMap();

					ingredientList.add(recipe.getResult());
					for (final String line: shaped.getShape())
					{
						for (int i = 0; i < 3; ++i)
						{
							if (line.length() > i) ingredientList.add(ingredients.get(line.charAt(i)));
							else ingredientList.add(null);
						}
					}
				}
				else if (recipe instanceof FurnaceRecipe)
				{
					inventoryType = InventoryType.FURNACE;
					
					final FurnaceRecipe furnace = (FurnaceRecipe)recipe;
					ingredientList.add(furnace.getInput());
					ingredientList.add(Helpers.Item.getItemStack("coal", 1));
					ingredientList.add(recipe.getResult());
				}
				
				if (ingredientList.isEmpty()) sender.sendError("Recipe type $1 is not supported.", recipe.getClass().getSimpleName());
				else
				{
					final Inventory inventory = Bukkit.createInventory(
							null,
							inventoryType,
							Helpers.Parameters.replace("Recipe for $1$2:", ChatColor.BOLD, name));
										
					int index = 0;
					for (final ItemStack ingredient: ingredientList)
					{
						if (ingredient != null)
						{
							final String[] names = Helpers.Item.getFriendlyNames(ingredient);							
							if (names.length > 0)
							{
								final int originalAmount = ingredient.getAmount();
								inventory.setItem(index, Helpers.Item.getItemStack(names[0], originalAmount));
							}
							else inventory.setItem(index, ingredient);
						}
						
						++index;
					}
										
					sender.getPlayer().openInventory(inventory);
				}
			}
		}
		return true;
	}

	private boolean doCull(final PagedOutputCommandSender sender, final String[] args)
	{
		final boolean all = args[0].contains("*");
		final boolean confirm = super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM);

		boolean arrows = false || all;
		boolean farmAnimals = false || all;
		boolean horses = false || all;
		boolean items = false || all;
		boolean mobs = false || all;
		boolean named = false || all;
		boolean tamed = false || all;
		boolean villagers = false || all;
		boolean wildAnimals = false || all;
		boolean xp = false || all;

		if (args[0].contains("?"))
		{
			sender.sendNotification("Valid switches:");
			sender.sendNotification("   $1tems, $2onsters, $3rrows, $4P orbs", "I", "M", "A", "X");
			sender.sendNotification("   $1ild, $2amed, $3amed, $4arm animals", "W", "N", "T", "F");
			sender.sendNotification("   $1orses, $2illagers", "H", "V");
			sender.sendNotification("   $1 All of the above", "*");
			return true;
		}
		else if (!all)
		{
			for (final char c: args[0].toLowerCase().toCharArray())
			{		
				switch (c)
				{
				case 'a': arrows = true; break;
				case 'f': farmAnimals = true; break;
				case 'h': horses = true; break;
				case 'i': items = true; break;
				case 'm': mobs = true; break;
				case 'n': named = true; break;
				case 't': tamed = true; break;
				case 'v': villagers = true; break;
				case 'w': wildAnimals = true; break;
				case 'x': xp = true; break;
				default: return false;
				}
			}
		}

		int radius = 0;
		Location location = null;
		if (args.length == 2)
		{
			try { radius = Integer.parseInt(args[1]); }
			catch (final NumberFormatException e)
			{
				sender.sendFailure("Invalid radius $1.", args[1]);
				return true;
			}

			if (sender.isConsole())
			{
				sender.sendFailure("Cannot specify radius from CONSOLE.");
				return true;
			}
			location = sender.getPlayer().getLocation();
		}

		final ArrayList<Entity> entities = new ArrayList<Entity>();
		for (final Entity entity: sender.getPlayer().getWorld().getEntities())
		{
			if (radius == 0 ||
					entity.getLocation().distance(location) < radius)
			{
				switch (entity.getType())
				{
				case ARROW:
					if (arrows) entities.add(entity);
					break;

				case CHICKEN:
				case COW:
				case MUSHROOM_COW:
				case PIG:
				case SHEEP:
					if (farmAnimals) entities.add(entity);
					break;

				case DROPPED_ITEM:
				case EGG:
					if (items) entities.add(entity);
					break;

				case BLAZE:
				case CAVE_SPIDER:
				case CREEPER:
				case ENDERMAN:
				case ENDERMITE:
				case GHAST:
				case GIANT:
				case GUARDIAN:
				case IRON_GOLEM:
				case MAGMA_CUBE:
				case PIG_ZOMBIE:
				case SILVERFISH:
				case SKELETON:
				case SLIME:
				case SNOWMAN:
				case SPIDER:
				case WITCH:
				case WITHER:
				case ZOMBIE:
					if (mobs) entities.add(entity);
					break;
					
				case EXPERIENCE_ORB:
					if (xp) entities.add(entity);
					break;

				case HORSE:
					if (horses) entities.add(entity);
					break;

				case BAT:
				case OCELOT:
				case RABBIT:
				case SQUID:
				case WOLF:
					if (wildAnimals) entities.add(entity);
					break;

				case VILLAGER:
					if (villagers) entities.add(entity);
					break;

				default: break;
				}
			}
		}

		sender.suppressPaging();
		sender.sendNotification("Removing entities.");

		final HashMap<String, Integer> removed = new HashMap<String, Integer>();
		for (final Entity entity: entities)
		{
			if (entity instanceof Tameable)
			{
				if (!tamed && ((Tameable)entity).isTamed()) continue;
			}
			if (entity instanceof Villager)
			{
				if (this.villagerSettings.isProtected((Villager)entity)) continue;
			}
			if (entity instanceof LivingEntity)
			{
				if (!named && (((LivingEntity)entity).getCustomName() != null)) continue;
			}

			if (confirm) entity.remove();
			
			final String name = Helpers.Entity.getDisplayName(entity);
			int count = 0;
			if (removed.containsKey(name)) count = removed.get(name);
			++count;
			removed.put(name, count);
		}

		int totalCount = 0;
		for (final String name: removed.keySet())
		{
			final int count = removed.get(name);
			sender.sendInfo("   $1: $2", name, count);
			totalCount += count;
		}

		sender.sendSuccess("Total entities removed: $1", totalCount);

		return true;
	}

	private boolean doRepair(final PagedOutputCommandSender sender, final String[] args)
	{
		final Player player = super.getPlayer(sender);
		if (args.length == 0)
		{
			final ItemStack stack = player.getItemInHand();
			if (stack == null) sender.sendFailure("You must be holding the item to repair.");
			else if (stack.getAmount() == 0) sender.sendFailure("Your hand is empty!");
			else if (stack.getDurability() > 0)
			{
				Helpers.Item.repair(stack);
				sender.sendSuccess("Item repaired.");
			}
		}
		else if (args[0].equals("*") || args[0].equalsIgnoreCase("all"))
		{
			final ArrayList<ItemStack> items = new ArrayList<ItemStack>();
			final Inventory inventory = player.getInventory();
			if (inventory == null)
			{
				sender.sendError("No inventory for $1!", player);
				return true;
			}
			else
			{
				for (final ItemStack stack: inventory.getContents())
				{
					if (stack != null && stack.getDurability() > 0) items.add(stack);
				}
				final EntityEquipment equipment = player.getEquipment();
				if (equipment != null)
				{
					final ItemStack helmet = equipment.getHelmet();
					final ItemStack chest = equipment.getChestplate();
					final ItemStack legs = equipment.getLeggings();
					final ItemStack boots = equipment.getBoots();

					if (helmet != null && helmet.getDurability() > 0) items.add(helmet);
					if (chest != null && chest.getDurability() > 0) items.add(chest);
					if (legs != null && legs.getDurability() > 0) items.add(legs);
					if (boots != null && boots.getDurability() > 0) items.add(boots);
				}
			}

			if (items.size() == 0) sender.sendFailure("Repair not required.");
			else
			{
				for (final ItemStack stack: items)
					Helpers.Item.repair(stack);
				sender.sendSuccess("Repaired $1 item$2.", items, Helpers.getPlural(items));
			}
		}
		else return false;

		return true;
	}

	private boolean doUnenchant(final PagedOutputCommandSender sender, final String[] args)
	{
		final Player player = super.getPlayer(sender);
		final ItemStack stack = player.getItemInHand();
		if (stack == null) sender.sendFailure("You must be holding the item to unenchant.");
		else if (stack.getAmount() == 0) sender.sendFailure("Your hand is empty!");
		else
		{
			final Set<Enchantment> enchantments = stack.getEnchantments().keySet();
			int removed = 0;
			for (final Enchantment enchantment: enchantments)
			{
				stack.removeEnchantment(enchantment);
				++removed;
			}
			if (removed == 0) sender.sendFailure("Item is not enchanted.");
			else sender.sendSuccess("Removed $1 enchantment$2.", removed, Helpers.getPlural(removed));
		}
		return true;
	}

	private boolean doEnchant(final PagedOutputCommandSender sender, final String[] args)
	{
		final Player player = super.getPlayer(sender);
		final ItemStack stack = player.getItemInHand();
		if (stack == null) sender.sendFailure("You must be holding the item to enchant.");
		else if (stack.getAmount() == 0) sender.sendFailure("Your hand is empty!");
		else
		{
			final String[] itemNames = Helpers.Item.getFriendlyNames(stack);
			if (args.length == 0)
			{
				final List<Enchantment> valid = new ArrayList<Enchantment>();
				for (final Enchantment enchantment: Enchantment.values())
					if (enchantment.canEnchantItem(stack)) valid.add(enchantment);
				if (valid.size() == 0) sender.sendFailure("No enchantments available for $1.", itemNames[0]);
				else
				{
					sender.sendNotification("Valid enchantments for $1:", itemNames[0]);
					for (final Enchantment enchantment: valid)
					{
						final String[] friendlyNames = Helpers.Item.getFriendlyNames(enchantment);
						sender.sendNotification(
								" - $1: $2 level $3",
								enchantment.getName(),
								friendlyNames[0],
								enchantment.getMaxLevel());
					}
				}

				return true;
			}

			final Enchantment enchantment = Helpers.Item.getEnchantment(args[0]);
			if (enchantment == null) sender.sendFailure("Unknown enchantment '$1'.", args[0]);
			else
			{
				int level = 0;

				if (args.length == 2)
				{
					try { level = Integer.parseInt(args[1]); }
					catch (final NumberFormatException e)
					{
						sender.sendFailure("Invalid value $1 for level.", args[1]);
						return true;
					}
				}

				final String[] friendlyNames = Helpers.Item.getFriendlyNames(enchantment);
				final String friendlyName = friendlyNames[0];
				if (level == 0 || level > enchantment.getMaxLevel())
				{
					level = enchantment.getMaxLevel();
					sender.sendInfo("Maximum level for $1 is $2.", friendlyName, level);
				}

				if (Helpers.Item.enchantItem(stack, enchantment, level))
					sender.sendSuccess("Enchanted $1 with $2 level $3.", itemNames[0], friendlyName, level);
				else sender.sendFailure("Enchantment $1 is not valid for $2.", friendlyName, itemNames[0]);
			}
		}

		return true;
	}

	private boolean doGive(final PagedOutputCommandSender sender, final String[] args)
	{
		Player player = null;
		switch (args[0])
		{
		case ".":
		case "me":
			player = super.getPlayer(sender);
			break;
		default:
			player = super.getPlugin().getPlayer(sender, args[0]);
			break;
		}

		if (player == null) return true;
		int quantity = -1;
		if (args.length == 3)
		{
			try { quantity = Integer.parseInt(args[2]); }
			catch (final NumberFormatException e)
			{
				sender.sendFailure("Non-numeric quantity $1 provided.", args[2]);
				return true;
			}
		}
		String friendlyName = args[1];
		
		if (!Helpers.Item.isMetadataValid(friendlyName))
		{
			sender.sendFailure("Invalid metadata for $1:", Helpers.Item.stripMetadata(friendlyName));
			sender.sendFailure("--> $1", Helpers.Item.getMetadata(friendlyName));
			return true;
		}
		
		final ItemStack stack = Helpers.Item.getItemStack(friendlyName, quantity);
		if (stack == null)
		{
			sender.sendFailure("Unknown item $1.", friendlyName);
			return true;
		}
		
		final int originalAmount = stack.getAmount();
		final HashMap<Integer, ItemStack> lostItems = player.getInventory().addItem(stack);
		final String[] friendlyNames = Helpers.Item.getFriendlyNames(stack);
		if (friendlyNames != null && friendlyNames.length > 0) friendlyName = friendlyNames[0];

		if (lostItems.size() == 0) sender.sendSuccess("Gave $1 x $2 to $3.", originalAmount, friendlyName, player);
		else
		{
			for (final ItemStack lostStack: lostItems.values())
			{
				sender.sendFailure(
						"$1 did not have enough space for $2 x $3, and $4 were lost.",
						player,
						originalAmount,
						friendlyName,
						lostStack.getAmount());
			}
		}

		return true;
	}

	private boolean doClearInventory(final PagedOutputCommandSender sender, final String[] args)
	{
		Player player = null;
		final boolean silent = super.hasFlag(ExecutorBase.FLAG_BOOL_SILENT); 
		if (args.length == 0)
		{
			if (silent) return false;
			player = super.getPlayer(sender);
		}
		else
		{
			player = super.getPlugin().getPlayer(sender, args[0]);
			if (player == null) return true;
		}

		player.getInventory().clear();
		if (!silent) Helpers.Messages.sendNotification(player, "Your inventory was cleared.");
		if (player.equals(sender.getPlayer())) sender.sendSuccess("Cleared your inventory.");
		else sender.sendSuccess("Cleared inventory for $1.", player);

		return true;
	}

	private boolean doName(final PagedOutputCommandSender sender, final String[] args)
	{
		ItemStack itemStack = null;
		
		if (args.length == 0)
		{
			final Player player = sender.getPlayer();
			if (player == null) return true;
			itemStack = player.getItemInHand();
			if (itemStack == null ||
					itemStack.getType() == Material.AIR ||
					itemStack.getAmount() == 0) sender.sendFailure("You are not holding anything!");
		}
		else
		{
			itemStack = Helpers.Item.getItemStack(args[0], 1);
			if (itemStack == null) sender.sendFailure("No such item.");
		}
		
		if (itemStack != null)
		{
			final String[] names = Helpers.Item.getFriendlyNames(itemStack);
			String format = "";
			for (int i = 0; i < names.length; ++i)
			{
				if (format.length() > 0) format += ", ";
				format += Helpers.Parameters.replace("$$1", i + 1);
			}
			if (!Helpers.Item.isShortNameCompatible(itemStack)) format += " (unsupported)";
			sender.sendNotification("Known as: " + format, (Object[])names);
			if (sender.hasPermission("vpcore.moderator"))
			{
				final Material material = itemStack.getType();
				sender.sendInfo(
						"Raw: $1:$2 [data=$3 durability=$4]",
						Helpers.Hacks.getMaterialId(material),
						material,
						itemStack.getData(),
						itemStack.getDurability());
				final String metadata = Helpers.Item.getFullMetadata(itemStack);
				if (metadata.length() > 0) sender.sendInfo("Meta: $1", metadata);
			}
		}

		return true;
	}

	private boolean doGoto(final PagedOutputCommandSender sender, final String[] args)
	{
		sender.suppressPaging();
		
		int number = 0;
		try { number = Integer.parseInt(args[0]); }
		catch (final Exception ex)
		{
			sender.sendFailure("Invalid value $1 for number.", args[0]);
			return false;
		}

		Location location = null;
		final Object item = super.getPlugin().getItem(number);
		if (item == null)
		{
			sender.sendFailure("No item at number $1.", number);
			return true;
		}
		else if (item instanceof Location) location = (Location)item;
		else if (item instanceof World) location = ((World)item).getSpawnLocation();
		else if (item instanceof Chunk)
		{
			final Chunk chunk = (Chunk)item;
			Block block = null;

			for (int x = 0; x < 16; ++x)
			{
				for (int z = 0; z < 16; ++z)
				{
					for (int y = 127; y >= 0; --y)
					{
						block = chunk.getBlock(x, y, z);
						if (block.getType() != Material.AIR) break;
						else block = null;
					}
				}
			}

			if (block == null) sender.sendFailure("No solid ground for teleportation.");
			else
			{
				final Block oneBlockUp = block.getRelative(BlockFace.UP);
				Block twoBlocksUp = null;
				if (oneBlockUp != null) twoBlocksUp = oneBlockUp.getRelative(BlockFace.UP);
				if (oneBlockUp == null || twoBlocksUp == null) sender.sendFailure("No empty space for teleportation.");
				else location = oneBlockUp.getLocation();
			}
		}
		else if(item instanceof OfflinePlayer)
		{
			final OfflinePlayer p = (OfflinePlayer)item;
			if (p.isOnline()) location = p.getPlayer().getLocation();
			else sender.sendFailure("Player is offline.");
		}
		else if (item instanceof Entity) location = ((Entity)item).getLocation();

		if (location == null) sender.sendFailure("Unable to teleport you to $1.", item);
		else
		{
			sender.getPlayer().teleport(location, TeleportCause.COMMAND);
			sender.sendSuccess("Teleported to $1.", item);
		}

		return true;
	}

	private boolean doHeal(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length == 0)
		{
			final Player player = super.getPlayer(sender);
			if (player == null) return false;
			player.setHealth(player.getMaxHealth());
			sender.sendSuccess("You have been fully healed.");
		}
		else
		{
			final ArrayList<Player> playersToHeal = new ArrayList<Player>();
			final boolean allTarget = args[0].equals("*");

			for (final Player player: Bukkit.getOnlinePlayers())
			{
				if (allTarget ||
						player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
						playersToHeal.add(player);
			}

			String healed = "";
			int healedCount = 0;
			if (playersToHeal.size() == 0) sender.sendFailure("No players matched.");
			else
			{
				for (final Player p: playersToHeal)
				{
					if (p.isDead()) sender.sendFailure("$1 is dead and cannot be healed.", p);
					else if (p.getHealth() != p.getMaxHealth())
					{
						p.setHealth(p.getMaxHealth());
						p.setSaturation(20);
						p.setFoodLevel(20);
						if (healed.length() > 0) healed += ", ";
						healed += p.getDisplayName();
						Helpers.Messages.sendInfo(p, "You have been healed.");
						++healedCount;
					}
				}

				if (healedCount == 0) sender.sendInfo("The specified player$1 didn't require healing.", Helpers.getPlural(playersToHeal));
				else sender.sendSuccess("Healed $1 player$2: $3", healedCount, Helpers.getPlural(healedCount), healed);
			}
		}

		return true;
	}

	private boolean doSpawnMobAt(final PagedOutputCommandSender sender, final String[] args)
	{
		Location targetLocation = null;

		int quantityIndex = 0;
		int quantity = 1;
		if (args.length == 3) quantityIndex = 2;
		else if (args.length == 5) quantityIndex = 4;

		if (quantityIndex > 0)
		{
			try { quantity = Integer.parseInt(args[quantityIndex]); }
			catch (final Exception ex)
			{
				sender.sendFailure("Invalid value $1 for quantity.", args[quantityIndex]);
				return false;
			}
		}

		if (args.length == 2 || args.length == 3)
		{
			final String name = args[1].toLowerCase();
			switch (name)
			{
			case "here":
			case "me":
				final Player player = super.getPlayer(sender);
				if (player == null) return false;
				targetLocation = player.getLocation();
				break;

			default:
				final OfflinePlayer[] offlinePlayers = Helpers.Bukkit.getOfflinePlayers();
				for (final OfflinePlayer offlinePlayer: offlinePlayers)
				{
					if (offlinePlayer.getName().equalsIgnoreCase(name))
					{
						if (offlinePlayer.isOnline()) targetLocation = ((Player)offlinePlayer).getLocation();
						else
						{
							sender.sendFailure("Player $1 is not online.", offlinePlayer);
							return true;
						}
						break;
					}
				}
				if (targetLocation == null)
				{
					sender.sendFailure("Unknown player or option $1.", name);
					return false;
				}
			}
		}
		else
		{
			final World world = super.getWorld(sender);
			if (world == null) return false;

			int x = 0;
			int y = 0;
			int z = 0;

			try { x = Integer.parseInt(args[1]); }
			catch (final Exception ex)
			{
				sender.sendFailure("Invalid value $1 for X coordinate.", args[1]);
				return false;
			}

			try { y = Integer.parseInt(args[2]); }
			catch (final Exception ex)
			{
				sender.sendFailure("Invalid value $1 for Y coordinate.", args[2]);
				return false;
			}

			try { z = Integer.parseInt(args[3]); }
			catch (final Exception ex)
			{
				sender.sendFailure("Invalid value $1 for Z coordinate.", args[3]);
				return false;
			}

			targetLocation = new Location(world, x, y, z);
		}

		String entityName = args[0];
		String[] metaData = new String[] { "random" };
		if (entityName.contains(":"))
		{
			final String[] parts = entityName.split(":");
			entityName = parts[0];
			metaData = parts[1].split(",");
		}

		final EntityType entityType = Helpers.Entity.getMobType(entityName);
		if (entityType == EntityType.UNKNOWN)
		{
			sender.sendFailure("Unknown entity type $1.", entityName);
			return false;
		}

		Helpers.Spawn.spawnMobAt(entityType, targetLocation, quantity, metaData);

		return true;
	}

	private boolean doFixup(final PagedOutputCommandSender sender, final String[] args)
	{
		sender.sendNotification("Fixing up worlds - check CONSOLE for output.");

		for (final World world: Bukkit.getWorlds())
		{
			int horsesFixed = 0;
			final Collection<Entity> horses = world.getEntitiesByClasses(Horse.class);

			for (final Entity entity: horses)
			{
				if (entity instanceof Horse)
				{
					final Horse horse = (Horse)entity;
					if (horse.getMaxHealth() > 30)
					{
						super.debug("Horse $1 $2 had MaxHealth>30; recalculating.", horse, horse.getLocation());
						horse.setMaxHealth((this.random.nextDouble() * 15) + 15);
						++horsesFixed;
					}
				}
			}

			if (horses.size() == 0) sender.sendNotification("No horses loaded in $1; skipping.", world);
			else sender.sendNotification("Fixed $1 of $2 horse$3 in $4.", horsesFixed, horses, Helpers.getPlural(horses), world);
		}

		sender.sendSuccess("Fixing up worlds completed.");

		return true;
	}

	private boolean doListPlayers(final PagedOutputCommandSender sender, final String[] args)
	{
		Group group = null;
		if (args.length == 1)
		{
			group = this.playerSettings.getGroup(args[0]);
			if (group == null)
			{
				sender.sendFailure("No such group $1.", args[0]);
				return true;
			}
		}
		
		List<OfflinePlayer> displayList = null;

		if (group == null)
		{
			displayList = new ArrayList<OfflinePlayer>();
			final OfflinePlayer[] players = Helpers.Bukkit.getOfflinePlayers();
			for (final OfflinePlayer player: players) displayList.add(player);
		}
		else displayList = super.getSettings().getPlayersInGroup(group);

		final OfflinePlayer[] displayPlayers = displayList.toArray(new OfflinePlayer[0]);
		if (displayPlayers.length > 1)
		{
			Arrays.sort(
					displayPlayers,
					new Comparator<OfflinePlayer>()
					{
						@Override
						public int compare(final OfflinePlayer p1, final OfflinePlayer p2)
						{
							return p1.getName().compareToIgnoreCase(p2.getName());
						}
					});
		}

		sender.sendNotification("There $1 $2 player$3.", Helpers.getIsAre(displayPlayers.length), displayPlayers.length, Helpers.getPlural(displayPlayers.length));
		for (int i = 0; i < displayPlayers.length; ++i) sender.sendInfoListItem(i, "$1", displayPlayers[i]);

		return true;
	}

	private boolean doEject(final PagedOutputCommandSender sender, final String[] args)
	{
		final World world = this.getWorld(sender);
		if (world == null) return true;

		if (!super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM)) return true;
		CommandExecutor.ejectFromWorld(
				sender,
				world,
				Helpers.formatMessage(Helpers.Args.concatenate(args, 0)));
		return true;
	}

	private boolean doDropAnvil(final PagedOutputCommandSender sender, final String[] args)
	{
		final Player player = (Player)super.getPlugin().getPlayer(sender, args[0]);
		if (player == null) return true;

		final World world = player.getWorld();
		final int targetEyeLocation = player.getEyeLocation().getBlockY();
		if (targetEyeLocation >= world.getMaxHeight())
		{
			sender.sendFailure("The player is at too high an altitude for that!");
			return true;
		}

		int anvilHeight = 10;
		if (args.length == 2)
		{
			try { anvilHeight = Integer.parseInt(args[1]); }
			catch (final NumberFormatException ex)
			{
				sender.sendFailure("The anvil height value was not a valid number.");
				return true;
			}
			if (anvilHeight <= 0)
			{
				sender.sendFailure("The anvil height value must be greater than zero.");
				return true;
			}
		}

		if (targetEyeLocation + anvilHeight > world.getMaxHeight())
		{
			final int newAnvilHeight = world.getMaxHeight() - targetEyeLocation;
			sender.sendNotification(
					"Anvil height has been adjusted from $1 to $2 (target eye Y=$3).",
					anvilHeight,
					newAnvilHeight,
					targetEyeLocation);
			anvilHeight = newAnvilHeight;
		}

		if (anvilHeight <= 3)
		{
			sender.sendFailure("Aborted: effective anvil height must be greater than 3.");
			return true;
		}

		final Location playerLoc = player.getEyeLocation();
		final Location anvilLoc = playerLoc.clone();
		anvilLoc.add(0, anvilHeight, 0);

		for (int i = 0; i <= anvilHeight; ++i)
		{
			final Block block = world.getBlockAt(playerLoc.getBlockX(), playerLoc.getBlockY() + i, playerLoc.getBlockZ());
			if (!block.isEmpty() && !block.isLiquid())
			{
				sender.sendFailure("Vertical path from anvil to player is blocked.", player);
				return true;
			}
		}

		world.getBlockAt(anvilLoc).setType(Material.ANVIL);
		sender.sendSuccess("Anvil dropped on $1!", player);
		return true;
	}

	private boolean doPlayerInfo(final PagedOutputCommandSender sender, final String[] args)
	{
		sender.suppressPaging();
		
		final boolean verbose = super.getFlagState(ExecutorBase.FLAG_BOOL_VERBOSE);

		OfflinePlayer player = null;
		if (args.length == 0) player = sender.getPlayer();
		else player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;

		final Group group = this.playerSettings.getCurrentGroup(player);		
		sender.sendNotification("======== $1 ($2) ========", player, group);
		
		if (verbose)
		{
			sender.sendNotification("UUID: $1", player.getUniqueId());
			
			final String forumUserName = super.getSettings().getForumUsername(player);
			if (forumUserName != null && forumUserName.length() > 0)
				sender.sendNotification("Forum username: $1", forumUserName);
	
			if (!group.isDefault())
			{
				final String profileUrl = this.playerSettings.getProfileUrl(player);
				sender.sendNotification("Forum: $1", profileUrl);
			}
		}

		final String name = player.getName();
		final String[] aliases = super.getPlugin().getAliases(name);
		String aliasString = "";
		for (final String alias: aliases)
		{
			if (!alias.equals(name))
			{
				if (aliasString.length() > 0) aliasString += Helpers.ColorNotification + "," + Helpers.ColorParameterNotification;
				aliasString += alias;
			}
		}
		if (aliasString.length() > 0) sender.sendNotification("Aliases: $1", aliasString);			
				
		final BanInfo banInfo = this.banSettings.getBanInfo(player);
		if (banInfo != null)
		{
			final long now = System.currentTimeMillis();
			if (banInfo.isTemporary())
				sender.sendInfo(
						ChatColor.RED + "BANNED $1 AGO (expires in $2)",
						Helpers.DateTime.getHumanReadableFromMillis(now - banInfo.getBannedOn()),
						Helpers.DateTime.getHumanReadableFromMillis(banInfo.getExpireOn() - now));
			else sender.sendInfo(
					ChatColor.RED + "BANNED $1 AGO",
					Helpers.DateTime.getHumanReadableFromMillis(now - banInfo.getBannedOn()));
		}

		final int exemption = this.playerSettings.getPlotDeletionExemption(player); 
		switch (exemption)
		{
			case 0:
			{
				if (group.isExempt())
					sender.sendNotification("Group exempt from plot deletion.");
			} break;
			case 1: sender.sendNotification("Temporarily exempt from plot deletion."); break;
			case 2: sender.sendNotification("Indefinitely exempt from plot deletion."); break;
			default:
				sender.sendError("Unknown exemption value $1.", exemption);
				break;
		}
		
		if (this.playerSettings.isMuted(player))
		{
			final long duration = this.playerSettings.getMuteDuration(player);
			sender.sendNotification("Muted for $1.", Helpers.DateTime.getHumanReadableFromMillis(duration));
		}
		
		if (super.getPlugin().isVaultEnabled())
		{
			final String balance = super.getPlugin().getEconomyManager().getBalanceString(player);
			if (player.isOnline()) sender.sendNotification("Balance: $$1   XP: $2", balance, player.getPlayer().getTotalExperience());
			else sender.sendNotification("Balance: $$1", balance);
		}
		
		String firstSeen = Helpers.DateTime.getTruncatedAtHourTimeDifference(player.getFirstPlayed());
		if (player.isOnline()) sender.sendNotification("First seen: $1 ago (online now)", firstSeen);
		else sender.sendNotification("First/last seen: $1 ago/$2 ago", firstSeen, Helpers.DateTime.getTruncatedAtHourTimeDifference(player.getLastPlayed()));

		final ProtectedRegion region = this.plotSettings.getPlotFor(player);
		if (region != null) sender.sendNotification("Orkida home: $1", region.getId());

		final Set<String> homeNames = this.playerSettings.getHomeNames(player);
		int homesCount = 0;
		if (homeNames != null) homesCount = homeNames.size();
		final Location plotHome = this.playerSettings.getPlotHomeLocation(player);
		if (plotHome != null) ++homesCount;
		if (homesCount > 0)
		{
			sender.sendNotification("Player has $1 home$2:", homesCount, Helpers.getPlural(homesCount));
			int homeNumber = 1;
			if (plotHome != null)
			{
				sender.sendInfoListItem(homeNumber - 1, "$1 $2", plotHome, PlayerSettings.PlotHomeName);
				++homeNumber;
			}
			
			if (homeNames != null)
			{
				for (final String homeName: homeNames)
				{
					final Location location = this.playerSettings.getHomeLocation(player, homeName);
					sender.sendInfoListItem(homeNumber - 1, "$1 $2", location, homeName);
					++homeNumber;
				}
			}
		}

		return true;
	}

	private boolean doPlayerPurge(final PagedOutputCommandSender sender, final String[] args)
	{
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER))
		{
			for (final String flag: new String[] { ExecutorBase.FLAG_ORDERBY, ExecutorBase.FLAG_GROUP })
			{
				if (super.hasFlag(flag))
				{
					sender.sendFailure("Flags $1 and $2 are mutually exclusive.", ExecutorBase.FLAG_PLAYER, flag);
					return true;
				}
			}
		}

		final String orderBy = super.getSortOrder(sender, "name", "name", "namec", "time");
		if (orderBy.length() == 0) return true;

		String group = "";
		if (super.hasFlag(ExecutorBase.FLAG_GROUP))
		{
			group = super.getFlagValue(ExecutorBase.FLAG_GROUP).toLowerCase();
			switch (group)
			{
			case "veteran":
			case "moderator":
			case "admin":
				sender.sendFailure("Cannot purge members of group $1.", group);
				return true;
			}
		}

		OfflinePlayer[] purgePlayers = null;
		final Map<String, Long> timeByPlayer = new HashMap<String, Long>();
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER))
		{
			if (args.length > 0)
			{
				sender.sendFailure("The $1 flag must be specified exclusively.", ExecutorBase.FLAG_PLAYER);
				return true;
			}
			final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, super.getFlagValue(ExecutorBase.FLAG_PLAYER));
			if (player == null) return true;
			purgePlayers = new OfflinePlayer[] { player };
		}
		else
		{
			if (args.length == 0) return false;

			OfflinePlayer[] players = null;
			if (group.length() > 0) players = super.getSettings().getPlayersInGroup(group).toArray(new OfflinePlayer[0]);
			else players = Helpers.Bukkit.getOfflinePlayers();

			long minAbsence = 0;
			long maxPlayTime = 0;
			if (args.length >= 1) minAbsence = Helpers.DateTime.getMillis(args[0]);
			if (minAbsence < 0) return false;
			if (args.length == 2) maxPlayTime = Helpers.DateTime.getMillis(args[1]);
			if (maxPlayTime < 0) return false;

			long lastPlayed = 0;
			final List<OfflinePlayer> purgeList = new ArrayList<OfflinePlayer>();
			final long now = System.currentTimeMillis();
			for (final OfflinePlayer player: players)
			{
				lastPlayed = player.getLastPlayed();
				if (now - lastPlayed >= minAbsence)
				{
					if (maxPlayTime == 0 || lastPlayed - player.getFirstPlayed() <= maxPlayTime)
					{
						purgeList.add(player);
						timeByPlayer.put(player.getName(), lastPlayed);
					}
				}
			}
			purgePlayers = purgeList.toArray(new OfflinePlayer[0]);
		}

		if (purgePlayers.length == 0) sender.sendFailure("No players matched the criteria provided.");
		else
		{
			if (purgePlayers.length > 1)
			{
				Arrays.sort(
						purgePlayers,
						new Comparator<OfflinePlayer>()
						{
							private final boolean isByName = orderBy.equalsIgnoreCase("name");
							private final boolean isByNameC = orderBy.equalsIgnoreCase("namec");
							private final boolean isByTime = orderBy.equalsIgnoreCase("time");
							@Override
							public int compare(final OfflinePlayer p1, final OfflinePlayer p2)
							{
								if (isByName) return p1.getName().compareToIgnoreCase(p2.getName());
								if (isByNameC) return p1.getName().compareTo(p2.getName());
								else if (isByTime) return Long.compare(timeByPlayer.get(p1.getName()), timeByPlayer.get(p2.getName()));
								else return 0;
							}
						});
			}

			sender.sendSuccess("Matched $1 player$2.", purgePlayers.length, Helpers.getPlural(purgePlayers.length));
			for (int i = 0; i < purgePlayers.length; ++i)
			{
				sender.sendInfoListItem(
						i,
						"$1 $2(last seen $3 ago)",
						purgePlayers[i],
						ChatColor.GRAY,
						Helpers.DateTime.getTruncatedAtHourTimeDifference(purgePlayers[i].getLastPlayed()));
			}

			if (super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM)) this.deletePlayerFiles(sender, purgePlayers);
		}
		return true;
	}

	private boolean doSetSpeed(final PagedOutputCommandSender sender, final String[] args)
	{
		Player player = null;
		if (args.length == 1) player = super.getPlayer(sender);
		else player = super.getPlugin().getPlayer(sender, args[1]);
		if (player == null) return true;

		final boolean playerIsTarget = sender.getName().equalsIgnoreCase(player.getName());
		if (!playerIsTarget && player.hasPermission("vpcore.command.speed.exempt"))
		{
			sender.sendFailure("Cannot set speed for $1 (exempt).", player);
		}
		else
		{
			int speed = 0;
			try { speed = Integer.parseInt(args[0]); }
			catch (final NumberFormatException ex)
			{
				sender.sendFailure("The speed value was not a valid number.");
				return true;
			}

			if (speed >= 1 && speed <= 10)
			{
				if (speed < 2) speed = 2;

				final float actualSpeed = ((float)speed) / 10;
				super.getSettings().saveSpeed(player, actualSpeed);
				super.getSettings().restoreSpeed(player);

				if (playerIsTarget) sender.sendSuccess("You set your speed to $1.", speed);
				else
				{
					sender.sendSuccess("Speed set to $1 for player $2.", speed, player);
					Helpers.Messages.sendNotification(player, "Your speed was set to $1.", speed);
				}
			}
			else sender.sendFailure("The speed value must be in the range 1 <= speed <= 10.");
		}
		return true;
	}

	private boolean doTime(final PagedOutputCommandSender sender, final String[] args)
	{
		if (super.hasFlag(ExecutorBase.FLAG_WORLD) && !sender.hasPermission("vpcore.moderator")) return false;

		final World world = super.getWorld(sender);
		if (world == null) return true;

		boolean success = false;
		if (args.length == 0)
		{
			if (super.hasFlag(ExecutorBase.FLAG_WORLD))
				sender.sendNotification(
						"The time in world $1 is $2 on $3.",
						world,
						Helpers.DateTime.getHumanReadableFromGameTime(world.getTime()),
						Helpers.DateTime.getHumanReadableFromGameFullTime(world.getFullTime()));
			else
				sender.sendNotification(
						"The time in this world is $1 on $2.",
						Helpers.DateTime.getHumanReadableFromGameTime(world.getTime()),
						Helpers.DateTime.getHumanReadableFromGameFullTime(world.getFullTime()));
			sender.sendInfo("Local time: $1", Calendar.getInstance().getTime().toString());
			success = true;
		}
		else if (sender.hasPermission("vpcore.time.set"))
		{
			final long toTicks = Helpers.DateTime.getGameTime(args[0]);
			if (toTicks < 0 || toTicks > 24000) sender.sendFailure("Invalid time $1.", args[0]);
			else
			{
				world.setTime(toTicks);
				sender.sendNotification(
						"The time in world $1 is $2 on $3.",
						world,
						Helpers.DateTime.getHumanReadableFromGameTime(world.getTime()),
						Helpers.DateTime.getHumanReadableFromGameFullTime(world.getFullTime()));
			}
			success = true;
		}
		return success;
	}

	private boolean doDisconnect(final PagedOutputCommandSender sender, final String[] args)
	{
		String reason = Helpers.formatMessage(Helpers.Args.concatenate(args, 1));
		if (reason.equals("") && this.serverListSettings.isServerLocked()) reason = this.serverListSettings.getLockMessage();
		if (args[0].equalsIgnoreCase("*")) this.disconnectAll(sender, reason);
		else if (sender.getName().equalsIgnoreCase(args[0])) sender.sendFailure("You cannot disconnect yourself.");
		else
		{
			final Player player = super.getPlugin().getPlayer(sender, args[0]);
			if (player == null) return true;
			else if(player.hasPermission("vpcore.moderator") && !sender.isConsole())
				sender.sendFailure(
						"$1 cannot be disconnected.",
						player.getName());
			else
			{
				player.kickPlayer(reason);
				if (reason.length() == 0) sender.sendSuccess("You disconnected $1.", player);
				else sender.sendSuccess("You disconnected $1: $2", player, reason);
				Helpers.Broadcasts.sendPerms(
						"vpcore.moderator",
						"$1 disconnected $2: $3",
						sender,
						player,
						reason);
			}
		}

		return true;
	}

	private boolean doWorld(final PagedOutputCommandSender sender, final String[] args)
	{
		final World world = sender.getWorld(args[0]);
		if (world == null) return true;

		final Player player = super.getPlayer(sender);
		if (player == null) return true;

		Location targetLocation = world.getSpawnLocation();
		if (world.getEnvironment() == Environment.THE_END)
		{
			Block block = targetLocation.getBlock();
			for (int y = block.getY(); y < 255 - 1; ++y)
			{
				block = block.getRelative(BlockFace.UP);
				if (block.getType() != Material.AIR)
				{
					targetLocation = block.getLocation();
					break;
				}
			}
		}

		if (player.teleport(targetLocation, TeleportCause.PLUGIN))
		{
			if (player.getName().equalsIgnoreCase(sender.getName()))
			{
				sender.sendSuccess("You teleported to $1.", targetLocation);
			}
			else
			{
				sender.sendSuccess("Player $1 was teleported to $2.", player, targetLocation);
				Helpers.Messages.sendNotification(player, "You were teleported to $1 by $2.", world, sender);
			}
		}

		return true;
	}

	private boolean doInfo(final PagedOutputCommandSender sender, final String[] args)
	{
		sender.suppressPaging();

		final World world = super.getWorld(sender);
		sender.sendNotification(
				"VPCore v$1 by $2",
				this.getPlugin().getDescription().getVersion(),
				this.getPlugin().getDescription().getAuthors().get(0));
		sender.sendNotification(
				"Running on $1 (Java $2/$3)",
				Bukkit.getBukkitVersion(),
				System.getProperty("java.version"),
				System.getProperty("os.arch"));
		sender.sendNotification(
				"Server $1 ($2) startup: $3 ago.",
				Bukkit.getServerName(),
				super.getPlugin().getHostName(),
				Helpers.DateTime.getHumanReadableFromMillis(System.currentTimeMillis() - super.getSettings().getStartMillis()));
		sender.sendNotification(
				"Players online: $1 of $2 ($3 seen)",
				Bukkit.getOnlinePlayers().size(),
				Bukkit.getMaxPlayers(),
				Helpers.Bukkit.getOfflinePlayers().length);

		boolean success = false;
		int[] cpuIdles = null;
		Process mpstat = null;
		final List<String> lines = new ArrayList<String>();
		final Runtime runtime = Runtime.getRuntime();
		try
		{
			mpstat = runtime.exec("mpstat -P ALL 1 1");
			final BufferedReader reader = new BufferedReader(new InputStreamReader(mpstat.getInputStream()));

			for (int i = 0; i < 4; ++i) reader.readLine();
			String readLine = "";
			while ((readLine = reader.readLine()) != null)
			{
				readLine = readLine.trim();
				if (readLine.length() == 0) break;
				lines.add(readLine.replace(",", ".").trim());
			}

			cpuIdles = new int[lines.size()];
			for (int i = 0; i < lines.size(); ++i)
			{
				final String[] parts = lines.get(i).split("\\s+");
				cpuIdles[i] = Math.round(Float.parseFloat(parts[parts.length - 1]));
			}

			success = true;
		}
		catch (final Exception ex) { }

		String message = "CPU Load: ";
		if (success)
		{
			for (int i = 0; i < cpuIdles.length; ++i)
			{
				final int load = 100 - cpuIdles[i];
				ChatColor loadColor = ChatColor.GREEN;
				if (load > 25)
				{
					if (load > 85) loadColor = ChatColor.RED;
					else loadColor = ChatColor.YELLOW;
				}

				message = Helpers.Parameters.replace(
						"$1 [$2$3:$4$5%$6]",
						message,
						ChatColor.DARK_GRAY,
						i + 1,
						loadColor,
						load,
						ChatColor.RESET);
			}
			sender.sendMessage(message);
		}
		else
		{
			sender.sendFailure("Unable to get CPU information.");
			sender.sendFailure("==> Ensure mpstat (package sysstat) is installed");
		}

		final double bytesPerPipe = runtime.maxMemory() / 50;
		final long maxMemory = runtime.maxMemory();
		final long freeMemory = runtime.freeMemory();
		final long usedMemory = runtime.totalMemory() - freeMemory;

		final int usedPipes = (int)(usedMemory / bytesPerPipe);
		final int freePipes = (int)(freeMemory / bytesPerPipe) + usedPipes;

		message = "Memory: [" + ChatColor.RED;
		for (int i = 0; i < 50; ++i)
		{
			if (i == usedPipes) message += ChatColor.YELLOW;
			else if (i == freePipes) message += ChatColor.GREEN;
			message += "|";
		}
		message += ChatColor.RESET + "]";
		sender.sendInfo(
				"$1 $2/$3/$4 MiB",
				message,
				usedMemory / (1024 * 1024),
				freeMemory / (1024 * 1024),
				maxMemory / (1024 * 1024));

		if (world != null)
		{
			sender.sendNotification("Chunk information for $1:", world);
			final Chunk[] chunks = world.getLoadedChunks();
			int cachedCount = 0;
			for (final Chunk chunk: chunks)
			{
				if (this.chunkSettings.isCached(chunk)) ++cachedCount;
			}
			sender.sendInfo(
					"Total/unloadable/unload-deferred chunks: $1/$2/$3",
					(Object)chunks,
					cachedCount,
					this.chunkSettings.getUnloadsDeferred());
		}

		return true;
	}

	private boolean deletePlayerFiles(final PagedOutputCommandSender sender, final OfflinePlayer[] players)
	{
		final TaskBase task = new TaskBase(super.isDebug(), "DeletePlayerFiles")
		{
			@Override
			public void runTask()
			{
				for (final OfflinePlayer player: players)
				{
					if (player.isOnline()) sender.sendFailure("Data for $1 cannot be deleted (player online).", player);
					else getSettings().deletePlayerData(player);
				}
				
				sender.sendSuccess("Purged data for $1 player$2.", players.length, Helpers.getPlural(players.length));
			}
		};
		task.runTaskAsynchronously(super.getPlugin());
		
		return true;
	}

	private void disconnectAll(final PagedOutputCommandSender sender, final String reason)
	{
		final Collection<? extends Player> players = Bukkit.getOnlinePlayers();
		int knownExempt = 0;
		String playerName = "";
		if (sender.isPlayer())
		{
			knownExempt = 1;
			playerName = sender.getName();
		}
		final List<Player> disconnectedPlayers = new ArrayList<Player>(players.size() - knownExempt);
		final List<Player> exemptPlayers = new ArrayList<Player>(knownExempt);

		for (final Player player: players)
		{
			if (player.getName().equalsIgnoreCase(playerName)) exemptPlayers.add(player);
			else if (player.hasPermission("vpcore.moderator")) exemptPlayers.add(player);
			else
			{
				player.kickPlayer(reason);
				disconnectedPlayers.add(player);
			}
		}

		for (final Player player: exemptPlayers)
			sender.sendFailure("Could not disconnect $1.", player);

		sender.sendSuccess(
				"Disconnected $1 player$2 ($3 remain online).",
				disconnectedPlayers.size(),
				Helpers.getPlural(disconnectedPlayers.size()),
				exemptPlayers.size());

		if (disconnectedPlayers.size() > 0)
			Helpers.Broadcasts.sendPerms(
					"vpcore.moderator",
					"$1 disconnected $2 player$3.",
					sender.getDisplayName(),
					disconnectedPlayers.size(),
					Helpers.getPlural(disconnectedPlayers.size()));
	}

	private boolean doWhoWhere(final PagedOutputCommandSender sender, final String[] args)
	{
		sender.suppressPaging();

		World world = null;
		//  We don't use super.GetWorld() since this command defaults to 'all worlds' - we only
		//  want to filter results by a world if a world is explicitly provided
		if (super.hasFlag(ExecutorBase.FLAG_WORLD))
		{
			world = sender.getWorld(super.getFlagValue(ExecutorBase.FLAG_WORLD));
			if (world == null) return true;
		}

		final Map<String, List<Player>> playerMap = new HashMap<String, List<Player>>();
		final ArrayList<String> worldNames = new ArrayList<String>();
		for (final Player player: Bukkit.getOnlinePlayers())
		{
			final World playerWorld = player.getWorld();
			if (playerWorld == null) continue;
			if (world == null || playerWorld.getName().equalsIgnoreCase(world.getName()))
			{
				final String worldName = player.getWorld().getName();
				if (!playerMap.containsKey(worldName))
				{
					playerMap.put(worldName, new ArrayList<Player>());
					worldNames.add(worldName);
				}
				final List<Player> list = playerMap.get(worldName);
				list.add(player);
			}
		}

		final Location myLocation = sender.getLocation();
		for (final String worldName: worldNames)
		{
			final int playerCount = playerMap.get(worldName).size();
			sender.sendInfo(
					"$1$2$3: $4 player$5",
					ChatColor.LIGHT_PURPLE,
					worldName,
					ChatColor.RESET,
					playerCount,
					Helpers.getPlural(playerCount));

			final Player[] players = playerMap.get(worldName).toArray(new Player[0]);
			for (int i = 0; i < players.length; ++i)
			{
				final Player player = players[i];
				final int health = (int)((player.getHealth() / player.getMaxHealth()) * 100);

				final boolean isVanished = super.getPlugin().isVanished(player);
				final boolean isAfk = this.playerSettings.isAfk(player);
				final boolean isGod = this.playerSettings.isGod(player);
				final boolean isBusy = this.playerSettings.isBusy(player);
				final boolean isSurvival = (player.getGameMode() == GameMode.SURVIVAL);
				final boolean isDead = player.isDead();
				final boolean isNearDeath = (!isDead && health <= 30);

				ChatColor playerColour = ChatColor.RESET;
				if (isDead) playerColour = ChatColor.DARK_RED;
				else if (isVanished) playerColour = ChatColor.DARK_GRAY;
				else if (isAfk) playerColour = ChatColor.GRAY;
				else if (isNearDeath) playerColour = ChatColor.RED;

				String playerOutput = playerColour + player.getName() + ChatColor.RESET;

				String flags = "";
				if (isVanished) flags += "V ";
				if (isAfk) flags += "AFK ";
				if (isGod) flags += "god ";
				if (isBusy) flags += "DND ";
				if (!isSurvival) flags += player.getGameMode().toString().substring(0, 3) + " ";
				if (player.isOp()) flags += "OP ";

				if (flags.length() > 0) playerOutput += Helpers.Parameters.replace(
						" [$1$2$3]",
						ChatColor.DARK_GRAY,
						flags.trim(),
						ChatColor.RESET);

				final long inactivity = System.currentTimeMillis() - this.playerSettings.getLastActivity(player);
				if (inactivity > (Helpers.DateTime.MillisecondsInASecond * 5))
				{
					playerOutput += Helpers.Parameters.replace(" <$1$2$3>",
							ChatColor.GRAY,
							Helpers.DateTime.getHumanReadableFromMillis(inactivity, true),
							ChatColor.RESET);
				}

				String distance = "";
				if (myLocation != null && !player.getName().equalsIgnoreCase(sender.getName()))
				{
					final Location otherLocation = player.getLocation();
					if (myLocation.getWorld().getName().equalsIgnoreCase(otherLocation.getWorld().getName()))
					{
						String altitudeDifference = "";
						final double altitudeDelta = myLocation.getY() - otherLocation.getY();
						if (altitudeDelta > 1) altitudeDifference = Helpers.Parameters.replace(" ($1m below)", (int)altitudeDelta);
						else if (altitudeDelta < -1) altitudeDifference = Helpers.Parameters.replace(" ($1m above)", Math.abs((int)altitudeDelta));

						otherLocation.setY(myLocation.getY());
						distance = ((int)otherLocation.distance(myLocation)) + "m ";
						final Vector delta = otherLocation.toVector().subtract(myLocation.toVector());
						distance += Helpers.getCompassDirection(Math.toDegrees(Math.atan2(delta.getX(), delta.getZ())));
						distance += altitudeDifference;
					}
				}

				ChatColor healthColor = ChatColor.GREEN;
				if (isDead) healthColor = ChatColor.DARK_RED;
				else if (isNearDeath) healthColor = ChatColor.RED;
				else if (health <= 60) healthColor = ChatColor.YELLOW;
				sender.sendInfoListItem(i, "$1 [$2$3%$4] $5", playerOutput, healthColor, health, ChatColor.RESET, distance);
			}
		}

		return true;
	}

	public static void ejectFromWorld(final PagedOutputCommandSender sender, final World world, final String message)
	{
		final World targetWorld = Bukkit.getWorlds().get(0);
		if (world.getName().equalsIgnoreCase(targetWorld.getName())) sender.sendFailure("You cannot eject players from $1.", world);
		else
		{
			final Location respawnLocation = targetWorld.getSpawnLocation();
			int ejectedCount = 0;
			int totalCount = 0;
			for (final Player player: world.getPlayers())
			{
				if (player.hasPermission("vpcore.moderator"))
					sender.sendFailure("You cannot eject $1 from $2.", player, world);
				else
				{
					if (player.teleport(respawnLocation, TeleportCause.PLUGIN))
					{
						sender.sendNotification("Ejected $1 from $2.", player, world);
						++ejectedCount;

						player.playSound(player.getLocation(), Sound.AMBIENCE_THUNDER, 10, 1);
						player.sendMessage(message);
					}
					else sender.sendFailure("Failed to eject $1 from $2.", player, world);
				}
				++totalCount;
			}

			sender.sendSuccess("Ejected $1 of $2 player$3 from $4.", ejectedCount, totalCount, Helpers.getPlural(totalCount), world);
		}
	}
}
