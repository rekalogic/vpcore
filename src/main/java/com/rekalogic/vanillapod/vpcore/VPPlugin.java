package com.rekalogic.vanillapod.vpcore;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.WeatherType;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;

import com.google.common.collect.ImmutableList;
import com.rekalogic.vanillapod.vpcore.animal.AnimalEventHandler;
import com.rekalogic.vanillapod.vpcore.animal.AnimalExecutor;
import com.rekalogic.vanillapod.vpcore.animal.AnimalSettings;
import com.rekalogic.vanillapod.vpcore.animal.AnimalSpawnTask;
import com.rekalogic.vanillapod.vpcore.animal.AnimalSpawnTaskData;
import com.rekalogic.vanillapod.vpcore.ban.BanEventHandler;
import com.rekalogic.vanillapod.vpcore.ban.BanExecutor;
import com.rekalogic.vanillapod.vpcore.ban.BanSettings;
import com.rekalogic.vanillapod.vpcore.book.BookEventHandler;
import com.rekalogic.vanillapod.vpcore.book.BookSettings;
import com.rekalogic.vanillapod.vpcore.bukkit.BukkitEventHandler;
import com.rekalogic.vanillapod.vpcore.bukkit.BukkitExecutor;
import com.rekalogic.vanillapod.vpcore.bukkit.BukkitSettings;
import com.rekalogic.vanillapod.vpcore.censor.CensorEventHandler;
import com.rekalogic.vanillapod.vpcore.censor.CensorSettings;
import com.rekalogic.vanillapod.vpcore.channel.ChannelEventHandler;
import com.rekalogic.vanillapod.vpcore.channel.ChannelExecutor;
import com.rekalogic.vanillapod.vpcore.channel.ChannelSettings;
import com.rekalogic.vanillapod.vpcore.chunk.ChunkEventHandler;
import com.rekalogic.vanillapod.vpcore.chunk.ChunkExecutor;
import com.rekalogic.vanillapod.vpcore.chunk.ChunkSettings;
import com.rekalogic.vanillapod.vpcore.command.CommandEventHandler;
import com.rekalogic.vanillapod.vpcore.command.CommandExecutor;
import com.rekalogic.vanillapod.vpcore.command.CommandSettings;
import com.rekalogic.vanillapod.vpcore.end.EndEventHandler;
import com.rekalogic.vanillapod.vpcore.end.EndExecutor;
import com.rekalogic.vanillapod.vpcore.end.EndResetTask;
import com.rekalogic.vanillapod.vpcore.end.EndSettings;
import com.rekalogic.vanillapod.vpcore.mail.MailEventHandler;
import com.rekalogic.vanillapod.vpcore.mail.MailExecutor;
import com.rekalogic.vanillapod.vpcore.mail.MailSettings;
import com.rekalogic.vanillapod.vpcore.news.NewsExecutor;
import com.rekalogic.vanillapod.vpcore.news.NewsSettings;
import com.rekalogic.vanillapod.vpcore.npc.NpcEventHandler;
import com.rekalogic.vanillapod.vpcore.npc.NpcExecutor;
import com.rekalogic.vanillapod.vpcore.npc.NpcSettings;
import com.rekalogic.vanillapod.vpcore.player.PlayerAbsenceTask;
import com.rekalogic.vanillapod.vpcore.player.PlayerAfkCheckTask;
import com.rekalogic.vanillapod.vpcore.player.PlayerEventHandler;
import com.rekalogic.vanillapod.vpcore.player.PlayerExecutor;
import com.rekalogic.vanillapod.vpcore.player.PlayerPromotionTask;
import com.rekalogic.vanillapod.vpcore.player.PlayerSettings;
import com.rekalogic.vanillapod.vpcore.plot.PlotEventHandler;
import com.rekalogic.vanillapod.vpcore.plot.PlotExecutor;
import com.rekalogic.vanillapod.vpcore.plot.PlotSettings;
import com.rekalogic.vanillapod.vpcore.region.RegionExecutor;
import com.rekalogic.vanillapod.vpcore.region.RegionSettings;
import com.rekalogic.vanillapod.vpcore.serverlist.ServerListEventHandler;
import com.rekalogic.vanillapod.vpcore.serverlist.ServerListExecutor;
import com.rekalogic.vanillapod.vpcore.serverlist.ServerListSettings;
import com.rekalogic.vanillapod.vpcore.sign.SignEventHandler;
import com.rekalogic.vanillapod.vpcore.sign.SignExecutor;
import com.rekalogic.vanillapod.vpcore.sign.SignSettings;
import com.rekalogic.vanillapod.vpcore.sleep.SleepEventHandler;
import com.rekalogic.vanillapod.vpcore.sleep.SleepExecutor;
import com.rekalogic.vanillapod.vpcore.sleep.SleepSettings;
import com.rekalogic.vanillapod.vpcore.util.EventHandlerRaw;
import com.rekalogic.vanillapod.vpcore.util.ExecutorBaseRaw;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.SettingsListener;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.rekalogic.vanillapod.vpcore.weather.WeatherEventHandler;
import com.rekalogic.vanillapod.vpcore.weather.WeatherExecutor;
import com.rekalogic.vanillapod.vpcore.weather.WeatherSettings;
import com.rekalogic.vanillapod.vpcore.zombie.ZombieEventHandler;
import com.rekalogic.vanillapod.vpcore.zombie.ZombieSettings;
import com.rekalogic.vanillapod.vpcore.zombie.ZombieTask;

public final class VPPlugin extends PluginBase implements SettingsListener
{
	private final List<SettingsBase> settingsList = new ArrayList<SettingsBase>();
	private final List<EventHandlerRaw> eventHandlerList = new ArrayList<EventHandlerRaw>();
	private final List<ExecutorBaseRaw> executorList = new ArrayList<ExecutorBaseRaw>();
	private AnimalSettings animalSettings = null;
	private BanSettings banSettings = null;
	private BookSettings bookSettings = null;
	private BukkitSettings bukkitSettings = null;
	private CensorSettings censorSettings = null;
	private ChannelSettings channelSettings = null;
	private ChunkSettings chunkSettings = null;
	private CommandSettings commandSettings = null;
	private EndSettings endSettings = null;
	private MailSettings mailSettings = null;
	private NewsSettings newsSettings = null;
	private NpcSettings npcSettings = null;
	private PlayerSettings playerSettings = null;
	private PlotSettings plotSettings = null;
	private RegionSettings regionSettings = null;
	private ServerListSettings serverListSettings = null;
	private SignSettings signSettings = null;
	private SleepSettings sleepSettings = null;
	private WeatherSettings weatherSettings = null;
	private ZombieSettings zombieSettings = null;

	private ZombieTask zombieTask = null;
	private final List<AnimalSpawnTask> animalSpawnTasks = new ArrayList<AnimalSpawnTask>();
	private PlayerAfkCheckTask afkCheckTask = null;
	private PlayerAbsenceTask absenceTask = null;
	private PlayerPromotionTask promotionTask = null;
	private final List<EndResetTask> endResetTasks = new ArrayList<EndResetTask>();

	private World mainWorld = null;
	
	private boolean isLiveServer = false;
	private String mailHost = "";
	private int mailPort = 0;
	private String mailFrom = "";
	
	public final boolean isLiveServer() { return this.isLiveServer; }
	public final String getMailHost() { return this.mailHost; }
	public final int getMailPort() { return this.mailPort; }
	public final String getMailFrom() { return this.mailFrom; }

	@Override
	public void onEnable()
	{
		this.mainWorld = Bukkit.getWorlds().get(0);
		
		String hostName = "localhost";
		try { hostName = InetAddress.getLocalHost().getHostName(); }
		catch (final Exception ex) {}

		super.setHostname(hostName);
		super.info("Local system name is: $1.", super.getHostName());

		super.setupFunctionalityProviders();

		this.animalSettings = new AnimalSettings(this);
		this.banSettings = new BanSettings(this);
		this.bookSettings = new BookSettings(this);
		this.bukkitSettings = new BukkitSettings(this);
		this.censorSettings = new CensorSettings(this);
		this.channelSettings = new ChannelSettings(this);
		this.chunkSettings = new ChunkSettings(this);
		this.commandSettings = new CommandSettings(this);
		this.endSettings = new EndSettings(this);
		this.mailSettings = new MailSettings(this);
		this.newsSettings = new NewsSettings(this);
		this.npcSettings = new NpcSettings(this);
		this.playerSettings = new PlayerSettings(this);
		this.plotSettings = new PlotSettings(this);
		this.regionSettings = new RegionSettings(this);
		this.serverListSettings = new ServerListSettings(this);
		this.signSettings = new SignSettings(this);
		this.sleepSettings = new SleepSettings(this);
		this.weatherSettings = new WeatherSettings(this);
		this.zombieSettings = new ZombieSettings(this);
		
		this.censorSettings.setBanSettings(this.banSettings);
		this.censorSettings.setPlayerSettings(this.playerSettings);
		this.playerSettings.setPlotSettings(this.plotSettings);
		this.playerSettings.setSignSettings(this.signSettings);
		this.plotSettings.setSignSettings(this.signSettings);
		this.signSettings.setPlayerSettings(this.playerSettings);
		this.signSettings.setPlotSettings(this.plotSettings);

		this.settingsList.add(this.animalSettings);
		this.settingsList.add(this.banSettings);
		this.settingsList.add(this.bookSettings);
		this.settingsList.add(this.bukkitSettings);
		this.settingsList.add(this.censorSettings);
		this.settingsList.add(this.channelSettings);
		this.settingsList.add(this.chunkSettings);
		this.settingsList.add(this.commandSettings);
		this.settingsList.add(this.endSettings);
		this.settingsList.add(this.mailSettings);
		this.settingsList.add(this.newsSettings);
		this.settingsList.add(this.playerSettings);
		this.settingsList.add(this.plotSettings);
		this.settingsList.add(this.regionSettings);
		this.settingsList.add(this.serverListSettings);
		this.settingsList.add(this.signSettings);
		this.settingsList.add(this.sleepSettings);
		this.settingsList.add(this.npcSettings);
		this.settingsList.add(this.weatherSettings);
		this.settingsList.add(this.zombieSettings);

		for (final SettingsBase settings: this.settingsList) settings.addListener(this);
		
		this.eventHandlerList.add(new MiscEventHandler(this, this.mainWorld));
		this.eventHandlerList.add(new VanishEventHandler(this, this.playerSettings));
		
		this.eventHandlerList.add(new AnimalEventHandler(this, this.animalSettings, this.playerSettings));
		this.eventHandlerList.add(new BanEventHandler(this, this.banSettings));
		this.eventHandlerList.add(new BookEventHandler(this, this.bookSettings));
		this.eventHandlerList.add(new BukkitEventHandler(this, this.bukkitSettings));
		this.eventHandlerList.add(new CensorEventHandler(this, this.censorSettings));
		this.eventHandlerList.add(new ChannelEventHandler(this, this.channelSettings));
		this.eventHandlerList.add(new ChunkEventHandler(this, this.chunkSettings));
		this.eventHandlerList.add(new CommandEventHandler(this, this.commandSettings));
		this.eventHandlerList.add(new EndEventHandler(this, this.endSettings));
		this.eventHandlerList.add(new MailEventHandler(this, this.mailSettings));
		this.eventHandlerList.add(new NpcEventHandler(this, this.npcSettings));
		this.eventHandlerList.add(new PlayerEventHandler(this, this.playerSettings));
		this.eventHandlerList.add(new PlotEventHandler(this, this.plotSettings));
		this.eventHandlerList.add(new ServerListEventHandler(this, this.serverListSettings));
		this.eventHandlerList.add(new SignEventHandler(this, this.signSettings, this.playerSettings, this.plotSettings));
		this.eventHandlerList.add(new SleepEventHandler(this, this.sleepSettings));
		this.eventHandlerList.add(new WeatherEventHandler(this, this.weatherSettings));
		this.eventHandlerList.add(new ZombieEventHandler(this, this.zombieSettings));
		
		this.executorList.add(new AnimalExecutor(this, this.animalSettings));
		this.executorList.add(new BanExecutor(this, this.banSettings));
		this.executorList.add(new BukkitExecutor(
				this,
				this.bukkitSettings,
				this.serverListSettings,
				this.commandSettings));
		this.executorList.add(new ChunkExecutor(this, this.chunkSettings));
		this.executorList.add(new ChannelExecutor(this, this.channelSettings));
		this.executorList.add(new CommandExecutor(
				this,
				this.commandSettings,
				this.serverListSettings,
				this.chunkSettings,
				this.playerSettings,
				this.npcSettings,
				this.banSettings,
				this.plotSettings));
		this.executorList.add(new EndExecutor(this, this.endSettings));
		this.executorList.add(new MailExecutor(this, this.mailSettings, this.banSettings));
		this.executorList.add(new NewsExecutor(this, this.newsSettings));
		this.executorList.add(new NpcExecutor(this, this.npcSettings));
		this.executorList.add(new PlayerExecutor(this, this.playerSettings, this.censorSettings));
		this.executorList.add(new PlotExecutor(this, this.plotSettings, this.banSettings));
		this.executorList.add(new RegionExecutor(this, this.regionSettings, this.playerSettings));
		this.executorList.add(new ServerListExecutor(this, this.serverListSettings));
		this.executorList.add(new SignExecutor(this, this.signSettings));
		this.executorList.add(new SleepExecutor(this, this.sleepSettings));
		this.executorList.add(new WeatherExecutor(this, this.weatherSettings));
		
		this.loadAllSettings(true);
		
		this.postEnableSetup();
				
		super.onEnable();
	};

	private void postEnableSetup()
	{
		this.playerSettings.closeUnfinishedSessions();

		if (this.serverListSettings.isEnabled() && this.serverListSettings.isStartupLockEnabled())
		{
			this.serverListSettings.lockServer(
					PagedOutputCommandSender.ConsoleInstance,
					this.serverListSettings.getStartupLockMessage(),
					this.serverListSettings.getStartupLockDuration());
		}
	}
	
	private void configureTasksAndSettings()
	{
		if (this.zombieTask != null)
		{
			this.zombieTask.cancel();
			this.zombieTask = null;
		}

		if (this.zombieSettings.isEnabled())
		{
			super.debug("Configuring zombie control task.");
			final long period = 3 * 20;
			this.zombieTask = new ZombieTask(this.zombieSettings);
			this.zombieTask.runTaskTimer(this, period, period);
		}

		for (AnimalSpawnTask task: this.animalSpawnTasks) task.cancel();
		this.animalSpawnTasks.clear();
		
		if (this.animalSettings.isSpawningEnabled())
		{
			int enabledTaskCount = 0;
			final ImmutableList<AnimalSpawnTaskData> tasks = this.animalSettings.getTaskData();
			for (final AnimalSpawnTaskData taskData: tasks)
			{
				if (taskData.isEnabled())
				{
					super.debug("Configuring animal spawning task '$1'.", taskData.getName());

					final long period = Helpers.DateTime.getTicksFromMillis(taskData.getFrequency());
					final AnimalSpawnTask task = new AnimalSpawnTask(this.animalSettings, taskData, super.getRandom());
					this.animalSpawnTasks.add(task);
					task.runTaskTimer(this, period, period);
					++enabledTaskCount;
				} else super.debug("Animal spawning task '$1' is disabled.", taskData.getName());
			}
			if (tasks.size() > 0) super.debug("Enabled spawn tasks: $1/$2", enabledTaskCount, tasks);
			else super.debug("No animal spawn tasks are configured.");
		}

		if (this.afkCheckTask != null)
		{
			this.afkCheckTask.cancel();
			this.afkCheckTask = null;
		}

		if (this.absenceTask != null)
		{
			this.absenceTask.cancel();
			this.absenceTask = null;
		}
		
		if (this.promotionTask != null)
		{
			this.promotionTask.cancel();
			this.promotionTask = null;
		}

		if (this.playerSettings.isEnabled())
		{
			super.debug("Configuring AFK check task.");
			final long period = Helpers.DateTime.getTicksFromMillis(this.playerSettings.getAfkCheckPeriod());
			this.afkCheckTask = new PlayerAfkCheckTask(this.playerSettings);
			this.afkCheckTask.runTaskTimer(this, period, period);
			
			if (this.playerSettings.isAbsenceJobEnabled())
			{
				super.debug("Configuring absence task.");
				final long absencePeriod = Helpers.DateTime.getTicksFromMillis(this.playerSettings.getAbsenceJobPeriod());
				this.absenceTask = new PlayerAbsenceTask(this, this.playerSettings, this.plotSettings, this.regionSettings);
				this.absenceTask.runTaskTimerAsynchronously(this, 10, absencePeriod);
			}
			
			if (this.playerSettings.isPromotionJobEnabled())
			{
				super.debug("Configuring promotion task.");
				final long promotionPeriod = Helpers.DateTime.getTicksFromMillis(this.playerSettings.getPromotionJobPeriod());
				this.promotionTask = new PlayerPromotionTask(this, this.playerSettings);
				this.promotionTask.runTaskTimerAsynchronously(this, 1, promotionPeriod);
			}
		}
		
		for (final EndResetTask task: this.endResetTasks) task.cancel();
		this.endResetTasks.clear();
		
		final long now = System.currentTimeMillis();
		for (final World world: Bukkit.getWorlds())
		{
			if (this.weatherSettings.isEnabled() && world.getEnvironment() == Environment.NORMAL)
			{
				final WeatherType currentWeather = WeatherSettings.getThisWeather(world);
				final int maxDuration = (int)(this.weatherSettings.getMaxWeatherDuration(currentWeather) / 1000) * 20;
				final int remainingDuration = world.getWeatherDuration();

				if (remainingDuration > maxDuration)
				{
					super.debug(
							"World $1 was set to be $2 for another $3; setting to the maximum $4.",
							world,
							WeatherSettings.getWeatherName(currentWeather),
							Helpers.DateTime.getHumanReadableFromTicks(remainingDuration),
							Helpers.DateTime.getHumanReadableFromTicks(maxDuration));
					world.setWeatherDuration(maxDuration);
				}
			}

			if (this.endSettings.isEnabled() && world.getEnvironment() == Environment.THE_END)
			{
				final long resetTime = this.endSettings.getWorldResetTime(world);
				if (resetTime > 0)
				{
					super.info("World $1 was scheduled to be reset at $2 (now=$3)", world, resetTime, now);

					final int resetDue = (int)(resetTime - now);
					if (resetDue >= 0) super.info("World $1 is scheduled to be reset in $2.", world, Helpers.DateTime.getHumanReadableFromMillis(resetDue));
					else super.info("World $1 was scheduled to be reset $2 ago.", world, Helpers.DateTime.getHumanReadableFromMillis(0 - resetDue));
					
					int actualReset = resetDue;
					if (resetDue < 5) actualReset = 5 * Helpers.DateTime.MillisecondsInASecond;

					super.info("World $1 will be reset in $2.", world, Helpers.DateTime.getHumanReadableFromMillis(actualReset));

					final EndResetTask task = new EndResetTask(this.endSettings, world);
					this.endResetTasks.add(task);
					task.runTaskLater(this, Helpers.DateTime.getTicksFromMillis(actualReset));
				}
			}
		}
		
		final TaskBase permissionsFinaliseTask = new TaskBase(super.isDebug(), "FinalisePermissions")
		{
			@Override
			protected void runTask()
			{
				getPermissionManager().finalisePermissions();
				super.info("Permissions recalculated.");
				
				for (final Player player: Bukkit.getOnlinePlayers())
					playerSettings.setGroup(player, true);
			}
		};
		permissionsFinaliseTask.runTask(this);
	}
	
	public void flushCaches()
	{
		this.chunkSettings.flushCache();
	}

	@Override
	public void onDisable()
	{
		this.playerSettings.closeUnfinishedSessions();

		if (this.animalSettings.isEnabled() && this.chunkSettings.isEnabled())
		{
			super.debug("Storing owned animal locations.");

			int animalCount = 0;
			for (final World world: Bukkit.getWorlds())
			{
				for (final Entity entity: world.getEntities())
				{
					if (entity instanceof Tameable)
					{
						if (((Tameable)entity).getOwner() != null)
						{
							this.animalSettings.rememberAnimalLocation(entity.getLocation());
							++animalCount;
						}
					}
				}
			}
			this.animalSettings.saveDataStore();
			super.debug("Stored $1 animal location$2.", animalCount, Helpers.getPlural(animalCount));
		}

		super.info("VPCore has been disabled.");
	}

	private FileConfiguration loadGlobalConfig(final boolean initialChecks)
	{
		super.saveDefaultConfig();

		super.reloadConfig();
		final FileConfiguration config = this.getConfig();
				
		super.setDebug(config.getBoolean("debug"));		
		super.setPageSize(config.getInt("page-size"));
		
		super.debug("Debug output enabled.");

		this.isLiveServer = config.getBoolean("live");
		this.mailHost = config.getString("mail-host");
		this.mailPort = config.getInt("mail-port");
		this.mailFrom = config.getString("mail-from");
		
		final String debugColorString = config.getString("log.color.debug");
		final String debugParameterColorString = config.getString("log.color.debug-parameter");
		
		ChatColor debugColor = ChatColor.DARK_GRAY;
		try { debugColor = ChatColor.valueOf(debugColorString); }
		catch (Exception ex)
		{
			super.severe("Invalid colour for $1: $2", "log.color.debug", debugColorString);
		}
		Helpers.setDebugColor(debugColor);
		super.info(
				"Debug colour: $1, debug parameter colour: $2",
				Helpers.getDebugColor().name(),
				Helpers.getDebugParameterColor().name());
		super.debug("This is debug output, and >$1< is a parameter.", "this");
		
		ChatColor debugParameterColor = ChatColor.GRAY;
		try { debugParameterColor = ChatColor.valueOf(debugParameterColorString); }
		catch (Exception ex)
		{
			super.severe("Invalid colour for $1: $2", "log.color.debug-parameter", debugParameterColorString);
		}
		Helpers.setDebugParameterColor(debugParameterColor);
		
		super.InitialiseConnectionFactory(
				config.getBoolean("db.debug"),
				config.getString("db.host"),
				config.getInt("db.port"),
				config.getString("db.database"),
				config.getString("db.username"),
				config.getString("db.password"));
		
		if (initialChecks) super.checkDatabase();

		return config;
	}

	@Override
	public void onSettingsSaved()
	{
		super.saveConfig();
	}

	@Override
	public void loadAllSettings() { this.loadAllSettings(false); }	
	public void loadAllSettings(final boolean initialChecks)
	{
		final FileConfiguration config = this.loadGlobalConfig(initialChecks);

		super.initialisePlayerCache(config.getBoolean("enable-player-cache"));
		
		super.info("$1== Loading settings and data ==", ChatColor.YELLOW);
		for (final SettingsBase settings: this.settingsList) settings.load(super.isDebug(), config);
		super.info("$1== Settings summary ==", ChatColor.YELLOW);
		for (final SettingsBase settings: this.settingsList) settings.emitSettings();
		for (final SettingsBase settings: this.settingsList) settings.emitInfo();		

		for (final EventHandlerRaw eventHandler: this.eventHandlerList) eventHandler.register();
		for (final ExecutorBaseRaw executor: this.executorList) executor.updateDebug();
		
		this.configureTasksAndSettings();
	}
}