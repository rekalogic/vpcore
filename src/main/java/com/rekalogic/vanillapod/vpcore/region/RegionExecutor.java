package com.rekalogic.vanillapod.vpcore.region;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;

import com.rekalogic.vanillapod.vpcore.player.PlayerSettings;
import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.md_5.bungee.api.ChatColor;

public final class RegionExecutor extends ExecutorBase<RegionSettings>
{
	final PlayerSettings playerSettings;
	
	public RegionExecutor(
			final PluginBase plugin,
			final RegionSettings settings,
			final PlayerSettings playerSettings)
	{
		super(plugin, settings, "regioncull", "regiondelete", "regionlist");
		
		this.playerSettings = playerSettings;
	}
	
	@Override
	protected boolean checkCommandAllowed(final String command, final boolean isConsole)
	{
		return super.checkCommandAllowed(command, isConsole);
	}
	
	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "regiondelete": return (argCount == 1);
		case "regionlist": return (argCount == 1);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "regioncull": return new String[] { ExecutorBase.FLAG_BOOL_CONFIRM };
		case "regiondelete": return new String[] { ExecutorBase.FLAG_BOOL_CONFIRM };
		}
		
		return super.getAllowedFlags(command);
	}

	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "regiondelete":
		case "regionlist":
			switch (argIndex)
			{
			case 0: return LookupSource.OFFLINE_PLAYER;				
			}
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}
	
	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		if (!super.getPlugin().isWorldGuardEnabled())
		{
			sender.sendFailure("WorldGuard is not available.");
			return true;
		}

		switch (command)
		{
		case "regioncull": return this.doRegionCull(sender, args);
		case "regiondelete": return this.doRegionDelete(sender, args);
		case "regionlist": return this.doRegionList(sender, args);
		}
		
		return super.onCommand(sender, command, label, args);
	}

	private boolean doRegionCull(PagedOutputCommandSender sender, String[] args)
	{
		final WorldGuardPlugin worldGuard = super.getPlugin().getWorldGuard();
		final boolean confirm = super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM);
		
		int deletedCount = 0;
		for (final World world: Bukkit.getWorlds())
		{
			final RegionManager manager = worldGuard.getRegionManager(world);
			if (manager != null)
			{
				for (final ProtectedRegion region: manager.getRegions().values())
				{
					final String regionId = region.getId();

					if (region.getOwners().size() == 0 &&
							super.getSettings().isRegionOrParentDeletable(region))
					{
						sender.sendInfo("$1 has no owners.", regionId);
						if (confirm)
						{
							manager.removeRegion(regionId);
							sender.sendSuccess("Region $1 deleted.", regionId);
						}
						++deletedCount;
					}
				}
			}
		}
		
		if (deletedCount == 0)
		{
			sender.sendSuccess("No regions needed deletion.");
			super.setConfirmFlag();
		}
		else sender.sendSuccess("Deleted $1 unowned region$2.", deletedCount, Helpers.getPlural(deletedCount));
		
		return true;
	}

	private boolean doRegionList(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;

		final WorldGuardPlugin worldGuard = super.getPlugin().getWorldGuard();
		final LocalPlayer localPlayer = worldGuard.wrapOfflinePlayer(player); 
		
		int totalCount = 0;
		int ownedCount = 0;
		for (final World world: Bukkit.getWorlds())
		{
			boolean isHeaderShown = false;
			int regionIndex = 0;
			final RegionManager manager = worldGuard.getRegionManager(world);
			if (manager != null)
			{
				for (final ProtectedRegion region: manager.getRegions().values())
				{
					final DefaultDomain owners = region.getOwners();
					boolean isOwner = owners.contains(localPlayer);
					boolean isMember = region.getMembers().contains(localPlayer);

					if (isOwner || isMember)
					{
						if (!isHeaderShown)
						{
							sender.sendInfo("=== $1 ===", world);
							isHeaderShown = true;
						}
						
						++totalCount;
					}

					if (isOwner)
					{
						String type = ChatColor.GREEN + "sole owner";
						if (owners.size() > 1) type = ChatColor.GRAY + "co-owner";
						
						sender.sendInfoListItem(regionIndex, "$1 $2", region.getId(), type);
						++regionIndex;
						++ownedCount;
					}
					else if (isMember)
					{
						sender.sendInfoListItem(
								regionIndex,
								"$1 $2member",
								region.getId(),
								ChatColor.DARK_GREEN);
						++regionIndex;						
					}
				}
			}
		}
		
		sender.sendSuccess(
				"$1 owns $2 region$3 and is a member in $4.",
				player,
				ownedCount,
				Helpers.getPlural(ownedCount),
				totalCount - ownedCount);
		
		return true;
	}

	private boolean doRegionDelete(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;

		final WorldGuardPlugin worldGuard = super.getPlugin().getWorldGuard();
		final LocalPlayer localPlayer = worldGuard.wrapOfflinePlayer(player); 
		final boolean confirm = super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM);
		
		if (this.playerSettings.isPlotDeletionExempt(player))
		{
			sender.sendFailure("$1 is exempt from plot deletion.", player);
			super.setConfirmFlag();
			return true;
		}
		
		int totalCount = 0;
		int deletedCount = 0;
		for (final World world: Bukkit.getWorlds())
		{
			final RegionManager manager = worldGuard.getRegionManager(world);
			if (manager != null)
			{
				for (final ProtectedRegion region: manager.getRegions().values())
				{
					final String regionId = region.getId();
					final DefaultDomain owners = region.getOwners();
					boolean isOwner = owners.contains(localPlayer);
					boolean isMember = region.getMembers().contains(localPlayer);
					
					if (isOwner || isMember) ++totalCount;

					if (isOwner)
					{
						sender.sendInfo("$1 is an owner of $2.", player, regionId);
						if (confirm)
						{
							owners.removePlayer(localPlayer);
							if (owners.size() == 0)
							{
								if (this.getSettings().isRegionOrParentDeletable(region))
								{
									manager.removeRegion(regionId);
									sender.sendSuccess("Region $1 deleted (no owners).", regionId);
									++deletedCount;
								}
								else sender.sendFailure("$1 has no owners but is undeletable.", regionId);
							}
						}
					}
					else if (isMember)
					{
						sender.sendInfo("$1 is a member of $2.", player, regionId);
						if (confirm)
						{
							region.getMembers().removePlayer(localPlayer);
							sender.sendSuccess("$1 removed from members of region $2.", player, regionId);							
						}
					}
				}
			}
		}
		
		if (totalCount == 0)
		{
			sender.sendSuccess("$1 is associated with no regions.", player);
			super.setConfirmFlag();
		}
		else sender.sendSuccess("Deleted $1 region$2 for $3.", deletedCount, Helpers.getPlural(deletedCount), player);
		
		return true;
	}
}
