package com.rekalogic.vanillapod.vpcore.region;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionType;

public final class RegionSettings extends SettingsBase
{
	private Set<String> undeletableRegions = null;

	public RegionSettings(VPPlugin plugin)
	{
		super(plugin);
	}

	@Override
	protected void onLoad()
	{
		if (!super.getPlugin().isWorldGuardEnabled())
			this.setEnabled(false);

		final List<String> undeletableRegionsList = super.getStringList("undeletable-regions");
		this.undeletableRegions = new HashSet<String>();
		for (final String regionName: undeletableRegionsList)
			this.undeletableRegions.add(regionName.toLowerCase());
	}

	@Override
	protected void onSave()
	{
		super.set("undeletable-regions", this.undeletableRegions);
	}
	
	@Override
	protected void onEmitInfo()
	{
		if (this.undeletableRegions.size() == 0) super.info("No regions are exempt from deletion.");
		else
		{
			super.info("The following regions (and their children) will never be deleted:");
			for (final String regionName: this.undeletableRegions)
				super.info("    $1", regionName);
		}
	}
	
	public boolean isRegionOrParentDeletable(final ProtectedRegion region)
	{
		boolean isDeletable = false;
		if (region != null)
		{
			ProtectedRegion testRegion = region;
			isDeletable = (testRegion.getType() != RegionType.GLOBAL);
			while (testRegion != null && isDeletable)
			{
				final String regionName = testRegion.getId().toLowerCase();
				if (this.undeletableRegions.contains(regionName))
				{
					isDeletable = false;
					break;
				}
				else testRegion = testRegion.getParent();
			}
		}
		return isDeletable;
	}	
}
