package com.rekalogic.vanillapod.vpcore.mail;

import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import com.rekalogic.vanillapod.vpcore.ban.BanInfo;
import com.rekalogic.vanillapod.vpcore.ban.BanSettings;
import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class MailExecutor extends ExecutorBase<MailSettings>
{
	private final BanSettings banSettings;

	public MailExecutor(final PluginBase plugin, final MailSettings settings, final BanSettings banSettings)
	{
		super(plugin, settings, "mail");

		this.banSettings = banSettings;
	}

	@Override
	protected boolean checkCommandAllowed(final String command, final boolean isConsole)
	{
		switch (command)
		{
		case "mail": return true;
		}

		return super.checkCommandAllowed(command, isConsole);
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "mail": return true;
		}

		return super.checkParameters(command, argCount);
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "mail": return new String[] { ExecutorBase.FLAG_PLAYER };
		}
		
		return super.getAllowedFlags(command);
	}

	@Override
	protected boolean outputCustomUsage(final CommandSender sender, final String command, final String label)
	{
		switch(command)
		{
		case "mail":
			super.outputCustomUsageDivider(sender, "General commands");
			super.outputCustomUsageLine(sender, label, "$1", "view your inbox");
			super.outputCustomUsageLine(sender, label, "$1 read", "read the next unread mail");
			super.outputCustomUsageLine(sender, label, "$1 read <n>", "read mail 'n'");
			super.outputCustomUsageLine(sender, label, "$1 delete <n>", "delete mail 'n'");
			super.outputCustomUsageDivider(sender, "Quick commands");
			super.outputCustomUsageLine(sender, label, "$1 send <player> <message>", "send a quick mail");
			super.outputCustomUsageLine(sender, label, "$1 reply <n> <message>", "send a quick reply to mail 'n'");
			super.outputCustomUsageDivider(sender, "Advanced commands");
			super.outputCustomUsageLine(sender, label, "$1 new <player>", "begin a new draft mail");
			super.outputCustomUsageLine(sender, label, "$1 reply <n>", "draft a reply to mail 'n'");
			super.outputCustomUsageLine(sender, label, "$1 subject <text>", "set the draft's subject");
			super.outputCustomUsageLine(sender, label, "$1 body <text>", "add to the draft's body");
			super.outputCustomUsageLine(sender, label, "$1 draft", "view your current draft");
			super.outputCustomUsageLine(sender, label, "$1 discard", "discard the current draft");
			super.outputCustomUsageLine(sender, label, "$1 send", "send the current draft");
			break;
		default: return false;
		}

		return true;
	}
	
	@Override
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "mail":
			switch (argIndex)
			{
			case 0: return Arrays.asList("body", "delete", "discard", "draft", "new", "subject", "read", "reply", "send");
			}
		}
		
		return super.onTabComplete(sender, command, argIndex, args);
	}
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "mail":
			switch (argIndex)
			{
			case 0: return LookupSource.FROM_PLUGIN;
			case 1:
				switch (args[0])
				{
				case "new": return LookupSource.OFFLINE_PLAYER;
				case "send":  return LookupSource.OFFLINE_PLAYER;
				}
				break;
			}
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		switch (command)
		{
		case "mail": return this.doMail(sender, args);
		}

		return super.onCommand(sender, command, label, args);
	}

	private boolean doMail(final PagedOutputCommandSender sender, final String[] args)
	{
		final boolean isAdmin = sender.hasPermission("vpcore.admin");
		if (args.length == 0)
		{
			if (super.hasFlag(ExecutorBase.FLAG_PLAYER) && !isAdmin)
				return false;

			return this.doListMail(sender);
		}
		else
		{
			String subCommand = args[0].toLowerCase();
			
			switch (subCommand)
			{
			case"del": subCommand = "delete"; break;
			}

			if (super.hasFlag(ExecutorBase.FLAG_PLAYER))
			{
				switch (subCommand)
				{
				case "read":
					if (!isAdmin) return false;
					break;
				
				default: return false;
				}
			}
			
			switch (subCommand)
			{
			case "new": return this.doMailPlayer(sender, args);
			case "body": return this.doMailBody(sender, args);
			case "delete": return this.doDeleteMail(sender, args);
			case "discard": return this.doDiscardMail(sender, args);
			case "draft": return this.doMailDraft(sender, args);
			case "read": return this.doReadMail(sender, args);
			case "reply": return this.doReplyMail(sender, args);
			case "send": return this.doSendMail(sender, args);
			case "subject": return this.doMailSubject(sender, args);

			default: return false;
			}
		}
	}
	
	private boolean doMailDraft(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length != 1) return false;

		Mail draft = null;
		if (sender.isConsole()) draft = super.getSettings().getServerDraft();
		else draft = super.getSettings().getDraft(sender.getPlayer());

		if (draft == null) sender.sendFailure("You have no draft to view.");
		else
		{
			OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(draft.getRecipient());
			sender.sendSuccess("You are composing a draft email:");
			sender.sendSuccess("Recipient: $1", player);
			sender.sendSuccess("Subject: $1", draft.getSubject());
			sender.sendSuccess("Body: $1", draft.getBody());
		}

		return true;
	}

	private boolean doReplyMail(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length < 2) return false;
		if (sender.isConsole())
		{
			sender.sendFailure("CONSOLE cannot receive (and therefore cannot reply to) mail.");
			return true;
		}

		final Mail mail = this.getMailById(sender, args[1]);
		if (mail == null) return true;

		if (mail.isFromServer())
		{
			sender.sendFailure("You cannot reply to mail from $1.", super.getSettings().getServerAlias());
			return true;
		}

		final OfflinePlayer recipient = Helpers.Bukkit.getOfflinePlayer(mail.getSender());
		if (args.length == 2)
		{
			final Mail reply = super.getSettings().createDraft(sender.getPlayer(), recipient);
			reply.setReplySubject(mail);
			sender.sendSuccess("Draft reply to $1 created.", recipient);
		}
		else
		{
			final Mail reply = new Mail(sender.getPlayer().getUniqueId(), recipient.getUniqueId());
			reply.setReplySubject(mail);
			reply.addToBody(Helpers.Args.concatenate(args, 2));

			super.getSettings().sendMail(reply);
			sender.sendSuccess("Mail sent.");
		}

		return true;
	}
	
	private boolean doDiscardMail(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length != 1) return false;

		Mail draft = null;
		if (sender.isConsole()) draft = super.getSettings().getServerDraft();
		else draft = super.getSettings().getDraft(sender.getPlayer());

		if (draft == null) sender.sendFailure("You have no draft to discard.");
		else
		{
			if (sender.isConsole()) super.getSettings().discardServerDraft();
			else super.getSettings().discardDraft(sender.getPlayer());
			sender.sendSuccess("Draft discarded.");
		}

		return true;
	}

	private boolean doSendMail(final PagedOutputCommandSender sender, final String[] args)
	{
		Mail mail = null;
		if (sender.isConsole()) mail = super.getSettings().getServerDraft();
		else mail = super.getSettings().getDraft(sender.getPlayer());
		
		if (args.length == 1)
		{
			if (mail == null)
			{
				sender.sendFailure("You don't have anything to send.  See $1.", "/mail help");
				return true;
			}
		}
		else if (args.length >= 2)
		{
			if (mail == null)
			{
				final OfflinePlayer recipient = super.getPlugin().getOfflinePlayer(sender, args[1]);
				if (recipient == null) return true;
				if (sender.isConsole()) mail = new Mail(recipient.getUniqueId());
				else mail = new Mail(sender.getPlayer().getUniqueId(), recipient.getUniqueId());
				mail.addToBody(Helpers.Args.concatenate(args, 2));
			}
			else
			{
				sender.sendFailure("You cannot quick-send as you already have a draft in progress.  See $1.", "/mail help");
				return true;
			}
		}

		if (!mail.hasSubjectOrBody())
		{
			sender.sendFailure("Message cannot be sent without subject or body.");
			return true;
		}
		
		super.getSettings().sendMail(mail);
		sender.sendSuccess("Mail sent.");

		return true;
	}

	private Mail getMailById(final PagedOutputCommandSender sender, final String idString)
	{
		Mail mail = null;

		int id = 0;
		try { id = Integer.parseInt(idString); }
		catch (final NumberFormatException e)
		{
			sender.sendFailure("Invalid message ID.");
			return null;
		}

		final OfflinePlayer player = super.getOfflinePlayer(sender);
		if (player == null) return null;

		mail = super.getSettings().hasMail(player, id);
		if (mail == null) sender.sendFailure("No such mail in your inbox.");

		return mail;
	}

	private boolean doReadMail(final PagedOutputCommandSender sender, final String[] args)
	{
		OfflinePlayer player = super.getOfflinePlayer(sender);
		if (player == null) return true;
		
		final boolean isReadingOwnMail = sender.isPlayer() && sender.getPlayer().equals(player);
		
		Mail mail = null;
		int unreadCount = 0;
		if (args.length == 1)
		{
			final List<Mail> mailbox = super.getSettings().getMailbox(player);
			for (final Mail mailboxMail: mailbox)
			{
				if (mailboxMail.isUnread())
				{
					if (mail == null) mail = mailboxMail;
					++unreadCount;
				}
			}
			
			if (unreadCount == 0) sender.sendFailure("You have no unread mail. Use $1 to see all mail.", "/mail");
		}
		else if (args.length == 2) mail = this.getMailById(sender, args[1]);
		else return false;
		
		if (mail == null) return true;

		this.showMail(sender, mail, isReadingOwnMail);

		if (unreadCount == 1) sender.sendSuccess("No more unread mail."); 
		else if (unreadCount > 1)
		{
			--unreadCount;
			sender.sendSuccess("You have $1 more unread mail$2.", unreadCount, Helpers.getPlural(unreadCount));
		}
		
		return true;
	}
	
	private boolean showMail(final PagedOutputCommandSender sender, final Mail mail, final boolean markAsRead)
	{
		String senderName = "";
		if (mail.isFromServer()) senderName = super.getSettings().getServerAlias();
		else
		{
			final OfflinePlayer fromPlayer = Helpers.Bukkit.getOfflinePlayer(mail.getSender());
			if (fromPlayer.isOnline()) senderName = fromPlayer.getPlayer().getDisplayName();
			else senderName = fromPlayer.getName();
		}

		final long sentAgo = System.currentTimeMillis() - mail.getSent();
		sender.sendNotification("Mail #$1 from: $2 [$3 ago]",
				mail.getId(),
				senderName,
				Helpers.DateTime.getHumanReadableFromMillis(sentAgo, (sentAgo > Helpers.DateTime.MillisecondsInASecond)));
		sender.sendNotification("Subject: $1", mail.getSubject());
		sender.sendNotification("Body: $1", mail.getBody());
		
		if (markAsRead) super.getSettings().readMail(mail);
		
		return true;
	}

	private boolean doDeleteMail(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length != 2) return false;

		final Mail mail = this.getMailById(sender, args[1]);
		if (mail == null) return true;

		super.getSettings().deleteMail(mail);
		sender.sendSuccess("Message $1 deleted.", mail.getId());

		return true;
	}

	private boolean doMailSubject(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length == 1) return false;
		final String subject = Helpers.Args.concatenate(args, 1);

		final Mail draft = this.getDraft(sender);
		if (draft == null) return true;

		if (subject.trim().length() == 0) sender.sendFailure("Cannot specify an empty message subject.");
		if (subject.length() + draft.getBody().length() > 100) sender.sendFailure("Message subject too long; max 100 characters.");
		else
		{
			draft.setSubject(subject);
			sender.sendSuccess("Message subject set.");
		}

		return true;
	}

	private boolean doMailBody(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length == 1) return false;
		final String bodyLine = Helpers.Args.concatenate(args, 1);

		final Mail draft = this.getDraft(sender);
		if (draft == null) return true;

		if (bodyLine.trim().length() == 0) sender.sendFailure("Cannot add empty string to message body.");
		if (bodyLine.length() + draft.getBody().length() > 1000) sender.sendFailure("Message body too long; max 1000 characters.");
		else
		{
			draft.addToBody(bodyLine);
			sender.sendSuccess("Added to message body.");
		}

		return true;
	}

	private boolean doListMail(final PagedOutputCommandSender sender)
	{
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER) && !sender.hasPermission("vpcore.admin"))
			return false;
		
		final OfflinePlayer player = super.getOfflinePlayer(sender);
		if (player == null) return true;

		final List<Mail> mailbox = super.getSettings().getMailbox(player);
		if (mailbox == null || mailbox.isEmpty()) sender.sendNotification("No mail in your inbox.");
		else
		{
			int unreadCount = 0;
			for (final Mail mail: mailbox)
			{
				if (mail.isUnread()) ++unreadCount;
			}
			
			final long now = System.currentTimeMillis();
			
			if (unreadCount == 0) sender.sendNotification("You have no new mail:");
			else sender.sendNotification("You have $1 unread mail$2:", unreadCount, Helpers.getPlural(unreadCount));
			for (final Mail mail: mailbox)
			{
				String when;
				if (mail.getSent() > (now - Helpers.DateTime.MillisecondsInAMinute)) when = "just now";
				else when = Helpers.DateTime.getTruncatedAtMinuteTimeDifference(mail.getSent()) + " ago"; 
				String from = "";
				if (mail.isFromServer()) from = super.getSettings().getServerAlias();
				else
				{
					final OfflinePlayer fromPlayer = Helpers.Bukkit.getOfflinePlayer(mail.getSender());
					if (fromPlayer == null) from = ChatColor.DARK_RED + "<unknown>";
					else
					{
						if (fromPlayer.isOnline()) from = fromPlayer.getPlayer().getDisplayName();
						else from = fromPlayer.getName();
					}
				}

				sender.sendInfoListItemWithId(
						mail.getId(),
						"$1$2 $3from$4 $5 $3[$6]",
						(mail.isUnread() ? ChatColor.LIGHT_PURPLE + "[NEW] " + ChatColor.RESET : ""),
						mail.getSubject(),
						ChatColor.GRAY,
						ChatColor.RESET,
						from,
						when);
			}
		}
		return true;
	}

	private boolean doMailPlayer(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length != 2) return false;
		
		final OfflinePlayer recipient = super.getPlugin().getOfflinePlayer(sender, args[1]);
		if (recipient == null) return true;
		
		if (sender.isConsole()) super.getSettings().createServerDraft(recipient);
		else if (recipient.equals(sender.getPlayer()))
		{
			sender.sendFailure("Don't waste a stamp mailing yourself!");
			return true;
		}
		else
		{
			final Mail oldDraft = super.getSettings().getDraft(sender.getPlayer());
			if (oldDraft != null)
			{
				final OfflinePlayer oldRecipient = Helpers.Bukkit.getOfflinePlayer(oldDraft.getRecipient());
				sender.sendNotification("Discarded draft to $1.", oldRecipient);
			}
			super.getSettings().createDraft(sender.getPlayer(), recipient);
		}
		
		sender.sendSuccess("Draft mail to $1 created.", recipient);

		final BanInfo banInfo = this.banSettings.getBanInfo(recipient);
		final long now = System.currentTimeMillis();
		final long sinceLastSeen = now - recipient.getLastPlayed();
		if (sender.hasPermission("vpcore.moderator"))
		{
			if (banInfo != null && banInfo.isPermanent())
				sender.sendInfo(
						"WARNING: $1 was banned $2 ago.",
						recipient,
						Helpers.DateTime.getHumanReadableFromMillis(now - banInfo.getBannedOn(), true));
			else if (sinceLastSeen >= super.getSettings().getWarnDuration())
				sender.sendInfo(
						"WARNING: $1 not seen for $2.",
						recipient,
						Helpers.DateTime.getHumanReadableFromMillis(sinceLastSeen));
		}

		return true;
	}

	private Mail getDraft(final PagedOutputCommandSender sender)
	{
		Mail mail = null;
		if (sender.isConsole()) mail = super.getSettings().getServerDraft();
		else
		{
			final OfflinePlayer player = super.getPlayer(sender);
			if (player != null)
			{
				mail = super.getSettings().getDraft(player);
				if (mail == null) sender.sendFailure("No draft started.  Use $1 first.", "/mail -p <player>");
			}
		}
		return mail;
	}
}
