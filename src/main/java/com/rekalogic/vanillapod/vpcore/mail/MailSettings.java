package com.rekalogic.vanillapod.vpcore.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class MailSettings extends SettingsBase
{
	private final MailDbInterface dbInterface;

	private String serverAlias = null;
	private long warnDuration = 0;
	private long notificationPeriod = 0;
	private String notificationMessage = "";
	
	private final Map<UUID, Mail> drafts = new ConcurrentHashMap<UUID, Mail>();
	private Map<UUID, List<Mail>> mailboxes = new ConcurrentHashMap<UUID, List<Mail>>();
	private Mail serverDraft = null;

	public String getServerAlias() { return Helpers.formatMessage(this.serverAlias); }
	public long getWarnDuration() { return this.warnDuration ; }

	public MailSettings(final VPPlugin plugin)
	{
		super(plugin);

		this.dbInterface = new MailDbInterface(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.serverAlias = super.getString("server-alias");
		this.warnDuration = Helpers.DateTime.getMillis(super.getString("warn-duration"));
		this.notificationPeriod = Helpers.DateTime.getMillis(super.getString("notification.period"));
		this.notificationMessage = super.getString("notification.message");
	}

	@Override
	protected void onSave()
	{
		super.set("server-alias", this.serverAlias);
		super.set("warn-duration", Helpers.DateTime.getHumanReadableFromMillis(this.warnDuration));
		super.set("notification.period", Helpers.DateTime.getHumanReadableFromMillis(this.notificationPeriod));
		super.set("notification.message", this.notificationMessage);
	}

	@Override
	protected void onEmitInfo()
	{
		super.info("Console mail alias: $1", this.serverAlias);
		super.info("Warn sender if recipient is not seen for $1.", Helpers.DateTime.getHumanReadableFromMillis(this.warnDuration));
		super.info("Every $1: $2", Helpers.DateTime.getHumanReadableFromMillis(this.notificationPeriod), this.notificationMessage);
	}

	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading mailboxes...");
		this.mailboxes = this.dbInterface.getMailboxes();
		
		super.onLoadDataStore();
	}

	public List<Mail> getMailbox(final OfflinePlayer player) { return this.getMailbox(player.getUniqueId()); }
	public List<Mail> getMailbox(final UUID uuid)
	{
		List<Mail> mailbox = null;
		if (this.mailboxes.containsKey(uuid)) mailbox = this.mailboxes.get(uuid);
		else
		{
			mailbox = new ArrayList<Mail>(0);
			this.mailboxes.put(uuid, mailbox);
		}
		return mailbox;
	}

	public boolean hasNewMail(final OfflinePlayer player) { return this.hasNewMail(player.getUniqueId()); }
	public boolean hasNewMail(final UUID uuid)
	{
		boolean hasNewMail = false;
		final List<Mail> mailbox = this.getMailbox(uuid);
		for (final Mail mail: mailbox)
		{
			if (mail.isUnread())
			{
				hasNewMail = true;
				break;
			}
		}
		return hasNewMail;
	}

	public Mail hasMail(final OfflinePlayer player, final int id)
	{
		Mail mail = null;
		final List<Mail> mailbox = this.getMailbox(player);
		if (!mailbox.isEmpty())
		{
			for (final Mail mailboxMail: mailbox)
			{
				if (mailboxMail.getId() == id)
				{
					mail = mailboxMail;
					break;
				}
			}
		}
		return mail;
	}

	public void deleteMail(final Mail mail)
	{
		final List<Mail> mailbox = this.getMailbox(mail.getRecipient());
		if (mailbox.contains(mail))
		{
			mailbox.remove(mail);
			this.dbInterface.delete(mail);
		}
	}

	public void sendMail(final Mail mail)
	{
		final boolean hasPreviousUnreadMail = this.hasNewMail(mail.getRecipient());
		
		mail.send();
		this.dbInterface.saveMail(mail);
		final List<Mail> mailbox = this.getMailbox(mail.getRecipient());
		mailbox.add(0, mail);

		final OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(mail.getRecipient());
		if (player.isOnline())
		{
			Helpers.Messages.sendNotification(
					player.getPlayer(),
					"You have new mail from $1.",
					(mail.isFromServer() ? this.serverAlias : Helpers.Bukkit.getOfflinePlayer(mail.getSender())));
			
			if (!hasPreviousUnreadMail) this.setupMailNotificationTask(player.getPlayer());
		}
		this.drafts.remove(mail.getSender());
	}
	
	public void setupMailNotificationTask(final Player player)
	{
		if (player != null && player.isOnline() && this.hasNewMail(player))
		{
			final long period = Helpers.DateTime.getTicksFromMillis(this.notificationPeriod); 
			final TaskBase task = new TaskBase(super.isDebug(), "MailNotification")
			{
				@Override
				public void runTask()
				{
					if (player != null && player.isOnline() && hasNewMail(player))
						Helpers.Messages.sendNotification(player, notificationMessage);
					else this.cancel();
				}
			};
			task.runTaskTimer(super.getPlugin(), period, period);
		}
	}

	public void readMail(final Mail mail)
	{
		mail.setUnread(false);
		this.dbInterface.markAsRead(mail);
	}

	public Mail createDraft(final OfflinePlayer sender, final OfflinePlayer recipient)
	{
		Mail mail = null;
		if (this.drafts.containsKey(sender.getUniqueId())) this.drafts.remove(sender.getUniqueId());
		mail = new Mail(sender.getUniqueId(), recipient.getUniqueId());
		this.drafts.put(sender.getUniqueId(), mail);
		return mail;
	}

	public Mail createServerDraft(final OfflinePlayer recipient)
	{
		this.serverDraft = new Mail(recipient.getUniqueId());
		return this.serverDraft;
	}

	public Mail getDraft(final OfflinePlayer player)
	{
		return this.drafts.get(player.getUniqueId());
	}

	public Mail getServerDraft()
	{
		return this.serverDraft;
	}

	public void discardDraft(final OfflinePlayer player)
	{
		this.drafts.remove(player.getUniqueId());
	}

	public void discardServerDraft()
	{
		this.serverDraft = null;
	}
}
