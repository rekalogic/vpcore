package com.rekalogic.vanillapod.vpcore.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class MailDbInterface extends DatabaseInterfaceBase {

	public MailDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}

	public void saveMail(final Mail mail)
	{
		final MySqlConnection connection = super.getConnection();
		final int id = connection.insertInto(
				"vp_mail",
				"sent, sender, recipient, subject, body, unread",
				mail.getSent(),
				mail.getSender(),
				mail.getRecipient(),
				mail.getSubject(),
				mail.getBody(),
				mail.isUnread());
		mail.setId(id);
		connection.close();
	}

	public void delete(final Mail mail)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update("vp_mail", "deleted = 1", "id = ?", mail.getId());
		connection.close();
	}

	public void markAsRead(final Mail mail)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update("vp_mail", "unread = 0", "id = ?", mail.getId());
		connection.close();
	}

	public Map<UUID, List<Mail>> getMailboxes()
	{
		final Map<UUID, List<Mail>> mailboxes = new ConcurrentHashMap<UUID, List<Mail>>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT * FROM vp_mail WHERE deleted = 0 ORDER BY id");
		while (result.moveNext())
		{
			final UUID uuid = result.getUuid("recipient");
			List<Mail> mailbox = null;
			if (mailboxes.containsKey(uuid)) mailbox = mailboxes.get(uuid);
			else
			{
				mailbox = new ArrayList<Mail>();
				mailboxes.put(uuid, mailbox);
			}

			final Mail mail = new Mail(
					result.getInt("id"),
					result.getLong("sent"),
					result.getUuid("sender"),
					uuid,
					result.getString("subject"),
					result.getString("body"),
					result.getBoolean("unread"));
			mailbox.add(0, mail);
		}
		connection.close();
		return mailboxes;
	}
}
