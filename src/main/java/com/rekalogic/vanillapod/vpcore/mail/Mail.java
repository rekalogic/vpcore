package com.rekalogic.vanillapod.vpcore.mail;

import java.util.UUID;

public final class Mail
{
	private static final UUID serverUuid = UUID.fromString("00000000-0000-0000-0000-000000000000");

	private int id;
	private long sent;
	private final UUID sender;
	private final UUID recipient;
	private String subject;
	private String body;
	private boolean isUnread;

	public int getId() { return this.id; }
	public long getSent() { return this.sent; }
	public UUID getSender() { return this.sender; }
	public UUID getRecipient() { return this.recipient; }
	public String getSubject() { return (this.subject.isEmpty() ? "<no subject>" : this.subject); }
	public String getBody() { return (this.body.isEmpty() ? "<no body>" : this.body); }
	public boolean isUnread() { return this.isUnread; }

	public void setId(final int id) { this.id = id; }
	public void setUnread(final boolean isUnread) { this.isUnread = isUnread; }
	public void setSubject(final String subject) { this.subject = subject; }

	public void addToBody(final String nextBody)
	{
		if (this.body.length() > 0) this.body += " ";
		this.body += nextBody.trim();
	}

	public Mail(final int id, final long sent, final UUID sender, final UUID recipient, final String subject, final String body, final boolean isUnread)
	{
		this(sender, recipient);
		this.id = id;
		this.sent = sent;
		this.subject = subject;
		this.body = body;
		this.isUnread = isUnread;
	}

	public Mail(final UUID sender, final UUID recipient)
	{
		this.sender = sender;
		this.recipient = recipient;
		this.subject = "";
		this.body = "";
	}

	public Mail(final UUID recipient)
	{
		this(Mail.serverUuid, recipient);
	}

	public void send()
	{
		this.sent = System.currentTimeMillis();
		this.isUnread = true;
	}

	public boolean hasSubjectOrBody()
	{
		return ((this.subject.length() + this.body.length()) > 0);
	}

	public boolean isFromServer()
	{
		return this.sender.equals(Mail.serverUuid);
	}
	
	public void setReplySubject(Mail mail)
	{
		String newSubject = mail.getSubject();
		if (!newSubject.toLowerCase().startsWith("re:")) newSubject = "RE:" + newSubject;
		if (newSubject.length() > 100) newSubject = newSubject.substring(0, 100);
		this.subject = newSubject;
	}
}
