package com.rekalogic.vanillapod.vpcore.mail;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;

public final class MailEventHandler extends EventHandlerBase<MailSettings>
{
	public MailEventHandler(final PluginBase plugin, final MailSettings settings)
	{
		super(plugin, settings);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerJoin(final PlayerJoinEvent event)
	{
		final Player player = event.getPlayer();
		super.getSettings().setupMailNotificationTask(player);
	}
}
