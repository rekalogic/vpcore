package com.rekalogic.vanillapod.vpcore;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.kitteh.vanish.event.VanishStatusChangeEvent;

import com.rekalogic.vanillapod.vpcore.player.PlayerSettings;
import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;

public final class VanishEventHandler extends EventHandlerBase<PlayerSettings>
{
	public VanishEventHandler(final PluginBase plugin, final PlayerSettings settings)
	{
		super(plugin, settings);
	}
	
	@Override
	protected boolean canEnable()
	{
		if (!super.getPlugin().isVanishNoPacketEnabled()) return false;
		else return super.canEnable();
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onVanishStatusChangeEvent(final VanishStatusChangeEvent event)
	{
		final Player player = event.getPlayer();
		final boolean isVisible = !super.getPlugin().isVanished(player);
		super.getSettings().updateVisibleStatus(player, isVisible);
	}
}
