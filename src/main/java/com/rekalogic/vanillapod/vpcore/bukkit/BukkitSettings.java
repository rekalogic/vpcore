package com.rekalogic.vanillapod.vpcore.bukkit;

import org.bukkit.Bukkit;
import org.bukkit.World;
import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class BukkitSettings extends SettingsBase
{
	private String broadcastFormat = "";
	private String shutdownMessage = "";

	public String getBroadcastFormat() { return this.broadcastFormat; }
	public String getShutdownMessage() { return this.shutdownMessage; }

	public BukkitSettings(final VPPlugin plugin)
	{
		super(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.broadcastFormat = super.getString("broadcast-format");
		this.shutdownMessage = super.getString("messages.shutdown");
		
		if (this.shutdownMessage == null ||
				this.shutdownMessage.length() == 0) this.shutdownMessage = Bukkit.getShutdownMessage();
	}

	@Override
	protected void onSave()
	{
		super.set("broadcast-format", this.broadcastFormat);
		super.set("messages.shutdown", this.shutdownMessage);
	}

	@Override
	protected void onEmitInfo()
	{
		super.info("Broadcast format:" + this.broadcastFormat);
		super.info(Helpers.formatMessage(this.broadcastFormat), "This is a test broadcast.");
		if (!this.broadcastFormat.contains("$1")) super.severe("Broadcast format does not contain '$1' placeholder.");
		
		super.info("Shutdown message: $1", this.shutdownMessage);

		for (final World world: Bukkit.getWorlds())
			super.info("World $1 auto-save: $2", world, (world.isAutoSave() ? "ON" : "OFF"));
	}
}
