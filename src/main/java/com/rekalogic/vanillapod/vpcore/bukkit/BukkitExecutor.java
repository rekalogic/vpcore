package com.rekalogic.vanillapod.vpcore.bukkit;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.command.CommandSettings;
import com.rekalogic.vanillapod.vpcore.serverlist.ServerListSettings;
import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class BukkitExecutor extends ExecutorBase<BukkitSettings>
{
	private final ServerListSettings serverListSettings;
	private final CommandSettings commandSettings;
	
	private TaskBase stopTask = null;
	private long stopCommandScheduledTime = 0;

	public BukkitExecutor(
			final PluginBase plugin,
			final BukkitSettings settings,
			final ServerListSettings serverListSettings,
			final CommandSettings commandSettings)
	{
		super(plugin, settings,
				"autosave",
				"broadcast",
				"gamemode",
				"me",
				"op",
				"save",
				"stop"
				);
		
		this.serverListSettings = serverListSettings;
		this.commandSettings = commandSettings;
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "autosave": return (argCount == 1);
		case "broadcast": return true;
		case "gamemode": return (argCount == 1);
		case "me": return (argCount > 0);
		case "op": return (argCount <= 1);
		case "stop": return (argCount <= 1);
		}

		return super.checkParameters(command, argCount);
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "gamemode": return new String[] { ExecutorBase.FLAG_PLAYER };
		case "me": return new String[] { ExecutorBase.FLAG_PLAYER };
		case "save": return new String[] { ExecutorBase.FLAG_BOOL_SILENT };
		}
		return null;
	}
	
	@Override
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "gamemode": return Arrays.asList("adventure", "creative", "spectator", "survival");
		}
		
		return super.onTabComplete(sender, command, argIndex, args);
	}

	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "gamemode":
			switch (argIndex)
			{
			case 0: return LookupSource.FROM_PLUGIN;
			}
			break;
		case "op": 
			switch (argIndex)
			{
			case 0: return LookupSource.OFFLINE_PLAYER;
			}
			break;
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}
	
	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		switch (command)
		{
		case "autosave": return this.doAutosave(sender, args);
		case "broadcast": return this.doBroadcast(sender, args);
		case "gamemode": return this.doGameMode(sender, args);
		case "me": return this.doMe(sender, args);
		case "op": return this.doOp(sender, args);
		case "save": return this.doSave(sender, args);
		case "stop": return this.doStop(sender, args);
		}

		return super.onCommand(sender, command, label, args);
	}

	private boolean doBroadcast(final PagedOutputCommandSender sender, final String[] args)
	{
		final String message = Helpers.Parameters.replace(
				Helpers.formatMessage(super.getSettings().getBroadcastFormat()),
				Helpers.Args.concatenate(args));
		Helpers.Broadcasts.sendAll(message);
		return true;
	}

	private boolean doMe(final PagedOutputCommandSender sender, final String[] args)
	{
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER) && !sender.hasPermission("vpcore.me.others")) return false;

		final Player player = super.getPlayer(sender);
		if (player == null) return true;

		final String message = Helpers.Parameters.replace(
				"$1* $2 $3",
				ChatColor.GRAY,
				player.getName(),
				Helpers.Args.concatenate(args));
		Helpers.Broadcasts.sendAll(message);

		return true;
	}

	private boolean doStop(final PagedOutputCommandSender sender, final String[] args)
	{
		long delay = 0;
		if (args.length == 1)
		{
			switch (args[0].toLowerCase())
			{
			case "abort":
				if (this.stopTask == null) sender.sendFailure("There is no pending STOP command to be aborted.");
				else
				{
					this.stopTask.cancel();
					this.stopTask = null;
					sender.sendSuccess("The pending STOP command was aborted.");
				}
				return true;

			case "query":
				if (this.stopTask == null) sender.sendSuccess("There is no pending STOP command.");
				else
				{
					delay = this.stopCommandScheduledTime - System.currentTimeMillis();
					sender.sendSuccess(
							"The pending STOP command will execute in $1",
							Helpers.DateTime.getHumanReadableFromMillis(delay));
				}
				return true;

			default:
				delay = Helpers.DateTime.getMillis(args[0]);
				if (delay < 0) return false;
				break;
			}
		}

		this.stopTask = new TaskBase(super.isDebug(), "Stop")
		{
			@Override
			public void runTask()
			{
				commandSettings.clearLogoutCommands();
				final String reason = getSettings().getShutdownMessage();
				serverListSettings.lockServer(PagedOutputCommandSender.ConsoleInstance, reason, 0);
				super.info("Kicking players: $1", reason);
				for (final Player player: Bukkit.getOnlinePlayers())
				{
					player.kickPlayer(reason);
					super.info("Kicked $1.", player);
				}
				Bukkit.shutdown();
			}
		};

		if (delay == 0)
		{
			sender.sendSuccess("Immediate STOP executed.");
			this.stopTask.runTask(super.getPlugin());
		}
		else
		{
			sender.sendSuccess("STOP command scheduled for $1 from now.", Helpers.DateTime.getHumanReadableFromMillis(delay));
			this.stopCommandScheduledTime = System.currentTimeMillis() + delay;
			this.stopTask.runTaskLater(super.getPlugin(), Helpers.DateTime.getTicksFromMillis(delay));
		}

		return true;
	}

	private boolean doAutosave(final PagedOutputCommandSender sender, final String[] args)
	{
		boolean save = false;
		switch (args[0].toLowerCase())
		{
		case "on":
		case "true":
		case "yes": save = true; break;

		case "off":
		case "false":
		case "no": save = false; break;

		default: return false;
		}
		for (final World world: Bukkit.getWorlds()) world.setAutoSave(save);
		sender.sendSuccess("Autosaving set to $1.", (save ? "ON" : "OFF"));
		return true;
	}

	private boolean doSave(final PagedOutputCommandSender sender, final String[] args)
	{
		final boolean silent = super.hasFlag(ExecutorBase.FLAG_BOOL_SILENT);

		if (silent) sender.sendInfo("Saving all worlds...");
		else Helpers.Broadcasts.sendAll("Saving all worlds...");
		for (final World world: Bukkit.getWorlds()) world.save();
		if (silent) sender.sendSuccess("Save complete.");
		else Helpers.Broadcasts.sendAll("Save complete.");
		return true;
	}

	private boolean doOp(final PagedOutputCommandSender sender, final String[] args)
	{
		OfflinePlayer player = null;
		if (args.length == 0) player = super.getOfflinePlayer(sender);
		else player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		
		if (player == null) return true;
		
		final boolean targetIsSelf = sender.isPlayer() && player.equals(sender.getPlayer());
		if (!targetIsSelf && !sender.hasPermission("vpcore.op.others"))
		{
			sender.sendFailure("You do not have permission to change other players' OP status.");
			return true;
		}

		if (player.isOp())
		{
			player.setOp(false);

			if (player.isOnline())
				Helpers.Messages.sendInfo(player.getPlayer(), "You are no longer OP.");

			super.info("$1 revoked OP from $2.", sender, player);
		}
		else
		{
			player.setOp(true);
			if (targetIsSelf) sender.sendSuccess("You are now OP.");
			
			if (player.isOnline())
			{
				if (!targetIsSelf) Helpers.Messages.sendSuccess(player.getPlayer(), "You have been given OP by $1.", sender);
				Helpers.Messages.sendInfo(player.getPlayer(), "You now have all permissions.");
			}

			super.info("$1 granted OP to $2.", sender, player);
		}

		return true;
	}

	private boolean doGameMode(final PagedOutputCommandSender sender, final String[] args)
	{
		final Player player = super.getPlayer(sender);
		if (player == null) return true;

		GameMode mode = null;
		final String requestedMode = args[0].toLowerCase();
		switch (requestedMode)
		{
		case "0": mode = GameMode.SURVIVAL; break;
		case "1": mode = GameMode.CREATIVE; break;
		case "2": mode = GameMode.ADVENTURE; break;
		case "3": mode = GameMode.SPECTATOR; break;
		default:
			for (GameMode testMode: GameMode.values())
			{
				if (testMode.name().toLowerCase().startsWith(requestedMode))
				{
					mode = testMode;
					break;
				}
			}
			
			if (mode == null) return false;
			break;
		}
		
		player.setGameMode(mode);
		sender.sendSuccess("Changed gamemode for $1 to $2.", player, mode.name());

		return true;
	}
}
