package com.rekalogic.vanillapod.vpcore.bukkit;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;

public final class BukkitEventHandler extends EventHandlerBase<BukkitSettings>
{
	public BukkitEventHandler(final PluginBase plugin, final BukkitSettings settings)
	{
		super(plugin, settings);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onPlayerJoin(final PlayerJoinEvent event)
	{
		final Player player = event.getPlayer();
		if (player.isOp()) player.setOp(false);
	}
}
