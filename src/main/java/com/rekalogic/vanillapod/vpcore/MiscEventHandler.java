package com.rekalogic.vanillapod.vpcore;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.material.Dye;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerRaw;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public final class MiscEventHandler extends EventHandlerRaw
{
	private final World mainWorld;
	private final ProtectedRegion orkidaRegion;

	public MiscEventHandler(final PluginBase plugin, final World mainWorld)
	{
		super(plugin);

		this.mainWorld = mainWorld;
		final RegionManager regionManager = super.getPlugin().getWorldGuard().getRegionManager(this.mainWorld);
		if (regionManager == null) this.orkidaRegion = null; 
		else this.orkidaRegion = regionManager.getRegion("orkida");
	}

	@Override
	protected void onRegister()
	{
		super.setDebug(super.getPlugin().isDebug());
		super.onRegister();
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	private void onAsyncPlayerChat(final AsyncPlayerChatEvent event)
	{
		final Player player = event.getPlayer();
		if (event.isCancelled() && super.getPlugin().isVanished(player))
			Helpers.Messages.sendNotification(player, "You are vanished and cannot chat.");
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	private void onEntityExplode(final EntityExplodeEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getEntityType() == EntityType.CREEPER)
		{
			final Creeper creeper = (Creeper)event.getEntity();
			final LivingEntity target = creeper.getTarget();
			if (target != null && target.getType() == EntityType.PLAYER)
			{
				super.info(
						Helpers.Parameters.replace(
								"CREEPER [$1] DETONATION: $2 at $3",
								creeper.getEntityId(),
								creeper.getTarget(),
								creeper.getLocation()));
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerChangedWorld(final PlayerChangedWorldEvent event)
	{
		final Player player = event.getPlayer();
		super.info("$1 travelled from $2 to $3.", player, event.getFrom(), player.getWorld());
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onStructureGrow(final StructureGrowEvent event)
	{
		if (event.isCancelled()) return;
		
		if (this.orkidaRegion != null &&
				event.getWorld().equals(this.mainWorld) &&
				this.orkidaRegion.contains(
						event.getLocation().getBlockX(),
						event.getLocation().getBlockY(),
						event.getLocation().getBlockZ()))
		{
			ProtectedRegion orkidaPlot = null;
			final RegionManager regionManager = super.getPlugin().getWorldGuard().getRegionManager(this.mainWorld);
			for (final ProtectedRegion region: regionManager.getApplicableRegions(event.getLocation()))
			{
				if (region.getId().startsWith("ok-plot"))
				{
					orkidaPlot = region;
					break;
				}
			}

			if (orkidaPlot != null)
			{
				for (final BlockState block: event.getBlocks())
				{
					if (!orkidaPlot.contains(block.getX(), block.getY(), block.getZ()))
					{
						event.setCancelled(true);

						if (event.getPlayer() != null) Helpers.Messages.sendFailure(
								event.getPlayer(),
								"Cannot grow: too close to plot boundary.");

						super.debug(
								"Tree/mushroom $1 at $2 prevented from growing ($3).",
								event.getSpecies(),
								event.getLocation(),
								orkidaPlot);

						break;
					}
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void onPlayerInteract(final PlayerInteractEvent event)
	{
		if (event.isCancelled()) return;
		
		final Block clickedBlock = event.getClickedBlock();

		if (this.orkidaRegion != null &&
				event.getAction() == Action.RIGHT_CLICK_BLOCK &&
				event.getMaterial() == Material.INK_SACK &&
				clickedBlock.getWorld().equals(this.mainWorld) &&
				clickedBlock.getType() == Material.GRASS &&
				this.orkidaRegion.contains(
						clickedBlock.getLocation().getBlockX(),
						clickedBlock.getLocation().getBlockY(),
						clickedBlock.getLocation().getBlockZ()))
		{
			final Dye dye = (Dye)event.getItem().getData();
			if (dye.getColor() == DyeColor.WHITE)
			{
				ProtectedRegion orkidaPlot = null;
				final RegionManager regionManager = super.getPlugin().getWorldGuard().getRegionManager(this.mainWorld);
				for (final ProtectedRegion region: regionManager.getApplicableRegions(clickedBlock.getLocation()))
				{
					if (region.getId().startsWith("ok-plot"))
					{
						orkidaPlot = region;
						break;
					}
				}
				
				if (orkidaPlot != null)
				{
					final boolean blockOne = orkidaPlot.contains(
							clickedBlock.getX() + 4,
							clickedBlock.getY(),
							clickedBlock.getZ() + 4);
					final boolean blockTwo = orkidaPlot.contains(
							clickedBlock.getX() - 4,
							clickedBlock.getY(),
							clickedBlock.getZ() - 4);
					
					if (!blockOne || !blockTwo)
					{
						event.setCancelled(true);
						Helpers.Messages.sendFailure(
								event.getPlayer(),
								"Cannot grow: too close to plot boundary.");
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onPlayerCommandPreprocess(final PlayerCommandPreprocessEvent event)
	{
		if (event.isCancelled()) return;
		
		final String message = event.getMessage().toLowerCase();
		if (message.startsWith("/region") || message.startsWith("/rg"))
		{
			if (!super.getPlugin().isWorldGuardEnabled()) return;

			final String[] args = event.getMessage().split(" ");
			if (args.length >= 3)
			{
				switch (args[1])
				{
				case "addowner":
				case "remowner":
				case "removeowner":
				case "remove":
				case "rem":
				case "delete":
				case "del":
				case "select":
				case "sel":
					World world = null;
					boolean worldFound = false;
					int worldIndex = 0;
					int regionIndex = 2;
					for (int i = 2; i < args.length; ++i)
					{
						if (args[i].equals("-w"))
						{
							worldIndex = i;
							worldFound = true;
							break;
						}
					}
					if (worldFound && worldIndex + 1 < args.length) world = Bukkit.getWorld(args[worldIndex + 1]);
					else world = event.getPlayer().getWorld();
					if (world == null) super.warning("Could not extract world from command: $1", message);
					else
					{
						if (worldFound && worldIndex <= regionIndex) regionIndex += 2;

						final RegionManager regionManager = super.getPlugin().getWorldGuard().getRegionManager(world);
						if (regionManager != null)
						{
							final ProtectedRegion region = regionManager.getRegion(args[regionIndex]);
							ProtectedRegion parent = null;
							if (region != null) parent = region.getParent();
							if (parent != null && parent.getId().equalsIgnoreCase("orkida"))
							{
								if (event.getPlayer().hasPermission("vpcore.moderator") || event.getPlayer().hasPermission("vpcore.admin"))
								{
									Helpers.Messages.sendNotification(event.getPlayer(), "Operation allowed due to permissions.");
								}
								else
								{
									Helpers.Messages.sendFailure(event.getPlayer(), "You cannot do that to an Orkida plot.");
									event.setCancelled(true);
								}
							}
						}
					}
					break;
				}
			}
		}
	}
}
