package com.rekalogic.vanillapod.vpcore.sign;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.player.EconomyManager;
import com.rekalogic.vanillapod.vpcore.player.PlayerSettings;
import com.rekalogic.vanillapod.vpcore.plot.PlotData;
import com.rekalogic.vanillapod.vpcore.plot.PlotSettings;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class SignSettings extends SettingsBase
{
	private final SignDbInterface dbInterface;

	private PlayerSettings playerSettings;
	private PlotSettings plotSettings;

	private String serverSignDisplayName = "";
	private ChatColor headerColour = ChatColor.BLACK;

	private final Map<UUID, String[]> clipboard = new ConcurrentHashMap<UUID, String[]>();
	private Map<Integer, SignData> signDataById = null;
	private Map<String, SignData> signDataByLocation = null;
	
	public static final String ForSaleText = "FOR SALE";
	public static final String OutOfStockText = "OUT OF STOCK";
	public static final String FulfilledText = "FULFILLED";

	public String getServerSignDisplayName() { return this.serverSignDisplayName; }
	
	public void setPlayerSettings(final PlayerSettings settings) { this.playerSettings = settings; }
	public void setPlotSettings(final PlotSettings settings) { this.plotSettings = settings; }

	public SignSettings(final VPPlugin plugin)
	{
		super(plugin);

		this.dbInterface = new SignDbInterface(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.serverSignDisplayName = super.getString("server-sign.display-name");

		final String colourString = super.getString("header-colour");
		if (!colourString.isEmpty())
		{
			try { this.headerColour = ChatColor.valueOf(colourString.toUpperCase()); }
			catch (final IllegalArgumentException e) { this.headerColour = ChatColor.BLACK; }
			if (this.headerColour.isFormat()) this.headerColour = ChatColor.BLACK;
		}
	}

	@Override
	protected void onSave()
	{
		super.set("server-sign.display-name", this.serverSignDisplayName);
		super.set("header-colour", this.headerColour.name().toLowerCase());
	}

	@Override
	protected void onEmitInfo()
	{
		super.info("Sign headers will be shown in: $1$2", this.headerColour, this.headerColour.name());
	}

	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading sign data...");
		this.signDataById = this.dbInterface.loadSignData();
		this.signDataByLocation = new ConcurrentHashMap<String, SignData>();
		for (final SignData signData: this.signDataById.values())
			this.signDataByLocation.put(signData.getKey(), signData);
		
		super.onLoadDataStore();
	}

	public void setCopiedSign(final Player player, final String[] lines)
	{
		this.clipboard.put(player.getUniqueId(), lines);
	}

	public String[] getCopiedSign(final Player player)
	{
		return this.clipboard.get(player.getUniqueId());
	}

	public SignData getSignData(final Location location)
	{
		SignData signData = null;
		final String key = SignData.getKey(location);
		if (this.signDataByLocation.containsKey(key))
			signData = this.signDataByLocation.get(key);
		return signData;
	}
	
	public List<SignData> getSignData()
	{
		return new ArrayList<SignData>(this.signDataById.values());
	}

	public void removeSignData(final Location location)
	{
		final String key = SignData.getKey(location);
		if (this.signDataByLocation.containsKey(key))
		{
			final SignData signData = this.signDataByLocation.remove(key);
			this.signDataById.remove(signData.getId());
			this.dbInterface.removeSignData(location);
		}
	}

	public SignData updateSignData(final SignData signData)
	{
		final String key = signData.getKey();
		final SignData existingData = this.signDataByLocation.get(key);
		if (existingData == null) this.dbInterface.createSignData(signData);
		else
		{
			signData.setId(existingData.getId());
			this.dbInterface.updateSignData(signData);
		}
		this.signDataById.put(signData.getId(), signData);
		this.signDataByLocation.put(key, signData);
		return signData;
	}

	public SignData updateWarpSignData(final OfflinePlayer player, final Sign sign, final String warp)
	{
		final SignData signData = SignData.createWarpSignData(sign.getLocation(), warp, player, true);
		return this.updateSignData(signData);
	}

	public SignData updateBuyingSignData(
			final OfflinePlayer player,
			final Sign sign,
			final String item,
			final String metadata,
			final int quantity,
			final double price,
			final boolean isServerOwned)
	{
		final SignData signData = SignData.createBuyingSignData(sign.getLocation(), item, metadata, quantity, price, player, isServerOwned);
		return this.updateSignData(signData);
	}

	public SignData updateSellingSignData(
			final OfflinePlayer player,
			final Sign sign,
			final String item,
			final String metadata,
			final int quantity,
			final double price,
			final boolean isServerOwned)
	{
		final SignData signData = SignData.createSellingSignData(sign.getLocation(), item, metadata, quantity, price, player, isServerOwned);
		return this.updateSignData(signData);
	}

	public SignData updateDisposalSignData(final OfflinePlayer player, final Sign sign)
	{
		final SignData signData = SignData.createDisposalSign(sign.getLocation(), player, true);
		return this.updateSignData(signData);
	}

	public SignData updatePlotSignData(final OfflinePlayer player, final Sign sign, final String plotName)
	{
		final SignData signData = SignData.createPlotSign(sign.getLocation(), plotName, player, true);
		return this.updateSignData(signData);
	}

	public Collection<SignData> getPlotSigns(final String plotName)
	{
		final Collection<SignData> signs = new ArrayList<SignData>();
		for (final SignData sign: this.signDataById.values())
		{
			if (sign.getType().equals("plot") &&
					sign.getItem().equalsIgnoreCase(plotName))
				signs.add(sign);
		}
		return signs;
	}
	
	public PlotData getPlotData(final String plotName)
	{
		return this.plotSettings.getPlotData(plotName);
	}

	public void recordPurchase(final UUID actor, final UUID otherParty, final String item, final int quantity, final double price)
	{
		this.dbInterface.recordPurchase(actor, otherParty, item, quantity, price);
	}

	public void recordSale(final UUID actor, final UUID otherParty, final String item, final int quantity, final double price)
	{
		this.dbInterface.recordSale(actor, otherParty, item, quantity, price);
	}

	public void removePlotSignData(final String plotName)
	{
		for (final SignData signData: this.getPlotSigns(plotName))
		{
			this.signDataById.remove(signData.getId());
			this.signDataByLocation.remove(signData.getKey());
		}
		this.dbInterface.removePlotSigns(plotName);
	}
	
	public boolean isSignBroken(Sign sign)
	{
		return sign.getLine(0).startsWith(ChatColor.RED.toString());
	}
	
	public boolean isControlledSign(final Block block)
	{
		boolean isControlledSign = false;
		if (block != null)
		{
			switch (block.getType())
			{
			case SIGN_POST:
			case WALL_SIGN:
				if (block.getState() instanceof Sign)
				{
					final Sign sign = (Sign)block.getState();
					isControlledSign = this.isControlledSign(sign);
				}
				break;
				
			default: break;
			}
		}
		return isControlledSign;
	}
	
	public boolean isControlledSign(final Sign sign) { return this.isControlledSign(sign.getLine(0)); }
	public boolean isControlledSign(final String topLine)
	{
		return this.isSellingSign(topLine) ||
				this.isBuyingSign(topLine) ||
				this.isWarpSign(topLine) ||
				this.isDisposalSign(topLine) ||
				this.isPlotSign(topLine);
	}
	
	public boolean isSellingSign(final Sign sign) { return this.isSellingSign(sign.getLine(0)); }
	public boolean isSellingSign(final String topLine)
	{
		final String strippedTopLine = ChatColor.stripColor(topLine);
		return (strippedTopLine.equalsIgnoreCase("[selling]"));
	}

	public boolean isBuyingSign(final Sign sign) { return this.isBuyingSign(sign.getLine(0)); }
	public boolean isBuyingSign(final String topLine)
	{
		final String strippedTopLine = ChatColor.stripColor(topLine);
		return (strippedTopLine.equalsIgnoreCase("[buying]"));
	}

	public boolean isWarpSign(final Sign sign) { return this.isWarpSign(sign.getLine(0)); }
	public boolean isWarpSign(final String topLine)
	{
		final String strippedTopLine = ChatColor.stripColor(topLine);
		return (strippedTopLine.equalsIgnoreCase("[warp]"));
	}

	public boolean isDisposalSign(final Sign sign) { return this.isDisposalSign(sign.getLine(0)); }
	public boolean isDisposalSign(final String topLine)
	{
		final String strippedTopLine = ChatColor.stripColor(topLine);
		return (strippedTopLine.equalsIgnoreCase("[disposal]"));
	}

	public boolean isPlotSign(final Sign sign) { return this.isPlotSign(sign.getLine(0)); }
	public boolean isPlotSign(final String topLine)
	{
		final String strippedTopLine = ChatColor.stripColor(topLine);
		return (strippedTopLine.equalsIgnoreCase("[plot]"));
	}
	
	public String getSignPermission(final Sign sign, final String permissionType)
	{
		final String strippedTopLine = ChatColor.stripColor(sign.getLine(0)).toLowerCase();
		String signName = "";
		switch (strippedTopLine)
		{
		case "[buying]": signName = "buying"; break;
		case "[selling]": signName = "selling"; break;
		case "[warp]": signName = "warp"; break;
		case "[disposal]": signName = "disposal"; break;
		case "[plot]": signName = "plot"; break;

		default:
			return "vpcore.signs.notimplemented";
		}

		return Helpers.Parameters.replace("vpcore.signs.$1.$2", signName, permissionType).toLowerCase();
	}
	
	public void formatSignHeader(final Sign sign)
	{
		sign.setLine(0, this.formatSignHeader(sign.getLine(0), true));
		sign.update();
	}

	private String formatSignHeader(final String topLine, final boolean withColour)
	{
		final String strippedTopLine = ChatColor.stripColor(topLine);

		if (strippedTopLine.equalsIgnoreCase("[buying]")) return this.constructSignHeader("Buying", withColour);
		if (strippedTopLine.equalsIgnoreCase("[selling]")) return this.constructSignHeader("Selling", withColour);
		if (strippedTopLine.equalsIgnoreCase("[warp]")) return this.constructSignHeader("Warp", withColour);
		if (strippedTopLine.equalsIgnoreCase("[disposal]")) return this.constructSignHeader("Disposal", withColour);
		if (strippedTopLine.equalsIgnoreCase("[plot]")) return this.constructSignHeader("Plot", withColour);

		return topLine;
	}

	private String constructSignHeader(final String header, final boolean withColour)
	{		
		if (withColour) return "[" + this.headerColour + header + ChatColor.RESET + "]";
		else return "[" + header + "]";
	}
	
	public void formatSignHeaderForError(final Sign sign)
	{
		sign.setLine(0, this.formatSignHeaderForError(sign.getLine(0)));
		sign.update();
	}

	public String formatSignHeaderForError(final String topLine)
	{
		final String newTopLine = ChatColor.STRIKETHROUGH + this.formatSignHeader(topLine, false);
		return ChatColor.RED + newTopLine + ChatColor.RESET;
	}
	
	public void updateSign(final Block block, final CommandSender sender, final OfflinePlayer owner, final String[] lines)
	{
		final Sign sign = (Sign)block.getState();
		for (int i = 0; i < lines.length; ++i)
			sign.setLine(i, lines[i]);
		sign.update();
		
		if (this.isControlledSign(sign))
			this.updateSign(sign, sender, owner, false);
	}	

	public SignData updateSign(final Sign sign, final CommandSender sender, final OfflinePlayer owner, final boolean withSideEffects)
	{
		SignData signData = null;
		if (this.isBuyingSign(sign)) signData = this.updateBuyingSellingSign(sign, sender, owner, withSideEffects);
		else if (this.isSellingSign(sign)) signData = this.updateBuyingSellingSign(sign, sender, owner, withSideEffects);
		else if (this.isWarpSign(sign)) signData = this.updateWarpSign(sign, sender, owner);
		else if (this.isDisposalSign(sign)) signData = this.updateDisposalSign(sign, sender, owner);
		else if (this.isPlotSign(sign)) signData = this.updatePlotSign(sign, sender, owner);
		else if (sender != null) Helpers.Messages.sendFailure(sender, "That type of sign is not supported.");
		
		if (signData == null) this.formatSignHeaderForError(sign);
		else
		{
			if (this.isBuyingSign(sign)) this.updateBuyingSellingSignText(sign, signData);
			else if (this.isSellingSign(sign)) this.updateBuyingSellingSignText(sign, signData);
			else if (this.isWarpSign(sign)) this.updateWarpSignText(sign, signData);
			else if (this.isDisposalSign(sign)) this.updateDisposalSignText(sign, signData);
			else if (this.isPlotSign(sign)) this.updatePlotSignText(sign, signData);
			
			Helpers.Messages.sendSuccess(sender, "Sign created successfully.");
			
			if (this.isBuyingSign(sign))
			{
                if (!sender.hasPermission("vpcore.admin")) Helpers.Messages.sendSuccess(
                            sender,
                            "You can right-click on the sign to add $$1 to it.",
                            Helpers.Money.formatString(signData.getPrice()));
			}
			else if (this.isSellingSign(sign))
			{
				if (!sender.hasPermission("vpcore.admin")) Helpers.Messages.sendSuccess(
                        sender,
                        "You can right-click on the sign to add $1 x $2 to it.",
                        signData.getQuantity(),
                        signData.getItem());
			}
			else if (this.isPlotSign(sign))
			{
				final Block signBlock = sign.getBlock();
                final PlotData plotData = this.plotSettings.searchForAssociatedPlotData(signBlock);
                final int signCount = this.getPlotSigns(plotData.getName()).size();
                Helpers.Messages.sendInfo(
                        sender,
                        "Plot $1 now has $2 sign$3.",
                        plotData,
                        signCount,
                        Helpers.getPlural(signCount));

                if (plotData.getGroundLevel() == 0)
                {
                	if (sender instanceof Player)
                	{
                		final Player player = (Player)sender;
                		this.plotSettings.updatePlotGroundLevel(plotData, player.getLocation().getBlockY());
                		Helpers.Messages.sendNotification(sender, "Plot ground level set to your feet.");
	                    Helpers.Messages.sendNotification(
	                            sender,
	                            "If this is not correct, move to the correct location and issue the command $1.",
	                            "/plotadmin setgroundlevel");
                	}
                	else this.plotSettings.updatePlotGroundLevel(plotData, sign.getBlock().getY());
                }
			}
		}
		
		return signData;
	}
	
	private void updatePlotSignText(final Sign sign, final SignData signData)
	{
		final PlotData plotData = this.plotSettings.searchForAssociatedPlotData(sign.getLocation().getBlock());
		this.updatePlotSignText(sign, signData, plotData);
		this.plotSettings.updatePlotHead(sign, plotData);
	}

	public void updatePlotSignText(final Sign sign, final SignData signData, final PlotData plotData)
	{
		if (plotData == null) this.formatSignHeaderForError(sign);
		else
		{
			String priceOrOwner = "";
			String status = "";
			OfflinePlayer owner = null;
			if (plotData.isOwned()) owner = Helpers.Bukkit.getOfflinePlayer(plotData.getOwnerUuid());

			if (owner == null)
			{
				status = SignSettings.ForSaleText;
				priceOrOwner = Helpers.Parameters.replace("$$1", Helpers.Money.formatString(plotData.getBuyPrice()));
			}
			else priceOrOwner = owner.getName();
			
			if (priceOrOwner == null || priceOrOwner.length() == 0)
				priceOrOwner = ChatColor.RED + "UNKNOWN";

			this.formatSignHeader(sign);
			sign.setLine(1, plotData.getName());
			sign.setLine(2, status);
			sign.setLine(3, priceOrOwner);
			sign.update();
		}
	}

	public void updateDisposalSignText(final Sign sign, final SignData signData)
	{
		this.formatSignHeader(sign);
		sign.setLine(1, "");
		sign.setLine(2, "");
		sign.setLine(3, "");
		sign.update();
	}

	public void updateWarpSignText(final Sign sign, final SignData signData)
	{
		this.formatSignHeader(sign);
		sign.setLine(1, "");
		sign.setLine(2, signData.getItem());
		sign.setLine(3, "");
		sign.update();
	}

	public void updateBuyingSellingSignText(final Sign sign, final SignData signData)
	{
		this.formatSignHeader(sign);
		String displayName = "";
		if (signData.isServerOwned()) displayName = this.getServerSignDisplayName();
		else
		{
			final OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(signData.getPlayerUuid());
			if (player == null || !player.hasPlayedBefore()) displayName = ChatColor.RED + "UNKNOWN";
			else displayName = player.getName();
		}
		boolean isLine1Handled = false;
		if (!signData.isServerOwned())
		{
			if (signData.isSellingSign() &&
					signData.getItemLevel() == 0)
			{
				sign.setLine(1, ChatColor.RED + SignSettings.OutOfStockText);
				isLine1Handled = true;
			}
			else if (signData.isBuyingSign() &&
					signData.getMoneyLevel() < signData.getPrice())
			{
				sign.setLine(1, ChatColor.RED + SignSettings.FulfilledText);
				isLine1Handled = true;
			}
		}
		if (!isLine1Handled) sign.setLine(1, Helpers.Parameters.replace("$1 @ $$2", signData.getQuantity(), Helpers.Money.formatString(signData.getPrice())));
		sign.setLine(2, Helpers.Parameters.replace("$1", signData.getItem()));
		sign.setLine(3, ChatColor.DARK_GRAY + displayName);
		sign.update();
	}

	private SignData updatePlotSign(final Sign sign, final CommandSender sender, final OfflinePlayer owner)
	{
		SignData signData = null;
		final PlotData plotData = this.plotSettings.searchForAssociatedPlotData(sign.getLocation().getBlock());
		if (plotData == null) Helpers.Messages.sendFailure(sender, "Could not locate a suitable plot.");
		else
		{
			super.debug("PLOT sign created at $1.", sign.getLocation());
			signData = this.updatePlotSignData(owner, sign, plotData.getName());
		}
		
		return signData;
	}

	private SignData updateDisposalSign(final Sign sign, final CommandSender sender, final OfflinePlayer owner)
	{
		return this.updateDisposalSignData(owner, sign);
	}

	private SignData updateWarpSign(final Sign sign, final CommandSender sender, final OfflinePlayer owner)
	{
		SignData signData = null;
		String warp = sign.getLine(1);
		if (warp.isEmpty()) warp = sign.getLine(2);
		if (warp.isEmpty()) warp = sign.getLine(3);

		if (warp.isEmpty()) Helpers.Messages.sendFailure(sender, "Warp name must be specified.");
		else if (this.playerSettings.getWarp(warp) == null) Helpers.Messages.sendFailure(sender, "Warp '$1' does not exist.", warp);
		else if (!sender.hasPermission(
				Helpers.Parameters.replace(
						"vpcore.warps.$1",
						warp.toLowerCase()))) Helpers.Messages.sendFailure(
								sender,
								"You do not have permission to use warp $1.", warp);
		else
		{
			super.debug("WARP sign created at $1.", sign.getLocation());
			signData = this.updateWarpSignData(owner, sign, warp);
		}

		if (signData == null)
		{
			sign.setLine(1, "");
			sign.setLine(2, warp);
			sign.setLine(3, "");
		}
		
		return signData;
	}

	private SignData updateBuyingSellingSign(
			final Sign sign,
			final CommandSender sender,
			final OfflinePlayer owner,
			final boolean withSideEffects)
	{
		SignData signData = null;
		String quantityString;
		String priceString;
		final String line1 = sign.getLine(1);
		boolean isOutOfStock = false;
		if (line1.contains("@"))
		{
			final String[] parts = line1.split("@");
			quantityString = parts[0].trim();
			priceString = parts[1].trim();
		}
		else
		{
			switch (ChatColor.stripColor(line1).toLowerCase())
			{
			case SignSettings.FulfilledText:
			case SignSettings.OutOfStockText:
				isOutOfStock = true;
				priceString = "0";
				quantityString = "0";
				break;
				
			default:
				quantityString = line1;
				priceString = sign.getLine(3).trim();
				break;
			}
		}
		
		Player player = null;
		boolean playerIsAdmin = false;
		if (sender instanceof Player) player = (Player)sender;
		if (player != null) playerIsAdmin = player.hasPermission("vpcore.admin");

		String item = sign.getLine(2).trim();
		String metadata = "";
		if (item.startsWith("#"))
		{
			if (player == null)
			{
				Helpers.Messages.sendFailure(sender, "Cannot automatically get item in inventory.");
				return null;
			}
			else
			{
				int index = -1;
				final String indexString = item.substring(1);
				try { index = Integer.parseInt(indexString); }
				catch (NumberFormatException ex)
				{
					Helpers.Messages.sendFailure(sender, "Specified inventory slot is not a valid number.");
					return null;
				}
				
				if (index < 1 || index > 9)
				{
					Helpers.Messages.sendFailure(sender, "You must specify a slot from $1 to $2.", "1", "9");
					return null;
				}
				
				index -= 1;				
				final ItemStack selectedItem = player.getInventory().getItem(index);
				if (selectedItem == null || selectedItem.getType() == Material.AIR)
				{
					Helpers.Messages.sendFailure(sender, "Inventory slot $1 is empty.", index + 1);
					return null;
				}
				else if (index == player.getInventory().getHeldItemSlot() && !playerIsAdmin)
				{
					Helpers.Messages.sendFailure(sender, "You cannot specify the currently-selected slot!");
					return null;
				}
				
				item = Helpers.Item.getFriendlyNames(selectedItem)[0];
				metadata = Helpers.Item.getFullMetadata(selectedItem);
			}
		}

		if (quantityString.length() == 0 ||
				item.length() == 0 ||
				priceString.length() == 0)
		{
			this.formatSignHeaderForError(sign);
			Helpers.Messages.sendFailure(sender, "Quantity, item name and price must be specified.");
			return null;
		}

		boolean valid = true;
		int quantity = 0;
		double price = 0;

		try
		{
			quantity = Integer.parseInt(quantityString);
			if (quantity <= 0 && !isOutOfStock)
			{
				valid = false;
				Helpers.Messages.sendFailure(sender, "Quantity must be greater than 0.");
			}
		}
		catch (final NumberFormatException e)
		{
			valid = false;
			Helpers.Messages.sendFailure(sender, "Quantity '$1' is not valid.", quantityString);
		}

		final ItemStack stack = Helpers.Item.getItemStack(item, quantity);
		if (stack == null)
		{
			valid = false;
			Helpers.Messages.sendFailure(sender, "Item name '$1' is not valid.", item);
		}
		else
		{
			item = Helpers.Item.getFriendlyNames(stack)[0];
			if (!Helpers.Item.isSupported(stack))
			{
				valid = false;
				Helpers.Messages.sendFailure(sender, "Item name '$1' is not supported yet.", item);
				Helpers.Messages.sendFailure(sender, "We're waiting for the Bukkit/Spigot teams to catch up with us.");
			}
			else if (!Helpers.Item.isShortNameCompatible(stack))
			{
				valid = false;
				Helpers.Messages.sendFailure(sender, "There are no short names for '$1' items.", item);
				Helpers.Messages.sendFailure(sender, "Support will be added in a future VPCore release.");
			}
			else
			{
				int maxStackSize = player.getInventory().getMaxStackSize();
				if (stack.getMaxStackSize() < maxStackSize) maxStackSize = stack.getMaxStackSize();
				if (quantity > maxStackSize)
				{
					valid = false;
					Helpers.Messages.sendFailure(sender, "The maximum quantity for $1 is $2.", item, maxStackSize);
				}
			}
		}

		try
		{
			price = Double.parseDouble(priceString.replace("$", ""));
			price = EconomyManager.round(price);
			if (price <= 0 && !isOutOfStock)
			{
				valid = false;
				Helpers.Messages.sendFailure(sender, "Price must be greater than $0.");
			}
		}
		catch (final NumberFormatException e)
		{
			valid = false;
			Helpers.Messages.sendFailure(sender, "Price '$1' is not valid.", priceString);
		}
		
		if (!super.getPlugin().isVaultEnabled())
		{
			valid = false;
			Helpers.Messages.sendFailure(sender, "Vault support is not enabled.");
		}

		if (valid)
		{
			final Location location = sign.getLocation();
			signData = this.getSignData(location);
			boolean isServerOwned = false;
			if (signData == null) isServerOwned = playerIsAdmin;
			else isServerOwned = signData.isServerOwned();

			if (this.isSellingSign(sign))
			{
				if (withSideEffects && !isServerOwned)
				{
					if (metadata.length() > 0) Helpers.Item.applyMetadata(stack, metadata);
					if (player.getInventory().containsAtLeast(stack, quantity))
					{
						player.getInventory().removeItem(stack);
						Helpers.Hacks.updateInventory(super.getPlugin(), player);
					}
					else
					{
						Helpers.Messages.sendFailure(sender, "You do not have enough $1 in your inventory.", item);
						valid = false;
					}
				}

				if (valid)
				{
					super.debug("SELLING sign created at $1.", sign.getLocation());
					signData = this.updateSellingSignData(owner, sign, item, metadata, quantity, price, isServerOwned);
				}
			}
			else if (this.isBuyingSign(sign))
			{
				if (withSideEffects && !isServerOwned)
				{
					final EconomyManager economyManager = super.getPlugin().getEconomyManager();
					if (economyManager.has(player, price))
					{
						economyManager.debit(player, price);
						Helpers.Messages.sendSuccess(
								sender,
								"You put $$1 into the sign.  You have $$2.",
								Helpers.Money.formatString(price),
								Helpers.Money.formatString(economyManager.getBalance(player)));
					}
					else
					{
						Helpers.Messages.sendFailure(
								sender,
								"You need $$1 to create this sign. You have $$2.",
								Helpers.Money.formatString(price),
								Helpers.Money.formatString(economyManager.getBalance(player)));
						valid = false;
					}
				}
				
				if (valid)
				{
					super.debug("BUYING sign created at $1.", sign.getLocation());
					signData = this.updateBuyingSignData(owner, sign, item, metadata, quantity, price, isServerOwned);
				}					
			}
		}

		if (signData == null)
		{
			String displayName = owner.getName();
			if (sender.hasPermission("vpcore.admin")) displayName = this.getServerSignDisplayName();
			sign.setLine(1, Helpers.Parameters.replace("$1 @ $$2", quantity, Helpers.Money.formatString(price)));
			sign.setLine(2, Helpers.Parameters.replace("$1", item));
			sign.setLine(3, ChatColor.DARK_GRAY + displayName);
			sign.update();
		}
		
		return signData;
	}
	
	public void withdrawMoneyFromSign(final SignData data, final Player player)
	{
		if (data.getMoneyLevel() == 0) return;
		
		final EconomyManager economyManager = super.getPlugin().getEconomyManager();
		if (economyManager.canReceive(player, data.getMoneyLevel()))
		{
			final double moneyLevel = data.getMoneyLevel();
			economyManager.credit(player, moneyLevel);
			
			data.withdrawMoney();
			this.updateSignData(data);

			Helpers.Messages.sendSuccess(player, "You withdrew $$1 from the sign.", moneyLevel);
		}
		else
		{
			Helpers.Messages.sendFailure(player, "Your balance is too close to its limit.");
			Helpers.Messages.sendFailure(
					player,
					"The sign still contains $$1.",
					Helpers.Money.formatString(data.getMoneyLevel()));
		}
	}
	
	public void withdrawItemsFromSign(final SignData data, final Player player)
	{
		if (data.getItemLevel() == 0) return;
		
		final ItemStack stack = Helpers.Item.getItemStack(data.getItem(), data.getItemLevel());
		if (data.hasMetadata()) Helpers.Item.applyMetadata(stack, data.getMetadata());
		
		Helpers.Item.putInInventory(player.getInventory(), stack);
		final int received = data.getItemLevel() - stack.getAmount();
		if (received == 0) Helpers.Messages.sendFailure(player, "Your inventory is full.");
		else
		{
			data.withdrawItems(received);
			this.updateSignData(data);

			Helpers.Messages.sendSuccess(player, "You took $1 x $2 from the sign.", received, data.getItem());
		}
		if (stack.getAmount() > 0)
			Helpers.Messages.sendInfo(player, "The sign still holds $1 x $2.", stack.getAmount(), data.getItem());
	}
}
