package com.rekalogic.vanillapod.vpcore.sign;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Attachable;

import com.rekalogic.vanillapod.vpcore.player.EconomyManager;
import com.rekalogic.vanillapod.vpcore.player.PlayerSettings;
import com.rekalogic.vanillapod.vpcore.plot.PlotData;
import com.rekalogic.vanillapod.vpcore.plot.PlotSettings;
import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public final class SignEventHandler extends EventHandlerBase<SignSettings>
{
	private final PlayerSettings playerSettings;
	private final PlotSettings plotSettings;

	public SignEventHandler(
			final PluginBase plugin,
			final SignSettings settings,
			final PlayerSettings playerSettings,
			final PlotSettings plotSettings)
	{
		super(plugin, settings);
		this.playerSettings = playerSettings;
		this.plotSettings = plotSettings;
	}

	private void useSellingSign(final Player player, final Sign sign)
	{
		if (!super.getPlugin().isVaultEnabled())
		{
			Helpers.Messages.sendFailure(player, "Vault support is not enabled.");
			return;
		}

		final SignData data = super.getSettings().getSignData(sign.getLocation());
		
		if (player.isSneaking())
		{
			Helpers.Messages.sendSuccess(
					player,
					"This sign is selling $1 x $2 for $$3.",
					data.getQuantity(),
					data.getItem(),
					Helpers.Money.formatString(data.getPrice()));
			if (data.hasMetadata()) Helpers.Messages.sendNotification(player, "Metadata: $1", data.getMetadata());
			if (data.isServerOwned()) Helpers.Messages.sendInfo(player, "This is a server-owned sign and has an infinite supply.");
			else if (player.getUniqueId().equals(data.getPlayerUuid()) || player.hasPermission("vpcore.moderator"))
			{
				Helpers.Messages.sendInfo(
						player,
						"Stock level: $1, earnings: $$2.",
						data.getItemLevel(),
						Helpers.Money.formatString(data.getMoneyLevel()));
			}
			
			return;
		}

		final EconomyManager economyManager = super.getPlugin().getEconomyManager();
		final ItemStack stack = Helpers.Item.getItemStack(data.getItem(), data.getQuantity());

		if (stack == null)
		{
			Helpers.Messages.sendFailure(player, "That item is not supported.");
			super.severe("Sign $1 at $2 has an invalid item.", sign.getLine(0), sign.getLocation());
			return;
		}
		
		if (data.hasMetadata()) Helpers.Item.applyMetadata(stack, data.getMetadata());		

		if (!data.isServerOwned() &&
				data.getPlayerUuid().equals(player.getUniqueId()))
		{
			if (data.getMoneyLevel() > 0) super.getSettings().withdrawMoneyFromSign(data, player);
			else if (stack.isSimilar(player.getItemInHand()))
			{
				ItemStack heldStack = player.getItemInHand();
				if (heldStack.getAmount() < data.getQuantity())
				{
					Helpers.Messages.sendFailure(
							player,
							"You need $1 x $2 in your hand to top up this sign.",
							data.getQuantity(),
							data.getItem());
				}
				else
				{
					final int remainingAmount = heldStack.getAmount() - data.getQuantity();
					if (remainingAmount == 0) player.setItemInHand(null);
					else heldStack.setAmount(remainingAmount);
					data.replenishItems(data.getQuantity());
					super.getSettings().updateSignData(data);
									
					Helpers.Messages.sendSuccess(
							player,
							"You topped up the sign.  It now has $1 x $2.",
							data.getItemLevel(),
							data.getItem());
					
					this.getSettings().updateBuyingSellingSignText(sign, data);
				}
			}
			else Helpers.Messages.sendFailure(player, "This sign holds no money that can be withdrawn.");
		}
		else if (!data.isServerOwned() && data.getItemLevel() < data.getQuantity()) Helpers.Messages.sendFailure(player, "This sign is out of stock.");
		else if (!economyManager.has(player, data.getPrice())) Helpers.Messages.sendFailure(player, "You do not have enough money to buy that.");
		else if (!Helpers.Item.canCarry(player.getInventory(), stack)) Helpers.Messages.sendFailure(player, "You do not have enough inventory space to buy that.");
		else
		{
			UUID ownerUuid = null;
			if (!data.isServerOwned())
			{
				data.doOnePurchase();
				super.getSettings().updateSignData(data);
				ownerUuid = data.getPlayerUuid();
			}

			super.getSettings().recordPurchase(
					player.getUniqueId(),
					ownerUuid,
					data.getItem(),
					data.getQuantity(),
					data.getPrice());

			Helpers.Item.putInInventory(player.getInventory(), stack);
			economyManager.debit(player, data.getPrice());

			Helpers.Messages.sendSuccess(
					player,
					"You purchased $1 x $2 for $$3.",
					data.getQuantity(),
					data.getItem(),
					Helpers.Money.formatString(data.getPrice()));
			
			if (!data.isServerOwned())
			{
				final Player sellingPlayer = Bukkit.getPlayer(data.getPlayerUuid());
				if (sellingPlayer != null)
				{
					Helpers.Messages.sendNotification(
							sellingPlayer,
							"$1 purchased $2 x $3 from your sign.",
							player,
							data.getQuantity(),
							data.getItem());
					if (data.getItemLevel() == 0)
						Helpers.Messages.sendInfo(sellingPlayer, "The sign at $1 is now out of stock.", data.getLocation());
				}
			}
			
			this.getSettings().updateBuyingSellingSignText(sign, data);
		}
	}

	private void useBuyingSign(final Player player, final Sign sign)
	{
		if (!super.getPlugin().isVaultEnabled())
		{
			Helpers.Messages.sendFailure(player, "Vault support is not enabled.");
			return;
		}
		
		final SignData data = super.getSettings().getSignData(sign.getLocation());
		
		if (player.isSneaking())
		{
			Helpers.Messages.sendSuccess(
					player,
					"This sign is buying $1 x $2 for $$3.",
					data.getQuantity(),
					data.getItem(),
					Helpers.Money.formatString(data.getPrice()));
			if (data.hasMetadata()) Helpers.Messages.sendNotification(player, "Metadata: $1", data.getMetadata());
			if (data.isServerOwned()) Helpers.Messages.sendInfo(player, "This is a server-owned sign and has an infinite budget.");
			else if (player.getUniqueId().equals(data.getPlayerUuid()) || player.hasPermission("vpcore.moderator"))
			{
				Helpers.Messages.sendInfo(
						player,
						"Remaining funds: $$1, items purchased: $2.",
						Helpers.Money.formatString(data.getMoneyLevel()),
						data.getItemLevel());
			}
			
			return;
		}
		
		final EconomyManager economyManager = super.getPlugin().getEconomyManager();
		final ItemStack stack = Helpers.Item.getItemStack(data.getItem(), data.getQuantity());
		
		if (data.hasMetadata()) Helpers.Item.applyMetadata(stack, data.getMetadata());

		if (stack == null)
		{
			Helpers.Messages.sendFailure(player, "That item is not supported.");
			super.severe("Sign $1 at $2 has an invalid item.", sign.getLine(0), sign.getLocation());
			return;
		}

		if (!data.isServerOwned() &&
				data.getPlayerUuid().equals(player.getUniqueId()))
		{
			if (data.getItemLevel() > 0) super.getSettings().withdrawItemsFromSign(data, player);
			else if (economyManager.has(player, data.getPrice()))
			{
				economyManager.debit(player, data.getPrice());
				data.replenishMoney(data.getPrice());
				super.getSettings().updateSignData(data);

				sign.setLine(
						1,
						Helpers.Parameters.replace(
								"$1 @ $$2",
								data.getQuantity(),
								Helpers.Money.formatString(data.getPrice())));
				sign.update();

				Helpers.Messages.sendSuccess(
						player,
						"You topped up the sign with $$1. It now has $$2.",
						Helpers.Money.formatString(data.getPrice()),
						Helpers.Money.formatString(data.getMoneyLevel()));
				
				this.getSettings().updateBuyingSellingSignText(sign, data);
			}
			else Helpers.Messages.sendFailure(
					player,
					"You need $$1 to top up this sign; you have $$2.",
					Helpers.Money.formatString(data.getPrice()),
					Helpers.Money.formatString(economyManager.getBalance(player)));
		}
		else if (!data.isServerOwned() && data.getMoneyLevel() < data.getPrice()) Helpers.Messages.sendFailure(player, "This sign has been fulfilled.");
		else if (!economyManager.canReceive(player, data.getPrice())) Helpers.Messages.sendFailure(player, "Your balance is too close to its limit.");
		else if (!player.getInventory().containsAtLeast(stack, data.getQuantity())) Helpers.Messages.sendFailure(player, "You do not have $1 x $2 to sell.", stack.getAmount(), stack.getType());
		else
		{
			UUID ownerUuid = null;
			if (!data.isServerOwned())
			{
				data.doOneSale();
				super.getSettings().updateSignData(data);
				ownerUuid = data.getPlayerUuid();
			}

			super.getSettings().recordSale(
					player.getUniqueId(),
					ownerUuid,
					data.getItem(),
					data.getQuantity(),
					data.getPrice());

			player.getInventory().removeItem(stack);
			economyManager.credit(player, data.getPrice());

			Helpers.Messages.sendSuccess(
					player,
					"You sold $1 x $2 for $$3.",
					stack.getAmount(),
					data.getItem(),
					Helpers.Money.formatString(data.getPrice()));
			
			if (!data.isServerOwned())
			{
				final Player sellingPlayer = Bukkit.getPlayer(data.getPlayerUuid());
				if (sellingPlayer != null)
				{
					Helpers.Messages.sendNotification(
							sellingPlayer,
							"$1 sold $2 x $3 to your sign.",
							player,
							stack.getAmount(),
							data.getItem());
					if (data.getMoneyLevel() < data.getPrice())
						Helpers.Messages.sendInfo(sellingPlayer, "The sign at $1 is now fulfilled.", data.getLocation());
				}
			}			

			this.getSettings().updateBuyingSellingSignText(sign, data);
		}
	}

	private void useWarpSign(final Player player, final Sign sign)
	{
		final String warpName = sign.getLine(2);
		final boolean warpPermission = player.hasPermission(
				Helpers.Parameters.replace(
						"vpcore.warps.$1",
						warpName.toLowerCase())) ||
				player.hasPermission("vpcore.moderator") ||
				player.hasPermission("vpcore.warps.*");
		boolean signWarpPermission = false;
		if (!warpPermission) signWarpPermission = player.hasPermission(
				Helpers.Parameters.replace(
						"vpcore.signs.warp.$1",
						warpName.toLowerCase())); 
		if (warpPermission || signWarpPermission)
		{
			final Location warp = this.playerSettings.getWarp(warpName);
			if (warp == null) Helpers.Messages.sendFailure(player, "The warp $1 does not exist.", warpName);
			else player.teleport(warp, TeleportCause.COMMAND);
		}
		else Helpers.Messages.sendFailure(player, "You do not have permission to use warp $1.", warpName);
	}

	private void useDisposalSign(final Player player)
	{
		final Inventory inventory = Bukkit.getServer().createInventory(null, 4 * 9, "Disposal");
		player.openInventory(inventory);
	}

	private boolean doesBlockSupportControlledSign(final Block block)
	{
		if (block == null) return false;

		for (final BlockFace face: BlockFace.values())
		{
			final Block relativeBlock = block.getRelative(face);
			if (super.getSettings().isControlledSign(relativeBlock))
			{
				final Sign sign = (Sign)relativeBlock.getState();
				final Attachable attachable = (Attachable)sign.getData();
				if (attachable.getAttachedFace().getOppositeFace() == face) return true;
			}
		}

		return false;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onSignChange(final SignChangeEvent event)
	{
		if (event.isCancelled()) return;

		final Player player = event.getPlayer();
		if (super.getSettings().isControlledSign(event.getLine(0)))
		{
			final Sign sign = (Sign)event.getBlock().getState();

			for (int i = 0; i < 4; ++i)
				sign.setLine(i, event.getLine(i));
			
			final String permission = super.getSettings().getSignPermission(sign, "place");
			if (player.hasPermission(permission) ||
					player.hasPermission("vpcore.admin")) super.getSettings().updateSign(sign, player, player, true);
			else Helpers.Messages.sendFailure(player, "You do not have permission to create that type of sign.");
			
			event.setCancelled(true);
		}
		else if (player.hasPermission("vpcore.sign.colour"))
		{
			for (int i = 0; i < 4; ++i)
				event.setLine(i, Helpers.formatMessage(event.getLine(i)));
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onBlockPlace(final BlockPlaceEvent event)
	{
		if (event.isCancelled()) return;

		final Block block = event.getBlockAgainst();
		if (super.getSettings().isControlledSign(block)) event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onBlockBreak(final BlockBreakEvent event)
	{
		if (event.isCancelled()) return;

		final Block block = event.getBlock();
		final Player player = event.getPlayer();
		if (super.getSettings().isControlledSign(block))
		{
			final Sign sign = (Sign)block.getState();

			if (super.getSettings().isSignBroken(sign)) return;

			if (player.hasPermission("vpcore.moderator") ||
					player.hasPermission(super.getSettings().getSignPermission(sign, "place")))
			{
				final SignData data = super.getSettings().getSignData(block.getLocation());
				if (data == null) super.severe("Sign at $1 had no sign data.", sign.getLocation());
				else if (player.hasPermission("vpcore.admin") ||
						data.getPlayerUuid().equals(player.getUniqueId()))
				{
					if (data.isBuyingSign() || data.isSellingSign())
					{
						if (!super.getPlugin().isVaultEnabled())
						{
							Helpers.Messages.sendFailure(player, "Vault support is not enabled.");
							event.setCancelled(true);
							return;
						}

						if (data.isServerOwned()) Helpers.Messages.sendSuccess(player, "No money/items returned (server sign).");
						else 
						{
							super.getSettings().withdrawMoneyFromSign(data, player);
							super.getSettings().withdrawItemsFromSign(data, player);
							
							if (data.getMoneyLevel() > 0 || data.getItemLevel() > 0)
							{
								event.setCancelled(true);
								return;
							}
						}
					}
					else if (data.isPlotSign())
					{
						final PlotData plotData = this.plotSettings.getPlotData(data.getItem());
						if (plotData == null)
						{
							Helpers.Messages.sendFailure(
									player,
									"Plot $1 has no plot data!",
									data.getItem());
						}
						else
						{
							final int signCount = super.getSettings().getPlotSigns(plotData.getName()).size() - 1;
							Helpers.Messages.sendSuccess(
									player,
									"Plot $1 now has $2 sign$3.",
									plotData,
									signCount,
									Helpers.getPlural(signCount));
						}
					}

					super.getSettings().removeSignData(block.getLocation());
				}
				else
				{
					Helpers.Messages.sendFailure(player, "You cannot break a sign owned by someone else.");
					event.setCancelled(true);
				}
			}
			else
			{
				Helpers.Messages.sendFailure(player, "You do not have permission to break that type of sign.");
				event.setCancelled(true);
			}
		}
		else if (this.doesBlockSupportControlledSign(block))
		{
			Helpers.Messages.sendFailure(player, "A sign is attached to that block. Break the sign first.");
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onBlockIgnite(final BlockIgniteEvent event)
	{
		if (event.isCancelled()) return;

		final Block block = event.getBlock();
		final Player player = event.getPlayer();
		if (super.getSettings().isControlledSign(block))
		{
			if (player != null) Helpers.Messages.sendFailure(player, "You do not have permission to ignite that sign.");
			event.setCancelled(true);
		}
		else if (this.doesBlockSupportControlledSign(block))
		{
			if (player != null) Helpers.Messages.sendFailure(player, "A sign is attached to that block. Break the sign first.");
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onBlockBurn(final BlockBurnEvent event)
	{
		if (event.isCancelled()) return;

		final Block block = event.getBlock();
		if (super.getSettings().isControlledSign(block) ||
				this.doesBlockSupportControlledSign(block))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onBlockPistonExtend(final BlockPistonExtendEvent event)
	{
		if (event.isCancelled()) return;

		for (final Block block: event.getBlocks())
		{
			if (super.getSettings().isControlledSign(block) ||
					this.doesBlockSupportControlledSign(block))
			{
				event.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onBlockPistonRetract(final BlockPistonRetractEvent event)
	{
		if (event.isCancelled()) return;
		if (!event.isSticky()) return;

		final Block block = event.getBlock();
		if (super.getSettings().isControlledSign(block) ||
				this.doesBlockSupportControlledSign(block))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
	private void onPlayerInteract(final PlayerInteractEvent event)
	{
		Block block = null;
		switch (event.getAction())
		{
		case RIGHT_CLICK_AIR:
			block = Helpers.getTargetBlock(event.getPlayer());
			break;
		case RIGHT_CLICK_BLOCK:
			block = event.getClickedBlock();
			break;

		default: return;
		}

		if (block == null) return;

		final Material material = block.getType();
		switch (material)
		{
		case WALL_SIGN:
		case SIGN_POST: break;

		default: return;
		}

		final Sign sign = (Sign)block.getState();
		if (super.getSettings().isControlledSign(sign))
		{
			final Player player = event.getPlayer();
			if (super.getSettings().isSignBroken(sign)) Helpers.Messages.sendFailure(player, "This sign appears to be broken.");
			else
			{
				final SignData data = super.getSettings().getSignData(sign.getLocation());
				if (data == null)
				{
					Helpers.Messages.sendError(player, "Sign data not found.");
					super.severe("Sign at $1 has no sign data.", sign.getLocation());
					event.setCancelled(true);
					return;
				}
				
				if (player.hasPermission(super.getSettings().getSignPermission(sign, "use")))
				{
					if (super.getSettings().isSellingSign(sign)) this.useSellingSign(player, sign);
					else if (super.getSettings().isBuyingSign(sign)) this.useBuyingSign(player, sign);
					else if (super.getSettings().isWarpSign(sign)) this.useWarpSign(player, sign);
					else if (super.getSettings().isDisposalSign(sign)) this.useDisposalSign(player);
					else if (super.getSettings().isPlotSign(sign))
					{
		                final PlotData plotData = this.plotSettings.searchForAssociatedPlotData(block);
		                boolean updatePlotHead = player.hasPermission("vpcore.moderator");
		                if (plotData.getOwnerUuid() == null)
		                {
		                	final ProtectedRegion plot = this.plotSettings.getRegionManager().getRegion(plotData.getName());
		                	final int x = plot.getMaximumPoint().getBlockX() - plot.getMinimumPoint().getBlockX();
		                	final int z = plot.getMaximumPoint().getBlockZ() - plot.getMinimumPoint().getBlockZ();
		                	Helpers.Messages.sendSuccess(
		                			player,
		                			"Plot $1 ($2 x $3) is for sale for $$4.",
		                			plotData.getName(),
		                			x,
		                			z,
		                			Helpers.Money.formatString(plotData.getBuyPrice()));
		                	Helpers.Messages.sendSuccess(
		                			player,
		                			"To buy it, stand in the plot and use the $1 command.",
		                			"/plot buy");
		                }
		                else if (player.getUniqueId().equals(plotData.getOwnerUuid()))
		                {
		                	Helpers.Messages.sendSuccess(player, "This plot is owned by you.");
		                	Helpers.Messages.sendSuccess(
		                			player,
		                			"To sell it, stand in the plot and use the $1 command.",
		                			"/plot sell");
		                	updatePlotHead = true;
		                }
		                else Helpers.Messages.sendFailure(player, "This plot is not for sale.");
		                
		                if (updatePlotHead) this.plotSettings.updatePlotHead(sign, plotData);
					}
					else
					{
						Helpers.Messages.sendError(player, "This type of sign is not handled!");
						super.severe("Sign of type '$1' is not handled at $2!", sign.getLine(0), sign.getLocation());
					}

					Helpers.Hacks.updateInventory(super.getPlugin(), player);
				}
				else Helpers.Messages.sendFailure(player, "You do not have permission to use this sign.");
			}
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onEntityExplode(final EntityExplodeEvent event)
	{
		if (event.isCancelled()) return;

		ArrayList<Block> removeList = null;
		for (final Block block: event.blockList())
		{
			if (super.getSettings().isControlledSign(block) ||
					this.doesBlockSupportControlledSign(block))
			{
				if (removeList == null) removeList = new ArrayList<Block>();
				removeList.add(block);
			}
		}
		if (removeList != null && removeList.size() > 0)
			event.blockList().removeAll(removeList);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onEntityChangeBlock(final EntityChangeBlockEvent event)
	{
		if (event.isCancelled()) return;

		final Block block = event.getBlock();
		if (super.getSettings().isControlledSign(block) ||
				this.doesBlockSupportControlledSign(block))
			event.setCancelled(true);
	}
}
