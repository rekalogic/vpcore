package com.rekalogic.vanillapod.vpcore.sign;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;

public final class SignData
{
	private int id;
	private final String worldName;
	private final int x;
	private final int y;
	private final int z;
	private final String key;

	private final String type;
	private final boolean isServerOwned;
	private final UUID playerUuid;
	private final String item;
	private final String metadata;
	private final int quantity;
	private final double price;
	private int itemLevel;
	private double moneyLevel;

	public int getId() { return this.id; }
	public void setId(final int id) { this.id = id; }
	public String getWorldName() { return this.worldName; }
	public int getX() { return this.x; }
	public int getY() { return this.y; }
	public int getZ() { return this.z; }
	public String getKey() { return this.key; }

	public String getType() { return this.type; }
	public boolean isServerOwned() { return this.isServerOwned; }
	public UUID getPlayerUuid() { return this.playerUuid; }
	public String getItem() { return this.item; }
	public boolean hasMetadata() { return (this.metadata != null && this.metadata.length() > 0); }
	public String getMetadata() { return this.metadata; }
	public int getQuantity() { return this.quantity; }
	public double getPrice() { return this.price; }
	public int getItemLevel() { return this.itemLevel; }
	public double getMoneyLevel() { return this.moneyLevel; }
	public boolean isBuyingSign() { return this.type.equalsIgnoreCase("buying"); }
	public boolean isSellingSign() { return this.type.equalsIgnoreCase("selling"); }
	public boolean isPlotSign() { return this.type.equalsIgnoreCase("plot"); }
	
	private Location location = null;
	public Location getLocation()
	{
		if (this.location == null)
		{
			this.location = new Location(
					Bukkit.getWorld(this.worldName),
					this.x,
					this.y,
					this.z);
		}
		return this.location;
	}

	public SignData(
			final Location location,
			final String type,
			final UUID playerUuid, final boolean isServerOwned,
			final String item, final String metadata,
			final int quantity, final double price,
			final int itemLevel, final double moneyLevel)
	{
		this(
				0,
				location.getWorld().getName(),
				location.getBlockX(),
				location.getBlockY(),
				location.getBlockZ(),
				type,
				playerUuid, isServerOwned, 
				item, metadata,
				quantity, price,
				itemLevel, moneyLevel);
	}

	public SignData(
			final int id,
			final String worldName, final int x, final int y, final int z,
			final String type,
			final UUID playerUuid, final boolean isServerOwned,
			final String item, final String metadata,
			final int quantity, final double price,
			final int itemLevel, final double moneyLevel)
	{
		this.id = id;
		this.worldName = worldName;
		this.x = x;
		this.y = y;
		this.z = z;
		this.type = type;
		this.playerUuid = playerUuid;
		this.isServerOwned = isServerOwned;
		this.item = item;
		this.metadata = metadata;
		this.quantity = quantity;
		this.price = price;
		this.itemLevel = itemLevel;
		this.moneyLevel = moneyLevel;
		
		this.key = SignData.getKey(worldName, x, y, z);
	}

	public static String getKey(final Location location)
	{
		return SignData.getKey(
				location.getWorld().getName(),
				location.getBlockX(),
				location.getBlockY(),
				location.getBlockZ());
	}

	private static String getKey(final String worldName, final int x, final int y, final int z)
	{
		return worldName.toLowerCase() + "#" + x + "#" + y + "#" + z;
	}

	public static SignData createWarpSignData(
			final Location location,
			final String warp,
			final OfflinePlayer player,
			final boolean isServerOwned)
	{
		return new SignData(
				location,
				"warp",
				player.getUniqueId(), isServerOwned,
				warp, "",
				0, 0, 0, 0);
	}

	public static SignData createBuyingSignData(
			final Location location,
			final String item,
			final String metadata,
			final int quantity,
			final double price,
			final OfflinePlayer player,
			final boolean isServerOwned)
	{
		return new SignData(
				location,
				"buying",
				player.getUniqueId(), isServerOwned,
				item, metadata,
				quantity, price,
				0, price);
	}

	public static SignData createSellingSignData(
			final Location location,
			final String item,
			final String metadata,
			final int quantity,
			final double price,
			final OfflinePlayer player,
			final boolean isServerOwned)
	{
		return new SignData(
				location,
				"selling",
				player.getUniqueId(), isServerOwned,
				item, metadata,
				quantity, price,
				quantity, 0);
	}

	public static SignData createDisposalSign(
			final Location location,
			final OfflinePlayer player,
			final boolean isServerOwned)
	{
		return new SignData(
				location,
				"disposal",
				player.getUniqueId(), isServerOwned,
				"", "",
				0, 0, 0, 0);
	}

	public static SignData createPlotSign(
			final Location location,
			final String plotName,
			final OfflinePlayer player,
			final boolean isServerOwned)
	{
		return new SignData(
				location,
				"plot",
				player.getUniqueId(), isServerOwned,
				plotName, "",
				0, 0, 0, 0);
	}

	public void doOnePurchase()
	{
		this.itemLevel -= this.quantity;
		if (this.itemLevel < 0) this.itemLevel = 0;
		this.moneyLevel += this.price;
	}

	public void doOneSale()
	{
		this.moneyLevel -= this.price;
		if (this.moneyLevel < 0) this.moneyLevel = 0;
		this.itemLevel += this.quantity;
	}

	public void replenishMoney(final double amount)
	{
		if (this.moneyLevel < 0) this.moneyLevel = 0;
		this.moneyLevel += amount;
	}

	public void withdrawMoney()
	{
		this.moneyLevel = 0;
	}

	public void withdrawItems(final int amount)
	{
		this.itemLevel -= amount;
		if (this.itemLevel < 0) this.itemLevel = 0;
	}

	public void replenishItems(final int amount)
	{
		if (this.itemLevel < 0) this.itemLevel = 0;
		this.itemLevel += amount;
	}
}
