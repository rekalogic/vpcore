package com.rekalogic.vanillapod.vpcore.sign;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.plot.PlotData;
import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class SignExecutor extends ExecutorBase<SignSettings>
{
	public SignExecutor(final VPPlugin plugin, final SignSettings settings)
	{
		super(plugin, settings, new String[] { "fixsigns", "sign" });
	}

	@Override
	protected boolean checkCommandAllowed(final String command, final boolean isConsole)
	{
		switch (command)
		{
		case "sign": return (!isConsole);
		}
		
		return super.checkCommandAllowed(command, isConsole);
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "sign": return (argCount >= 1);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "fixsigns": return new String[] { ExecutorBase.FLAG_BOOL_CONFIRM };		
		}
		
		return super.getAllowedFlags(command);
	}

	@Override
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "sign":
			switch (argIndex)
			{
			case 0: return Arrays.asList("clipboard", "copy", "edit", "paste", "validate");
			}
		}
		
		return super.onTabComplete(sender, command, argIndex, args);
	}
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "sign":
			switch (argIndex)
			{
			case 0: return LookupSource.FROM_PLUGIN;
			}
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		switch (command)
		{
		case "fixsigns": return this.doFixSigns(sender, args);
		case "sign": return this.doSign(sender, args);
		}

		return super.onCommand(sender, command, label, args);
	}
	
	private boolean doSign(final PagedOutputCommandSender sender, final String[] args)
	{
		final Player player = sender.getPlayer();
		Block block = null;
		Sign sign = null;
		String subCommand = args[0].toLowerCase();

		switch (subCommand)
		{
		case "c":
		case "cp": subCommand = "copy"; break;
		case "p": subCommand = "paste"; break;
		case "e":
		case "ed": subCommand = "edit"; break;
		case "v":
		case "val":
		case "valid": subCommand = "validate"; break;
		case "clip": subCommand = "clipboard"; break;
		}
		
		if (!sender.hasPermission(Helpers.Parameters.replace("vpcore.command.sign.$1", subCommand)))
		{
			sender.sendFailure("You do not have permission to use the $1 subcommand.", subCommand);
			return true;
		}

		switch (subCommand)
		{
		case "edit":
			if (args.length == 1) return false;
			break;
		default:
			if (args.length != 1) return false;
			break;
		}

		switch (subCommand)
		{
		case "copy":
		case "paste":
		case "edit":
		case "validate":
			block = Helpers.getTargetBlock(player);
			if (block != null)
			{
				switch (block.getType())
				{
				case WALL_SIGN:
				case SIGN_POST:
					break;
				default:
					block = null;
					break;
				}
			}
			if (block == null)
			{
				sender.sendFailure("You must target a sign when using this command.");
				return true;
			}
			sign = (Sign)block.getState();
			break;
		}
		
		switch (subCommand)
		{
		case "validate":
			if (!super.getSettings().isControlledSign(sign))
			{
				sender.sendFailure("This sign is not a controlled type.");
				return true;
			}
			break;
		}
		
		switch (subCommand)
		{
		case "edit":
		case "validate":
			{
				final SignData data = super.getSettings().getSignData(block.getLocation());
				boolean canUseSign = false;
				if (data == null) canUseSign = true;
				else if (data.isServerOwned())
				{
					if (sender.hasPermission("vpcore.admin")) canUseSign = true;
					else if (sender.hasPermission("vpcore.moderator"))
					{
						sender.sendFailure("You can not do this to server-owned signs.");
						return true;
					}
				}
				else if (data.getPlayerUuid().equals(sender.getPlayer().getUniqueId())) canUseSign = true;
				else if (sender.hasPermission("vpcore.admin")) canUseSign = true;
				else if (sender.hasPermission("vpcore.moderator")) canUseSign = true;

				if (!canUseSign)
				{
					sender.sendFailure("You can only do this to your own signs.");
					return true;
				}
			} break;
		}
		
		
		OfflinePlayer owner = null;
		switch (subCommand)
		{
		case "paste":
		case "edit":
		case "validate":
		{
			final SignData signData = super.getSettings().getSignData(block.getLocation());
			if (signData == null) owner = player;
			else
			{
				owner = Helpers.Bukkit.getOfflinePlayer(signData.getPlayerUuid());
				if (owner == null || !owner.hasPlayedBefore())
				{
					sender.sendError(
							"Player $1 was not found; you will take ownership of the sign.",
							signData.getPlayerUuid());
					owner = player;
				}
			}
		} break;
		}
		
		
		switch (subCommand)
		{
		case "copy":
			super.getSettings().setCopiedSign(player, sign.getLines());
			sender.sendSuccess("Sign copied to clipboard.");
			break;
		case "paste":
		{
			final String[] lines = super.getSettings().getCopiedSign(player);
			if (lines == null) sender.sendFailure("Clipboard is empty.");
			else super.getSettings().updateSign(block, player, owner, lines);
		} break;
		case "edit":
		{
			int lineNumber = 0;
			try { lineNumber = Integer.parseInt(args[1]); }
			catch (final NumberFormatException e) { return false; }
			if (lineNumber < 1 || lineNumber > 4)
			{
				sender.sendFailure("Line number must be in range (1..4)");
				return true;
			}
			final String[] lines = sign.getLines();
			String text = Helpers.Args.concatenate(args, 2);
			if (player.hasPermission("vpcore.sign.colour"))
				text = Helpers.formatMessage(text);
			lines[lineNumber - 1] = text;
			super.getSettings().updateSign(block, player, owner, lines);
		} break;
		case "validate":
			super.getSettings().updateSign(block, player, owner, sign.getLines());
			break;
		case "clipboard":
		{
			final String[] lines = super.getSettings().getCopiedSign(player);
			if (lines == null) sender.sendFailure("Clipboard is empty.");
			else
			{
				sender.sendNotification("Clipboard content:");
				for (int i = 0; i < lines.length; ++i)
					sender.sendInfo(lines[i]);
			}
		} break;
		case "fix": return this.doFixSigns(sender, args);
		default: return false;
		}
		return true;
	}

	private boolean doFixSigns(final PagedOutputCommandSender sender, final String[] args)
	{
		final boolean confirm = super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM);
		int signsFixed = 0;
		final Set<String> keys = new HashSet<String>();
		final Map<String, PlotData> plotDataCache = new HashMap<String, PlotData>();
		for (final SignData signData: super.getSettings().getSignData())
		{
			String reason = "";
			final Block block = signData.getLocation().getBlock();
			final String key = signData.getKey();
			
			Sign sign = null;
			OfflinePlayer owner = null;
			boolean mustRemoveSignData = false;
			boolean mustUpdateSign = false;

			if (keys.contains(key))
			{
				reason = "Duplicate sign data.";
				mustRemoveSignData = true;
			}
			else
			{
				keys.add(key);
				if (block.getState() instanceof Sign)
				{
					sign = (Sign)block.getState();
					if (super.getSettings().isControlledSign(sign))
					{
						UUID ownerUuid = null;
						PlotData plotData = null;
						if (super.getSettings().isPlotSign(sign))
						{
							if (plotDataCache.containsKey(signData.getItem())) plotData = plotDataCache.get(signData.getItem());
							else
							{
								plotData = super.getSettings().getPlotData(signData.getItem());
								plotDataCache.put(signData.getItem(), plotData);
							}
							if (plotData != null) ownerUuid = plotData.getOwnerUuid();
						}
						else if (!signData.isServerOwned()) ownerUuid = signData.getPlayerUuid(); 
							
						if (ownerUuid != null)
						{
							owner = Helpers.Bukkit.getOfflinePlayer(ownerUuid);
							if (owner == null || !owner.hasPlayedBefore())
							{
								reason = "Owning player does not exist.";
								super.debug("Player does not exist: $1.", ownerUuid);
								mustRemoveSignData = true;
							}
						}

						if (!mustRemoveSignData)
						{
							if (super.getSettings().isBuyingSign(sign) ||
									super.getSettings().isSellingSign(sign))
							{
								final String nameOnSign = ChatColor.stripColor(sign.getLine(3));
								if (signData.isServerOwned())
								{
									mustUpdateSign = (!nameOnSign.equals(super.getSettings().getServerSignDisplayName()));
									if (mustUpdateSign)
									{
										super.debug("Sign $1: Name was $2, should be $3.", signData.getId(), nameOnSign, super.getSettings().getServerSignDisplayName());
										reason = "Server sign shows wrong owner.";
									}
								}
								else
								{
									mustUpdateSign = (!nameOnSign.equals(owner.getName()));
									if (mustUpdateSign)
									{
										super.debug("Sign $1: Name was $2, should be $3.", signData.getId(), nameOnSign, owner.getName());
										reason = "Player sign shows wrong owner.";
									}
								}
							}
							else if (super.getSettings().isPlotSign(sign))
							{
								if (plotData == null)
								{
									super.getSettings().formatSignHeaderForError(sign);
									reason = "No plot data associated with plot.";
								}
								else if (owner == null)
								{
									final String nameOnSign = ChatColor.stripColor(sign.getLine(2));
									mustUpdateSign = (!nameOnSign.equals(SignSettings.ForSaleText));
									if (mustUpdateSign)
									{
										super.debug("Sign $1: Name was $2, should be $3.", signData.getId(), nameOnSign, SignSettings.ForSaleText);
										reason = "Vacant plot sign shows owner.";
									}
								}
								else
								{
									final String nameOnSign = ChatColor.stripColor(sign.getLine(3));
									mustUpdateSign = (!nameOnSign.equals(owner.getName()));
									if (mustUpdateSign)
									{
										super.debug("Sign $1: Name was $2, should be $3.", signData.getId(), nameOnSign, owner.getName());
										reason = "Plot sign shows wrong owner.";
									}
								}
							}
						}
					}
					else
					{
						reason = "Not controlled.";
						mustRemoveSignData = true;
					}
				}
				else
				{
					reason = "No longer exists.";
					mustRemoveSignData = true;
				}
			}
			
			if (mustRemoveSignData || mustUpdateSign)
			{
				sender.sendInfoListItemWithId(signData.getId(), "$1 sign $2", signData.getType(), signData.getLocation());
				sender.sendFailure(reason);
				if (confirm)
				{
					if (mustRemoveSignData)
					{
						super.debug("Removing data for $1 sign #$2 $3.", signData.getType(), signData.getId(), signData.getLocation());
						super.getSettings().removeSignData(block.getLocation());
					}					
					else if (mustUpdateSign)
					{
						super.debug("Updating text on $1 sign #$2 $3.", signData.getType(), signData.getId(), signData.getLocation());
						super.getSettings().updateSign(sign, sender, owner, false); 
					}

					sender.sendSuccess("Sign fixed.");
				}
				++signsFixed;
			}
		}
		
		if (signsFixed == 0) sender.sendSuccess("No signs needed fixing.");
		else sender.sendSuccess("Fixed $1 sign$2.", signsFixed, Helpers.getPlural(signsFixed));
		
		return true;
	}
}
