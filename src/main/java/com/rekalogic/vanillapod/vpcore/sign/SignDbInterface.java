package com.rekalogic.vanillapod.vpcore.sign;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Location;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class SignDbInterface extends DatabaseInterfaceBase
{
	public SignDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}

	public Map<Integer, SignData> loadSignData()
	{
		final Map<Integer, SignData> signDataMap = new ConcurrentHashMap<Integer, SignData>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT * FROM vp_sign WHERE destroyed IS NULL");
		while (result.moveNext())
		{
			final SignData signData = new SignData(
					result.getInt("id"),
					result.getString("world"),
					result.getInt("x"),
					result.getInt("y"),
					result.getInt("z"),
					result.getString("type"),
					result.getUuid("uuid"),
					result.getBoolean("isserverowned"),
					result.getString("item"),
					result.getString("metadata"),
					result.getInt("quantity"),
					result.getDouble("price"),
					result.getInt("itemLevel"),
					result.getDouble("moneylevel"));
			signDataMap.put(signData.getId(), signData);
		}
		connection.close();
		return signDataMap;
	}

	public void updateSignData(final SignData signData)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update(
				"vp_sign",
				"type = ?, isserverowned = ?, uuid = ?, item = ?, metadata = ?, quantity = ?, price = ?, itemlevel = ?, moneylevel = ?",
				"id = ?",
				signData.getType(),
				signData.isServerOwned(),
				signData.getPlayerUuid(),
				signData.getItem(),
				signData.getMetadata(),
				signData.getQuantity(),
				signData.getPrice(),
				signData.getItemLevel(),
				signData.getMoneyLevel(),
				signData.getId());
		connection.close();
	}

	public void createSignData(final SignData signData)
	{
		final MySqlConnection connection = super.getConnection();
		final int id = connection.insertInto(
				"vp_sign",
				"world, x, y, z, type, isserverowned, uuid, item, metadata, quantity, price, itemlevel, moneylevel, created",
				signData.getWorldName(),
				signData.getX(),
				signData.getY(),
				signData.getZ(),
				signData.getType(),
				signData.isServerOwned(),
				signData.getPlayerUuid(),
				signData.getItem(),
				signData.getMetadata(),
				signData.getQuantity(),
				signData.getPrice(),
				signData.getItemLevel(),
				signData.getMoneyLevel(),
				System.currentTimeMillis());
		signData.setId(id);
		connection.close();
	}

	public void removeSignData(final Location location)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update(
				"vp_sign",
				"destroyed = ?",
				"world = ? AND x = ? AND y = ? AND z = ? AND destroyed IS NULL",
				System.currentTimeMillis(),
				location.getWorld().getName().toLowerCase(),
				location.getBlockX(),
				location.getBlockY(),
				location.getBlockZ());
		connection.close();
	}

	public void recordPurchase(final UUID actor, final UUID otherParty, final String item, final int quantity, final double price)
	{
		final MySqlConnection connection = super.getConnection();
		connection.insertInto("vp_transaction",
				"uuid, execution, type, otheruuid, item, quantity, price, total",
				actor,
				System.currentTimeMillis(),
				"purchase",
				otherParty,
				item,
				quantity,
				price,
				price);
		connection.close();
	}

	public void recordSale(final UUID actor, final UUID otherParty, final String item, final int quantity, final double price)
	{
		final MySqlConnection connection = super.getConnection();
		connection.insertInto("vp_transaction",
				"uuid, execution, type, otheruuid, item, quantity, price, total",
				actor,
				System.currentTimeMillis(),
				"sale",
				otherParty,
				item,
				quantity,
				price,
				price);
		connection.close();
	}

	public void removePlotSigns(final String plotName)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update("vp_sign", "destroyed = ?", "type = ? AND item = ?", System.currentTimeMillis(), "plot", plotName);
		connection.close();
	}
}
