package com.rekalogic.vanillapod.vpcore.player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.util.LoggingBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class EconomyManager extends LoggingBase implements Economy
{
	private double defaultBalance;
	private final PlayerDbInterface dbInterface;
	private final Map<UUID, Double> balances = new ConcurrentHashMap<UUID, Double>();

	private final double maxBalance = 999999999;
	private final int balancePreserveDecimalPlaces = 2;
	
	public double getMaxBalance() { return this.maxBalance; }
	
	public EconomyManager(final PluginBase plugin, final PlayerDbInterface dbInterface)
	{
		super(plugin.isDebug());
		this.dbInterface = dbInterface;
	}

	public OfflinePlayer[] getSortedNames()
	{
		final OfflinePlayer[] players = Helpers.Bukkit.getOfflinePlayers();
		Arrays.sort(
				players,
				new Comparator<OfflinePlayer>()
				{
					@Override
					public int compare(final OfflinePlayer o1, final OfflinePlayer o2)
					{
						double balance1 = 0;
						double balance2 = 0;
						if (balances.containsKey(o1.getUniqueId())) balance1 = balances.get(o1.getUniqueId());
						if (balances.containsKey(o2.getUniqueId())) balance2 = balances.get(o2.getUniqueId());
						
						return Double.compare(balance2, balance1);
					}
				});
		return players;
	}

	public void Initialise(final double defaultBalance)
	{
		super.info("Loading economy...");
		
		double roundingFactor = 1;
		for (int i = 0; i < this.balancePreserveDecimalPlaces; ++i) roundingFactor *= 10;
		
		this.defaultBalance = defaultBalance;
		
		this.balances.clear();

		final Map<UUID, Double> dbBalances = this.dbInterface.getBalances();
		super.info("Setting up $1 account$2...", dbBalances, Helpers.getPlural(dbBalances));
		double totalWealth = 0;		
		for (final UUID uuid: dbBalances.keySet())
		{
			final double balance = dbBalances.get(uuid);
			double roundedBalance = Math.round(balance * roundingFactor) / roundingFactor;
			
			if (balance != roundedBalance)
			{
				super.info("Balance for $1 was rounded.", uuid);
				this.dbInterface.updateBalance(uuid, roundedBalance);
			}
			
			if (roundedBalance > this.maxBalance) roundedBalance = this.maxBalance;
			this.balances.put(uuid, roundedBalance);
			totalWealth += roundedBalance;
		}

		super.info(
				"Economy initialised with $1 accounts (approx. $$2 total wealth).",
				dbBalances,
				Math.round(totalWealth));
	}

	private UUID getPlayerUUID(final String playerName)
	{
		UUID uuid = null;
		final Iterator<? extends Player> iterator = Bukkit.getOnlinePlayers().iterator();
		while (iterator.hasNext())
		{
			final Player player = iterator.next();
			if (playerName.equalsIgnoreCase(player.getName()))
			{
				uuid = player.getUniqueId();
				break;
			}
		}
		
		if (uuid == null)
			uuid = this.dbInterface.getUuidFromLastUserName(playerName);		
		
		if (uuid == null)
		{
			for (final OfflinePlayer player: Helpers.Bukkit.getOfflinePlayers())
			{
				if (playerName.equalsIgnoreCase(player.getName()))
				{
					uuid = player.getUniqueId();
					break;
				}
			}
		}
				
		return uuid;
	}

	public String getBalanceString(final OfflinePlayer player) { return this.getBalanceString(player.getUniqueId()); }
	public String getBalanceString(final UUID uuid)
	{
		return Helpers.Money.formatString(this.getBalance(uuid));
	}

	public static double round(double amount)
	{
		amount = Math.round(amount * 100);
		return amount /= 100;
	}

	public void setBalance(final OfflinePlayer player, final double balance) { this.setBalance(player.getUniqueId(), balance); }
	public void setBalance(final UUID uuid, final double balance)
	{
		this.balances.put(uuid, balance);
		this.dbInterface.updateBalance(uuid, balance);
	}

	public boolean debit(final OfflinePlayer player, final double amount) { return this.withdraw(player, amount); }
	public boolean withdraw(final OfflinePlayer player, final double amount) { return this.withdraw(player.getUniqueId(), amount); }
	public boolean withdraw(final UUID uuid, final double amount)
	{
		if (uuid == null) return false;
		else if (amount < 0) return false;
		else if (!this.has(uuid, amount)) return false;
		else
		{
			final double balance = this.getBalance(uuid) - amount;		
			this.setBalance(uuid, balance);
		}
		
		return true;
	}

	public boolean credit(final OfflinePlayer player, final double amount) { return this.deposit(player, amount); }
	public boolean deposit(final OfflinePlayer player, final double amount) { return this.deposit(player.getUniqueId(), amount); }
	public boolean deposit(final UUID uuid, final double amount)
	{
		if (uuid == null) return false;
		else if (amount < 0) return false;
		else if (!this.canReceive(uuid, amount)) return false;
		else
		{
			final double balance = this.getBalance(uuid) + amount;
			this.setBalance(uuid, balance);
		}

		return true;
	}

	public boolean transferFunds(final OfflinePlayer fromPlayer, final OfflinePlayer toPlayer, final double amount)
	{ return this.transferFunds(fromPlayer.getUniqueId(), toPlayer.getUniqueId(), amount); }
	public boolean transferFunds(final UUID fromUUID, final UUID toUUID, final double amount)
	{
		if (fromUUID.equals(toUUID)) return true;
		
		if (amount == 0) return true;
		if (amount < 0) return false;

		if (!this.has(fromUUID, amount)) return false;
		if (!this.canReceive(toUUID, amount)) return false;

		final double fromBalance = this.getBalance(fromUUID) - amount;
		final double toBalance = this.getBalance(toUUID) + amount;

		this.setBalance(fromUUID, fromBalance);
		this.setBalance(toUUID, toBalance);

		return true;
	}

	public boolean canReceive(final OfflinePlayer player, final double amount) { return this.canReceive(player.getUniqueId(), amount); }
	public boolean canReceive(final UUID uuid, final double amount)
	{
		final double balance = this.getBalance(uuid);
		return (balance < (this.maxBalance - amount));
	}

	public boolean createPlayerAccount(final UUID uuid)
	{
		if (uuid == null) return false;
		if (!this.balances.containsKey(uuid))
			this.balances.put(uuid, this.defaultBalance);
		return true;
	}

	@Override
	public boolean createPlayerAccount(final OfflinePlayer player) { return this.createPlayerAccount(player.getUniqueId()); }

	@Override
	public boolean createPlayerAccount(final OfflinePlayer player, final String worldName) { return this.createPlayerAccount(player.getUniqueId()); }

	@Override
	public String currencyNamePlural() { return ""; }

	@Override
	public String currencyNameSingular() { return ""; }

	public EconomyResponse depositPlayer(final UUID uuid, final double amount)
	{
		final boolean success = this.deposit(uuid, amount);
		ResponseType responseType = ResponseType.SUCCESS;
		String errorMessage = "";
		if (!success)
		{
			responseType = ResponseType.FAILURE;
			errorMessage = "Account overflow, negative deposit or invalid UUID.";
		}
		return new EconomyResponse(amount, this.getBalance(uuid), responseType, errorMessage);
	}

	@Override
	public EconomyResponse depositPlayer(final OfflinePlayer player, final double amount) { return this.depositPlayer(player.getUniqueId(), amount); }

	@Override
	public EconomyResponse depositPlayer(final OfflinePlayer player, final String worldName, final double amount) { return this.depositPlayer(player.getUniqueId(), amount); }

	@Override
	public String format(final double amount) { return Helpers.Money.formatString(amount); }

	@Override
	public int fractionalDigits() { return -1; }

	public double getBalance(final UUID uuid)
	{
		double balance = this.defaultBalance;
		if (this.balances.containsKey(uuid)) balance = this.balances.get(uuid);
		else if (uuid != null) this.balances.put(uuid, balance);
		return balance;
	}

	@Override
	public double getBalance(final OfflinePlayer player) { return this.getBalance(player.getUniqueId()); }

	@Override
	public double getBalance(final OfflinePlayer player, final String worldName) { return this.getBalance(player.getUniqueId()); }

	@Override
	public List<String> getBanks() { return new ArrayList<String>(); }

	@Override
	public String getName() { return "VPCore Economy"; }

	public boolean has(final UUID uuid, final double amount)
	{
		return (this.getBalance(uuid) >= amount);
	}

	@Override
	public boolean has(final OfflinePlayer player, final double amount) { return this.has(player.getUniqueId(), amount); }

	@Override
	public boolean has(final OfflinePlayer player, final String worldName, final double amount) { return this.has(player.getUniqueId(), amount); }

	@Override
	public boolean hasAccount(final OfflinePlayer player) { return true; }

	@Override
	public boolean hasAccount(final OfflinePlayer player, final String worldName) { return true; }

	@Override
	public boolean hasBankSupport() { return false; }

	@Override
	public boolean isEnabled() { return true; }

	public EconomyResponse withdrawPlayer(final UUID uuid, final double amount)
	{
		final boolean success = this.withdraw(uuid, amount);
		ResponseType responseType = ResponseType.SUCCESS;
		String errorMessage = "";
		if (!success)
		{
			responseType = ResponseType.FAILURE;
			errorMessage = "Insufficient funds, negative withdrawal or invalid UUID.";
		}
		return new EconomyResponse(amount, this.getBalance(uuid), responseType, errorMessage);
	}

	@Override
	public EconomyResponse withdrawPlayer(final OfflinePlayer player, final double amount) { return this.withdrawPlayer(player.getUniqueId(), amount); }

	@Override
	public EconomyResponse withdrawPlayer(final OfflinePlayer player, final String worldName, final double amount) { return this.withdrawPlayer(player.getUniqueId(), amount); }

	@Override
	public EconomyResponse createBank(final String name, final OfflinePlayer player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}

	@Override
	public EconomyResponse deleteBank(final String name) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}

	@Override
	public EconomyResponse bankHas(final String name, final double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}

	@Override
	public EconomyResponse bankWithdraw(final String name, final double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}

	@Override
	public EconomyResponse bankDeposit(final String name, final double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}

	@Override
	public EconomyResponse isBankOwner(final String name, final OfflinePlayer player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}

	@Override
	public EconomyResponse isBankMember(final String name, final OfflinePlayer player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}

	@Override
	public EconomyResponse bankBalance(final String name) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}

	@Deprecated @Override
	public boolean has(final String playerName, final double amount) { return this.has(this.getPlayerUUID(playerName), amount); }

	@Deprecated @Override
	public boolean has(final String playerName, final String worldName, final double amount) { return has(playerName, amount); }

	@Deprecated @Override
	public boolean hasAccount(final String playerName) { return true; }

	@Deprecated @Override
	public boolean hasAccount(final String playerName, final String worldName) { return true; }

	@Deprecated @Override
	public boolean createPlayerAccount(final String playerName) { return this.createPlayerAccount(this.getPlayerUUID(playerName)); }

	@Deprecated @Override
	public boolean createPlayerAccount(final String playerName, final String worldName) { return this.createPlayerAccount(playerName); }

	@Deprecated @Override
	public EconomyResponse depositPlayer(final String playerName, final double amount) { return this.depositPlayer(this.getPlayerUUID(playerName), amount); }

	@Deprecated @Override
	public EconomyResponse depositPlayer(final String playerName, final String worldName, final double amount) { return this.depositPlayer(playerName, amount); }

	@Deprecated @Override
	public double getBalance(final String playerName) { return this.getBalance(this.getPlayerUUID(playerName)); }

	@Deprecated @Override
	public double getBalance(final String playerName, final String worldName) { return this.getBalance(playerName); }

	@Deprecated @Override
	public EconomyResponse withdrawPlayer(final String playerName, final double amount) { return this.withdrawPlayer(this.getPlayerUUID(playerName), amount); }

	@Deprecated @Override
	public EconomyResponse withdrawPlayer(final String playerName, final String worldName, final double amount) { return this.withdrawPlayer(playerName, amount); }

	@Deprecated @Override
	public EconomyResponse createBank(final String name, final String player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}
	
	@Deprecated @Override
	public EconomyResponse isBankOwner(final String name, final String playerName) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}

	@Deprecated @Override
	public EconomyResponse isBankMember(final String name, final String playerName) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VPCore does not support bank accounts.");
	}	
}
