package com.rekalogic.vanillapod.vpcore.player;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class PlayerPromotionTask extends TaskBase
{
	private final VPPlugin plugin;
	private final PlayerSettings settings;
	
	public PlayerPromotionTask(final VPPlugin plugin, final PlayerSettings settings)
	{
		super(settings);
		
		this.plugin = plugin;
		this.settings = settings;
	}

	@Override
	public void runTask()
	{
		Group memberGroup = this.settings.getGroup("Member");
		if (memberGroup == null)
		{
			super.severe("Could not find group 'Member'!");
			return;
		}
		
		Group regularGroup = this.settings.getGroup("Regular");
		if (regularGroup == null)
		{
			super.severe("Could not find group 'Regular'!");
			return;
		}

		for (final UUID uuid: this.settings.getRegisteredPlayersForPromotion())
		{
			final OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
			if (!player.hasPlayedBefore()) continue;
			if (this.plugin.isLiveServer())
			{
				this.settings.changeGroup(player, memberGroup, false);
				super.info("Promoted $1 to $2.", player, memberGroup);
				if (player.isOnline()) Helpers.Broadcasts.sendAll("$1 was promoted to $2!", player, memberGroup);
			}
			else super.info("NOTLIVE: Skipped promoting $1 to $2.", player, memberGroup);
		}
		
		for (final UUID uuid: this.settings.getMemberPlayersForPromotion())
		{
			final OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
			if (!player.hasPlayedBefore()) continue;
			if (this.plugin.isLiveServer())
			{
				this.settings.changeGroup(player, regularGroup, false);
				super.info("Promoted $1 to $2.", player, regularGroup);
				if (player.isOnline()) Helpers.Broadcasts.sendAll("$1 was promoted to $2!", player, regularGroup);
			}
			else super.info("NOTLIVE: Skipped promoting $1 to $2.", player, memberGroup);
		}

		for (final UUID uuid: this.settings.getRegularPlayersForDemotion())
		{
			final OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
			if (!player.hasPlayedBefore()) continue;
			if (this.plugin.isLiveServer())
			{
				this.settings.changeGroup(player, memberGroup, false);
				this.settings.resetAccruedPlaytime(player);
				super.info("Demoted $1 to $2.", player, memberGroup);
			}
			else super.info("NOTLIVE: Skipped demoting $1 to $2.", player, memberGroup);
		}
	}
}
