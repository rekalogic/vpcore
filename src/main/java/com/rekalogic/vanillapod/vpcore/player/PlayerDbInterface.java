package com.rekalogic.vanillapod.vpcore.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class PlayerDbInterface extends DatabaseInterfaceBase
{
	public PlayerDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}
	
	public static void fixLoginData(final MySqlConnection connection)
	{
		MySqlResult result = connection.execute("SELECT uuid, lastlogout FROM vp_online");
		while (result.moveNext())
		{
			UUID uuid = result.getUuid("uuid");
			OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
			if (player.hasPlayedBefore() && player.getFirstPlayed() != 0)
				connection.update(
						"vp_online",
						"firstlogin = ?",
						"uuid = ?",
						player.getFirstPlayed(),
						uuid);
			if (result.getLong("lastlogout") == 0)
				connection.update(
						"vp_online",
						"lastlogout = ?, lastusername = ?",
						"uuid = ?",
						player.getLastPlayed(),
						player.getName(),
						uuid);
		}
	}
	
	public boolean playerHasAssociatedForumProfileData(final String name)
	{
		int count = 0;
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT count(1) AS count FROM mcforum.phpbb_profile_fields_data WHERE lower(pf_mc_username) = ?",
				name.toLowerCase());
		if (result.moveNext()) count = result.getInt("count");
		return (count > 0);
	}

	public boolean isRegistered(final UUID uuid)
	{
		final MySqlConnection connection = super.getConnection();
		final boolean isRegistered = this.isRegistered(connection, uuid);
		connection.close();
		return isRegistered;
	}
	
	private boolean isRegistered(final MySqlConnection connection, final UUID uuid)
	{
		int count = 0;
		final MySqlResult result = connection.execute(
				"SELECT count(1) AS count FROM vp_player WHERE uuid = ? AND phpbb_userid IS NOT NULL",
				uuid);
		if (result.moveNext()) count = result.getInt("count");
		return (count > 0);
	}
	
	public boolean finaliseRegistration(final UUID uuid, final String name)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update(
				"vp_player",
				"phpbb_userid = (SELECT user_id FROM mcforum.phpbb_profile_fields_data WHERE lower(pf_mc_username) = ?)",
				"uuid = ?",
				name.toLowerCase(),
				uuid);
		final boolean isRegistered = this.isRegistered(connection, uuid);
		connection.close();
		return isRegistered;
	}

	public int getForumAccountRegistrations(final String email)
	{
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT count(1) AS count FROM mcforum.phpbb_users u, vp_player p " +
				"WHERE p.phpbb_userid = u.user_id AND lower(u.user_email) = ?",
				email.toLowerCase());

		int count = 0;
		if (result.moveNext()) count = result.getInt("count");
		connection.close();
		return count;
	}
	
	public boolean getEmailExists(final String email)
	{
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT count(1) AS count FROM mcforum.phpbb_users WHERE lower(user_email) = ?",
				email.toLowerCase());

		int count = 0;
		if (result.moveNext()) count = result.getInt("count");
		connection.close();
		return (count > 0);
	}
	
	public String getEmail(final OfflinePlayer player)
	{
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT u.user_email FROM mcforum.phpbb_users u, vp_player p " +
				"WHERE p.phpbb_userid = u.user_id AND p.uuid = ?",
				player);
		
		String email = "";
		if (result.moveNext())
			email = result.getString("user_email");
		connection.close();
		return email;		
	}
	
	public Map<UUID, String> getRegisteredPlayerGroups()
	{
		final Map<UUID, String> registeredPlayerGroups = new ConcurrentHashMap<UUID, String>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT p.uuid, g.groupname " +
				"FROM vp_group g, mcforum.phpbb_users u, vp_player p " +
				"WHERE g.phpbb_groupid = u.group_id AND u.user_id = p.phpbb_userid");
		while (result.moveNext())
			registeredPlayerGroups.put(
					result.getUuid("uuid"),
					result.getString("groupname"));
		connection.close();
		return registeredPlayerGroups;
	}
	
	public String getGroupName(final OfflinePlayer player)
	{
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT g.groupname " +
				"FROM vp_group g, mcforum.phpbb_users u, vp_player p " +
				"WHERE g.phpbb_groupid = u.group_id AND u.user_id = p.phpbb_userid AND p.uuid = ?",
				player);

		String name = "";
		if (result.moveNext())
			name = result.getString("groupname");
		connection.close();
		return name;
	}

	public Map<String, Location> getSpawns()
	{
		return this.getNamedLocations("vp_spawn", "groupname");
	}

	public Map<String, Location> getWarps()
	{
		return this.getNamedLocations("vp_warp", "name");
	}

	public Map<UUID, ConcurrentHashMap<String, Location>> getHomes()
	{
		final Map<UUID, ConcurrentHashMap<String, Location>> homes = new ConcurrentHashMap<UUID, ConcurrentHashMap<String, Location>>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT uuid, name, world, x, y, z, yaw, pitch " +
				"FROM vp_home, vp_location " +
				"WHERE locationid = id " +
				"ORDER BY uuid ASC, name ASC");
		World world = null;
		while (result.moveNext())
		{
			if (world == null ||
					!world.getName().equalsIgnoreCase(result.getString("world")))
				world = Bukkit.getWorld(result.getString("world"));
			final Location location = new Location(
					world,
					result.getDouble("x"),
					result.getDouble("y"),
					result.getDouble("z"),
					result.getFloat("yaw"),
					result.getFloat("pitch"));
			final UUID uuid = result.getUuid("uuid");
			ConcurrentHashMap<String, Location> hashMap = null;
			if (!homes.containsKey(uuid))
			{
				hashMap = new ConcurrentHashMap<String, Location>();
				homes.put(uuid, hashMap);
			}
			else hashMap = homes.get(uuid);
			hashMap.put(result.getString("name"), location);
		}

		return homes;
	}

	private ConcurrentHashMap<String, Location> getNamedLocations(final String nameTable, final String nameColumn)
	{
		final ConcurrentHashMap<String, Location> locations = new ConcurrentHashMap<String, Location>();
		final MySqlConnection connection = super.getConnection();

		final String sql = Helpers.Parameters.replace(
				"SELECT l.id, n.$1 AS name, l.world AS world, l.x AS x, l.y AS y, l.z AS z, l.yaw AS yaw, l.pitch AS pitch " +
				"FROM $2 n, vp_location l WHERE n.locationid = l.id ORDER BY $1 ASC",
				nameColumn,
				nameTable);

		final MySqlResult result = connection.execute(sql);
		while (result.moveNext())
		{
			final Location location = super.getLocationFromMySqlResult(connection, result);
			if (location == null) super.severe(
					"Could not retrieve $1 location '$2' [locationid=$3].",
					nameTable,
					result.getString("name"),
					result.getInt("id"));
			else locations.put(result.getString("name"), location);
		}

		connection.close();
		return locations;
	}

	public List<Group> loadGroups()
	{
		final List<Group> groups = new ArrayList<Group>();
		final MySqlConnection connection = super.getConnection();
		final List<String> worlds = new ArrayList<String>();		
		MySqlResult result = connection.execute("SELECT distinct(world) AS world FROM vp_group_bundle");
		while (result.moveNext())
			worlds.add(result.getString("world"));
		result = connection.execute("SELECT * FROM vp_group");
		while (result.moveNext())
		{
			final Group group = new Group(
					result.getString("groupname"),
					result.getString("chatcolor"),
					result.getString("prefix"),
					result.getBoolean("restrictbuild"),
					result.getBoolean("isdefault"),
					result.getInt("maxhomes"),
					result.getBoolean("isregistered"),
					result.getBoolean("exempt"),
					result.getInt("phpbb_groupid"));
			groups.add(group);
			super.debug("Group $1 color=$2 ($3).", group.getName(), result.getString("chatcolor"), ChatColor.valueOf(result.getString("chatcolor")).name());

			
			for (final String world: worlds)
			{
				final List<String> bundles = this.getBundles(connection, group, world);
				final List<String> inheritedGroups = this.getInheritedGroups(connection, group, world);
				final List<String> directPermissions = this.getDirectPermissions(connection, group, world);

				group.parseWorldDbInfo(
						world,
						bundles,
						inheritedGroups,
						directPermissions);
			}
		}
		connection.close();
		return groups;
	}

	private List<String> getBundles(final MySqlConnection connection, final Group group, final String worldName)
	{
		final List<String> bundles = new ArrayList<String>();
		final MySqlResult result = connection.execute(
				"SELECT bundle FROM vp_group_bundle WHERE groupname = ? AND world = ?",
				group.getName(),
				worldName);
		while (result.moveNext())
			bundles.add(result.getString("bundle"));
		return bundles;
	}

	private List<String> getInheritedGroups(final MySqlConnection connection, final Group group, final String worldName)
	{
		final List<String> inheritance = new ArrayList<String>();
		final MySqlResult result = connection.execute(
				"SELECT inherits FROM vp_group_inheritance WHERE groupname = ? AND world = ?",
				group.getName(),
				worldName);
		while (result.moveNext())
			inheritance.add(result.getString("inherits"));
		return inheritance;
	}

	private List<String> getDirectPermissions(final MySqlConnection connection, final Group group, final String worldName)
	{
		final List<String> permissions = new ArrayList<String>();
		final MySqlResult result = connection.execute(
				"SELECT permission FROM vp_group_directpermission WHERE groupname = ? AND world = ?",
				group.getName(),
				worldName);
		while (result.moveNext())
			permissions.add(result.getString("permission"));
		return permissions;
	}

	public Map<String, List<String>> getBundles()
	{
		final ConcurrentHashMap<String, List<String>> bundles = new ConcurrentHashMap<String, List<String>>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT bundle, permission FROM vp_bundle ORDER BY bundle ASC");
		String lastBundle = "";
		while (result.moveNext())
		{
			final String bundle = result.getString("bundle");
			List<String> permissions = null;
			if (lastBundle.equals(bundle)) permissions = bundles.get(bundle);
			else
			{
				lastBundle = bundle;
				permissions = new ArrayList<String>();
				bundles.put(bundle, permissions);
			}
			permissions.add(result.getString("permission"));
		}
		connection.close();
		return bundles;
	}

	public void closeUnfinishedSessions()
	{
		final long now = System.currentTimeMillis();
		final MySqlConnection connection = super.getConnection();
		connection.update("vp_online", "online = 0, lastlogout = ?", "online = 1", now);
		connection.close();
	}
	
	public boolean recordLogin(final Player player, final double defaultBalance, final boolean online, final boolean recordIpHistory)
	{
		final MySqlConnection connection = super.getConnection();
		MySqlResult result = connection.execute("SELECT count(1) AS result FROM vp_online WHERE uuid = ?", player);
		boolean newPlayer = true;
		if (result.moveNext())
			newPlayer = (result.getInt("result") == 0);
		final UUID uuid = player.getUniqueId();
		final String ip = player.getAddress().getHostString();
		final String name = player.getName();
		final long now = System.currentTimeMillis();
		boolean unexempted = false;
		if (newPlayer)
		{
			this.createPlayerData(connection, uuid, defaultBalance, ip);
			long firstLogin = now;
			if (player.hasPlayedBefore() &&
					player.getFirstPlayed() != 0)
				firstLogin = player.getFirstPlayed();
			connection.insertInto(
					"vp_online",
					"uuid, online, firstlogin, lastlogin, lastlogout, playtime, exempt, lastusername",
					uuid,
					online,
					firstLogin,
					now,
					0,
					0,
					0,
					name);
		}
		else
		{
			result = connection.execute("SELECT exempt, lastusername FROM vp_online WHERE uuid = ?", uuid);
			if (result.moveNext())
			{
				final String lastUserName = result.getString("lastusername");
				if (!lastUserName.equals(name))
				{
					connection.insertInto("vp_alias", "uuid, firstseen, name", uuid, System.currentTimeMillis(), name);
					super.getPlugin().addAlias(name, lastUserName);
				}
				int exempt = result.getInt("exempt");
				if (exempt == 1)
				{
					exempt = 0;
					unexempted = true;
				}
				connection.update(
						"vp_online",
						"online = ?, lastlogin = ?, lastusername = ?, exempt = ?",
						"uuid = ?",
						online,
						now,
						name,
						exempt,
						uuid);
			}
		}
		if (recordIpHistory)
			this.createIpHistory(connection, uuid, ip, now);
		connection.close();
		
		return unexempted;
	}
	
	public void updateVisibleStatus(final Player player, final boolean isVisible)
	{
		final MySqlConnection connection = super.getConnection();
		connection.executeVoid("UPDATE vp_online SET online = ? WHERE uuid = ?", isVisible, player);
		connection.close();
	}

	public void recordLogout(final Player player)
	{
		final MySqlConnection connection = super.getConnection();
		final long now = System.currentTimeMillis();
		connection.update(
				"vp_online",
				"online = 0, lastlogout = ?, playtime = playtime + (? - lastlogin), accrued = accrued + (? - lastlogin)",
				"uuid = ?",
				now,
				now,
				now,
				player.getUniqueId());
		connection.close();
	}

	public void createSpawn(final Group group, final Location location)
	{
		final MySqlConnection connection = super.getConnection();
		final int locationId = super.createLocation(connection, location);
		connection.insertInto("vp_spawn", "groupname, locationid", group, locationId);
		connection.close();
	}
	
	public void updateSpawn(final Group group, final Location location)
	{
		super.updateNamedLocation("vp_spawn", "groupname", group.getDisplayName(), location);
	}

	public void createWarp(final String name, final Location location)
	{
		final MySqlConnection connection = super.getConnection();
		final int locationId = super.createLocation(connection, location);
		connection.insertInto("vp_warp", "name, locationid", name, locationId);
		connection.close();
	}

	public void updateWarp(final String name, final Location location)
	{
		super.updateNamedLocation("vp_warp", "name", name, location);
	}

	public void deleteWarp(final String name)
	{
		final String warp = name.toLowerCase();
		final MySqlConnection connection = super.getConnection();
		connection.deleteFrom("vp_location", "id = (SELECT locationid FROM vp_warp WHERE lower(name) = ?)", warp);
		connection.deleteFrom("vp_warp", "lower(name) = ?", warp);
		connection.close();
	}

	public void createIpHistory(final UUID uuid, final String ip, final long seen)
	{
		final MySqlConnection connection = super.getConnection();
		this.createIpHistory(connection, uuid, ip, seen);
		connection.close();
	}

	private void createIpHistory(final MySqlConnection connection, final UUID uuid, final String ip, final long seen)
	{
		connection.update("vp_player", "ip = ?", "uuid = ?", ip, uuid);
		connection.insertInto("vp_iphistory", "uuid, ip, seen", uuid, ip, seen);
	}

	public boolean createPlayerData(final MySqlConnection connection, final UUID uuid, final double balance, final String ip)
	{
		final MySqlResult result = connection.execute("SELECT count(1) AS count FROM vp_player WHERE uuid = ?", uuid);
		boolean accountExists = false;
		if (result.moveNext())
			accountExists = (result.getInt("count") == 1);
		if (!accountExists) connection.insertInto("vp_player", "uuid, balance, ip", uuid, balance, ip);
		return !accountExists;
	}

	public Map<UUID, Double> getBalances()
	{
		final Map<UUID, Double> balances = new ConcurrentHashMap<UUID, Double>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT uuid, balance FROM vp_player");
		while (result.moveNext())
		{
			balances.put(
					result.getUuid("uuid"),
					result.getDouble("balance"));
		}
		connection.close();
		return balances;
	}

	public void updateBalance(final UUID uuid, final double balance)
	{
		final MySqlConnection connection = super.getConnection();
		if (!this.createPlayerData(connection, uuid, balance, ""))
			connection.update("vp_player", "balance = ?", "uuid = ?", balance, uuid);
		connection.close();
	}

	public void deleteHomeLocation(final UUID uuid, final String name)
	{
		final String lowerName = name.toLowerCase();
		final MySqlConnection connection = super.getConnection();
		connection.deleteFrom("vp_location", "id = (SELECT locationid FROM vp_home WHERE uuid = ? AND lower(name) = ?)", uuid, lowerName);
		connection.deleteFrom("vp_home", "uuid = ? AND lower(name) = ?", uuid, lowerName);
		connection.close();
	}

	public void createOrUpdateHomeLocation(final UUID uuid, final String name, final Location location)
	{
		final String lowerName = name.toLowerCase();
		final MySqlConnection connection = super.getConnection();
		connection.deleteFrom(
				"vp_location",
				"id = (SELECT locationid FROM vp_home WHERE uuid = ? AND lower(name) = ?)",
				uuid,
				lowerName);
		connection.deleteFrom("vp_home", "uuid = ? AND lower(name) = ?", uuid, lowerName);
		final int locationId = super.createLocation(connection, location);
		connection.insertInto("vp_home", "uuid, name, locationid", uuid, lowerName, locationId);
		connection.close();
	}

	public void setExempt(final OfflinePlayer player, final int exempt)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update("vp_online", "exempt = ?", "uuid = ?", exempt, player.getUniqueId());
		connection.close();
	}
	
	public int getExemption(final OfflinePlayer player)
	{
		int exemption = 0;
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT exempt FROM vp_online WHERE uuid = ?", player.getUniqueId());
		if (result.moveNext())
			exemption = result.getInt("exempt");
		connection.close();
		return exemption;
	}

	public void changeToRegisteredGroup(final UUID uuid, final String name, final int registeredGroupId)
	{
		final MySqlConnection connection = super.getConnection();
		
		super.changeUserPhpbbGroup(connection, uuid, registeredGroupId);
		connection.update(
				"vp_player",
				"phpbb_userid = (SELECT user_id FROM mcforum.phpbb_profile_fields_data WHERE lower(pf_mc_username) = ?)",
				"uuid = ?",
				name.toLowerCase(),
				uuid);

		connection.close();
	}
	
	public void setAbsenceBaselineData(final long warnDuration)
	{
		final MySqlConnection connection = super.getConnection();
		final long now = System.currentTimeMillis();
		connection.deleteFrom(
				"vp_absence_info",
				"status IN (?, ?)",
				"absent",
				"banned");
		connection.executeVoid(
				"DELETE ai FROM vp_absence_info ai, vp_online o " +
				"WHERE ai.uuid = o.uuid AND ai.updated < o.lastlogin");
		connection.executeVoid(
				"INSERT INTO vp_absence_info " +
				"SELECT o.uuid, ?, ? FROM vp_online o, vp_player p, mcforum.phpbb_users u " +
				"WHERE o.uuid = p.uuid " + 
				"AND p.phpbb_userid = u.user_id " +
				"AND o.online = 0 AND o.lastlogout <= ? " +
				"AND o.uuid NOT IN (SELECT ai.uuid FROM vp_absence_info ai)",
				now,
				"absent",
				now - warnDuration);		
		connection.update(
				"vp_absence_info",
				"status = ?",
				"uuid IN (SELECT uuid FROM vp_baninfo WHERE active = 1 AND (expireon = 0 OR expireon > ?))",
				"banned",
				now);
		connection.close();
	}
	
	public List<UUID> getPlayersToWarn()
	{
		final List<UUID> uuids = new ArrayList<UUID>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT ai.uuid " +
				"FROM vp_absence_info ai, vp_online o, vp_player p, mcforum.phpbb_users u, vp_group g " +
				"WHERE ai.uuid = o.uuid " +
				"AND o.uuid = p.uuid " +
				"AND p.phpbb_userid = u.user_id " +
				"AND u.group_id = g.phpbb_groupid " +
				"AND o.exempt = 0 " +
				"AND g.exempt = 0 " +
				"AND lower(ai.status) = ?",
				"absent");
		while (result.moveNext())
			uuids.add(result.getUuid("uuid"));
		connection.close();
		return uuids;
	}
		
	public List<UUID> getPlayersToDelete(final long duration)
	{
		final List<UUID> uuids = new ArrayList<UUID>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT ai.uuid " +
				"FROM vp_absence_info ai, vp_online o, vp_player p, mcforum.phpbb_users u, vp_group g " +
				"WHERE ai.uuid = o.uuid " +
				"AND o.uuid = p.uuid " +
				"AND p.phpbb_userid = u.user_id " +
				"AND u.group_id = g.phpbb_groupid " +
				"AND o.exempt = 0 " +
				"AND g.exempt = 0 " +
				"AND lower(ai.status) = ? AND updated <= ?",
				"warned",
				System.currentTimeMillis() - duration);
		while (result.moveNext())
			uuids.add(result.getUuid("uuid"));
		connection.close();
		return uuids;
	}
	
	public List<UUID> getBannedPlayersToDelete(final long duration)
	{
		final List<UUID> uuids = new ArrayList<UUID>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT ai.uuid " +
				"FROM vp_absence_info ai, vp_online o, vp_player p, mcforum.phpbb_users u, vp_group g, vp_baninfo bi " +
				"WHERE ai.uuid = o.uuid " +
				"AND o.uuid = p.uuid " +
				"AND p.phpbb_userid = u.user_id " +
				"AND u.group_id = g.phpbb_groupid " +
				"AND o.uuid = bi.uuid " +
				"AND o.exempt = 0 " +
				"AND g.exempt = 0 " +
				"AND lower(ai.status) = ? AND bi.bannedon <= ?",
				"banned",
				System.currentTimeMillis() - duration);
		while (result.moveNext())
			uuids.add(result.getUuid("uuid"));
		connection.close();
		return uuids;
	}
	
	public void markPlayerAsWarned(final UUID uuid)
	{
		this.markPlayerAs(uuid, "warned");
	}
	
	public void markPlayerAsDeleted(final UUID uuid)
	{
		this.markPlayerAs(uuid, "deleted");
	}
	
	private void markPlayerAs(final UUID uuid, final String status)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update(
				"vp_absence_info",
				"updated = ?, status = ?",
				"uuid = ?",
				System.currentTimeMillis(),
				status,
				uuid);
		connection.close();
	}

	public List<UUID> getRegisteredPlayersForPromotion(final long minPlaytime)
	{
		final List<UUID> uuids = new ArrayList<UUID>();
		final MySqlConnection connection = super.getConnection();
		final long now = System.currentTimeMillis();
		final MySqlResult result = connection.execute(
				"SELECT p.*, o.playtime, o.lastlogout, o.online FROM vp_group g, mcforum.phpbb_users u, vp_player p, vp_online o " +
				"WHERE lower(g.groupname) = ? " +
				"AND g.phpbb_groupid = u.group_id " +
				"AND u.user_id = p.phpbb_userid " +
				"AND p.uuid = o.uuid " +
				"AND (o.playtime >= ? OR (o.online = 1 AND ? - o.lastlogin + o.playtime > ?))",
				"registered",
				minPlaytime,
				now,
				minPlaytime);
		while (result.moveNext())
			uuids.add(result.getUuid("uuid"));
		connection.close();
		return uuids;
	}
	
	public List<UUID> getMemberPlayersForPromotion(final long minPlaytime, final long maxInactivity)
	{		
		final List<UUID> uuids = new ArrayList<UUID>();
		final MySqlConnection connection = super.getConnection();
		final long now = System.currentTimeMillis();
		final MySqlResult result = connection.execute(
				"SELECT p.*, o.playtime, o.lastlogout, o.online FROM vp_group g, mcforum.phpbb_users u, vp_player p, vp_online o " +
				"WHERE lower(g.groupname) = ? " +
				"AND g.phpbb_groupid = u.group_id " +
				"AND u.user_id = p.phpbb_userid " +
				"AND p.uuid = o.uuid " +
				"AND ((o.accrued >= ? AND o.lastlogout >= ?) OR (o.online = 1 AND ? - o.lastlogin + o.accrued > ?))",
				"member",
				minPlaytime,
				now - maxInactivity,
				now,
				minPlaytime);
		while (result.moveNext())
			uuids.add(result.getUuid("uuid"));
		connection.close();
		return uuids;
	}

	public List<UUID> getRegularPlayersForDemotion(final long maxInactivity)
	{
		final List<UUID> uuids = new ArrayList<UUID>();
		final MySqlConnection connection = super.getConnection();
		final long now = System.currentTimeMillis();
		final MySqlResult result = connection.execute(
				"SELECT p.*, o.playtime, o.lastlogout, o.online FROM vp_group g, mcforum.phpbb_users u, vp_player p, vp_online o " +
				"WHERE lower(g.groupname) = ? " +
				"AND g.phpbb_groupid = u.group_id " +
				"AND u.user_id = p.phpbb_userid " +
				"AND p.uuid = o.uuid " +
				"AND o.online = 0 " +
				"AND o.lastlogout < ?",
				"regular",
				now - maxInactivity);
		while (result.moveNext())
			uuids.add(result.getUuid("uuid"));
		connection.close();
		return uuids;
	}
	
	public List<UUID> getFirstLoggedInPlayers(final long since)
	{
		final List<UUID> uuids = new ArrayList<UUID>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT uuid FROM vp_online " +
				"WHERE firstlogin >= ? " +
				"ORDER BY firstlogin DESC",
				since);
		while (result.moveNext())
			uuids.add(result.getUuid("uuid"));
		connection.close();
		return uuids;
	}

	public List<UUID> getLastLoggedInPlayers(final long since)
	{
		final List<UUID> uuids = new ArrayList<UUID>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT uuid FROM vp_online " +
				"WHERE lastlogin > lastlogout OR lastlogout >= ? " +
				"ORDER BY online DESC, lastlogout DESC",
				since);
		while (result.moveNext())
			uuids.add(result.getUuid("uuid"));
		connection.close();
		return uuids;
	}

	public int getForumAccountId(UUID uuid)
	{
		int id = 0;
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(				
				"SELECT phpbb_userid FROM vp_player WHERE uuid = ?",
				uuid);
		while (result.moveNext())
			id = result.getInt("phpbb_userid");
		connection.close();
		return id;
	}

	public void resetAccruedPlaytime(UUID uuid)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update("vp_online", "accrued = 0", "uuid = ?", uuid);
		connection.close();
	}

	public void recordDeathInformation(
			final String name,
			final UUID uuid,
			final DamageCause cause,
			final Location location,
			final List<ItemStack> items)
	{
		final long now = System.currentTimeMillis();
		final MySqlConnection connection = super.getConnection();
		final int locationId = super.createLocation(connection, location);
		
		int inventoryId = 0;
		if (items.size() > 0) inventoryId = connection.insertInto("vp_inventory", "items", items.size());
		
		for (ItemStack stack: items)
		{
			final String[] names = Helpers.Item.getFriendlyNames(stack, false);
			final String metadata = Helpers.Item.getFullMetadata(stack);
						
			final int itemId = connection.insertInto(
					"vp_item",
					"item, quantity, metadata",
					names[0],
					stack.getAmount(),
					(metadata.length() == 0 ? null : metadata));
			connection.insertInto("vp_inventory_item", "inventoryid, itemid", inventoryId, itemId);
		}
		
		final int deathId = connection.insertInto(
				"vp_death",
				"uuid, occurred, cause, locationid, lostitems, inventoryid",
				uuid,
				now,
				cause.toString(),
				locationId,
				items.size(),
				(inventoryId == 0 ? null : inventoryId));
		super.info("Death of $1 was recorded as #$2.", name, deathId);
		
		connection.close();
	}

	public ItemStack[] getDeathInventoryItems(final int deathId)
	{
		List<ItemStack> items = new ArrayList<ItemStack>();
		final MySqlConnection connection = super.getConnection();
		
		final MySqlResult result = connection.execute(
				"SELECT i.id, i.quantity, i.item, i.metadata " +
				"FROM vp_death d, vp_inventory_item ii, vp_item i " +
				"WHERE d.id = ? AND d.inventoryid = ii.inventoryid AND ii.itemid = i.id",
				deathId);
		
		while (result.moveNext())
		{
			final String item = result.getString("item");
			final ItemStack stack = Helpers.Item.getItemStack(item, result.getInt("quantity"));

			super.debug("$1->$2x$3", item, stack.getAmount(), stack.getType());
						
			final String metadata = result.getString("metadata");
			if (!result.wasNull())
			{
				if (!Helpers.Item.isMetadataValid(metadata))
				{
					super.severe("Bad metadata for $1 (item $2):", item, result.getInt("id"));
					super.severe("--> $1", metadata);
					continue;
				}
			
				Helpers.Item.applyMetadata(stack, metadata);
			}
			
			items.add(stack);
		}

		connection.close();
		
		return items.toArray(new ItemStack[0]);
	}

	public List<PlayerDeathData> getPlayerDeathData(final UUID uuid, final long since)
	{
		List<PlayerDeathData> playerDeathData = new ArrayList<PlayerDeathData>();
		final MySqlConnection connection = super.getConnection();		
		MySqlResult result = null;
		if (uuid == null)
		{
			result = connection.execute(
					"SELECT id, uuid, occurred, lostitems, cause " +
					"FROM vp_death " +
					"WHERE occurred >= ? " +
					"ORDER BY occurred DESC",
					since);
		}
		else
		{
			result = connection.execute(
					"SELECT id, uuid, occurred, lostitems, cause " +
					"FROM vp_death " +
					"WHERE uuid = ? AND occurred >= ? " +
					"ORDER BY occurred DESC",
					uuid,
					since);
		}
		while (result.moveNext())
		{
			PlayerDeathData data = new PlayerDeathData(
					result.getInt("id"),
					result.getUuid("uuid"),
					result.getLong("occurred"),
					result.getInt("lostitems"),
					result.getString("cause"));
			playerDeathData.add(data);
		}
		connection.close();
		return playerDeathData;
	}

	public Map<String, List<String>> getAliases(final long maxAge)
	{
		final Map<String, List<String>> aliases = new HashMap<String, List<String>>();
		final MySqlConnection connection = super.getConnection();		
		final MySqlResult result = connection.execute(
				"SELECT o.lastusername AS name, a.name AS alias " +
				"FROM vp_online o, vp_alias a " +
				"WHERE o.uuid = a.uuid " +
				"AND o.lastusername != a.name " +
				"AND o.lastusername IS NOT NULL " +
				"AND a.firstseen >= ?",
				System.currentTimeMillis() - maxAge);
		while (result.moveNext())
		{
			final String alias = result.getString("alias"); 
			List<String> names = null;
			if (aliases.containsKey(alias)) names = aliases.get(alias);
			else
			{
				names = new ArrayList<String>(1);
				aliases.put(alias, names);
			}
			names.add(result.getString("name"));
		}			
		return aliases;
	}

	public UUID getUuidFromLastUserName(final String name)
	{
		UUID uuid = null;
		final MySqlConnection connection = super.getConnection();		
		final MySqlResult result = connection.execute(
				"SELECT uuid FROM vp_online WHERE lower(lastusername) = lower(?)",
				name);
		if (result.moveNext())
			uuid = result.getUuid("uuid");
		connection.close();
		return uuid;
	}
}
