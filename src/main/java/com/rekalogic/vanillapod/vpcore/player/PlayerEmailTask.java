package com.rekalogic.vanillapod.vpcore.player;

import java.util.List;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class PlayerEmailTask extends TaskBase
{
	private final VPPlugin plugin;
	private final List<PlayerEmailData> emails;
	
	public PlayerEmailTask(
			final VPPlugin plugin,
			final PlayerSettings playerSettings,
			final List<PlayerEmailData> emails)
	{
		super(playerSettings);
		
		this.plugin = plugin;
		this.emails = emails;
	}

	@Override
	public void runTask()
	{
		if (this.emails != null)
		{
			for (PlayerEmailData data: emails)
			{
				if (this.plugin.isLiveServer())
				{
					Helpers.Mail.sendMail(
							this.plugin.getMailHost(),
							this.plugin.getMailPort(),
							data.getEmail(),
							this.plugin.getMailFrom(),
							data.getSubject(),
							data.getBody());
					
					super.info("Sent email to $1 ($2).", data.getName(), data.getEmail());
				}
				else super.info("NOTLIVE: Skipped emailing $1 ($2).", data.getName(), data.getEmail());
			}
		}
	}
}
