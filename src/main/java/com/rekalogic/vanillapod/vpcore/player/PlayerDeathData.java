package com.rekalogic.vanillapod.vpcore.player;

import java.util.UUID;

public final class PlayerDeathData
{
	private final int id;
	private final UUID uuid;
	private final long occurred;
	private final int items;
	private final String cause;
	
	public int getId() { return this.id; }
	public UUID getUuid() { return this.uuid; }
	public long getOccurred() { return this.occurred; }
	public int getItems() { return this.items; }
	public String getCause() { return this.cause; }
	
	public PlayerDeathData(final int id, final UUID uuid, final long occurred, final int items, final String cause)
	{
		this.id = id;
		this.uuid = uuid;
		this.occurred = occurred;
		this.items = items;
		this.cause = cause;
	}
}
