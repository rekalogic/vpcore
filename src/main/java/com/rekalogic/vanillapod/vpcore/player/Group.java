package com.rekalogic.vanillapod.vpcore.player;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.ChatColor;

public final class Group
{
	private final String displayName;
	private final String name;
	private final Map<String, PermissionDescriptor> permissions = new ConcurrentHashMap<String, PermissionDescriptor>();

	private final boolean isDefault ;
	private final ChatColor color;
	private final String prefix;
	private final boolean restrictBuild;
	private final int maxHomes;
	private final boolean isRegistered;
	private final boolean exempt;
	private final int phpBbGroupId;

	private boolean isResolved = false;

	public String getDisplayName() { return this.displayName; }
	public String getName() { return this.name; }
	public boolean isBuildRestricted() { return this.restrictBuild; }
	public ChatColor getColor() { return this.color; }
	public String getPrefix() { return this.prefix; }
	public boolean isDefault() { return this.isDefault; }
	public boolean isResolved() { return this.isResolved; }
	public int getMaxHomes() { return this.maxHomes; }
	public boolean isRegistered() { return this.isRegistered; }
	public boolean isExempt() { return this.exempt; }
	public boolean isAdmin() { return this.name.equalsIgnoreCase("admin"); }
	public int getPhpBbGroupId() { return this.phpBbGroupId; }

	public Group(
			final String name,
			final String color,
			final String prefix,
			final boolean restrictBuild,
			final boolean isDefault,
			final int maxHomes,
			final boolean isRegistered,
			final boolean exempt,
			final int phpBbGroupId)
	{
		this.displayName = name;
		this.name = name;
		this.color = ChatColor.valueOf(color);
		this.prefix = prefix;
		this.restrictBuild = restrictBuild;
		this.isDefault = isDefault;
		this.maxHomes = maxHomes;
		this.isRegistered = isRegistered;
		this.exempt = exempt;
		this.phpBbGroupId = phpBbGroupId;
	}

	public boolean hasPermissions(final String worldName) { return this.permissions.containsKey(worldName.toLowerCase()); }
	public PermissionDescriptor getPermissions(final String worldName) { return this.permissions.get(worldName.toLowerCase()); }
	public Set<String> getWorldNames() { return this.permissions.keySet(); }
	
	public boolean hasPermission(final String worldName, final String permission)
	{
		boolean hasPermission = false;
		final String lowerWorldName = worldName.toLowerCase();
		if (this.permissions.containsKey(lowerWorldName))
			hasPermission = this.permissions.get(lowerWorldName).getFinalPermissions().contains(permission);
		return hasPermission;
	}

	private PermissionDescriptor getOrCreateWorldPermission(final String worldName)
	{
		final String lowerWorldName = worldName.toLowerCase();
		PermissionDescriptor worldPerm = null;
		if (this.permissions.containsKey(lowerWorldName))
			worldPerm = this.permissions.get(lowerWorldName);
		else
		{
			worldPerm = new PermissionDescriptor();
			this.permissions.put(lowerWorldName, worldPerm);
		}
		return worldPerm;
	}

	public void parseWorldDbInfo(
			final String worldName,
			final List<String> bundles,
			final List<String> inheritedGroups,
			final List<String> directPermissions)
	{
		final PermissionDescriptor worldPerm = this.getOrCreateWorldPermission(worldName);

		worldPerm.addBundles(bundles);
		worldPerm.addInheritedGroups(inheritedGroups);
		worldPerm.addDirectPermissions(directPermissions);
	}

	public void calculate()
	{
		for (final PermissionDescriptor worldPerm: this.permissions.values())
			worldPerm.calculate();

		this.isResolved = true;
	}
}
