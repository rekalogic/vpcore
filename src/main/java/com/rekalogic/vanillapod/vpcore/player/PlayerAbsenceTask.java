package com.rekalogic.vanillapod.vpcore.player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.plot.PlotData;
import com.rekalogic.vanillapod.vpcore.plot.PlotSettings;
import com.rekalogic.vanillapod.vpcore.region.RegionSettings;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public final class PlayerAbsenceTask extends TaskBase
{
	private final VPPlugin plugin;
	private final PlayerSettings playerSettings;
	private final PlotSettings plotSettings;
	private final RegionSettings regionSettings;

	public PlayerAbsenceTask(
			final VPPlugin plugin,
			final PlayerSettings playerSettings,
			final PlotSettings plotSettings,
			final RegionSettings regionSettings)
	{
		super(playerSettings);
		
		this.plugin = plugin;
		this.playerSettings = playerSettings;
		this.plotSettings = plotSettings;
		this.regionSettings = regionSettings;
	}
	
	@Override
	public void runTask()
	{
		this.playerSettings.setAbsenceBaselineData();
		
		final List<PlayerEmailData> emails = new ArrayList<PlayerEmailData>();
		
		for (final UUID uuid: this.playerSettings.getPlayersToDelete(false))
			this.resetPlayer(uuid, emails);
		
		for (final UUID uuid: this.playerSettings.getPlayersToDelete(true))
			this.resetPlayer(uuid, null);

		for (final UUID uuid: this.playerSettings.getPlayersToWarn())
			this.warnPlayer(uuid, emails);

		if (emails.size() > 0)
		{
			final PlayerEmailTask emailTask = new PlayerEmailTask(this.plugin, this.playerSettings, emails);
			emailTask.runTaskLaterAsynchronously(this.plugin, Helpers.DateTime.getTicksFromSeconds(5));
		}
	}

	private void warnPlayer(final UUID uuid, final List<PlayerEmailData> emails)
	{
		final OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
		if (player == null) super.warning("Unable to look up player $1 for warning.", uuid);
		else if (player.hasPlayedBefore())
		{
			if (this.playerSettings.isRegistered(player))
			{
				emails.add(
						new PlayerEmailData(
								player,
								this.playerSettings.getEmail(player),
								this.playerSettings.getWarningEmailSubject(),
								this.playerSettings.getWarningEmailBody()));
				super.info("Prepared warning email for $1.", player);
			}
			else super.info("$1 ($2) not registered, no warning email necessary.", player, uuid);
			
			this.playerSettings.markPlayerAsWarned(player);				
		}
	}

	private void resetPlayer(final UUID uuid, final List<PlayerEmailData> emails)
	{
		final OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
		if (player == null) super.warning("Unable to look up player $1 for deletion.", uuid);
		else
		{
			super.info("Deleting plot and regions for $1.", player);
			
			final PlotData plotData = this.plotSettings.getPlotDataFor(player);
			if (plotData == null) super.debug("No plot for $1.", player);
			else
			{
				this.plotSettings.resetPlot(plotData, true);
				super.debug("Plot $1 reset for $2 ($3).", plotData, player, uuid);
			}
			
			if (this.plugin.isWorldGuardEnabled())
			{
				for (World world: Bukkit.getWorlds())
				{
					final RegionManager regionManager = this.plugin.getWorldGuard().getRegionManager(world);
					if (regionManager == null) continue;
					
					final Map<String, ProtectedRegion> regions = regionManager.getRegions();
					
					for (final ProtectedRegion region: regions.values())
					{
						boolean wasRemovedAsOwner = false;
						boolean wasRemovedAsMember = false;
						final DefaultDomain owners = region.getOwners();
						final DefaultDomain members = region.getMembers();
						
						if (owners.contains(player.getUniqueId()))
						{
							if (this.plugin.isLiveServer())
							{
								owners.removePlayer(player.getUniqueId());
								super.debug("Removed $1 as owner of region $2.", player, region);
								wasRemovedAsOwner = true;
							}
							else super.info("NOTLIVE: Skipped removing $1 as owner of region $2.", player, region);
						}
						
						if (members.contains(player.getUniqueId()))
						{
							if (this.plugin.isLiveServer())
							{
								members.removePlayer(player.getUniqueId());
								super.debug("Removed $1 as member of region $2.", player, region);
								wasRemovedAsMember = true;
							}
							else super.info("NOTLIVE: Skipped removing $1 member of region $2.", player, region);
						}
						
						final boolean ownerDeletion = (wasRemovedAsOwner && owners.size() == 0);
						final boolean memberDeletion = (wasRemovedAsMember && owners.size() == 0 && members.size() == 0);
						
						if (ownerDeletion || memberDeletion)
						{
							if (this.regionSettings.isRegionOrParentDeletable(region))
							{
								if (this.plugin.isLiveServer())
								{
									regionManager.removeRegion(region.getId());
									super.info("Deleted region $1 (no owners or members).", region);
								}
								else super.info("NOTLIVE: Skipped deleting empty region $1.", region);
							}
						}
					}
				}
			}
			
			double previousBalance = 0;
			if (this.plugin.isVaultEnabled())
			{
				final EconomyManager economyManager = this.plugin.getEconomyManager();
				final double amount = this.playerSettings.getDefaultBalance();
				previousBalance = economyManager.getBalance(player);
				if (this.plugin.isLiveServer())
				{
					economyManager.setBalance(player, amount);
					super.info("Reset balance for $1 to $$2.", player, Helpers.Money.formatString(amount));
				}
				else super.info("NOTLIVE: Skipped balance reset for $1 to $$2.", player, Helpers.Money.formatString(amount));
			}
			
			if (emails == null) super.info("$1 ($2) is banned, no deletion email necessary.", player, uuid);
			else
			{
				if (this.playerSettings.isRegistered(player))
				{
					emails.add(
							new PlayerEmailData(
									player,
									this.playerSettings.getEmail(player),
									this.playerSettings.getDeletionEmailSubject(),
									this.playerSettings.getDeletionEmailBody()));
					super.info("Prepared deletion email for $1.", player);
				}
				else super.info("$1 ($2) is not registered, no deletion email necessary.", player, uuid);
			}
			
			this.playerSettings.markPlayerAsDeleted(player);
			
			super.info(
					"PLAYER RESET: $1, balance was $$2, plot was $3.",
					player,
					Helpers.Money.formatString(previousBalance),
					(plotData == null ? "none" : plotData.getName()));
		}
	}
}
