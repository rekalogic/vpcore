package com.rekalogic.vanillapod.vpcore.player;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class PlayerAfkCheckTask extends TaskBase
{
	private final PlayerSettings settings;

	public PlayerAfkCheckTask(final PlayerSettings settings)
	{
		super(settings);

		this.settings = settings;
	}

	@Override
	public void runTask()
	{
		final long timestamp = System.currentTimeMillis() - this.settings.getAfkInactivity();
		for (final Player player: Bukkit.getOnlinePlayers())
		{
			if (this.settings.getLastActivity(player) < timestamp)
			{
				if (this.settings.getLastActivity(player) < (timestamp - this.settings.getAfkKick()))
				{
					if (!player.hasPermission("vpcore.afk.kick-exempt"))
					{
						super.info("$1 has been AFK for too long.", player);
						Helpers.Broadcasts.sendAll(
								"$1$2 has been AFK for too long.",
								ChatColor.GRAY,
								player.getName());
						player.kickPlayer(this.settings.getAfkKickMessage());
					}
				}
				else if (!this.settings.isAfk(player))
				{
					if (this.settings.isBusy(player)) this.settings.registerActivity(player);
					else if (!player.hasPermission("vpcore.afk.auto-exempt"))
					{
						super.debug("$1 has been detected as AFK.", player);
						this.settings.setIsAfk(player, true);
					}
				}
			}
			else if (!player.hasPermission("vpcore.afk.auto-exempt"))
			{
				if (this.settings.isAfk(player))
				{
					super.debug("$1 has been detected as no longer AFK.", player);
					this.settings.setIsAfk(player, false);
				}
			}
		}
	}
}
