package com.rekalogic.vanillapod.vpcore.player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.censor.CensorSettings;
import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionType;

public final class PlayerExecutor extends ExecutorBase<PlayerSettings>
{
	private final CensorSettings censorSettings;
	
	private OfflinePlayer[] sortedBalances = null;
	private long balancesLastRefreshed = 0;
	
	public PlayerExecutor(final VPPlugin plugin, final PlayerSettings settings, final CensorSettings censorSettings)
	{
		super(plugin, settings,

				//  Economy
				"balance", "baltop", "eco", "pay",

				//  Homes
				"delhome", "home", "homes", "sethome",
				
				//  Kits
				"delkit", "kit", "setkit",

				//  Messaging
				"busy", "ignore", "reply", "tell",

				//  Permissions and related
				"haspermission", "list", "listgrouppermissions", "listplayerpermissions", "movegroup",

				// Player monitoring
				"firstlog", "lastlog", "recover", "listdeaths",

				//  Spawns
				"setspawn", "spawn",

				//  Teleporting
				"tp", "tpa", "tpahere", "tphere", "tpno", "tptest", "tpyes",

				//  Warps
				"delwarp", "setwarp", "warp", "warps",

				//  Misc
				"afk", "exempt", "god", "holiday", "mute", "register", "unmute"
				
				);
		
		this.censorSettings = censorSettings;
	}

	@Override
	protected boolean checkCommandAllowed(final String command, final boolean isConsole)
	{
		switch (command)
		{
		case "busy":
		case "delhome":
		case "god":
		case "holiday":
		case "home":
		case "ignore":
		case "kit":
		case "pay":
		case "register":
		case "reply":
		case "sethome":
		case "setkit":
		case "setspawn":
		case "setwarp":
		case "spawn":
		case "tell":
		case "tp":
		case "tpa":
		case "tpahere":
		case "tphere":
		case "tpno":
		case "tpyes":
		case "warp":
			return (!isConsole);
		}
		
		return super.checkCommandAllowed(command, isConsole);
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "delhome": return (argCount == 1);
		case "delkit": return (argCount == 1);
		case "delwarp": return (argCount == 1);
		case "eco": return (argCount >= 1 && argCount <= 3);
		case "exempt": return (argCount == 2);
		case "firstlog": return (argCount <= 1);
		case "haspermission": return (argCount == 2);
		case "home": return (argCount <= 1);
		case "ignore": return (argCount <= 1);
		case "kit": return (argCount <= 1);
		case "lastlog": return (argCount <= 1);
		case "listdeaths": return (argCount <= 1);
		case "listgrouppermissions": return (argCount >= 1 && argCount <= 2);
		case "listplayerpermissions": return (argCount >= 1 && argCount <= 2);
		case "movegroup": return (argCount == 2);
		case "mute": return (argCount >= 1 && argCount <= 2);
		case "pay": return (argCount == 2);
		case "recover": return (argCount == 1);
		case "reply": return (argCount >= 1);
		case "sethome": return (argCount <= 1);
		case "setkit": return (argCount == 1);
		case "setspawn": return (argCount == 1);
		case "setwarp": return (argCount == 1);
		case "tell": return (argCount >= 2);
		case "tp": return (argCount == 1 || argCount == 3);
		case "tpa": return (argCount == 1);
		case "tpahere": return (argCount == 1);
		case "tphere": return (argCount == 1);
		case "tptest": return (argCount <= 1);
		case "unmute": return (argCount == 1);
		case "warp": return (argCount == 1);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "afk": return new String[] { ExecutorBase.FLAG_PLAYER };
		case "balance": return new String[] { ExecutorBase.FLAG_PLAYER };
		case "eco": return new String[] { ExecutorBase.FLAG_BOOL_SILENT };
		case "haspermission": return new String[] { ExecutorBase.FLAG_WORLD };
		case "holiday": return new String[] { ExecutorBase.FLAG_PLAYER }; 
		case "homes": return new String[] { ExecutorBase.FLAG_PLAYER };
		case "kit": return new String[] { ExecutorBase.FLAG_PLAYER };
		}
		
		return super.getAllowedFlags(command);
	}
	
	@Override
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "eco":return Arrays.asList("give", "take", "set", "reset");
		case "exempt": return Arrays.asList("none", "temporary", "indefinite");
		}

		if (sender instanceof Player)
		{
			final Player player = (Player)sender;
			switch (command)
			{
			case "delhome":
			case "home": return this.getHomesFor(player);
			
			case "delkit":
			case "kit": return this.getKitsFor(player);
			
			case "delwarp":
			case "warp": return this.getWarpsFor(player);
			}
		}
		else
		{
			switch (command)
			{
			case "delkit": return this.getKits();
			case "delwarp": return this.getWarps();
			}
		}
		
		return super.onTabComplete(sender, command, argIndex, args);
	}
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)		
		{
		case "delhome":
		case "delkit":
		case "delwarp":		
		case "home":
		case "kit":
		case "warp":
			switch (argIndex)
			{
			case 0: return LookupSource.FROM_PLUGIN;
			}
			break;

		case "god":
		case "haspermission":
		case "ignore":
		case "listdeaths":
		case "mute":
		case "pay":
		case "unmute":		
			switch (argIndex)
			{
			case 0: return LookupSource.OFFLINE_PLAYER;
			}
			break;
		
		case "tell":
		case "tp":
		case "tpa":
		case "tpahere":
		case "tphere":
			switch (argIndex)
			{
			case 0: return LookupSource.PLAYER;
			}
			break;
			
		case "setspawn":
			switch (argIndex)
			{
			case 0: return LookupSource.GROUP;
			}
			break;

		case "eco":
			switch (argIndex)
			{
			case 0: return LookupSource.FROM_PLUGIN;
			case 1: return LookupSource.OFFLINE_PLAYER;
			}
			break;
			
		case "exempt":
			switch (argIndex)
			{
			case 0: return LookupSource.OFFLINE_PLAYER;
			case 1: return LookupSource.FROM_PLUGIN;
			}
			break;
			
		
		case "listgrouppermissions":
			switch (argIndex)
			{
			case 0: return LookupSource.GROUP;
			case 1: return LookupSource.WORLD;
			}
			break;

		case "listplayerpermissions":
			switch (argIndex)
			{
			case 0: return LookupSource.OFFLINE_PLAYER;
			case 1: return LookupSource.WORLD;
			}
			break;

		case "movegroup":
			switch (argIndex)
			{
			case 0: return LookupSource.OFFLINE_PLAYER;
			case 1: return LookupSource.GROUP;
			}
			break;
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		switch (command)
		{
		case "afk": return this.doAfk(sender, args);
		case "busy": return this.doBusy(sender);
		case "balance": return this.doBalance(sender, args);
		case "baltop": return this.doBalTop(sender, args);
		case "delhome": return this.doDelHome(sender, args);		
		case "delkit": return this.doDelKit(sender, args);
		case "delwarp": return this.doDelWarp(sender, args);
		case "eco": return this.doEco(sender, args);
		case "exempt": return this.doExempt(sender, args);
		case "firstlog": return this.doFirstLog(sender, args);
		case "god": return this.doGod(sender, args);
		case "haspermission": return this.doHasPermissions(sender, args);
		case "holiday": return this.doHoliday(sender, args);
		case "home": return this.doHome(sender, args);
		case "homes": return this.doHomes(sender, args);
		case "ignore": return this.doIgnore(sender, args);
		case "lastlog": return this.doLastLog(sender, args);
		case "list": return this.doList(sender, args);
		case "listdeaths": return this.doListDeaths(sender, args);
		case "listgrouppermissions": return this.doListGroupPermissions(sender, args);
		case "listplayerpermissions": return this.doListPlayerPermissions(sender, args);
		case "kit": return this.doKit(sender, args);
		case "movegroup": return this.doMoveGroup(sender, args);
		case "mute": return this.doMute(sender, args);
		case "pay": return this.doPay(sender, args);
		case "recover": return this.doRecover(sender, args);
		case "register": return this.doRegister(sender, args);
		case "reply": return this.doReply(sender, args);
		case "sethome": return this.doSetHome(sender, args);
		case "setkit": return this.doSetKit(sender, args);
		case "setspawn": return this.doSetSpawn(sender, args);
		case "setwarp": return this.doSetWarp(sender, args);
		case "spawn": return this.doSpawn(sender, args);
		case "tell": return this.doTell(sender, args);
		case "tp": return this.doTeleportRequest(sender, args, false, true);
		case "tpa": return this.doTeleportRequest(sender, args, true, false);
		case "tpahere": return this.doTeleportHereRequest(sender, args, true);
		case "tphere": return this.doTeleportHereRequest(sender, args, false);
		case "tpno": return this.doTeleportAccept(sender, false);
		case "tptest": return this.doTeleportTest(sender, args);
		case "tpyes": return this.doTeleportAccept(sender, true);
		case "unmute": return this.doUnmute(sender, args);
		case "warp": return this.doWarp(sender, args);
		case "warps": return this.doListWarps(sender, args);
		}

		return super.onCommand(sender, command, label, args);
	}
	
	private boolean doListDeaths(final PagedOutputCommandSender sender, final String[] args)
	{
		OfflinePlayer player = null;
		if (args.length == 1)
		{
			player = super.getPlugin().getOfflinePlayer(sender, args[0]);
			if (player == null) return true;
		}
		
		final long since = super.getFlagLong(ExecutorBase.FLAG_TIME, Helpers.DateTime.MillisecondsInADay * 7);
		final long now = System.currentTimeMillis();
		final List<PlayerDeathData> playerDeathData = super.getSettings().getPlayerDeathData(player, now - since);
		if (playerDeathData.size() == 0)
		{
			if (player == null) sender.sendSuccess(
					"No player deaths within the last $1.",
					Helpers.DateTime.getHumanReadableFromMillis(since));
			else sender.sendSuccess(
					"No deaths for $1 within the last $2.",
					player,
					Helpers.DateTime.getHumanReadableFromMillis(since));
		}
		else
		{
			if (player == null) sender.sendSuccess(
					"Showing $1 player death$2 within the last $3.",
					playerDeathData.size(),
					Helpers.getPlural(playerDeathData),
					Helpers.DateTime.getHumanReadableFromMillis(since));
			else sender.sendSuccess(
					"Showing $1 player death$2 for $3 within the last $4.",
					playerDeathData.size(),
					Helpers.getPlural(playerDeathData),
					player,
					Helpers.DateTime.getHumanReadableFromMillis(since));
			for (PlayerDeathData data: playerDeathData)
			{
				final UUID uuid = data.getUuid();
				if (player == null ||
						!player.getUniqueId().equals(uuid))
				{
					player = Helpers.Bukkit.getOfflinePlayer(uuid);
					if (player == null || !player.hasPlayedBefore()) continue;
				}
				sender.sendNotification(
						"$1: $2 [$3] $4 item$5 $6 ago.",
						data.getId(),
						player,
						data.getCause(),
						data.getItems(),
						Helpers.getPlural(data.getItems()),
						Helpers.DateTime.getTruncatedAtHourTimeDifference(data.getOccurred(), now));
			}
		}
		
		return true;
	}

	private boolean doRecover(final PagedOutputCommandSender sender, final String[] args)
	{
		int deathId = 0;
		try { deathId = Integer.parseInt(args[0]); }
		catch (NumberFormatException ex)
		{
			sender.sendFailure("Invalid value $1 for deathId.", deathId);
			return true;
		}
		
		final Player player = sender.getPlayer();
		final ItemStack[] items = super.getSettings().getDeathInventoryItems(deathId);
		if (items == null || items.length == 0) sender.sendFailure("No items for, or no such death as, death #$1.", deathId);
		else
		{
			for (final ItemStack item: items)
			{
				final HashMap<Integer, ItemStack> lostItems = player.getInventory().addItem(item);
				final String[] friendlyNames = Helpers.Item.getFriendlyNames(item);
	
				if (lostItems.size() == 0) sender.sendInfo("Recovered $1 x $2.", item.getAmount(), friendlyNames[0], player);
				else
				{
					for (final ItemStack lostStack: lostItems.values())
					{
						sender.sendFailure(
								"$1 did not have enough space for $2 x $3, and $4 were lost.",
								player,
								item.getAmount(),
								friendlyNames[0],
								lostStack.getAmount());
					}
				}
			}
			
			sender.sendSuccess("Recovered inventory for death #$1.", deathId);
		}		

		return true;
	}

	private boolean doFirstLog(final PagedOutputCommandSender sender, final String[] args)	
	{
		final long now = System.currentTimeMillis();
		long timeLimit = Helpers.DateTime.MillisecondsInADay;
		if (args.length == 1) timeLimit = Helpers.DateTime.getMillis(args[0]);
		if (timeLimit < 0) return false;		
		final long since = now - timeLimit;
		
		final List<UUID> uuids = super.getSettings().getFirstLoggedInPlayers(since);
		final List<OfflinePlayer> players = new ArrayList<OfflinePlayer>(uuids.size());
		for (final UUID uuid: uuids)
		{
			OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
			if (player.hasPlayedBefore()) players.add(player);
		}
		
		sender.sendNotification(
				"Showing $1 player$2 in the last $3.",
				players,
				Helpers.getPlural(players),
				Helpers.DateTime.getHumanReadableFromMillis(timeLimit));
		
		for (final OfflinePlayer player: players)
		{
			final Group group = super.getSettings().getCurrentGroup(player);
			sender.sendNotification(
					"$1 ($2) first seen $3 ago",
					player,
					group,
					Helpers.DateTime.getTruncatedAtHourTimeDifference(player.getFirstPlayed(), now));
		}
		
		return true;
	}

	private boolean doLastLog(final PagedOutputCommandSender sender, final String[] args)	
	{
		final long now = System.currentTimeMillis();
		long timeLimit = Helpers.DateTime.MillisecondsInADay;
		if (args.length == 1) timeLimit = Helpers.DateTime.getMillis(args[0]);
		if (timeLimit < 0) return false;		
		final long since = now - timeLimit;
		
		final List<UUID> uuids = super.getSettings().getLastLoggedInPlayers(since);
		final List<OfflinePlayer> players = new ArrayList<OfflinePlayer>(uuids.size());
		for (final UUID uuid: uuids)
		{
			OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
			if (player.hasPlayedBefore()) players.add(player);
		}
		
		sender.sendNotification(
				"Showing $1 player$2 in the last $3.",
				players,
				Helpers.getPlural(players),
				Helpers.DateTime.getHumanReadableFromMillis(timeLimit));

		for (final OfflinePlayer player: players)
		{			
			if (player.isOnline())
			{
				final Group group = super.getSettings().getCurrentGroup(player);
				sender.sendNotification("$1 ($2) online", player, group);
			}
		}
		
		for (final OfflinePlayer player: players)
		{
			if (!player.isOnline())
			{
				final Group group = super.getSettings().getCurrentGroup(player);
				sender.sendNotification(
						"$1 ($2) seen $3 ago",
						player,
						group,
						Helpers.DateTime.getTruncatedAtHourTimeDifference(player.getLastPlayed(), now));
			}
		}
		
		return true;
	}

	private boolean doHoliday(final PagedOutputCommandSender sender, final String[] args)
	{
		OfflinePlayer player = null;
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER))
		{
			if (!sender.hasPermission("vpcore.moderator")) return false;
			player = super.getOfflinePlayer(sender);
			if (player == null) return true;
		}
		else player = sender.getPlayer();

		final Group group = super.getSettings().getCurrentGroup(player);
		if (group.isExempt()) sender.sendFailure("Group $1 is automatically exempt from plot deletions.", group);
		else
		{
			switch (super.getSettings().getPlotDeletionExemption(player))
			{
			case 0:
				super.getSettings().setExemptFromPlotDeletion(player, 1);
				if (player.equals(sender.getPlayer()))
					sender.sendSuccess("You will be exempt from plot deletions until your next login.");
				else sender.sendSuccess("$1 will be exempt from plot deletions until their next login.", player);
				break;
			case 1:
				if (player.equals(sender.getPlayer()))
					sender.sendNotification("You're already exempt. To cancel, log out and log back in.");
				else sender.sendSuccess("$1 is already exempt.", player);
				break;
			case 2:
				if (player.equals(sender.getPlayer()))
					sender.sendNotification("You're already permanently exempt.");
				else sender.sendSuccess("$1 is already permanently exempt.", player);
				break;
			}
		}
		
		return true;
	}

	private boolean doMoveGroup(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;

		final Group group = super.getSettings().getGroup(args[1]);
		if (group == null)
		{
			sender.sendFailure("No such group $1.", args[1]);
			return true;
		}
		
		final Group currentGroup = super.getSettings().getCurrentGroup(player);
		if (!sender.isConsole())
		{
			if (sender.getPlayer().equals(player))
			{
				sender.sendFailure("You cannot move yourself into a different group.");
				return true;
			}
			else if (currentGroup.isAdmin())
			{
				sender.sendFailure("You cannot move an Admin into a different group.");
				return true;
			}
		}
		
		if (group.equals(currentGroup))
		{
			sender.sendFailure("$1 is already a member of $2.", player, group);
			return true;
		}
		
		super.getSettings().changeGroup(player, group, false);
		sender.sendSuccess("$1 moved to group $2.", player, group);
		
		return true;
	}

	private boolean doSetKit(final PagedOutputCommandSender sender, final String[] args)
	{
		final String kitName = args[0];
		if (super.getSettings().kitExists(kitName))
		{
			String realKitName = super.getSettings().getRealKitName(kitName);
			sender.sendFailure("Kit $1 already exists.", realKitName);
		}
		else
		{
			super.getSettings().createKit(kitName, sender.getPlayer().getInventory().getContents());
			super.getSettings().save();
			sender.sendSuccess("Created kit $1.", kitName);
		}
		return true;
	}

	private boolean doDelKit(final PagedOutputCommandSender sender, final String[] args)
	{
		final String kitName = args[0];
		if (super.getSettings().kitExists(kitName))
		{
			String realKitName = super.getSettings().getRealKitName(kitName);
			super.getSettings().deleteKit(realKitName);
			super.getSettings().save();
			sender.sendSuccess("Deleted kit $1.", realKitName);
		}
		else sender.sendFailure("No such kit $1.", kitName);
		return true;
	}

	private boolean doTeleportTest(final PagedOutputCommandSender sender, final String[] args)
	{
		sender.suppressPaging();

		Player player = null;
		if (args.length == 1) player = super.getPlugin().getPlayer(sender, args[0]);
		else player = sender.getPlayer();
		if (player == null) return true;
		
		sender.sendNotification("Testing location of $1 $2.", player, player.getLocation());
		super.getSettings().validateTeleportDestination(null, sender.getPlayer(), player.getLocation().getBlock(), true);
		return true;
	}

	private boolean doKit(final PagedOutputCommandSender sender, final String[] args)
	{
		Player targetPlayer = null;
		Player permissionPlayer = null;
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER))
		{
			if (!sender.hasPermission("vpcore.moderator")) return false;
		}
		else
		{
			permissionPlayer = sender.getPlayer();
			if (permissionPlayer == null) return true;
		}
		
		targetPlayer = super.getPlayer(sender);
		if (targetPlayer == null) return true;

		if (args.length == 0)
		{
			List<String> kitNames = null;
			if (permissionPlayer == null) kitNames = super.getSettings().getKitNames();
			else kitNames = this.getKitsFor(permissionPlayer);
			
			String kitList = "";
			for (final String kit: kitNames)
			{
				if (kitList.length() > 0) kitList += ", ";
				kitList += kit;
			}
			
			if (kitList.length() > 0)
			{
				sender.sendNotification("Available kits:");
				sender.sendInfo(kitList);
			}
			else sender.sendFailure("No kits available.");
		}
		else
		{
			String kit = args[0];
			if (super.getSettings().kitExists(kit) && 
					(sender.hasPermission("vpcore.moderator") ||
					sender.hasPermission(Helpers.Parameters.replace("vpcore.kits.$1", kit))))
			{
				final long now = System.currentTimeMillis();
				final long lastUsedKit = super.getSettings().getTimeLastUsedKit(targetPlayer, kit);
				final long timeSinceLastUsedKit = (now - lastUsedKit);
				final long kitCoolDown = super.getSettings().getKitCoolDown();
				
				if (sender.hasPermission("vpcore.moderator") ||
						timeSinceLastUsedKit >= kitCoolDown)
				{
					super.getSettings().giveKit(targetPlayer, kit);
					 
					String realKitName = super.getSettings().getRealKitName(kit); 
					Helpers.Messages.sendSuccess(targetPlayer, "You received kit $1.", realKitName);
					if (!targetPlayer.equals(permissionPlayer))
						sender.sendSuccess("You gave $1 kit $2.", targetPlayer, realKitName);
				}
				else sender.sendFailure(
						"You must wait $1 before using kit $2 again.",
						Helpers.DateTime.getHumanReadableFromMillis(kitCoolDown - timeSinceLastUsedKit, true),
						kit);
			}
			else sender.sendFailure("Kit $1 does not exist.", kit);
		}
		
		return true;
	}
	
	private List<String> getKits() { return this.getKitsFor(null); }
	private List<String> getKitsFor(final Player player)
	{
		final List<String> kits = new ArrayList<String>();
		for (final String kit: super.getSettings().getKitNames())
		{
			if (player == null ||
					player.hasPermission("vpcore.moderator") ||
					player.hasPermission(Helpers.Parameters.replace("vpcore.kits.$1", kit)))
				kits.add(kit);
		}
		return kits;
	}

	private boolean doExempt(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;

		if (sender.isPlayer() && player.equals(sender.getPlayer()))
		{
			sender.sendFailure("You cannot exempt yourself from plot deletion.");
			return false;
		}
		
		final Group group = super.getSettings().getCurrentGroup(player);
		if (group.isExempt()) sender.sendFailure("Group $1 is automatically exempt from plot deletions.", group);
		else
		{
			int exempt = 0;
			switch (args[1].toLowerCase())
			{
			case "0":
			case "off":
			case "no":
			case "none":
				exempt = 0;
				break;
				
			case "1":
			case "on":
			case "yes":
			case "temp":
			case "temporary":
				exempt = 1;
				break;
				
			case "2":
			case "indef":
			case "indefinite":
				exempt = 2;
				break;
				
			default: return false;
			}
	
			super.getSettings().setExemptFromPlotDeletion(player, exempt);
			if (exempt == 1) sender.sendSuccess("Player $1 is now exempt from plot deletion until next login.", player);
			else if (exempt == 2) sender.sendSuccess("Player $1 is now exempt from plot deletion indefinitely.", player);
			else sender.sendSuccess("Player $1 is no longer exempt from plot deletion.", player);
		}

		return true;
	}

	private boolean doIgnore(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length == 0)
		{
			final List<UUID> ignored = super.getSettings().getIgnoredList(sender.getPlayer());
			if (ignored == null || ignored.isEmpty()) sender.sendSuccess("You are not ignoring anyone.");
			else
			{
				sender.sendNotification("You are ignoring the following player$1:", Helpers.getPlural(ignored));
				int index = 0;
				for (final UUID uuid: ignored)
				{
					final OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
					sender.sendInfoListItem(index, "$1", player);
					++index;
				}
			}
		}
		else
		{
			final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
			if (player == null) return true;
			
			if (player.isOnline())
			{
				if (player.equals(sender.getPlayer()))
				{
					sender.sendFailure("You try ignoring yourself, but you don't take any notice.");
					return true;
				}
				else if (player.getPlayer().hasPermission("vpcore.moderator"))
				{
					sender.sendFailure("You cannot ignore $1.", player);
					return true;
				}
			}

			final boolean isIgnored = super.getSettings().toggleIgnore(sender.getPlayer(), player);
			if (isIgnored) sender.sendSuccess("You are now ignoring $1.", player);
			else sender.sendSuccess("You are no longer ignoring $1.", player);
		}

		return true;
	}

	private boolean doGod(final PagedOutputCommandSender sender, final String[] args)
	{
		OfflinePlayer player = null;
		if (args.length == 0) player = sender.getPlayer();
		else
		{
			player = super.getPlugin().getOfflinePlayer(sender, args[0]);
			if (player == null) return true;
		}		
		
		boolean god = super.getSettings().isGod(player);
		
		if (!god && player.isOnline())
		{
			switch (player.getPlayer().getGameMode())
			{
			case ADVENTURE:
			case SURVIVAL: break;
			
			case CREATIVE:
			case SPECTATOR:
				sender.sendFailure(
						"Cannot turn on GOD mode for $1 players.",
						player.getPlayer().getGameMode());
				return true;
			}
		}

		god = super.getSettings().setGod(player, !god);

		if (sender.is(player))
		{
			if (god) sender.sendSuccess("You are now invulnerable.");
			else sender.sendSuccess("You are no longer invulnerable.");
		}
		else
		{
			if (god) sender.sendSuccess("$1 is now invulnerable.", player);
			else sender.sendSuccess("$1 is no longer invulnerable.", player);
		}

		return true;
	}

	private boolean doList(final PagedOutputCommandSender sender, final String[] args)
	{
		Player player = null;
		if (sender.isPlayer()) player = sender.getPlayer();
		super.getSettings().displayPlayerList(sender, player);
		
		return true;
	}
	

	private boolean doUnmute(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;

		if (!sender.isConsole() &&
				player.isOnline() &&
				player.equals(sender.getPlayer()))
		{
			sender.sendFailure("You cannot unmute yourself!");
			return true;
		}

		if (super.getSettings().isMuted(player))
		{
			super.getSettings().unmute(player);
			sender.sendSuccess("$1 was unmuted.", player);
		}
		else sender.sendFailure("$1 is not muted.", player);

		return true;
	}

	private boolean doMute(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;
		
		if (!sender.isConsole() &&
				player.isOnline() &&
				player.equals(sender.getPlayer()))
		{
			sender.sendFailure("Self-muting is best achieved through conversational restraint.");
			return true;
		}
		
		if (player.isOp() ||
				(player.isOnline() && player.getPlayer().hasPermission("vpcore.moderator")))
		{
			if (!sender.isConsole())
			{
				sender.sendFailure("Ops and staff players can only be muted by CONSOLE.");
				return true;
			}
		}

		long duration = 0;
		if (args.length == 2)
		{
			duration = Helpers.DateTime.getMillis(args[1]);
			if (duration == -1)
			{
				sender.sendFailure("Could not parse duration '$1'.", args[1]);
				return true;
			}
			else if (duration < Helpers.DateTime.MillisecondsInAMinute)
			{
				sender.sendFailure("Minimum mute duration is 1m.");
				return true;
			}
		}
		
		duration = super.getSettings().mute(player, duration);
		final String durationHumanReadable = Helpers.DateTime.getHumanReadableFromMillis(duration);
		sender.sendSuccess("$1 was muted for $2.", player, durationHumanReadable);
		super.info("$1 muted $2 for $3.", sender.getDisplayName(), player, durationHumanReadable);

		return true;
	}
	
	private boolean doListPlayerPermissions(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;
		
		final Group group = super.getSettings().getCurrentGroup(player);
		
		World world = null;
		if (args.length == 2)
		{
			world = Helpers.getWorld(sender, args[1]);
			if (world == null) return true;
		}
		else world = Bukkit.getWorlds().get(0);

		return this.listGroupPermissionsForWorld(sender, group, world);
	}

	private boolean doListGroupPermissions(final PagedOutputCommandSender sender, final String[] args)
	{
		final Group group = super.getSettings().getGroup(args[0]);
		if (group == null)
		{
			sender.sendFailure("No such group $1.", args[0]);
			return true;
		}
		
		World world = null;
		if (args.length == 2)
		{
			world = Helpers.getWorld(sender, args[1]);
			if (world == null) return true;
		}
		else world = Bukkit.getWorlds().get(0);
		
		return this.listGroupPermissionsForWorld(sender, group, world);
	}
	
	private boolean listGroupPermissionsForWorld(final PagedOutputCommandSender sender, final Group group, final World world)
	{
		sender.sendNotification("Parameters for group $1 in world $2:", group, world);
		final PermissionDescriptor descriptor = group.getPermissions(world.getName());
		if (descriptor == null)
		{
			sender.sendFailure("No descriptor exists in $1 for $2.", world, group);
			return true;
		}

		String inheritances = "";
		if (descriptor.getInheritedGroups().size() == 0) inheritances = "<none>";
		else
		{
			for (final String inheritance: descriptor.getInheritedGroups())
			{
				if (inheritances.length() > 0) inheritances += ", ";
				inheritances += inheritance;
			}
		}

		sender.sendNotification("Inheritance: $1", inheritances);

		String[] permissions = null;

		permissions = descriptor.getDirectPermissions().toArray(new String[0]);
		if (permissions.length == 0) sender.sendNotification("Direct permissions: $1", "<none>");
		else
		{
			sender.sendNotification("Direct permissions:");
			for (final String permission: permissions)
				sender.sendNotification("  $1", permission);
		}

		permissions = descriptor.getPositivePermissions().toArray(new String[0]);
		if (permissions.length == 0) sender.sendNotification("Positive permissions: $1", "<none>");
		else
		{
			sender.sendNotification("Positive permissions:");
			for (final String permission: permissions)
				sender.sendNotification("  $1", permission);
		}

		permissions = descriptor.getNegativePermissions().toArray(new String[0]);
		if (permissions.length == 0) sender.sendNotification("Negative permissions: $1", "<none>");
		else
		{
			sender.sendNotification("Negative permissions:");
			for (final String permission: permissions)
				sender.sendNotification("  $1", permission);
		}

		permissions = descriptor.getFinalPermissions().toArray(new String[0]);
		if (permissions.length == 0) sender.sendNotification("FINAL permissions: $1", "<none>");
		else
		{
			sender.sendNotification("FINAL permissions:");
			for (final String permission: permissions)
				sender.sendNotification("  $1", permission);
		}

		return true;
	}

	private boolean doHasPermissions(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer offlinePlayer = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (offlinePlayer == null) return true;
		
		final String permission = args[1];
		
		final World world = super.getWorld(sender);
		if (world == null) return true;
		
		final Group group = super.getSettings().getCurrentGroup(offlinePlayer);
		final String effectiveWorld = super.getPlugin().getPermissionManager().getWorldMapping(world);
		final boolean groupHasPermission = group.hasPermission(effectiveWorld, permission);
		if (groupHasPermission) sender.sendSuccess("$1 does have $2 (group) in $3.", offlinePlayer, permission, world);
		else sender.sendFailure("$1 does NOT have $2 (group) in $3.", offlinePlayer, permission, world);
		
		if (offlinePlayer.isOnline())
		{
			final boolean playerHasPermission = offlinePlayer.getPlayer().hasPermission(permission);
			if (groupHasPermission != playerHasPermission)
			{
				if (playerHasPermission) sender.sendSuccess("$1 does have $2 (player) in $3.", offlinePlayer, permission, world);
				else sender.sendFailure("$1 does NOT have $2 (player) in $3.", offlinePlayer, permission, world);
			}
		}

		return true;
	}

	private boolean doReply(final PagedOutputCommandSender sender, final String[] args)
	{
		final UUID uuid = super.getSettings().getLastTell(super.getPlayer(sender));
		if (uuid != null)
		{
			final Player player = Bukkit.getPlayer(uuid);
			if (player == null) sender.sendFailure("The player no longer exists.");
			else if (!player.isOnline()) sender.sendFailure("$1 is not online.", player);
			else
			{
				final String message = Helpers.Args.concatenate(args);
				final Player sendingPlayer = sender.getPlayer();
				this.sendTell(sendingPlayer, player, message);
				super.getSettings().registerActivity(sendingPlayer);
			}
		}
		else sender.sendFailure("There is nobody to whom you can reply.");
		return true;
	}

	private boolean doTell(final PagedOutputCommandSender sender, final String[] args)
	{
		final Player player = super.getPlugin().getPlayer(sender, args[0]);
		if (player != null)
		{
			final String message = Helpers.Args.concatenate(args, 1);
			final Player sendingPlayer = sender.getPlayer();  
			this.sendTell(sendingPlayer, player, message);
			super.getSettings().registerActivity(sendingPlayer);
		}
		return true;
	}

	private void sendTell(final Player fromPlayer, final Player toPlayer, final String message)
	{
		if (super.getSettings().isMuted(fromPlayer))
		{
			Helpers.Messages.sendFailure(fromPlayer, "You are muted.");
			super.info("$1 is MUTED: [$2] $3", fromPlayer, toPlayer, message);
			return;
		}
		
		if (fromPlayer.equals(toPlayer))
		{
			Helpers.Messages.sendFailure(fromPlayer, "Try telling yourself that!");
			return;
		}

		if (super.getSettings().isIgnoredBy(fromPlayer, toPlayer))
		{
			Helpers.Messages.sendFailure(fromPlayer, "$1 is ignoring you.", toPlayer);
			return;
		}

		if (super.getSettings().isBusy(toPlayer) && !fromPlayer.hasPermission("vpcore.moderator"))
		{
			Helpers.Messages.sendFailure(fromPlayer, "$1 is currently BUSY and cannot receive messages.", toPlayer);
			return;
		}
		
		String censoredMessage = this.censorSettings.censorChat(fromPlayer, message);
		if (fromPlayer.hasPermission("vpcore.chat.colour"))
			censoredMessage = Helpers.formatMessage(censoredMessage);

		Helpers.Messages.sendNotification(fromPlayer, "[$1->$2] $3", "me", toPlayer, censoredMessage);
		Helpers.Messages.sendNotification(toPlayer, "[$1->$2] $3", fromPlayer, "me", censoredMessage);
		Helpers.Broadcasts.sendPerms("vpcore.monitor.tell", "[$1->$2] $3", fromPlayer, toPlayer, censoredMessage);
		super.getSettings().setLastTell(fromPlayer, toPlayer);
	}

	private boolean doBusy(final PagedOutputCommandSender sender)
	{
		final boolean busy = super.getSettings().toggleBusy(sender.getPlayer());
		final Player player = sender.getPlayer();

		String message = "";
		String messageToOthers = "";
		if (busy)
		{
			message = "You are now BUSY.";
			messageToOthers = Helpers.Parameters.replace("$1- $2 is now BUSY.", ChatColor.GRAY, player.getName());
		}
		else
		{
			message = "You are no longer BUSY.";
			messageToOthers = Helpers.Parameters.replace("$1- $2 is no longer BUSY.", ChatColor.GRAY, player.getName());
		}

		Helpers.Messages.sendNotification(player, message);
		if (!super.getPlugin().isVanished(player))
		{
			for (final Player otherPlayer: Bukkit.getOnlinePlayers())
			{
				if (!player.equals(otherPlayer))
					Helpers.Messages.sendInfo(otherPlayer, messageToOthers);
			}
		}

		return true;
	}

	private boolean doTeleportAccept(final PagedOutputCommandSender sender, final boolean accepted)
	{
		final Player player = sender.getPlayer();
		final TeleportRequest request = super.getSettings().getTeleportRequestToAccept(player);
		if (request == null) sender.sendFailure("You have no pending teleport request.");
		else
		{
			if (!request.getRequestor().isOnline())
			{
				sender.sendFailure("$1 is no longer online.", request.getRequestor());
				request.decline();
			}
			else if (accepted)
			{
				if (request.getTeleportee().equals(request.getRequestor()) &&
						!super.getSettings().validateTeleportDestination(request.getTeleportee(), request.getDestination()))
					return true;

				Helpers.Messages.sendSuccess(request.getAcceptor(), "You accepted the teleport request from $1.", request.getRequestor());
				Helpers.Messages.sendSuccess(request.getRequestor(), "Your teleport request was accepted by $1.", request.getAcceptor());

				request.accept();
			}
			else
			{
				Helpers.Messages.sendFailure(request.getAcceptor(), "You declined the teleport request from $1.", request.getRequestor());
				Helpers.Messages.sendFailure(request.getRequestor(), "Your teleport request was declined by $1.", request.getAcceptor());

				request.decline();
			}
		}
		return true;
	}

	private boolean doTeleportHereRequest(final PagedOutputCommandSender sender, final String[] args, boolean requireAuth)
	{
		final Player player = super.getPlayer(sender);
		final Player targetPlayer = super.getPlugin().getPlayer(sender, args[0]);

		if (targetPlayer != null)
		{
			if (targetPlayer.equals(player))
			{
				sender.sendFailure("You are already at yourself.");
				return true;
			}

			if (!player.hasPermission("vpcore.moderator"))
			{
				requireAuth = true;
				if (!targetPlayer.hasPermission("vpcore.command.tpyes"))
				{
					sender.sendFailure("$1 does not have permission to accept this request.", targetPlayer);
					return true;
				}
				
				final TeleportRequest existingRequest = super.getSettings().getLastTeleportRequestSent(player);
				if (existingRequest != null)
				{
					sender.sendFailure(
							"Your previous teleport request is still pending ($1).",
							Helpers.DateTime.getHumanReadableFromMillis(
									existingRequest.getTimeUntilExpiry(super.getSettings().getTeleportRequestWindow()),
									true));
									
					return true;
				}
			}

			final TeleportRequest request = TeleportRequest.getTpaHereRequest(player, targetPlayer);
			if (super.getSettings().validateTeleportDestination(request.getRequestor(), request.getDestination()))
			{
				if (requireAuth)
				{
					super.getSettings().createTeleportRequest(request);

					Helpers.Messages.sendNotification(request.getAcceptor(), "Teleport request: do you wish to teleport to $1?", player);
					Helpers.Messages.sendNotification(request.getAcceptor(), "Accept with $1 or decline with $2.", "/tpyes", "/tpno");

					Helpers.Messages.sendSuccess(request.getRequestor(), "Request sent to $1.", request.getAcceptor());
				}
				else
				{
					request.getAcceptor().teleport(request.getDestination(), TeleportCause.PLUGIN);
					if (request.getAcceptor().canSee(request.getRequestor())) Helpers.Messages.sendSuccess(request.getAcceptor(), "Teleporting you.");
					else Helpers.Messages.sendSuccess(request.getAcceptor(), "Teleported you to $1.", request.getRequestor());
					Helpers.Messages.sendSuccess(request.getRequestor(), "Teleported $1 to you.", request.getAcceptor());
				}
			}
		}

		return true;
	}

	private boolean doTeleportRequest(final PagedOutputCommandSender sender, final String[] args, boolean requireAuth, final boolean allowCoords)
	{
		final Player player = super.getPlayer(sender);
		if (args.length == 3)
		{
			if (!allowCoords) return false;
			if (!player.hasPermission("vpcore.moderator")) return false;

			double x = 0;
			double y = 0;
			double z = 0;

			try
			{
				x = Double.parseDouble(args[0]);
				y = Double.parseDouble(args[1]);
				z = Double.parseDouble(args[2]);
			}
			catch (final NumberFormatException ex) { return false; }

			final Location location = new Location(
					player.getWorld(),
					x,
					y,
					z,
					player.getLocation().getYaw(),
					player.getLocation().getPitch());

			if (super.getSettings().validateTeleportDestination(player, location))
			{
				player.teleport(location, TeleportCause.COMMAND);
				sender.sendSuccess("Teleporting to $1.", location);
			}
		}
		else
		{
			final Player targetPlayer = super.getPlugin().getPlayer(sender, args[0]);
			if (targetPlayer != null)
			{
				if (targetPlayer.equals(player))
				{
					sender.sendFailure("You are already at yourself.");
					return true;
				}

				if (!player.hasPermission("vpcore.moderator"))
				{
					requireAuth = true;
					if (!targetPlayer.hasPermission("vpcore.command.tpyes"))
					{
						sender.sendFailure("$1 does not have permission to accept this request.", targetPlayer);
						return true;
					}
					
					final TeleportRequest existingRequest = super.getSettings().getLastTeleportRequestSent(player);
					if (existingRequest != null)
					{
						sender.sendFailure(
								"Your previous teleport request is still pending ($1).",
								Helpers.DateTime.getHumanReadableFromMillis(
										existingRequest.getTimeUntilExpiry(super.getSettings().getTeleportRequestWindow()),
										true));
										
						return true;
					}
				}

				final TeleportRequest request = TeleportRequest.getTpaRequest(player, targetPlayer);
				if (super.getSettings().validateTeleportDestination(request.getTeleportee(), request.getDestination()))
				{
					if (requireAuth)
					{
						super.getSettings().createTeleportRequest(request);
	
						Helpers.Messages.sendNotification(request.getAcceptor(), "Teleport request: $1 wishes to teleport to you.", player);
						Helpers.Messages.sendNotification(request.getAcceptor(), "Accept with $1 or decline with $2.", "/tpyes", "/tpno");
	
						Helpers.Messages.sendSuccess(request.getRequestor(), "Request sent to $1.", request.getAcceptor());
					}
					else
					{
						player.teleport(request.getDestination(), TeleportCause.PLUGIN);
						sender.sendSuccess("Teleporting to $1.", request.getDestination());
					}
				}
			}
		}

		return true;
	}
	
	private boolean canSetHomeHere(final Player player, final Location location)
	{
		boolean canSetHomeHere = true;
		
		if (super.getSettings().isHomeRestrictionEnabled() &&
				super.getPlugin().isWorldGuardEnabled() &&
				!player.hasPermission("vpcore.moderator"))
		{
			final RegionManager manager = super.getPlugin().getWorldGuard().getRegionManager(location.getWorld());
			if (manager != null)
			{
				for (final ProtectedRegion region: manager.getApplicableRegions(location))
				{
					if (region.getType() == RegionType.GLOBAL) continue;
					if (super.getSettings().regionIsIgnoredForHomes(region.getId())) continue;
					
					final LocalPlayer localPlayer = super.getPlugin().getWorldGuard().wrapPlayer(player);
					if (!region.isMember(localPlayer) && !region.isOwner(localPlayer))
					{
						super.info("Region $1 preventing home ($2).", region.getId(), region);
						canSetHomeHere = false;
						break;
					}
				}
			}
		}
		
		return canSetHomeHere;
	}

	private boolean doSetHome(final PagedOutputCommandSender sender, final String[] args)
	{
		String homeName = PlayerSettings.DefaultHomeName;
		if (args.length == 1) homeName = args[0];
		final Player player = super.getPlayer(sender);
		final Location location = super.getSettings().getHomeLocation(player, homeName);
		if (location == null)
		{
			final Set<String> homeNames = super.getSettings().getHomeNames(player);
			if (homeNames != null)
			{
				int homeCount = 0;
				for (final String home: homeNames)
					if (!home.equalsIgnoreCase(PlayerSettings.BedHomeName))
						++homeCount;
				int maxHomes = 0;
				final Group group = super.getSettings().getCurrentGroup(player);
				if (group != null) maxHomes = group.getMaxHomes();
				if (maxHomes == 0)
				{
					sender.sendFailure("You are not allowed to set any homes.");
					return true;
				}
				else if (homeCount >= maxHomes)
				{
					sender.sendFailure("You already have a maximum of $1 home$2.", maxHomes, Helpers.getPlural(maxHomes));
					return true;
				}
			}
		}
		
		if (homeName.equalsIgnoreCase(PlayerSettings.BedHomeName)) sender.sendFailure("To set your bed home you must use a bed.");
		else if (homeName.equalsIgnoreCase(PlayerSettings.PlotHomeName)) sender.sendFailure("Your plot home is automatically set when you purchase an Orkida plot.");
		else
		{
			final Location newHomeLocation = player.getLocation();
			if (this.canSetHomeHere(player, newHomeLocation))
			{
				super.getSettings().setHomeLocation(player, homeName, newHomeLocation);
				sender.sendSuccess("Home $1 set.", homeName);
			}
			else sender.sendFailure("You do not have permission to set a home here.");
		}
		return true;
	}

	private boolean doHome(final PagedOutputCommandSender sender, final String[] args)
	{
		String homeName = PlayerSettings.DefaultHomeName;
		if (args.length == 1) homeName = args[0].toLowerCase();
		final Player player = sender.getPlayer();
		Location location = super.getSettings().getHomeLocation(player, homeName);
		if (location == null)
		{
			if (args.length == 0)
			{
				homeName = PlayerSettings.BedHomeName;
				location = super.getSettings().getHomeLocation(player, homeName);
				if (location == null) sender.sendFailure(
						"Default homes ($1 and $2) not set.",
						PlayerSettings.DefaultHomeName,
						PlayerSettings.BedHomeName);
				else if (!Helpers.Block.isBedUnobstructed(location))
				{
					sender.sendFailure("Bed home is obstructed or destroyed.");
					return true;
				}
			}
			else if (homeName.equals(PlayerSettings.PlotHomeName)) sender.sendFailure("You do not have an Orkida plot."); 
			else sender.sendFailure("You have no home named $1.", homeName);
		}

		if (location != null)
		{
			if (this.canSetHomeHere(player, location)) player.teleport(location, TeleportCause.COMMAND);
			else sender.sendFailure("You do not have permission to teleport there.");
		}

		return true;
	}

	private boolean doDelHome(final PagedOutputCommandSender sender, final String[] args)
	{
		final String homeName = args[0].toLowerCase();
		final Location location = super.getSettings().getHomeLocation(super.getPlayer(sender), homeName);
		if (homeName.equalsIgnoreCase(PlayerSettings.BedHomeName))
		{
			if (location == null) sender.sendFailure("You do not have a bed home.");
			else sender.sendFailure("You cannot delete your bed home.");
		}
		else if (homeName.equalsIgnoreCase(PlayerSettings.PlotHomeName))
		{
			if (location == null) sender.sendFailure("You do not have a plot home.");
			else sender.sendFailure("You cannot delete your plot home.");
		}
		else
		{
			if (location == null) sender.sendFailure("You do not have a home named $1.", homeName);
			else
			{
				this.getSettings().deleteHomeLocation(super.getPlayer(sender), homeName);
				sender.sendSuccess("Home $1 deleted.", homeName);
			}
		}
		return true;
	}

	private boolean doHomes(final PagedOutputCommandSender sender, final String[] args)
	{
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER))
		{
			if (sender.hasPermission("vpcore.moderator"))
			{
				if (sender.isConsole())
				{
					sender.sendFailure("Must specify $1 flag if running as CONSOLE.", "-p");
					return true;
				}
			}
			else return false;
		}
		
		final OfflinePlayer player = super.getOfflinePlayer(sender);
		if (player == null) return true;
		
		final List<String> homes = this.getHomesFor(player);
		if (homes.size() == 0)
		{
			if (super.hasFlag(ExecutorBase.FLAG_PLAYER)) sender.sendFailure("$1 has no homes.", player);
			else sender.sendFailure("You have no homes.");
		}
		else
		{
			String homeList = "";
						
			for (final String home: homes)
			{
				final Location location = super.getSettings().getHomeLocation(player, home);
				if (homeList.length() > 0) homeList += ", ";
				if (home.equalsIgnoreCase(PlayerSettings.BedHomeName))
				{
					if (!Helpers.Block.isBedUnobstructed(location)) homeList += ChatColor.STRIKETHROUGH;
					homeList += ChatColor.ITALIC;
				}
				if (home.equalsIgnoreCase(PlayerSettings.PlotHomeName)) homeList += ChatColor.ITALIC;

				if (player.isOnline() && 
						!this.canSetHomeHere(player.getPlayer(), location)) homeList += ChatColor.RED;

				homeList += home + ChatColor.RESET;
			}

			if (super.hasFlag(ExecutorBase.FLAG_PLAYER)) sender.sendSuccess("Homes for $1:", player);
			sender.sendInfo("Homes: " + homeList);
		}
		return true;
	}
	
	private List<String> getHomesFor(final OfflinePlayer player)
	{
		return new ArrayList<String>(super.getSettings().getHomeNames(player));
	}

	private boolean doPay(final PagedOutputCommandSender sender, final String[] args)
	{
		if (!super.getPlugin().isVaultEnabled())
		{
			sender.sendFailure("Vault support is not enabled.");
			return true;
		}
		
		final OfflinePlayer fromPlayer = super.getPlayer(sender);
		if (fromPlayer == null) return true;

		final OfflinePlayer toPlayer = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (toPlayer == null) return true;

		if (toPlayer.equals(fromPlayer))
		{
			sender.sendFailure("You cannot pay yourself!");
			return true;
		}

		double amount = 0;
		final String amountString = args[1];
		try { amount = Double.parseDouble(amountString); }
		catch (final NumberFormatException e)
		{
			sender.sendFailure("Amount was not numeric: $1", amountString);
			return true;
		}
		
		if (amount == 0) sender.sendFailure("How generous.");
		else if (amount < 0) sender.sendFailure("If $1 owes you money, ask them to $2 you.", toPlayer, "/pay");
		else if (super.getPlugin().getEconomyManager().transferFunds(fromPlayer, toPlayer, amount))
		{
			sender.sendSuccess("You paid $$1 to $2.", Helpers.Money.formatString(amount), toPlayer);
			
			final Player player = toPlayer.getPlayer();
			if (player != null) Helpers.Messages.sendNotification(
						player,
						"You have received $$1 from $2.",
						Helpers.Money.formatString(amount),
						fromPlayer);
		}
		else sender.sendFailure("Could not pay $$1 to $2.", Helpers.Money.formatString(amount), toPlayer);

		return true;
	}

	private boolean doBalTop(final PagedOutputCommandSender sender, final String[] args)
	{
		if (!super.getPlugin().isVaultEnabled())
		{
			sender.sendFailure("Vault support is not enabled.");
			return true;
		}

		final EconomyManager economyManager = super.getPlugin().getEconomyManager();
		boolean refreshed = false;

		if (this.balancesLastRefreshed < (System.currentTimeMillis() - (Helpers.DateTime.SecondsInAMinute * 5)) ||
				sender.hasPermission("vpcore.admin"))
		{
			sender.sendInfo("Sorting balances...");
			this.sortedBalances = economyManager.getSortedNames();

			this.balancesLastRefreshed = System.currentTimeMillis();
			refreshed = true;
		}

		if (!refreshed) sender.sendInfo("Balances are delayed by up to 5 mins.");
		for (int i = 0; i < this.sortedBalances.length; ++i)
		{
			final OfflinePlayer player = this.sortedBalances[i];
			sender.sendInfoListItem(i, "$1: $$2", player, economyManager.getBalanceString(player.getUniqueId()));
		}

		return true;
	}

	private boolean doBalance(final PagedOutputCommandSender sender, final String[] args)
	{
		if (!super.getPlugin().isVaultEnabled())
		{
			sender.sendFailure("Vault support is not enabled.");
			return true;
		}
		
		OfflinePlayer player = null;
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER))
		{
			if (!sender.hasPermission("vpcore.balance.others")) return false;
			player = super.getPlugin().getOfflinePlayer(sender, super.getFlagValue(ExecutorBase.FLAG_PLAYER));
		}
		else player = super.getOfflinePlayer(sender);
		if (player == null) return true;

		final String balance = this.getPlugin().getEconomyManager().getBalanceString(player);

		if (!sender.isConsole() &&
				player.equals(sender.getPlayer()))
			sender.sendSuccess("Balance: $$1", balance);
		else sender.sendSuccess("Balance for $1: $$2", player, balance);

		return true;
	}

	private boolean doEco(final PagedOutputCommandSender sender, final String[] args)
	{
		if (!super.getPlugin().isVaultEnabled())
		{
			sender.sendFailure("Vault support is not enabled.");
			return true;
		}

		final String subCommand = args[0].toLowerCase();
		double amount = 0;
		final boolean isSilent = super.getFlagState(ExecutorBase.FLAG_BOOL_SILENT);

		switch (subCommand)
		{
		case "give":
		case "take":
		case "set":
			if (args.length != 3) return false;
			break;
		case "reset":
			if (args.length != 2) return false;
			break;
		default: return false;
		}

		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[1]);
		if (player == null) return true;

		switch (subCommand)
		{
		case "give":
		case "take":
		case "set":
			final String amountString = args[2];
			try { amount = Double.parseDouble(amountString); }
			catch (final NumberFormatException e)
			{
				sender.sendFailure("Amount was not numeric: $1", amountString);
				return true;
			}
			break;
		case "reset":
			amount = super.getSettings().getDefaultBalance();
			break;
		default: return false;
		}

		final EconomyManager economyManager = super.getPlugin().getEconomyManager();

		boolean balanceUpdated = false;
		switch (subCommand)
		{
		case "give":
			if (economyManager.canReceive(player, amount))
			{
				economyManager.deposit(player, amount);
				if (!isSilent && player.isOnline())
					Helpers.Messages.sendNotification(
							player.getPlayer(),
							"You have been given $$1.",
							Helpers.Money.formatString(amount));
				sender.sendSuccess("You gave $$1 to $2.", Helpers.Money.formatString(amount), player);
				balanceUpdated = true;
			}
			else sender.sendFailure("Account for $1 cannot receive $$2", player, Helpers.Money.formatString(amount));
			break;
		case "take":
			if (economyManager.has(player, amount))
			{
				economyManager.withdraw(player, amount);
				if (!isSilent && player.isOnline())
					Helpers.Messages.sendNotification(
							player.getPlayer(),
							"You have had $$1 taken from you.",
							Helpers.Money.formatString(amount));
				sender.sendSuccess("You took $$1 from $2.", Helpers.Money.formatString(amount), player);
				balanceUpdated = true;
			}
			else sender.sendFailure("Account for $1 does not have $$2", player, Helpers.Money.formatString(amount));
			break;
		case "set":
		case "reset":
			final double maxBalance = super.getPlugin().getEconomyManager().getMaxBalance();
			if (amount > maxBalance) amount = maxBalance;
			economyManager.setBalance(player, amount);
			if (!isSilent && player.isOnline())
				Helpers.Messages.sendNotification(
						player.getPlayer(),
						"Your balance has been set to $$1.",
						Helpers.Money.formatString(amount));			
			break;
		default: return false;
		}

		if (balanceUpdated) sender.sendSuccess("New balance for $1: $$2", player, economyManager.getBalanceString(player));

		return true;
	}

	private boolean doAfk(final PagedOutputCommandSender sender, final String[] args)
	{
		if (super.hasFlag(ExecutorBase.FLAG_PLAYER) &&
				!sender.hasPermission("vpcore.afk.others")) return false;

		final Player player = super.getPlayer(sender);
		if (player == null) return true;

		super.getSettings().setIsAfk(player, !super.getSettings().isAfk(player), false, true);
		return true;
	}

	private boolean doSpawn(final PagedOutputCommandSender sender, final String[] args)
	{
		Location spawn = super.getSettings().getSpawn(sender.getPlayer());
		if (spawn == null)
		{
			super.warning("Unable to get spawn for $1.", sender.getPlayer());
			spawn = Bukkit.getWorlds().get(0).getSpawnLocation();
		}

		sender.getPlayer().teleport(spawn, TeleportCause.COMMAND);

		return true;
	}

	private boolean doListWarps(final PagedOutputCommandSender sender, final String[] args)
	{
		String list = "";
		Player player = super.getPlayer(sender);
		if (player == null) return true;
		
		List<String> warps = this.getWarpsFor(player);
		for (final String warp: warps)
		{
			if (list.length() > 0) list += ", ";
			list += warp;
		}

		if (warps.size() > 0)
		{
			sender.sendInfo("Available warps:");
			sender.sendInfo(list);
		}
		else sender.sendFailure("No warp is available.");

		return true;
	}
	
	private List<String> getWarps() { return this.getWarpsFor(null); }
	private List<String> getWarpsFor(final Player player)
	{
		final List<String> warps = new ArrayList<String>();
		for (final String warp: super.getSettings().getWarpNames())
		{
			if (player == null ||
					player.hasPermission("vpcore.moderator") ||
					player.hasPermission("vpcore.warps.*") ||
					player.hasPermission(Helpers.Parameters.replace("vpcore.warps.$1", warp.toLowerCase())))
				warps.add(warp);
		}
		return warps;
	}

	private boolean doWarp(final PagedOutputCommandSender sender, final String[] args)
	{
		Location warp = super.getSettings().getWarp(args[0]);
		if (warp != null)
		{
			if (!sender.hasPermission("vpcore.moderator") &&
					!sender.hasPermission("vpcore.warps.*") &&
					!sender.hasPermission(
							Helpers.Parameters.replace(
									"vpcore.warps.$1",
									args[0].toLowerCase()))) warp = null;

			if (warp == null) super.debug("No permission!");
		}

		if (warp == null) sender.sendFailure("Warp $1 does not exist.", args[0]);
		else sender.getPlayer().teleport(warp, TeleportCause.COMMAND);

		return true;
	}

	private boolean doSetSpawn(final PagedOutputCommandSender sender, final String[] args)
	{
		final Group group = super.getSettings().getGroup(args[0]);
		if (group == null)
		{
			sender.sendFailure("No such group $1.", args[0]);
			return true;
		}

		super.getSettings().createOrUpdateSpawn(group, sender.getLocation());
		sender.sendSuccess("Spawn for $1 set to $2.", group, sender.getLocation());

		return true;
	}

	private boolean doSetWarp(final PagedOutputCommandSender sender, final String[] args)
	{
		final Location warp = super.getSettings().getWarp(args[0]);
		super.getSettings().createOrUpdateWarp(args[0], sender.getLocation());
		if (warp == null) sender.sendSuccess("Warp $1 created.", args[0]);
		else sender.sendSuccess("Warp $1 updated.", args[0]);
		return true;
	}

	private boolean doDelWarp(final PagedOutputCommandSender sender, final String[] args)
	{
		if (super.getSettings().getWarp(args[0]) != null)
		{
			super.getSettings().deleteWarp(args[0]);
			sender.sendSuccess("Warp $1 deleted.", args[0]);
		}
		else sender.sendFailure("Warp $1 does not exist.", args[0]);
		return true;
	}

	private boolean doRegister(final PagedOutputCommandSender sender, final String[] args)
	{
		final Player player = super.getPlayer(sender);
		if (super.getSettings().isRegistered(player)) sender.sendSuccess("Your account is already registered.");
		else if (super.getSettings().playerHasAssociatedForumProfileData(player)) super.getSettings().changeToRegisteredGroup(player);
		else
		{
			sender.sendFailure("You must create a forum account first ($1).", "http://mc.rv.rs");
			sender.sendInfo("If you have a forum account, ensure your Minecraft name is entered in your forum profile.");
		}
		return true;
	}

}
