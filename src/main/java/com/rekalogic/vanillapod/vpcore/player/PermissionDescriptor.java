package com.rekalogic.vanillapod.vpcore.player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;

public final class PermissionDescriptor
{
	private final List<String> directPermissions = new ArrayList<String>();
	private final List<String> bundles = new ArrayList<String>();
	private final List<String> inheritedGroups = new ArrayList<String>();
	private final List<String> positivePermissions = new ArrayList<String>();
	private final List<String> negativePermissions = new ArrayList<String>();
	private final List<String> inheritedPermissions = new ArrayList<String>();
	private final List<String> finalPermissions = new ArrayList<String>();

	public List<String> getDirectPermissions() { return directPermissions; }

	public List<String> getBundles() { return bundles; }

	public List<String> getInheritedGroups() { return inheritedGroups; }

	public List<String> getPositivePermissions() { return positivePermissions; }

	public List<String> getNegativePermissions() { return negativePermissions; }

	public List<String> getInheritedPermissions() { return this.inheritedPermissions; }

	public List<String> getFinalPermissions() { return finalPermissions; }

	public void addDirectPermissions(final List<String> permissions)
	{
		this.directPermissions.addAll(permissions);
	}

	public void addInheritedGroups(final List<String> groupNames)
	{
		this.inheritedGroups.addAll(groupNames);
	}

	public void addInheritedPermission(final String permission)
	{
		this.inheritedPermissions.add(permission);
	}

	public void addBundles(final List<String> bundles)
	{
		this.bundles.addAll(bundles);
	}

	public void addPermission(final String permission)
	{
		if (permission.startsWith("-"))
		{
			final String negatedPermission = permission.substring(1);
			if (!this.negativePermissions.contains(negatedPermission))
				this.negativePermissions.add(negatedPermission);
		}
		else if (!this.positivePermissions.contains(permission))
			this.positivePermissions.add(permission);
	}

	public void calculate()
	{
		final PluginManager pluginManager = Bukkit.getPluginManager();
		
		this.finalPermissions.clear();
		for (final String permission: this.inheritedPermissions)
			this.applyPermission(pluginManager, permission, true);
		for (final String permission: this.positivePermissions)
			this.applyPermission(pluginManager, permission, true);
		for (final String permission: this.negativePermissions)
			this.applyPermission(pluginManager, permission, false);
	}
	
	private void applyPermission(final PluginManager pluginManager, final String permission, final boolean value)
	{
		if (value)
		{
			if (!this.finalPermissions.contains(permission))
				this.finalPermissions.add(permission);
		}
		else
		{
			if (this.finalPermissions.contains(permission))
				this.finalPermissions.remove(permission);
		}
		
		final Permission permissionObject = pluginManager.getPermission(permission);
		if (permissionObject != null)
		{
			final Map<String, Boolean> childPermissions = permissionObject.getChildren();
			for (final String childPermission: childPermissions.keySet())
			{
				boolean childValue = value;
				if (!childPermissions.get(childPermission)) childValue = !childValue;
				this.applyPermission(pluginManager, childPermission, childValue);
			}
		}
	}
}
