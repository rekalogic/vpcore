package com.rekalogic.vanillapod.vpcore.player;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public final class TeleportRequest
{
	private final long initiated;
	private final Player requestor;
	private final Player acceptor;
	private final Player teleportee;
	private final Location destination;

	private boolean hasBeenUsed = false;

	public long getInitiated() { return this.initiated; }
	public Player getRequestor() { return this.requestor; }
	public Player getAcceptor() { return this.acceptor; }
	public Player getTeleportee() { return this.teleportee; }
	public Location getDestination() { return this.destination; }

	private TeleportRequest(final Player requestor, final Player acceptor, final Player teleportee, final Location destination)
	{
		this.initiated = System.currentTimeMillis();
		this.requestor = requestor;
		this.acceptor = acceptor;
		this.teleportee = teleportee;
		this.destination = destination;
	}

	public boolean hasExpired(final long window)
	{
		return (this.hasBeenUsed || this.initiated < (System.currentTimeMillis() - window));
	}
	
	public long getTimeUntilExpiry(final long window)
	{
		long timeUntilExpiry = 0;
		if (!this.hasBeenUsed)
		{			
			final long now = System.currentTimeMillis();
			final long expiry = this.initiated + window;
			if (expiry > now) timeUntilExpiry = expiry - now;
		}
		return timeUntilExpiry;
	}

	public void accept()
	{
		this.teleportee.teleport(this.destination, TeleportCause.COMMAND);
		this.hasBeenUsed = true;
	}

	public void decline()
	{
		this.hasBeenUsed = true;
	}

	public static TeleportRequest getTpaRequest(final Player requestor, final Player acceptor)
	{
		return new TeleportRequest(requestor, acceptor, requestor, acceptor.getLocation());
	}

	public static TeleportRequest getTpaHereRequest(final Player requestor, final Player acceptor)
	{
		return new TeleportRequest(requestor, acceptor, acceptor, requestor.getLocation());
	}
}
