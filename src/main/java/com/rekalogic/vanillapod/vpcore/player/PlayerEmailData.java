package com.rekalogic.vanillapod.vpcore.player;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class PlayerEmailData
{
	private final UUID uuid;
	private final String name;
	private final String email;
	private final String subject;
	private final String body;
	
	public UUID getUuid() { return this.uuid; }
	public String getName() { return this.name; }
	public String getEmail() { return this.email; }
	public String getSubject() { return Helpers.Parameters.replace(this.subject, this.name); }
	public String getBody() { return Helpers.Parameters.replace(this.body, this.name); }
	
	public PlayerEmailData(final OfflinePlayer player, final String email, final String subject, final String body)
	{
		this(player.getUniqueId(), player.getName(), email, subject, body);
	}
	
	public PlayerEmailData(final UUID uuid, final String name, final String email, final String subject, final String body)
	{
		this.uuid = uuid;
		this.name = name;
		this.email = email;
		this.subject = subject;
		this.body = body;
	}
}
