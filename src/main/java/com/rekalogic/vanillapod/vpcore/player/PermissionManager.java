package com.rekalogic.vanillapod.vpcore.player;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import com.rekalogic.vanillapod.vpcore.util.LoggingBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class PermissionManager extends LoggingBase
{
	private final Map<String, String> worldMappings = new ConcurrentHashMap<String, String>();
	private final Map<String, List<String>> bundles = new ConcurrentHashMap<String, List<String>>();
	private final Map<String, Group> groupsByName = new ConcurrentHashMap<String, Group>();

	private final PluginBase plugin;
	private final Map<UUID, PermissionAttachment> playerPermissions;
	private final Map<UUID, Group> playerGroups;

	private Group defaultGroup = null;
	private Group registeredGroup = null;

	public Map<String, List<String>> getBundles() { return this.bundles; }
	public Collection<Group> getGroups() { return this.groupsByName.values(); }
	public Group getDefaultGroup() { return this.defaultGroup; }
	public Group getRegisteredGroup() { return this.registeredGroup; }

	public PermissionManager(final PluginBase plugin, final boolean debug, final PlayerSettings settings)
	{
		super(debug);
		this.plugin = plugin;
		this.playerPermissions = new ConcurrentHashMap<UUID, PermissionAttachment>();
		this.playerGroups = new ConcurrentHashMap<UUID, Group>();
	}

	public void addWorldMapping(final String fromWorld, final String toWorld)
	{
		this.worldMappings.put(fromWorld.toLowerCase(), toWorld.toLowerCase());
	}

	public String getWorldMapping(final World world) { return this.getWorldMapping(world.getName()); }
	public String getWorldMapping(final String worldName)
	{
		final String lowerWorldName = worldName.toLowerCase();
		if (this.worldMappings.containsKey(lowerWorldName)) return this.worldMappings.get(lowerWorldName);
		else return lowerWorldName;
	}

	public void addGlobalGroup(final String name, final List<String> permissions)
	{
		this.bundles.put(name, permissions);
	}

	public void addGroup(final Group group)
	{
		this.groupsByName.put(group.getName().toLowerCase(), group);
	}

	public void finishLoad()
	{
		this.defaultGroup = null;
		this.registeredGroup = null;

		for (final String groupName: this.groupsByName.keySet())
		{
			final Group group = this.groupsByName.get(groupName);

			if (group.isDefault())
			{
				if (this.defaultGroup == null) this.defaultGroup = group;
				else super.severe("Multiple default groups were found.");
			}
			
			if (group.isRegistered())
			{
				if (this.registeredGroup == null) this.registeredGroup = group;
				else super.severe("Multiple registered groups were found.");
			}
		}

		if (this.defaultGroup == null) super.severe("No default group was found.");
	}

	private void resolveGroup(final Group group)
	{
		if (group.isResolved()) return;
		
		for (final String worldName: group.getWorldNames())
		{
			final PermissionDescriptor descriptor = group.getPermissions(worldName);

			for (final String groupName: descriptor.getInheritedGroups())
			{
				final Group inheritedGroup = this.getGroup(groupName);
				if (inheritedGroup != null)
				{
					this.resolveGroup(inheritedGroup);
					for (final String permission: inheritedGroup.getPermissions(worldName).getFinalPermissions())
						descriptor.addInheritedPermission(permission);
				}
				else super.severe("Group $1 specifies unknown inheritance: $2.", group, groupName);
			}

			for (final String bundle: descriptor.getBundles())
			{
				if (this.bundles.containsKey(bundle))
				{
					for (final String permission: this.bundles.get(bundle))
						descriptor.addPermission(permission);
				}
				else super.warning("No definition for bundle $1.", bundle);

				for (final String permission: descriptor.getDirectPermissions())
					descriptor.addPermission(permission);
			}
		}

		group.calculate();
	}
	
	public Group applyGroup(final Player player, final boolean silent)
	{
		final UUID uuid = player.getUniqueId();
		Group group = null;
		if (this.playerGroups.containsKey(uuid)) group = this.playerGroups.get(uuid);
		else group = this.defaultGroup;
		
		final String fancyName = Helpers.Parameters.replace("$1$2$3", group.getColor(), player.getName(), ChatColor.RESET);
		player.setDisplayName(fancyName);
		player.setPlayerListName(fancyName);

		super.info("Moving $1 to group $2.", player, group);

		PermissionAttachment attachment = null;
		if (this.playerPermissions.containsKey(uuid))
		{
			attachment = this.playerPermissions.get(uuid);
			player.removeAttachment(attachment);
		}

		attachment = player.addAttachment(this.plugin);
		this.playerPermissions.put(uuid, attachment);
		this.playerGroups.put(uuid, group);

		String worldName = player.getWorld().getName().toLowerCase();
		if (this.worldMappings.containsKey(worldName))
		{
			final String oldWorldName = worldName;
			worldName = this.worldMappings.get(worldName);
			super.debug("Permissions in $1 are mapped to $2.", oldWorldName, worldName);
		}

		this.applyPermissions(attachment, group, worldName);
		player.recalculatePermissions();

		if (group.isBuildRestricted())
		{
			attachment.setPermission("vpcore.build-restricted", true);
			super.info("$1 was build-restricted.", player);
		}

		if (!silent)
			Helpers.Messages.sendNotification(
					player,
					"You were moved to group '$1'.",
					group);
		
		return group;
	}

	private void applyPermissions(final PermissionAttachment attachment, final Group group, final String worldName)
	{
		if (group.hasPermissions(worldName))
		{
			final PermissionDescriptor permissions = group.getPermissions(worldName);
			for (final String permission: permissions.getFinalPermissions())
				attachment.setPermission(permission, true);
		}
		else super.severe("No permissions defined for $1 in $2.", group, worldName);
	}
		
	public void removeData(final Player player)
	{
		final UUID uuid = player.getUniqueId();
		final PermissionAttachment attachment = this.playerPermissions.remove(uuid);
		if (attachment != null)
		{
			player.removeAttachment(attachment);
			super.debug("Removed group/permission data for $1.", player);
		}
		else super.debug("No group/permission data for $1.", player);
	}

	public Group getCurrentGroup(final OfflinePlayer player) { return this.getCurrentGroup(player.getUniqueId()); }
	public Group getCurrentGroup(final UUID uuid)
	{
		Group group = null;
		if (this.playerGroups.containsKey(uuid))
			group = this.playerGroups.get(uuid);
		return group;
	}

	public Group getGroup(final String name)
	{
		Group group = null;
		final String lowerName = name.toLowerCase();
		if (this.groupsByName.containsKey(lowerName))
			group = this.groupsByName.get(lowerName);
		return group;
	}

	public void setGroup(final OfflinePlayer player, final Group group) { this.setGroup(player.getUniqueId(), group.getName()); }
	public void setGroup(final UUID uuid, final String groupName)
	{
		this.playerGroups.put(uuid, this.getGroup(groupName));
	}
	
	public void finalisePermissions()
	{
		for (final String groupName: this.groupsByName.keySet())
		{
			final Group group = this.groupsByName.get(groupName);
			this.resolveGroup(group);
		}
	}
}
