package com.rekalogic.vanillapod.vpcore.player;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Tree;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class PlayerEventHandler extends EventHandlerBase<PlayerSettings>
{
	public PlayerEventHandler(final PluginBase plugin, final PlayerSettings settings)
	{
		super(plugin, settings);
	}
	
	private boolean hasPermission(final Player player, final Block block)
	{
		boolean hasPermission = true;
		if (player.hasPermission("vpcore.build-restricted") && !player.isOp())
		{
			final Material material = block.getType();
			hasPermission = player.hasPermission(
					Helpers.Parameters.replace(
							"vpcore.build.$1",
							material));

			if (!hasPermission)
			{
				switch (material)
				{
				case LEAVES:
				case LEAVES_2:
				case LOG:
				case LOG_2:
					final Tree tree = (Tree)block.getState().getData();
					hasPermission = player.hasPermission(
							Helpers.Parameters.replace(
									"vpcore.build.$1.$2",
									material,
									tree.getSpecies()));

					break;

				default: break;
				}
			}

			if (!hasPermission) Helpers.Messages.sendNotification(player, super.getSettings().getAntiBuildMessage());
		}

		return hasPermission;
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	private void onPlayerJoinLowest(final PlayerJoinEvent event)
	{
		final Player player = event.getPlayer();
		final Group group = super.getSettings().setGroup(player);
		
		if (group.isDefault() && player.hasPlayedBefore())
		{
			final TaskBase task = new TaskBase(super.isDebug(), "PromoteOnJoin")
			{
				@Override
				protected void runTask()
				{
					if (getSettings().playerHasAssociatedForumProfileData(player))
						getSettings().changeToRegisteredGroup(player);
				}
			};
			task.runTaskLater(super.getPlugin(), Helpers.DateTime.TicksInASecond * 2);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void onPlayerJoinHighest(final PlayerJoinEvent event)
	{
		final Player player = event.getPlayer();

		if (player.hasPlayedBefore())
		{
			Helpers.Messages.sendNotification(player, super.getSettings().getJoinMessage(), player);
			
			super.getSettings().displayPlayerList(player, player);
		}
		else
		{
			super.info("$1 is a new player.", player);
			Helpers.Broadcasts.sendPerms("vpcore.registered", super.getSettings().getNewPlayerMessage(), player.getName());
			player.teleport(super.getSettings().getSpawn(player), TeleportCause.PLUGIN);
			super.getSettings().giveStartKit(player);
		}

		if (super.getSettings().recordLogin(player))
		{
			Helpers.Messages.sendNotification(
					player,
					"The last time you were online you used the $1 command to exempt yourself from plot deletion, " +
					"but now you've logged in again it has been cancelled and everything is back to normal.",
					"/holiday");
			super.info("$1 is no longer exempt from plot deletion.", player);
		}
				
		super.getSettings().registerActivity(player, true);
		super.getSettings().setTemporaryInvulnerability(player);

		if (super.getSettings().isBusy(player))
			Helpers.Messages.sendNotification(player, "Your status is set to BUSY.");
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerChangedWorld(final PlayerChangedWorldEvent event)
	{
		final Player player = event.getPlayer();
		super.getSettings().registerActivity(player);
		super.getSettings().setGroup(player, true);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerQuit(final PlayerQuitEvent event)
	{
		final Player player = event.getPlayer();
		super.getSettings().removeData(player);
		super.getSettings().recordLogout(player);

		if (!super.getSettings().isGod(player))
			super.getSettings().setInvulnerability(player, false);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onBlockPlace(final BlockPlaceEvent event)
	{
		if (event.isCancelled()) return;
		if (!event.canBuild()) return;

		final Player player = event.getPlayer();
		if (!this.hasPermission(player, event.getBlock())) event.setCancelled(true);

		super.getSettings().registerActivity(player);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onBlockBreak(final BlockBreakEvent event)
	{
		if (event.isCancelled()) return;

		final Player player = event.getPlayer();
		if (!this.hasPermission(player, event.getBlock())) event.setCancelled(true);

		super.getSettings().registerActivity(player);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void onPlayerRespawn(final PlayerRespawnEvent event)
	{
		event.setRespawnLocation(super.getSettings().getSpawn(event.getPlayer()));
	}

	@EventHandler(priority = EventPriority.LOWEST)
	private void onAsyncPlayerChatLowest(final AsyncPlayerChatEvent event)
	{
		if (event.isCancelled()) return;

		final Player player = event.getPlayer();
		super.getSettings().registerActivity(player);

		if (super.getSettings().isMuted(player))
		{
			event.setCancelled(true);
			long duration = super.getSettings().getMuteDuration(player);
			Helpers.Messages.sendNotification(player, "You are muted ($1).", Helpers.DateTime.getHumanReadableFromMillis(duration, true));
			super.info("$1 is MUTED: $2", player, event.getMessage());
			return;
		}
		
		if (super.getSettings().isIgnored(player))
		{
			for (final UUID uuid: super.getSettings().getIgnorers(player))
			{
				final Player ignoringPlayer = Bukkit.getPlayer(uuid);
				if (player != null &&
						!player.hasPermission("vpcore.moderator"))
					event.getRecipients().remove(ignoringPlayer);
			}
		}		
	}
	
	@EventHandler(priority = EventPriority.LOW)
	private void onAsyncPlayerChatLow(final AsyncPlayerChatEvent event)
	{
		if (event.isCancelled()) return;
		
		final Player player = event.getPlayer();
		event.setFormat(
				Helpers.Parameters.replace(
						"<$1> %2$s",
						super.getSettings().getGroupPrefixedName(player)));
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onAsyncPlayerChatHighest(final AsyncPlayerChatEvent event)
	{
		if (event.isCancelled()) return;

		final Player player = event.getPlayer();
		String message = event.getMessage();		
		if (player.hasPermission("vpcore.chat.colour"))
			message = Helpers.formatMessage(message);

		if (ChatColor.stripColor(message).trim().isEmpty()) event.setCancelled(true);
		else event.setMessage(message);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onPlayerTeleport(final PlayerTeleportEvent event)
	{
		if (event.isCancelled())
		{
			super.getSettings().cancelPendingTeleport(event.getPlayer());
			return;
		}

		final Player player = event.getPlayer();
		boolean canDamage = false;

		switch (event.getCause())
		{
		case COMMAND:
			if (super.getSettings().isTeleportPending(player))
			{
				Helpers.Messages.sendFailure(player, "Cancelling existing teleport.");
				super.getSettings().cancelPendingTeleport(player);
			}

			Block block = event.getTo().getBlock();
			if (!player.hasPermission("vpcore.moderator"))
			{
				if (!super.getSettings().validateTeleportDestination(player, block))
				{
					event.setCancelled(true);
					return;
				}
			}

			if (!player.isOp())
			{
				if (player.hasPermission("vpcore.teleport.cooldown"))
				{
					final long nextTeleport = super.getSettings().getNextTeleportTimestamp(player);
					final long timeRemaining = nextTeleport - System.currentTimeMillis();
					if (timeRemaining > 0)
					{
						Helpers.Messages.sendFailure(
								player,
								"Cannot teleport again for another $1.",
								Helpers.DateTime.getHumanReadableFromMillis(
										timeRemaining,
										(timeRemaining > Helpers.DateTime.MillisecondsInASecond)));
						event.setCancelled(true);
						return;
					}
				}

				if (player.hasPermission("vpcore.teleport.delay"))
				{
					boolean solidBlock = false;
					for (int i = 0; i < 5; ++i)
					{
						block = block.getRelative(BlockFace.DOWN);
						if (!block.isEmpty())
						{
							solidBlock = true;
							break;
						}
					}
					if (!solidBlock) Helpers.Messages.sendNotification(player, "WARNING: risk of fall damage at destination!");

					super.getSettings().doDelayedTeleport(player, event.getTo());
					super.debug("Teleport ($1) for $2 will be delayed.", event.getCause(), player);
					Helpers.Messages.sendNotification(player, "Do not move; teleporting shortly.");
					event.setCancelled(true);
				}
			}
			break;
			
		case ENDER_PEARL:
			canDamage = true;
			break;
		default: break;
		}

		if (!event.isCancelled() && !canDamage)
			super.getSettings().setTemporaryInvulnerability(player);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerMove(final PlayerMoveEvent event)
	{
		if (event.isCancelled()) return;

		final int distance = Math.abs(event.getFrom().getBlockX() - event.getTo().getBlockX()) +
				Math.abs(event.getFrom().getBlockY() - event.getTo().getBlockY()) +
				Math.abs(event.getFrom().getBlockZ() - event.getTo().getBlockZ());
		if (distance == 0) return;

		final Player player = event.getPlayer();
		if (super.getSettings().isTeleportPending(player))
		{
			if (!player.isOp() &&
					player.hasPermission("vpcore.teleport.movecancels"))
			{
				super.getSettings().cancelPendingTeleport(player);
				Helpers.Messages.sendFailure(player, "You moved! Teleport cancelled!");
			}
		}
		super.getSettings().registerActivity(player);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onInventoryOpen(final InventoryOpenEvent event)
	{
		if (event.isCancelled()) return;

		super.getSettings().registerActivity((Player)event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onInventoryClick(final InventoryClickEvent event)
	{
		if (event.isCancelled()) return;

		super.getSettings().registerActivity((Player)event.getWhoClicked());
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onPlayerPickupItem(final PlayerPickupItemEvent event)
	{
		if (event.isCancelled()) return;

		if (super.getSettings().isAfk(event.getPlayer()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onPlayerInteract(final PlayerInteractEvent event)
	{
		if (event.isCancelled()) return;
				
		final Player player = event.getPlayer();

		if (event.getAction() == Action.RIGHT_CLICK_BLOCK)
		{
			final Block clickedBlock = event.getClickedBlock();

			if (clickedBlock != null && clickedBlock.getType() == Material.BED_BLOCK)
			{
				final ItemStack itemInHand = player.getItemInHand();
				if (itemInHand == null || itemInHand.getType() == Material.AIR || !player.isSneaking())
				{
					if (player.hasPermission("vpcore.command.sethome"))
					{
						final Location currentBedHome = super.getSettings().getHomeLocation(player, PlayerSettings.BedHomeName);
						final Location newBedHome = event.getClickedBlock().getLocation().getBlock().getRelative(BlockFace.UP).getLocation();
						if (currentBedHome == null ||
								!currentBedHome.equals(newBedHome))
						{
							if (Helpers.Block.isBedUnobstructed(newBedHome))
							{
								super.getSettings().setHomeLocation(player, PlayerSettings.BedHomeName, newBedHome);
								Helpers.Messages.sendSuccess(player, "Your bed home has been set.");
								super.debug("Bed home for $1 set to $2.", player, newBedHome);
							}
							else Helpers.Messages.sendFailure(player, "Cannot set bed home: not enough space above bed.");

							event.setCancelled(true);
						}
					}
				}
			}
		}
		
		super.getSettings().registerActivity(player);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onEntityRegainHealth(final EntityRegainHealthEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getEntityType() == EntityType.PLAYER)
		{
			if (super.getSettings().isAfk((Player)event.getEntity())) event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onPlayerDeath(final PlayerDeathEvent event)
	{
		final Player player = event.getEntity();

		super.getSettings().recordDeathInformation(player);
		super.getSettings().registerActivity(player, true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void onEntityDamage(final EntityDamageEvent event)
	{
		if (event.isCancelled()) return;
		
		if (event.getEntity().getType() == EntityType.PLAYER)
		{
			final Player player = (Player)event.getEntity();
			if (super.getSettings().isInvulnerable(player))
			{
				event.setCancelled(true);

				switch (event.getCause())
				{
				case FIRE:
				case FIRE_TICK:
				case LAVA:
					player.setFireTicks(0);
					break;

				default: break;
				}
			}
		}		
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onFoodLevelChange(final FoodLevelChangeEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getEntity().getType() == EntityType.PLAYER)
		{
			final Player player = (Player)event.getEntity();
			if (super.getSettings().isGod(player)) event.setCancelled(true);
		}
	}
}
