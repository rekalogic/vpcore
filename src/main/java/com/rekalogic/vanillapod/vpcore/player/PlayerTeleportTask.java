package com.rekalogic.vanillapod.vpcore.player;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class PlayerTeleportTask extends TaskBase
{
	private final PlayerSettings settings;
	private final Location location;
	private final Player player;

	private boolean hasExecuted = false;

	public PlayerTeleportTask(final PlayerSettings settings, final Player player, final Location location)
	{
		super(settings);

		this.settings = settings;
		this.player = player;
		this.location = location;
	}

	@Override
	protected void runTask()
	{
		if (this.settings.validateTeleportDestination(player, this.location))
		{
			Helpers.Messages.sendNotification(this.player, "Teleporting now!");
			this.player.teleport(this.location, TeleportCause.PLUGIN);
		}

		this.hasExecuted = true;
	}

	@Override
	protected void cancelTask()
	{
		this.hasExecuted = true;
	}

	public boolean hasExecuted() { return this.hasExecuted; }
}
