package com.rekalogic.vanillapod.vpcore.player;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.plot.PlotData;
import com.rekalogic.vanillapod.vpcore.plot.PlotSettings;
import com.rekalogic.vanillapod.vpcore.sign.SignData;
import com.rekalogic.vanillapod.vpcore.sign.SignSettings;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public final class PlayerSettings extends SettingsBase
{
	private final PlayerDbInterface dbInterface;
	
	private PlotSettings plotSettings = null;
	private SignSettings signSettings = null;

	final private PermissionManager permissionManager;
	private EconomyManager economyManager;
	private String antiBuildMessage = "";
	private String newPlayerMessage = "";
	private String joinMessage = "";
	private long teleportDelay = 0;
	private long teleportCooldown = 0;
	private long teleportRequestWindow = 0;
	private long afkInactivity = 0;
	private long afkKick = 0;
	private String afkKickMessage = "";
	private double defaultBalance = 0;
	private long defaultMuteDuration = 0;
	private long teleportInvulnerabilityDuration = 0;
	private String startKit = "";
	private boolean isAbsenceJobEnabled = false;
	private long absenceJobPeriod = 0;
	private long absenceWarningDuration = 0;
	private long absenceDeletionDuration = 0;
	private String warningEmailSubject = "";
	private String warningEmailBody = "";
	private String deletionEmailSubject = "";
	private String deletionEmailBody = "";
	private boolean isPromotionJobEnabled = false;
	private long promotionJobPeriod = 0;
	private long minPlaytimeForMember = 0;
	private long minPlaytimeForRegular = 0;
	private long maxRegularInactivity = 0;
	private boolean isHomeRestrictionEnabled = false;
	private List<String> homeIgnoreRegions = null;
	private long afkCheckPeriod = 0;
	private long kitCoolDown = 0;
	private long aliasMaxAge = 0;

	private Map<String, Location> spawnLocations = null;
	private Map<String, Location> warpLocations = null;
	private Map<UUID, ConcurrentHashMap<String, Location>> homes = null;
	private final Map<String, List<String>> kits = new ConcurrentHashMap<String, List<String>>();
	private final Map<UUID, Map<String, Long>> lastUsedKitTimes = new ConcurrentHashMap<UUID, Map<String,Long>>();
	private final HashSet<String> lowerKitNames = new HashSet<String>();

	private final Map<UUID, Long> nextTeleportTimestamps = new ConcurrentHashMap<UUID, Long>();
	private final Map<UUID, PlayerTeleportTask> teleportTasks = new ConcurrentHashMap<UUID, PlayerTeleportTask>();
	private final Map<UUID, Long> lastActivity = new ConcurrentHashMap<UUID, Long>();
	private final Map<UUID, Boolean> afkPlayers = new ConcurrentHashMap<UUID, Boolean>();
	private final Map<UUID, TeleportRequest> teleportRequestsByAcceptor = new ConcurrentHashMap<UUID, TeleportRequest>();
	private final Map<UUID, TeleportRequest> teleportRequestsByRequestor = new ConcurrentHashMap<UUID, TeleportRequest>();
	private final Map<UUID, Boolean> busyFlags = new ConcurrentHashMap<UUID, Boolean>();
	private final Map<UUID, UUID> lastTells = new ConcurrentHashMap<UUID, UUID>();
	private final Map<UUID, Long> mutedPlayers = new ConcurrentHashMap<UUID, Long>();
	private final Map<UUID, Boolean> invulnerableFlags = new ConcurrentHashMap<UUID, Boolean>();
	private final Map<UUID, TaskBase> invulnerabilityTasks = new ConcurrentHashMap<UUID, TaskBase>();
	private final Map<UUID, Boolean> godFlags = new ConcurrentHashMap<UUID, Boolean>();
	private final Map<UUID, List<UUID>> ignoreLists = new ConcurrentHashMap<UUID, List<UUID>>();

	public String getAntiBuildMessage() { return this.antiBuildMessage; }
	public String getNewPlayerMessage() { return this.newPlayerMessage; }
	public String getJoinMessage() { return this.joinMessage; }
	public long getTeleportRequestWindow() { return this.teleportRequestWindow; }
	public long getAfkInactivity() { return this.afkInactivity; }
	public long getAfkKick() { return this.afkKick; }
	public String getAfkKickMessage() { return this.afkKickMessage; }
	public boolean isHomeRestrictionEnabled() { return this.isHomeRestrictionEnabled; }
	public long getAfkCheckPeriod() { return this.afkCheckPeriod; }
	public double getDefaultBalance() { return this.defaultBalance; }
	public boolean isAbsenceJobEnabled() { return this.isAbsenceJobEnabled; }
	public long getAbsenceJobPeriod() { return this.absenceJobPeriod; }
	public String getWarningEmailSubject() { return this.warningEmailSubject; }
	public String getWarningEmailBody() { return this.warningEmailBody; }
	public String getDeletionEmailSubject() { return this.deletionEmailSubject; }
	public String getDeletionEmailBody() { return this.deletionEmailBody; }
	public boolean isPromotionJobEnabled() { return this.isPromotionJobEnabled; }
	public long getPromotionJobPeriod() { return this.promotionJobPeriod; }
	public long getKitCoolDown() { return this.kitCoolDown; }
	
	public final static String BedHomeName = "bed";
	public final static String DefaultHomeName = "home";
	public final static String PlotHomeName = "plot";
	
	public void setPlotSettings(final PlotSettings settings) { this.plotSettings = settings; }
	public void setSignSettings(final SignSettings settings) { this.signSettings = settings; }

	public PlayerSettings(final VPPlugin plugin)
	{
		super(plugin);

		this.dbInterface = new PlayerDbInterface(plugin);
		
		this.permissionManager = new PermissionManager(super.getPlugin(), super.isDebug(), this);
		super.getPlugin().setPermissionManager(this.permissionManager);
	}

	@Override
	protected void onLoad()
	{
		this.antiBuildMessage = super.getString("messages.anti-build");
		this.newPlayerMessage = super.getString("messages.new-player");
		this.joinMessage = super.getString("messages.join");
		this.startKit = super.getString("start-kit");
		this.teleportDelay = Helpers.DateTime.getMillis(super.getString("teleport.delay"));
		this.teleportCooldown = Helpers.DateTime.getMillis(super.getString("teleport.cooldown"));
		this.teleportRequestWindow = Helpers.DateTime.getMillis(super.getString("teleport.request-window"));
		this.teleportInvulnerabilityDuration = Helpers.DateTime.getMillis(super.getString("teleport.invulnerability"));
		this.afkInactivity = Helpers.DateTime.getMillis(super.getString("afk.inactivity"));
		this.afkKick = Helpers.DateTime.getMillis(super.getString("afk.kick"));
		this.afkKickMessage = super.getString("afk.kick-message");
		this.defaultBalance = super.getDouble("default-balance");
		this.defaultMuteDuration = Helpers.DateTime.getMillis(super.getString("mute.default-duration"));
		this.isAbsenceJobEnabled = super.getBoolean("absence.enabled");
		this.absenceJobPeriod = Helpers.DateTime.getMillis(super.getString("absence.period"));
		this.absenceWarningDuration = Helpers.DateTime.getMillis(super.getString("absence.warning"));
		this.absenceDeletionDuration = Helpers.DateTime.getMillis(super.getString("absence.deletion"));
		this.warningEmailSubject = super.getString("absence.warning-email-subject");
		this.deletionEmailSubject = super.getString("absence.deletion-email-subject");
		this.isPromotionJobEnabled = super.getBoolean("promotion.enabled");
		this.promotionJobPeriod = Helpers.DateTime.getMillis(super.getString("promotion.period"));
		this.minPlaytimeForMember = Helpers.DateTime.getMillis(super.getString("promotion.min-playtime-for-member"));
		this.minPlaytimeForRegular = Helpers.DateTime.getMillis(super.getString("promotion.min-playtime-for-regular"));
		this.maxRegularInactivity = Helpers.DateTime.getMillis(super.getString("promotion.max-regular-inactivity"));
		this.isHomeRestrictionEnabled = super.getBoolean("homes.restriction.enabled");
		this.homeIgnoreRegions = super.getStringList("homes.restriction.ignored-regions");
		this.afkCheckPeriod = Helpers.DateTime.getMillis(super.getString("afk.check-period"));
		this.kitCoolDown = Helpers.DateTime.getMillis(super.getString("kit-cooldown"));
		this.aliasMaxAge = Helpers.DateTime.getMillis(super.getString("alias-max-age"));
		
		String warningEmailPath = this.getString("absence.warning-email-path");
		String deletionEmailPath = this.getString("absence.deletion-email-path");
		String basePath = super.getPlugin().getDataFolder().getAbsolutePath();
		
		this.dbInterface.setDebug(super.isDebug());
		this.permissionManager.setDebug(super.isDebug());
		
		File warningEmailFile = new File(basePath + File.separatorChar + warningEmailPath);
		if (warningEmailFile.exists())
		{
			FileInputStream stream = null;
			try
			{
				stream = new FileInputStream(warningEmailFile);
				byte[] data = new byte[(int)warningEmailFile.length()];
				stream.read(data);
				this.warningEmailBody = new String(data, "UTF-8");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				try { stream.close(); }
				catch (Exception e) { e.printStackTrace(); }
			}
		}
		else super.severe("Could not find file $1/$2.", basePath, warningEmailFile);		

		File deletionEmailFile = new File(basePath + File.separatorChar + deletionEmailPath);
		if (deletionEmailFile.exists())
		{
			FileInputStream stream = null;
			try
			{
				stream = new FileInputStream(deletionEmailFile);
				byte[] data = new byte[(int)deletionEmailFile.length()];
				stream.read(data);
				this.deletionEmailBody = new String(data, "UTF-8");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				try { stream.close(); }
				catch (Exception e) { e.printStackTrace(); }
			}
		}
		else super.severe("Could not find file $1/$2.", basePath, deletionEmailFile);		

		final Map<String, String> groupMappings = new HashMap<String, String>();
		for (final String key : super.getSubKeys("group-mappings", false))
		{
			final String mapping = super.getString(Helpers.Parameters.replace("group-mappings.$1", key));

			if (groupMappings.containsKey(key))
				this.severe("Duplicate group-mappings entry: $1", key);
			else
			{
				groupMappings.put(key.toLowerCase(), mapping);
				this.permissionManager.addWorldMapping(key, mapping);
				super.debug("World $1 will use groups from world $2.", key, mapping);

				boolean worldExists = false;
				for (final World world : Bukkit.getWorlds())
				{
					if (world.getName().equalsIgnoreCase(mapping)) worldExists = true;
				}
				if (!worldExists) this.severe("Mapping to nonexistent world: $1", mapping);
			}
		}
		
		this.kits.clear();
		this.lowerKitNames.clear();
		if (super.isSet("kits"))
		{
			for (final String kitName: super.getSubKeys("kits", false))
			{
				if (!this.lowerKitNames.contains(kitName.toLowerCase()))
				{
					this.lowerKitNames.add(kitName.toLowerCase());
					
					final String taskPath = Helpers.Parameters.replace("kits.$1", kitName);
					final List<String> items = super.getStringList(taskPath);
					this.kits.put(kitName, items);
				}
				else super.severe("A kit definition already exists for '$1'.", kitName);
			}
		}
	}

	@Override
	protected void onSave()
	{
		super.set("messages.anti-build", this.antiBuildMessage);
		super.set("messages.new-player", this.newPlayerMessage);
		super.set("messages.join", this.joinMessage);
		super.set("start-kit", this.startKit);

		super.set("teleport.delay", Helpers.DateTime.getHumanReadableFromMillis(this.teleportDelay));
		super.set("teleport.cooldown", Helpers.DateTime.getHumanReadableFromMillis(this.teleportCooldown));
		super.set("teleport.request-window", Helpers.DateTime.getHumanReadableFromMillis(this.teleportRequestWindow));
		super.set("teleport.invulnerability", Helpers.DateTime.getHumanReadableFromMillis(this.teleportInvulnerabilityDuration));
		super.set("afk.inactivity", Helpers.DateTime.getHumanReadableFromMillis(this.afkInactivity));
		super.set("afk.kick", Helpers.DateTime.getHumanReadableFromMillis(this.afkKick));
		super.set("afk.kick-message", this.afkKickMessage);
		super.set("mute.default-duration", Helpers.DateTime.getHumanReadableFromMillis(this.defaultMuteDuration));
		super.set("absence.enabled", this.isAbsenceJobEnabled);
		super.set("absence.period", Helpers.DateTime.getHumanReadableFromMillis(this.absenceJobPeriod));
		super.set("absence.warning", Helpers.DateTime.getHumanReadableFromMillis(this.absenceWarningDuration));
		super.set("absence.deletion", Helpers.DateTime.getHumanReadableFromMillis(this.absenceDeletionDuration));
		super.set("promotion.enabled", this.isPromotionJobEnabled);
		super.set("promotion.period", Helpers.DateTime.getHumanReadableFromMillis(this.promotionJobPeriod));
		super.set("promotion.min-playtime-for-member", Helpers.DateTime.getHumanReadableFromMillis(this.minPlaytimeForMember));
		super.set("promotion.min-playtime-for-regular", Helpers.DateTime.getHumanReadableFromMillis(this.minPlaytimeForRegular));
		super.set("promotion.max-regular-inactivity", Helpers.DateTime.getHumanReadableFromMillis(this.maxRegularInactivity));
		super.set("default-balance", this.defaultBalance);
		super.set("homes.restriction.enabled", this.isHomeRestrictionEnabled);
		super.set("homes.restriction.ignored-regions", this.homeIgnoreRegions);
		super.set("afk.check-period", Helpers.DateTime.getHumanReadableFromMillis(this.afkCheckPeriod));
		super.set("kit-cooldown", Helpers.DateTime.getHumanReadableFromMillis(this.kitCoolDown));
		super.set("alias-max-age", Helpers.DateTime.getHumanReadableFromMillis(this.aliasMaxAge));
		
		for (final String kitName: this.kits.keySet())
		{
			List<String> items = this.kits.get(kitName);
			super.set(Helpers.Parameters.replace("kits.$1", kitName), items);
		}
	}

	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading permissions...");
		this.loadPermissionsFromDatabase();
		this.permissionManager.finishLoad();
		
		super.info("Loading player groups...");		
		final Map<UUID, String> playerGroups = this.dbInterface.getRegisteredPlayerGroups();
		for (final UUID uuid: playerGroups.keySet())
		{
			final String group = playerGroups.get(uuid);
			this.permissionManager.setGroup(uuid, group);
		}

		super.info("Loading spawns...");
		this.spawnLocations = this.dbInterface.getSpawns();

		super.info("Loading warps...");
		this.warpLocations = this.dbInterface.getWarps();

		super.info("Loading homes...");
		this.homes = this.dbInterface.getHomes();
		
		if (super.getPlugin().isVaultEnabled())
		{
			if (this.economyManager == null)
			{
				this.economyManager = new EconomyManager(super.getPlugin(), this.dbInterface);
				super.getPlugin().setEconomyManager(this.economyManager);
			}
			this.economyManager.Initialise(this.defaultBalance);
		}
		
		super.info("Loading aliases...");
		super.getPlugin().clearAliases();
		final Map<String, List<String>> aliases = this.dbInterface.getAliases(this.aliasMaxAge);
		for (final String alias: aliases.keySet())
		{
			final List<String> names = aliases.get(alias);
			for (final String name: names)
				super.getPlugin().addAlias(name, alias);
		}
	}

	@Override
	protected void onEmitInfo()
	{
		super.info("Teleport delay: $1", Helpers.DateTime.getHumanReadableFromMillis(this.teleportDelay));
		super.info("Teleport cooldown: $1", Helpers.DateTime.getHumanReadableFromMillis(this.teleportCooldown));
		super.info("Teleport request window: $1", Helpers.DateTime.getHumanReadableFromMillis(this.teleportRequestWindow));
		super.info("Teleport invulnerability: $1", Helpers.DateTime.getHumanReadableFromMillis(this.teleportInvulnerabilityDuration));
		super.info("Players will go AFK after: $1", Helpers.DateTime.getHumanReadableFromMillis(this.afkInactivity));
		super.info("Players be AFK-kicked after: $1", Helpers.DateTime.getHumanReadableFromMillis(this.afkKick));
		super.info("AFK-kicked players will be told: $1", this.afkKickMessage);

		String groupList = "";
		final Collection<Group> groups = this.permissionManager.getGroups();
		if (groups == null) super.severe("No groups loaded.");
		{
			for (final Group group: groups)
			{
				if (groupList.length() > 0) groupList += ", ";
				groupList += group.getDisplayName();
			}
			super.info("Loaded groups: " + groupList);
		}

		for (final World world: Bukkit.getWorlds())
		{
			final String target = this.permissionManager.getWorldMapping(world);
			if (!target.equalsIgnoreCase(world.getName()))
				super.info("Permissions in $1 will be copied from $2.", world, target);
		}
		
		if (this.startKit == null ||
				this.startKit.isEmpty()) super.info("No start kit defined for new players.");
		else
		{
			if (!this.kitExists(this.startKit)) super.severe("Starting kit '$1' not defined!", this.startKit);
			else super.info("New players will receive kit '$1'.", this.startKit);
		}
		
		if (this.isAbsenceJobEnabled)
		{
			super.info(
					"Absence job will run every $1.",
					Helpers.DateTime.getHumanReadableFromMillis(this.absenceJobPeriod));
			super.info(
					"Players will receive warning after $1 of absence.",
					Helpers.DateTime.getHumanReadableFromMillis(this.absenceWarningDuration));
			super.info(
					"Players will be deleted after a further $1 of absence.",
					Helpers.DateTime.getHumanReadableFromMillis(this.absenceDeletionDuration));
		}
		else super.info("Absence job disabled.");

		if (this.isPromotionJobEnabled)
		{
			super.info(
					"Promotion job will run every $1.",
					Helpers.DateTime.getHumanReadableFromMillis(this.promotionJobPeriod));
			super.info(
					"Players must play for $1 before promotion to Member.",
					Helpers.DateTime.getHumanReadableFromMillis(this.minPlaytimeForMember));
			super.info(
					"Players must accrue $1 of playtime before promotion to Regular.",
					Helpers.DateTime.getHumanReadableFromMillis(this.minPlaytimeForRegular));
			super.info(
					"Players will be demoted from regular after $1 of inactivity.",
					Helpers.DateTime.getHumanReadableFromMillis(this.maxRegularInactivity));
		}
		else super.info("Promotion job disabled.");
		
		if (this.isHomeRestrictionEnabled) super.info("Homes are being restricted.");
		else super.info("Homes are not being restricted.");
	}

	private void loadPermissionsFromDatabase()
	{
		final Map<String, List<String>> bundleMap = this.dbInterface.getBundles();
		for (final String bundle: bundleMap.keySet())
			this.permissionManager.addGlobalGroup(bundle, bundleMap.get(bundle));

		for (final Group group: this.dbInterface.loadGroups())
			this.permissionManager.addGroup(group);
	}

	public int getForumAccountRegistrations(final String email)
	{
		return this.dbInterface.getForumAccountRegistrations(email);
	}
	
	public boolean getEmailExists(final String email)
	{
		return this.dbInterface.getEmailExists(email);
	}

	public String getEmail(final OfflinePlayer player)
	{
		return this.dbInterface.getEmail(player);
	}

	public void changeGroup(final OfflinePlayer offlinePlayer, final Group group, boolean silent)
	{
		if (super.getPlugin().isLiveServer()) this.dbInterface.changeUserPhpbbGroup(offlinePlayer.getUniqueId(), group.getName());
		else super.info("NOTLIVE: Skipped updating forum group for $1.", offlinePlayer);
		this.permissionManager.setGroup(offlinePlayer.getUniqueId(), group.getName());
		if (offlinePlayer.isOnline())
			this.permissionManager.applyGroup(offlinePlayer.getPlayer(), silent);
	}
	
	public Group setGroup(final Player player) { return this.setGroup(player, false); }
	public Group setGroup(final Player player, final boolean silent)
	{
		return this.permissionManager.applyGroup(player, silent);
	}

	public Location getSpawn(final Player player)
	{
		final Group group = this.permissionManager.getCurrentGroup(player);
		Location spawn = this.getSpawn(group);
		if (spawn == null)
		{
			super.severe("No spawn location set for group $1.", group);
			spawn = player.getWorld().getSpawnLocation();
		}
		return spawn;
	}

	public Location getSpawn(final Group group)
	{
		Location spawn = null;
		for (final String spawnName: this.spawnLocations.keySet())
		{
			if (spawnName.equalsIgnoreCase(group.getName()))
			{
				spawn = this.spawnLocations.get(spawnName);
				break;
			}
		}
		return spawn;
	}

	public Location getWarp(final String name)
	{
		Location warp = null;
		for (final String warpName: this.warpLocations.keySet())
		{
			if (warpName.equalsIgnoreCase(name))
			{
				warp = this.warpLocations.get(warpName);
				break;
			}
		}
		return warp;
	}
	
	public void removeData(final Player player)
	{
		this.permissionManager.removeData(player);
		this.lastActivity.remove(player.getUniqueId());
		this.afkPlayers.remove(player.getUniqueId());
	}
	
	public void createKit(final String kitName, ItemStack[] items)
	{
		if (this.kitExists(kitName)) this.deleteKit(kitName);
		List<String> itemStrings = new ArrayList<String>();
		for (ItemStack stack: items)
		{
			if (stack == null) continue;
			if (stack.getAmount() == 0) continue;
			itemStrings.add(
					Helpers.Parameters.replace(
							"$1 $2",
							stack.getAmount(),
							Helpers.Item.getFriendlyNames(stack, true)[0]));
		}
		this.kits.put(kitName, itemStrings);
		this.lowerKitNames.add(kitName.toLowerCase());
	}
	
	public void deleteKit(final String kitName)
	{
		if (!this.kitExists(kitName)) return;
		String realKitName = this.getRealKitName(kitName);
		this.kits.remove(realKitName);
		this.lowerKitNames.remove(realKitName.toLowerCase());
	}

	public void giveStartKit(final Player player)
	{
		this.giveKit(player, this.startKit);
	}
	
	public boolean kitExists(final String kitName)
	{
		return this.lowerKitNames.contains(kitName.toLowerCase());
	}
	
	public List<String> getKitNames()
	{
		return new ArrayList<String>(this.kits.keySet());
	}
	
	public long getTimeLastUsedKit(final OfflinePlayer player, final String kit)
	{
		long timeLastUsedKit = 0;
		final UUID uuid = player.getUniqueId();
		final Map<String, Long> kitTimes = this.lastUsedKitTimes.get(uuid);
		if (kitTimes != null) timeLastUsedKit = kitTimes.getOrDefault(kit.toLowerCase(), 0L);
		return timeLastUsedKit;
	}	
	
	public void giveKit(final Player player, final String kitName)
	{
		if (!this.kitExists(kitName))
		{
			super.info("Couldn't give kit $1 to $2 because it doesn't exist.", kitName, player);
			return;
		}
		
		final String realKitName = this.getRealKitName(kitName);
		
		List<String> equipmentList = this.getKit(realKitName);
		for (final String equipment: equipmentList)
		{
			final String[] data = equipment.split(" ");
			if (data.length <= 1) super.severe("Invalid entry in definition for kit $1.", realKitName);
			else
			{
				final String itemName = Helpers.Args.concatenate(data, 1);
				int quantity = 0;
				try { quantity = Integer.parseInt(data[0]); }
				catch (NumberFormatException ex)
				{ super.severe("Invalid quantity in definition for kit $1.", realKitName); }
				if (quantity > 0)
				{
					if (!Helpers.Item.isMetadataValid(itemName))
					{
						super.severe("Bad metadata for $1 in kit $2:", Helpers.Item.stripMetadata(itemName), realKitName);
						super.severe("--> $1", Helpers.Item.getMetadata(itemName));
						continue;
					}

					final ItemStack stack = Helpers.Item.getItemStack(itemName, quantity);
					if (stack == null) super.severe("Could not give $1 x $2 to $3 (kit $4)", quantity, itemName, player, realKitName);
					else player.getInventory().addItem(stack);
				}
			}
		}
		
		final UUID uuid = player.getUniqueId();
		Map<String, Long> kitTimes = this.lastUsedKitTimes.get(uuid);
		if (kitTimes == null)
		{
			kitTimes = new ConcurrentHashMap<String, Long>();
			this.lastUsedKitTimes.put(uuid, kitTimes);
		}
		kitTimes.put(kitName.toLowerCase(), System.currentTimeMillis());
		
		super.info("Gave kit $1 to $2.", realKitName, player);
	}

	private List<String> getKit(final String kitName)
	{
		List<String> kit = null;
		for (final String testKitName: this.kits.keySet())
		{
			if (testKitName.equalsIgnoreCase(kitName))
			{
				kit = this.kits.get(testKitName);
				break;
			}
		}
		return kit;
	}
	
	public String getRealKitName(final String kitName)
	{
		String kit = null;
		for (final String testKitName: this.kits.keySet())
		{
			if (testKitName.equalsIgnoreCase(kitName))
			{
				kit = testKitName;
				break;
			}
		}
		return kit;
	}
	
	public boolean recordLogin(final Player player)
	{
		final boolean showOnline = !super.getPlugin().isVanished(player);
		boolean unexempted = false;
		if (super.getPlugin().isLiveServer())
		{
			final boolean recordIpHistory = !player.hasPermission("vpcore.join.noiphistory");
			unexempted = this.dbInterface.recordLogin(player, this.defaultBalance, showOnline, recordIpHistory);			
		}
		else super.info("NOTLIVE: Skipped recording login for $1.", player);
		return unexempted;
	}

	public void recordLogout(final Player player)
	{
		if (super.getPlugin().isLiveServer()) this.dbInterface.recordLogout(player);
		else super.info("NOTLIVE: Skipped recording logout for $1.", player);
	}

	public String getGroupPrefixedName(final Player player)
	{
		final Group group = this.permissionManager.getCurrentGroup(player);
		if (group == null || group.getPrefix().length() == 0) return player.getDisplayName();
		else return Helpers.Parameters.replace("$1$2 $3", group.getColor(), group.getPrefix(), player.getDisplayName());
	}

	public void closeUnfinishedSessions()
	{
		if (super.getPlugin().isLiveServer()) this.dbInterface.closeUnfinishedSessions();
		else super.info("NOTLIVE: Skipped closing unfinished sessions.");
	}

	public Group getGroup(final String groupName)
	{
		return this.permissionManager.getGroup(groupName);
	}

	public void createOrUpdateSpawn(final Group group, final Location location)
	{
		if (this.getSpawn(group) == null)
			this.dbInterface.createSpawn(group, location);
		else this.dbInterface.updateSpawn(group, location);
		this.spawnLocations = this.dbInterface.getSpawns();
	}

	public void createOrUpdateWarp(final String name, final Location location)
	{
		if (this.getWarp(name) == null)
			this.dbInterface.createWarp(name, location);
		else this.dbInterface.updateWarp(name, location);
		this.warpLocations = this.dbInterface.getWarps();
	}

	public void deleteWarp(final String name)
	{
		for (final String warpName: this.warpLocations.keySet())
		{
			if (warpName.equalsIgnoreCase(name))
			{
				this.dbInterface.deleteWarp(warpName);
				this.warpLocations = this.dbInterface.getWarps();
				break;
			}
		}
	}

	public Set<String> getWarpNames()
	{
		return this.warpLocations.keySet();
	}

	public long getNextTeleportTimestamp(final Player player)
	{
		long timestamp = 0;
		if (this.nextTeleportTimestamps.containsKey(player.getUniqueId()))
			timestamp = this.nextTeleportTimestamps.get(player.getUniqueId());
		return timestamp;
	}

	public void doDelayedTeleport(final Player player, final Location location)
	{
		if (this.isTeleportPending(player))
			super.warning("Attempted to create delayed teleport for $1, but one already existed!", player);
		else
		{
			final PlayerTeleportTask task = new PlayerTeleportTask(this, player, location);
			task.runTaskLater(super.getPlugin(), Helpers.DateTime.getTicksFromMillis(this.teleportDelay));
			this.teleportTasks.put(player.getUniqueId(), task);
			this.nextTeleportTimestamps.put(
					player.getUniqueId(),
					System.currentTimeMillis() + this.teleportCooldown);
		}
	}

	public boolean isTeleportPending(final Player player)
	{
		boolean isTeleportPending = false;
		if (this.teleportTasks.containsKey(player.getUniqueId()))
		{
			if (!this.teleportTasks.get(player.getUniqueId()).hasExecuted())
				isTeleportPending = true;
		}
		return isTeleportPending;
	}

	public void cancelPendingTeleport(final Player player)
	{
		if (this.isTeleportPending(player))
		{
			final UUID uuid = player.getUniqueId();
			this.teleportTasks.get(uuid).cancel();
			this.teleportTasks.remove(uuid);
		}
	}

	public boolean isAfk(final Player player)
	{
		boolean isAfk = false;
		final UUID uuid = player.getUniqueId();
		if (this.afkPlayers.containsKey(uuid)) isAfk = this.afkPlayers.get(uuid);
		else this.afkPlayers.put(uuid, false);
		return isAfk;
	}

	public void setIsAfk(final Player player, final boolean afk) { this.setIsAfk(player, afk, false, false); }
	public void setIsAfk(final Player player, final boolean afk, final boolean silent, final boolean overridePermission)
	{
		boolean existingValue = !afk;

		final UUID uuid = player.getUniqueId();
		if (this.afkPlayers.containsKey(uuid))
			existingValue = this.afkPlayers.get(uuid);

		if (afk != existingValue)
		{
			if (afk) this.lastActivity.put(uuid, System.currentTimeMillis() - this.afkInactivity);
			else this.lastActivity.put(uuid, System.currentTimeMillis());

			if (overridePermission || player.hasPermission("vpcore.afk.auto"))
			{
				this.afkPlayers.put(uuid, afk);

				if (!silent)
				{
					String message = "";
					String messageToOthers = "";
					if (afk)
					{
						message = "You are now AFK.";
						messageToOthers = Helpers.Parameters.replace("$1- $2 is now AFK.", ChatColor.GRAY, player.getName());
					}
					else
					{
						message = "You are no longer AFK.";
						messageToOthers = Helpers.Parameters.replace("$1- $2 is no longer AFK.", ChatColor.GRAY, player.getName());
					}

					Helpers.Messages.sendNotification(player, message);
					if (!super.getPlugin().isVanished(player))
					{
						for (final Player otherPlayer: Bukkit.getOnlinePlayers())
						{
							if (!player.equals(otherPlayer))
								Helpers.Messages.sendInfo(otherPlayer, messageToOthers);
						}
					}
				}
			}
		}
	}

	public void registerActivity(final Player player) { this.registerActivity(player, false); }
	public void registerActivity(final Player player, final boolean silent)
	{
		this.lastActivity.put(player.getUniqueId(), System.currentTimeMillis());
		this.setIsAfk(player, false, silent, false);
	}

	public long getLastActivity(final Player player)
	{
		long lastActivity = System.currentTimeMillis();
		final UUID uuid = player.getUniqueId();
		if (this.lastActivity.containsKey(uuid)) lastActivity = this.lastActivity.get(uuid);
		else this.lastActivity.put(uuid, lastActivity);
		return lastActivity;
	}

	public Group getCurrentGroup(final OfflinePlayer player) { return this.getCurrentGroup(player, false); }
	public Group getCurrentGroup(final OfflinePlayer player, final boolean forceFromDb)
	{
		Group group = this.permissionManager.getCurrentGroup(player);
		if (group == null || forceFromDb)
		{
			final String groupName = this.dbInterface.getGroupName(player);
			group = this.permissionManager.getGroup(groupName);
			if (group == null) group = this.permissionManager.getDefaultGroup();
		}
		return group;
	}

	public boolean setHomeLocation(final OfflinePlayer player, String name, final Location home)
	{
		final UUID uuid = player.getUniqueId();
		ConcurrentHashMap<String, Location> playerHomes = null;
		if (this.homes.containsKey(uuid)) playerHomes = this.homes.get(uuid);
		else
		{
			playerHomes = new ConcurrentHashMap<String, Location>();
			this.homes.put(uuid, playerHomes);
		}

		name = name.toLowerCase();
		playerHomes.put(name, home);

		this.dbInterface.createOrUpdateHomeLocation(uuid, name, home);

		return true;
	}

	public Location getHomeLocation(final OfflinePlayer player, final String name)
	{
		Location home = null;
		final UUID uuid = player.getUniqueId();
		final String lowerName = name.toLowerCase();
		if (lowerName.equals(PlayerSettings.PlotHomeName)) home = this.getPlotHomeLocation(player);
		else if (this.homes.containsKey(uuid)) home = this.homes.get(uuid).get(lowerName);
		return home;
	}

	public Location getPlotHomeLocation(final OfflinePlayer player)
	{
		Location plotHome = null;
		if (super.getPlugin().isWorldGuardEnabled() && this.plotSettings.isEnabled())
		{
			final ProtectedRegion plot = this.plotSettings.getPlotFor(player);
			if (plot != null)
			{
				final PlotData plotData = this.plotSettings.getPlotData(plot);
				if (plotData != null)
				{
					for (final SignData signData: this.signSettings.getPlotSigns(plotData.getName()))
					{
						plotHome = signData.getLocation();
						final Location midPoint = this.plotSettings.getPlotMidPoint(plot, plotHome.getBlockY());
						plotHome.setDirection(midPoint.toVector().subtract(plotHome.toVector()));
						break;
					}
				}
				
			}
		}
		return plotHome;
	}
	
	public Set<String> getHomeNames(final OfflinePlayer player)
	{
		final UUID uuid = player.getUniqueId();
		if (!this.homes.containsKey(uuid)) this.homes.put(uuid, new ConcurrentHashMap<String, Location>());
		final Set<String> homeNames = new HashSet<String>();
		
		homeNames.addAll(this.homes.get(uuid).keySet());
		
		if (this.getPlotHomeLocation(player) != null)
			homeNames.add(PlayerSettings.PlotHomeName);

		return homeNames;
	}

	public void deleteHomeLocation(final OfflinePlayer player, String name)
	{
		final UUID uuid = player.getUniqueId();
		name = name.toLowerCase();
		if (!name.equals(PlayerSettings.BedHomeName))
		{
			if (this.homes.containsKey(uuid))
			{
				for (final String home: this.homes.get(uuid).keySet())
				{
					if (home.equalsIgnoreCase(name))
					{
						this.homes.get(uuid).remove(home);
						break;
					}
				}
				this.dbInterface.deleteHomeLocation(uuid, name);
			}
		}
	}

	public void createTeleportRequest(final TeleportRequest request)
	{
		this.teleportRequestsByAcceptor.put(request.getAcceptor().getUniqueId(), request);
		this.teleportRequestsByRequestor.put(request.getRequestor().getUniqueId(), request);
	}

	public TeleportRequest getTeleportRequestToAccept(final Player player)
	{
		TeleportRequest request = null;
		final UUID uuid = player.getUniqueId();
		if (this.teleportRequestsByAcceptor.containsKey(uuid))
		{
			request = this.teleportRequestsByAcceptor.get(uuid);
			if (request.hasExpired(this.teleportRequestWindow)) request = null;
		}
		return request;
	}
	
	public TeleportRequest getLastTeleportRequestSent(final Player player)
	{
		TeleportRequest request = null;
		final UUID uuid = player.getUniqueId();
		if (this.teleportRequestsByRequestor.containsKey(uuid))
		{
			request = this.teleportRequestsByRequestor.get(uuid);
			if (request.hasExpired(this.teleportRequestWindow)) request = null;
		}
		return request;
	}

	public boolean toggleBusy(final Player player)
	{
		boolean busy = false;
		final UUID uuid = player.getUniqueId();
		if (this.busyFlags.containsKey(uuid))
			busy = this.busyFlags.get(uuid);
		busy = !busy;
		this.busyFlags.put(uuid, busy);
		return busy;
	}

	public boolean isBusy(final Player player)
	{
		boolean busy = false;
		final UUID uuid = player.getUniqueId();
		if (this.busyFlags.containsKey(uuid))
			busy = this.busyFlags.get(uuid);
		return busy;
	}

	public void setLastTell(final Player sender, final Player receiver)
	{
		this.lastTells.put(receiver.getUniqueId(), sender.getUniqueId());
	}

	public UUID getLastTell(final Player player)
	{
		return this.lastTells.get(player.getUniqueId());
	}

	public void recordDeathInformation(final Player player)
	{
		final PlayerInventory inventory = player.getInventory();
		final List<ItemStack> items = new ArrayList<ItemStack>();
		if (inventory.getHelmet() != null) items.add(inventory.getHelmet());
		if (inventory.getChestplate() != null) items.add(inventory.getChestplate());
		if (inventory.getLeggings() != null) items.add(inventory.getLeggings());
		if (inventory.getBoots() != null) items.add(inventory.getBoots());
		for (ItemStack stack: inventory.getContents())
		{
			if (stack != null) items.add(stack);
		}
				
		if (items.isEmpty()) super.info("Not recording death for $1; no items lost.", player);		
		else if (super.getPlugin().isLiveServer())
		{
			final String name = player.getDisplayName();
			final UUID uuid = player.getUniqueId();
			final Location location = player.getLocation();
			final DamageCause cause = player.getLastDamageCause().getCause();

			final TaskBase task = new TaskBase(super.isDebug(), "RecordDeathInformation")
			{
				@Override
				public void runTask()
				{
					dbInterface.recordDeathInformation(
							name,
							uuid,
							cause,
							location,
							items);
				}
			};
			task.runTaskAsynchronously(super.getPlugin());
		}
		else super.info("NOTLIVE: Skipped recording death for $1.", player);
	}

	public boolean isMuted(final OfflinePlayer player)
	{
		return (this.getMuteDuration(player) > 0);
	}
	
	public long getMuteDuration(final OfflinePlayer player)
	{
		long muteDuration = 0;
		final UUID uuid = player.getUniqueId();
		if (this.mutedPlayers.containsKey(uuid))
		{
			muteDuration = this.mutedPlayers.get(uuid) - System.currentTimeMillis();
			if (muteDuration <= 0)
			{
				this.mutedPlayers.remove(uuid);
				muteDuration = 0;
			}
		}
		return muteDuration;
	}

	public long mute(final OfflinePlayer player, long duration)
	{
		if (duration <= 0) duration = this.defaultMuteDuration;
		final String durationHumanReadable = Helpers.DateTime.getHumanReadableFromMillis(duration);
		if (player.isOnline())
			Helpers.Messages.sendNotification(player.getPlayer(), "You have been muted for $1.", durationHumanReadable);
		this.mutedPlayers.put(player.getUniqueId(), System.currentTimeMillis() + duration);
		return duration;
	}

	public void unmute(final OfflinePlayer player)
	{
		this.mutedPlayers.put(player.getUniqueId(), 0L);
	}

	public boolean isInvulnerable(final OfflinePlayer player)
	{
		boolean invulnerable = false;
		final UUID uuid = player.getUniqueId();
		if (this.invulnerableFlags.containsKey(uuid))
			invulnerable = this.invulnerableFlags.get(uuid);
		return invulnerable;
	}

	public void setTemporaryInvulnerability(final OfflinePlayer player)
	{
		if (this.isGod(player)) return;
		if (this.isInvulnerable(player)) return;
		
		if (player.isOnline())
		{
			switch (player.getPlayer().getGameMode())
			{
			case ADVENTURE:
			case SURVIVAL: break;
			
			case CREATIVE:
			case SPECTATOR:
				return;
			}
		}

		this.setInvulnerability(player, true);
		final TaskBase task = new TaskBase(super.isDebug(), "PlayerInvulnerability")
		{
			@Override
			public void runTask()
			{
				setInvulnerability(player, false);
				debug("$1 has lost temporary invulnerability!", player);
			}
		};
		task.runTaskLater(super.getPlugin(), Helpers.DateTime.getTicksFromMillis(this.teleportInvulnerabilityDuration));
		this.invulnerabilityTasks.put(player.getUniqueId(), task);
		super.debug("Red elf ($1) now has temporary invulnerability!", player);
	}

	public void setInvulnerability(final OfflinePlayer player, final boolean invulnerable) { this.setInvulnerability(player, invulnerable, false); }
	private void setInvulnerability(final OfflinePlayer player, final boolean invulnerable, final boolean ignoreGodCheck)
	{
		if (!ignoreGodCheck && this.isGod(player)) return;

		final UUID uuid = player.getUniqueId();
		if (this.invulnerabilityTasks.containsKey(uuid))
		{
			this.invulnerabilityTasks.get(uuid).cancel();
			this.invulnerabilityTasks.remove(uuid);
		}
		this.invulnerableFlags.put(uuid, invulnerable);
		super.debug("$1.invulnerablity=$2", player, invulnerable);
	}

	public boolean isGod(final OfflinePlayer player)
	{
		boolean god = false;
		final UUID uuid = player.getUniqueId();
		if (this.godFlags.containsKey(uuid))
			god = this.godFlags.get(uuid);
		return god;
	}

	public boolean setGod(final OfflinePlayer player, final boolean god)
	{
		this.godFlags.put(player.getUniqueId(), god);
		this.setInvulnerability(player, god, true);
		super.debug("$1.god=$2", player, god);
		return god;
	}

	public boolean isIgnored(final OfflinePlayer player)
	{
		boolean isIgnored = false;
		final UUID uuid = player.getUniqueId();
		for (final UUID ignorer: this.ignoreLists.keySet())
		{
			if (this.ignoreLists.get(ignorer).contains(uuid))
			{
				isIgnored = true;
				break;
			}
		}
		return isIgnored;
	}
	
	public boolean isIgnoredBy(final OfflinePlayer player, final OfflinePlayer ignorer)
	{
		boolean isIgnoredBy = false;
		final UUID ignorerUuid = ignorer.getUniqueId();
		if (this.ignoreLists.containsKey(ignorerUuid))
			isIgnoredBy = (this.ignoreLists.get(ignorerUuid).contains(player.getUniqueId()));
		return isIgnoredBy;
	}

	public List<UUID> getIgnorers(final OfflinePlayer player)
	{
		final List<UUID> ignorers = new ArrayList<UUID>();
		final UUID uuid = player.getUniqueId();
		for (final UUID ignorer: this.ignoreLists.keySet())
			if (this.ignoreLists.get(ignorer).contains(uuid))
				ignorers.add(ignorer);
		return ignorers;
	}

	public boolean toggleIgnore(final OfflinePlayer ignorer, final OfflinePlayer ignoree)
	{
		final UUID ignorerUuid = ignorer.getUniqueId();
		List<UUID> ignored = null;
		boolean isIgnored = false;
		if (this.ignoreLists.containsKey(ignorerUuid)) ignored = this.ignoreLists.get(ignorerUuid);
		else
		{
			ignored = new ArrayList<UUID>();
			this.ignoreLists.put(ignorerUuid, ignored);
		}
		final UUID ignoreeUuid = ignoree.getUniqueId();
		if (ignored.contains(ignoreeUuid)) ignored.remove(ignoreeUuid);
		else
		{
			ignored.add(ignoreeUuid);
			isIgnored = true;
		}
		return isIgnored;
	}

	public List<UUID> getIgnoredList(final Player player)
	{
		return this.ignoreLists.get(player.getUniqueId());
	}

	public boolean validateTeleportDestination(final Player player, final Location location)
	{
		return this.validateTeleportDestination(player, player, location.getBlock(), false);
	}

	public boolean validateTeleportDestination(final Player player, final Block block)
	{
		return this.validateTeleportDestination(player, player, block, false);
	}
	
	public boolean validateTeleportDestination(
			final Player permissionPlayer,
			final Player messagePlayer,
			final Block block,
			final boolean outputInfo)
	{
		if (permissionPlayer != null && 
				permissionPlayer.hasPermission("vpcore.moderator"))
			return true;
		
		final Block targetBlock = block.getRelative(BlockFace.UP);
		boolean canTeleport = true;
		boolean foundSafeBlock = false;
		String reason = "";
		Block testBlock = null;
		for (int i = 0; i < 5; ++i)
		{
			if (testBlock == null) testBlock = targetBlock;
			else testBlock = testBlock.getRelative(BlockFace.DOWN);
			
			if (outputInfo && messagePlayer != null)
			{
				if (testBlock == null) Helpers.Messages.sendInfo(messagePlayer, "Found a NULL block!");
				else Helpers.Messages.sendInfo(messagePlayer, "Testing $1 $2.", testBlock.getType(), testBlock.getLocation());
			}

			if (testBlock == null)
			{
				reason = "block not found";
				canTeleport = false;
			}			
			else if (testBlock.getLocation().getBlockY() < 1)
			{
				reason = "fall into void";
				canTeleport = false;
			}
			else
			{
				switch (testBlock.getType())
				{
				case LAVA:
				case STATIONARY_LAVA:
					if (i < 2) reason = "lava hazard";
					else reason = "fall into lava";
					canTeleport = false;
					break;
				case WATER:
				case STATIONARY_WATER:
					if (i == 0)
					{
						reason = "water hazard";
						canTeleport = false;
					}
					else if (i >= 2)
					{
						reason = "fall into water";
						canTeleport = false;
					}
					break;
				case FIRE:
					if (i < 2) reason = "fire hazard";
					else reason = "fall into fire";
					canTeleport = false;
					break;
				default:
					if (Helpers.Block.isPassable(testBlock))
					{
						if (i >= 2)
						{
							reason = "fall hazard";
							canTeleport = false;
						}
					}
					else if (Helpers.Block.isHalfHeight(testBlock))
					{
						if (i == 0)
						{
							reason = "blocked";
							canTeleport = false;							
						}
						else if (i == 1)
						{
							Block aboveBlock = block.getRelative(BlockFace.UP);
							if (aboveBlock.isEmpty()) foundSafeBlock = true;
							else
							{
								reason = "blocked";
								canTeleport = false;							
							}
						}
						else foundSafeBlock = true;
					}
					else
					{
						if (testBlock.getType() == Material.CACTUS)
						{
							if (i >= 2)
							{
								reason = "cactus hazard";
								canTeleport = false;
							}
						}
						else foundSafeBlock = true;
					}
					break;
				}
				
				if (foundSafeBlock || !canTeleport) break;
			}
		}

		if (messagePlayer != null)
		{
			if (canTeleport)
			{
				if (outputInfo) Helpers.Messages.sendSuccess(
						messagePlayer,
						"Destination $1 is safe.",
						block.getLocation());
			}
			else Helpers.Messages.sendFailure(messagePlayer, "Cannot teleport: $1.", reason);
		}

		return canTeleport;
	}

	public int getPlotDeletionExemption(final OfflinePlayer player)
	{
		return this.dbInterface.getExemption(player);
	}
	
	public boolean isPlotDeletionExempt(final OfflinePlayer player)
	{
		boolean isPlotDeletionExempt = true;
		
		if (this.getPlotDeletionExemption(player) == 0)
		{
			final Group group = this.getCurrentGroup(player);
			if (!group.isExempt()) isPlotDeletionExempt = false;
		}
		
		return isPlotDeletionExempt;
	}

	public void setExemptFromPlotDeletion(final OfflinePlayer player, final int exempt)
	{
		this.dbInterface.setExempt(player, exempt);
	}
	
	public void changeToRegisteredGroup(final OfflinePlayer player)
	{
		Group group = this.getCurrentGroup(player, true);
		if (group.isDefault())
		{
			super.debug("Registering account for $2 ($3).", player, player.getUniqueId());
			group = this.permissionManager.getRegisteredGroup();
			this.dbInterface.changeToRegisteredGroup(player.getUniqueId(), player.getName(), group.getPhpBbGroupId());
			
			final UUID uuid = player.getUniqueId();
			final TaskBase task = new TaskBase(super.isDebug(), "PromotionToMember")
			{
				@Override
				protected void runTask()
				{
					final OfflinePlayer playerToPromote = Bukkit.getOfflinePlayer(uuid);
					super.info("Automatically promoting $1.", playerToPromote);
					
					final Group memberGroup = getGroup("Member");
					if (memberGroup == null) super.severe("Could not find group 'Member'!");
					else
					{
						final Group currentGroup = getCurrentGroup(playerToPromote);
						if (currentGroup.isRegistered())
						{
							changeGroup(playerToPromote, memberGroup, true);
							
							if (playerToPromote.isOnline())
								Helpers.Broadcasts.sendAll("$1 was promoted to $2!", playerToPromote, memberGroup);
						}
						else super.warning("$1 is in group $2 and will not be moved to $3.", playerToPromote, currentGroup, memberGroup);
					}
				}
			};
			task.runTaskLater(super.getPlugin(), Helpers.DateTime.getTicksFromMillis(this.minPlaytimeForMember));

			this.changeGroup(player, group, false);
			
			if (player.isOnline())
			{
				final Player onlinePlayer = player.getPlayer();
				Helpers.Broadcasts.sendAll("$1 was promoted to $2!", onlinePlayer, group);
				Helpers.Messages.sendSuccess(onlinePlayer, "Congratulations, your account is now registered!");
				Helpers.Messages.sendSuccess(
						onlinePlayer,
						"You will be promoted to $1 automatically in $2.",
						"Member",
						Helpers.DateTime.getHumanReadableFromMillis(this.minPlaytimeForMember, true));
			}
		}
	}
	
	public boolean isRegistered(final OfflinePlayer player)
	{
		return this.dbInterface.isRegistered(player.getUniqueId());
	}

	public void setAbsenceBaselineData()
	{
		this.dbInterface.setAbsenceBaselineData(this.absenceWarningDuration);
	}
	
	public List<UUID> getPlayersToWarn()
	{
		return this.dbInterface.getPlayersToWarn();
	}
	
	public List<UUID> getPlayersToDelete(final boolean banned)
	{
		if (banned) return this.dbInterface.getBannedPlayersToDelete(this.absenceDeletionDuration);
		else return this.dbInterface.getPlayersToDelete(this.absenceDeletionDuration);
	}
	
	public void markPlayerAsWarned(final OfflinePlayer player)
	{
		if (super.getPlugin().isLiveServer()) this.dbInterface.markPlayerAsWarned(player.getUniqueId());
		else super.info("NOTLIVE: Skipped marking $1 as warned.", player);
	}

	public void markPlayerAsDeleted(final OfflinePlayer player)
	{
		if (super.getPlugin().isLiveServer()) this.dbInterface.markPlayerAsDeleted(player.getUniqueId());
		else super.info("NOTLIVE: Skipped marking $1 as deleted.", player);
	}
	
	public List<UUID> getRegisteredPlayersForPromotion()
	{
		return this.dbInterface.getRegisteredPlayersForPromotion(this.minPlaytimeForMember);
	}
	
	public List<UUID> getMemberPlayersForPromotion()
	{
		return this.dbInterface.getMemberPlayersForPromotion(this.minPlaytimeForRegular, this.maxRegularInactivity);
	}
	
	public List<UUID> getRegularPlayersForDemotion()
	{
		return this.dbInterface.getRegularPlayersForDemotion(this.maxRegularInactivity);
	}
	
	public List<UUID> getFirstLoggedInPlayers(final long since)
	{
		return this.dbInterface.getFirstLoggedInPlayers(since);
	}

	public List<UUID> getLastLoggedInPlayers(final long since)
	{
		return this.dbInterface.getLastLoggedInPlayers(since);
	}

	public String getProfileUrl(final OfflinePlayer player)
	{ return this.getProfileUrl(player.getUniqueId()); }	
	public String getProfileUrl(final UUID uuid)
	{
		String url = "";
		int id = this.dbInterface.getForumAccountId(uuid);
		if (id > 0) url = Helpers.Parameters.replace("http://mc.rv.rs/memberlist.php?mode=viewprofile&u=$1", id);
		return url;
	}
	
	public void resetAccruedPlaytime(final OfflinePlayer player)
	{
		this.dbInterface.resetAccruedPlaytime(player.getUniqueId());
	}
	
	public ItemStack[] getDeathInventoryItems(final int deathId)
	{
		return this.dbInterface.getDeathInventoryItems(deathId);
	}
	
	public List<PlayerDeathData> getPlayerDeathData(final OfflinePlayer player, final long since)
	{
		UUID uuid = null;
		if (player != null) uuid = player.getUniqueId();
		return this.dbInterface.getPlayerDeathData(uuid, since);
	}
	
	public void displayPlayerList(final CommandSender sender, final Player player)
	{
		final Map<Group, List<Player>> sortedPlayers = new HashMap<Group, List<Player>>();
		final Player[] onlinePlayers = Bukkit.getOnlinePlayers().toArray(new Player[0]);
				
		Arrays.sort(
				onlinePlayers,
				new Comparator<Player>()
				{
					@Override
					public int compare(final Player o1, final Player o2)
					{
						return o1.getName().compareTo(o2.getName());
					}
				});
		
		int playerCount = 0;
		boolean canSeeAll = true;
		if (player != null) canSeeAll = player.hasPermission("vpcore.moderator");		
		for (final Player onlinePlayer: onlinePlayers)
		{		
			if (canSeeAll || !super.getPlugin().isVanished(onlinePlayer))
			{
				final Group group = this.getCurrentGroup(onlinePlayer);
				List<Player> groupPlayers = sortedPlayers.get(group);
				if (groupPlayers == null)
				{
					groupPlayers = new ArrayList<Player>();
					sortedPlayers.put(group, groupPlayers);
				}
				groupPlayers.add(onlinePlayer);
				++playerCount;
			}
		}
		
		if (playerCount == 0) Helpers.Messages.sendInfo(sender, "Nobody is online.");
		else
		{
			final Group[] groups = sortedPlayers.keySet().toArray(new Group[0]);
			Arrays.sort(
					groups,
					new Comparator<Group>()
					{
						@Override
						public int compare(final Group o1, final Group o2)
						{
							return o1.getName().compareTo(o2.getName());
						}
					});
	
			Helpers.Messages.sendInfo(
					sender,
					"There $1 $2 player$3 online:",
					Helpers.getIsAre(playerCount),
					playerCount,
					Helpers.getPlural(playerCount));
			for (final Group group: groups)
			{				
				List<Player> playersByGroup = sortedPlayers.get(group);
				String playerList = group.getDisplayName() + ": ";
				int index = 1;
				for (final Player groupPlayer: playersByGroup)
				{
					if (index > 1) playerList += ", ";
					playerList += groupPlayer.getDisplayName();
					String flags = "";
					if (this.isAfk(groupPlayer)) flags += "AFK ";
					if (this.isBusy(groupPlayer)) flags += "DND ";
					if (super.getPlugin().isVanished(groupPlayer)) flags += "INVIS ";
					if (flags.length() > 0) playerList += ChatColor.GRAY + "[" + flags.trim() + "]" + ChatColor.RESET;
					++index;
				}
				Helpers.Messages.sendInfo(sender, "  " + playerList);
			}
		}
	}
	
	public void updateVisibleStatus(final Player player, final boolean isVisible)
	{
		if (super.getPlugin().isLiveServer())
		{
			this.dbInterface.updateVisibleStatus(player, isVisible);
			if (isVisible) Helpers.Messages.sendSuccess(player, "You're now visible on the website.");
			else Helpers.Messages.sendSuccess(player, "You are no longer visible on the website.");
		}
	}
	public boolean playerHasAssociatedForumProfileData(final OfflinePlayer player)
	{
		return this.dbInterface.playerHasAssociatedForumProfileData(player.getName());
	}
		
	public final boolean regionIsIgnoredForHomes(final String id)
	{
		boolean regionIsIgnoredForHomes = false;
		
		for (final String regionName: this.homeIgnoreRegions)
		{
			if (regionName.equalsIgnoreCase(id))
			{
				regionIsIgnoredForHomes = true;
				break;
			}
		}
		
		return regionIsIgnoredForHomes;
	}
}
