package com.rekalogic.vanillapod.vpcore.chunk;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Chunk;
import org.bukkit.World.Environment;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class ChunkSettings extends SettingsBase
{
	private boolean keepSpawnChunksInMemory = true;
	private List<String> cacheSources = null;
	private boolean loadOnStart = false;
	private final Map<Integer, Chunk> chunkMap = new ConcurrentHashMap<Integer, Chunk>();
	private final Map<String, Integer> chunkCountMap = new ConcurrentHashMap<String, Integer>();
	private final Map<Integer, String> chunkSourceMap = new ConcurrentHashMap<Integer, String>();
	private int totalUnloadsDeferred = 0;
	private long unloadDeferDuration = 0;
	private int chunkKeepEntityLevel = 0;
	
	public int getChunkMapSize() { return this.chunkMap.size(); }
	public int getCacheSourcesSize() { return this.cacheSources.size(); }
	public String[] getCacheSources() { return this.cacheSources.toArray(new String[0]); }
	public int getChunkKeepEntityLevel() { return this.chunkKeepEntityLevel; }	
	public int getUnloadsDeferred() { return this.deferredUnloads.size(); }
	public int getTotalUnloadsDeferred() { return this.totalUnloadsDeferred; }
	
	private final Map<Integer, Long> deferredUnloads = new ConcurrentHashMap<Integer, Long>();

	public ChunkSettings(final VPPlugin plugin)
	{
		super(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.chunkMap.clear();
		this.chunkCountMap.clear();
		this.chunkSourceMap.clear();
		this.deferredUnloads.clear();

		this.keepSpawnChunksInMemory = super.getBoolean("keep-spawn-in-memory");
		this.cacheSources = super.getStringList("cache-sources");
		this.loadOnStart = super.getBoolean("load-on-start");
		this.unloadDeferDuration = Helpers.DateTime.getMillis(super.getString("unload-defer-duration"));
		this.chunkKeepEntityLevel = super.getInt("chunk-keep-entity-level");
	}

	@Override
	protected void onSave()
	{
		super.set("enabled", super.isEnabled());
		super.set("keep-spawn-in-memory", this.keepSpawnChunksInMemory);
		super.set("cache-sources", this.cacheSources);
		super.set("load-on-start", this.loadOnStart);
		super.set("unload-defer-duration", Helpers.DateTime.getHumanReadableFromMillis(this.unloadDeferDuration));
		super.set("chunk-keep-entity-level", this.chunkKeepEntityLevel);
	}

	@Override
	protected void onEmitSettings()
	{
		super.emitSetting("Keep spawn in memory", this.keepSpawnChunksInMemory);
		super.emitSetting("Load on start", this.loadOnStart);
	}

	@Override
	protected void onEmitInfo()
	{
		if (!this.hasCacheSources()) super.info("No chunk caching sources configured.");
		else
		{
			super.info("Chunk caching sources configured ($1):", this.cacheSources.size());
			for (final String source: this.cacheSources)
				super.info("    $1", this.getCacheSourceNameForDisplay(source));
			super.info("Total: $1 chunk$2.", this.chunkMap.size(), Helpers.getPlural(this.chunkMap.size()));
		}

		super.info("Chunk unloads will be deferred for $1.", Helpers.DateTime.getHumanReadableFromMillis(this.unloadDeferDuration));
		super.info("Chunk unload defer entity level: $1 or more.", this.chunkKeepEntityLevel);
	}

	@Override
	protected void onLoadDataStore()
	{
		if (this.cacheSources.size() > 0)
		{
			super.info("Loading cache sources...");

			PagedOutputCommandSender sender = PagedOutputCommandSender.NullInstance;
			if (super.isDebug()) sender = PagedOutputCommandSender.DebugInstance;
			
			for (final String source: this.cacheSources)
			{
				final String args[] = source.split(" ");
				switch (args[0].toLowerCase())
				{
				case "wg":
					if (super.getPlugin().isWorldGuardEnabled())
					{
						if (args.length != 3) super.severe("Malformatted WorldGuard cache source: $1", source);
						else ChunkExecutor.addWorldGuardRegion(this, sender, args[1], args[2], false);
					}
					else super.warning("Cache source '$1' could not be loaded: WorldGuard not detected.", source);
					break;
				case "chunk":
					if (args.length != 4) super.severe("Malformatted chunk cache source: $1", source);
					else ChunkExecutor.addSingleChunk(this, sender, args[1], args[2], args[3], false);
					break;
				}
			}

			super.debug(
					"Loaded $1 chunk$2 from $3 source$4.",
					this.chunkMap.size(),
					Helpers.getPlural(this.chunkMap.size()),
					this.cacheSources.size(),
					Helpers.getPlural(this.cacheSources.size()));

			if (this.loadOnStart)
			{
				super.info("Pre-loading chunks...");
				int chunkCount = 0;
				for (final Chunk chunk: this.chunkMap.values())
				{					
					if (chunk.load(true)) ++chunkCount;
				}
				super.info(
						"Chunk load complete ($1 of $2 chunk$3).",
						chunkCount,
						this.chunkMap.size(),
						Helpers.getPlural(this.chunkMap.size()));
			}
		}
		
		super.onLoadDataStore();
	}

	public boolean hasCacheSources()
	{
		return (this.cacheSources != null && this.cacheSources.size() > 0);
	}

	public String getCacheSourceNameForDisplay(final String source)
	{
		if (ChunkSettings.isWorldGuardCacheSource(source) &&
				!super.getPlugin().isWorldGuardEnabled()) return source + " [NO WG]";
		else if (!this.chunkCountMap.containsKey(source)) return source + " [INVALID]";
		else
		{
			final int chunkCount = this.chunkCountMap.get(source);
			return source + Helpers.Parameters.replace(" [$1 chunk$2]", chunkCount, Helpers.getPlural(chunkCount));
		}
	}

	private static boolean isWorldGuardCacheSource(final String source)
	{
		return (source.trim().toLowerCase().startsWith("wg"));
	}

	public boolean isCached(final Chunk chunk)
	{
		return this.chunkMap.containsKey(chunk.hashCode());
	}

	public boolean tryAddToChunkMap(final Chunk chunk)
	{
		boolean added = false;
		if (chunk.getWorld().getEnvironment() == Environment.NORMAL)
		{
			final int hashCode = chunk.hashCode();		
			if (!this.chunkMap.containsKey(hashCode))
			{
				this.chunkMap.put(hashCode, chunk);
				added = true;
			}
		}
		return added;
	}
	
	public void deferUnload(final Chunk chunk)
	{
		if (chunk.getWorld().getEnvironment() == Environment.NORMAL)
		{
			this.deferredUnloads.put(chunk.hashCode(), System.currentTimeMillis() + this.unloadDeferDuration);
			++this.totalUnloadsDeferred;
		}
	}
	
	public void undeferUnload(final Chunk chunk)
	{
		this.deferredUnloads.remove(chunk.hashCode());
	}

	public long getDeferredUntil(final Chunk chunk)
	{
		final int hashCode = chunk.hashCode();
		long deferredUntil = 0;
		if (this.deferredUnloads.containsKey(hashCode))
			deferredUntil = this.deferredUnloads.get(hashCode);
		return deferredUntil;		
	}
	
	public void flushCache()
	{
		final int deferredUnloadCount = this.deferredUnloads.size();
		this.deferredUnloads.clear();
		super.debug("Cleared $1 deferred unload$2.", deferredUnloadCount, Helpers.getPlural(deferredUnloadCount));
	}
	
	public void addCacheSource(final String source, final Collection<Chunk> chunks)
	{
		for (final Chunk chunk: chunks) this.addCacheSource(source, chunk);
		this.chunkCountMap.put(source, chunks.size());
	}
	
	public void addCacheSource(final String source, final Chunk chunk)
	{
		final int hashCode = chunk.hashCode();
		if (this.chunkSourceMap.containsKey(hashCode))
		{
			final String otherSource = this.chunkSourceMap.get(hashCode);
			if (!source.equalsIgnoreCase(otherSource))
				this.chunkSourceMap.put(hashCode, this.chunkSourceMap.get(hashCode) + "@" + source);
		}
		else this.chunkSourceMap.put(hashCode, source);
		
		this.chunkCountMap.put(source, 1);
	}

	public String[] getChunkCacheSources(final Chunk chunk)
	{
		String[] sources = null;
		final int hashCode = chunk.hashCode();
		if (this.chunkSourceMap.containsKey(hashCode))
			sources = this.chunkSourceMap.get(hashCode).split("@");
		return sources;
	}

	public boolean tryAddSource(final String source)
	{
		boolean added = false;
		if (!this.cacheSources.contains(source))
		{
			this.cacheSources.add(source);
			added = true;
		}
		return added;
	}
	
	public void removeCacheSourceByIndex(final int index)
	{
		if (index >= 0 && index < this.cacheSources.size())
			this.cacheSources.remove(index);		
	}
	
	public String getCacheSourceByIndex(final int index)
	{
		String source = "";
		if (index >= 0 && index < this.cacheSources.size())
			source = this.cacheSources.get(index);
		return source;
	}
	
	public void clearCacheSources()
	{
		this.cacheSources.clear();
	}
}
