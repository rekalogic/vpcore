package com.rekalogic.vanillapod.vpcore.chunk;

import org.bukkit.Chunk;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.ItemStack;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;

public final class ChunkEventHandler extends EventHandlerBase<ChunkSettings>
{
	public ChunkEventHandler(final PluginBase plugin, final ChunkSettings settings)
	{
		super(plugin, settings);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onChunkUnload(final ChunkUnloadEvent event)
	{
		if (event.isCancelled()) return;
		
		final Chunk chunk = event.getChunk();
				
		if (super.getSettings().isCached(chunk))
		{
			event.setCancelled(true);
			return;
		}
		
		final long lastDeferredUntil = super.getSettings().getDeferredUntil(chunk);
		if (lastDeferredUntil > System.currentTimeMillis())
		{
			super.debug("DEFERRED: $1 (existing)", chunk);
			event.setCancelled(true);
			return;
		}
		
		if (lastDeferredUntil > 0)
		{
			super.debug("UNDEFERRED: $1 (expired)", chunk);
			super.getSettings().undeferUnload(chunk);
			return;
		}
		
		final Entity[] entities = chunk.getEntities();
		boolean deferUnload = (entities.length >= super.getSettings().getChunkKeepEntityLevel());
		boolean forceUnload = false;
		for (final Entity entity: entities)
		{
			switch (entity.getType())
			{
			case DROPPED_ITEM:
				final ItemStack stack = ((Item)entity).getItemStack();
				switch (stack.getType())
				{
				case EGG:
					break;
					
				default:
					super.debug("Found a $1 in chunk $2 at $3.", entity.getType(), chunk, entity.getLocation());
					deferUnload = true;
					break;
				}
				break;
			
			//  Minecarts force unload
			case MINECART:
			case MINECART_CHEST:
			case MINECART_COMMAND:
			case MINECART_FURNACE:
			case MINECART_HOPPER:
			case MINECART_MOB_SPAWNER:
			case MINECART_TNT:
				forceUnload = true;
				break;
				
			//  Tamed entities defer an unload
			case HORSE:
			case WOLF:
			case OCELOT:
				deferUnload = (entity instanceof Tameable && ((Tameable)entity).isTamed());						
				break;
				
			//  Hitch => possible nearby horse; defer unload
			case LEASH_HITCH:
				deferUnload = true;
				break;

			//  Villager => possible village; could be expensive to reload, so defer
			case VILLAGER:
				deferUnload = true;
				break;

			default: break;
			}
			
			if (deferUnload || forceUnload) break;
		}
			
		if (deferUnload && !forceUnload)
		{
			super.debug("DEFERRED $1.", chunk);
			super.getSettings().deferUnload(chunk);
			event.setCancelled(true);
		}
	}
}
