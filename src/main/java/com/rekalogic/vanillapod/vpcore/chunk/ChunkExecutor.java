package com.rekalogic.vanillapod.vpcore.chunk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public final class ChunkExecutor extends ExecutorBase<ChunkSettings>
{
	public ChunkExecutor(final PluginBase plugin, final ChunkSettings settings)
	{
		super(plugin, settings,
				"cachechunk", "cacheflush", "cachelist", "cachequery", "cacheremove",
				"cachestatus", "debugchunk", "reportchunks");
	}

	@Override
	protected boolean checkCommandAllowed(final String command, final boolean isConsole)
	{
		switch (command)
		{
		case "cachequery": return (!isConsole);
		}
		
		return super.checkCommandAllowed(command, isConsole);
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "cachechunk": return (argCount >= 1 && argCount <= 3);
		case "cacheremove": return (argCount == 1);
		case "debugchunk": return (argCount == 0 || argCount == 2);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "cachechunk": return new String[] { ExecutorBase.FLAG_WORLD };
		case "debugchunk": return new String[] { ExecutorBase.FLAG_BOOL_LOAD, ExecutorBase.FLAG_WORLD };
		}
		
		return super.getAllowedFlags(command);
	}

	@Override
	public boolean onCommand(final PagedOutputCommandSender sender, final String command, final String shortcut, final String[] args)
	{
		switch (command)
		{
		case "cachechunk": return this.doCacheCommand(sender, args);
		case "cacheflush": return this.doCacheFlush(sender, args);
		case "cachelist": return this.doListCommand(sender, args);
		case "cacheremove": return this.doRemoveCommand(sender, args);
		case "cachestatus": return this.doStatusCommand(sender, args);
		case "cachequery": return this.doQueryCommand(sender, args);
		case "debugchunk": return this.doDebugChunk(sender, args);
		case "reportchunks": return this.doReportChunks(sender, args);
		}

		return false;
	}

	private boolean doCacheFlush(final PagedOutputCommandSender sender, final String[] args)
	{
		sender.sendNotification("Flushing unload cache...");
		super.getSettings().flushCache();
		sender.sendSuccess("Flushed cache.");
		return true;
	}

	private boolean doReportChunks(final PagedOutputCommandSender sender, final String[] args)
	{
		List<World> worlds = null;
		if (super.hasFlag(ExecutorBase.FLAG_WORLD))
		{
			worlds = new ArrayList<World>();
			worlds.add(super.getWorld(sender));
		}
		else worlds = Bukkit.getWorlds();

		final Map<Chunk, Integer> entitiesByChunk = new HashMap<Chunk, Integer>();
		int chunkCount = 0;
		int entityCount = 0;
		for (final World world: worlds)
		{
			for (final Chunk chunk: world.getLoadedChunks())
			{
				++chunkCount;
				final int entities = chunk.getEntities().length;
				if (entities > 0)
				{
					entitiesByChunk.put(chunk, entities);
					entityCount += entities;
				}
			}
		}

		final Chunk[] chunks = entitiesByChunk.keySet().toArray(new Chunk[0]);
		Arrays.sort(
				chunks,
				new Comparator<Chunk>()
				{
					@Override
					public int compare(final Chunk c1, final Chunk c2)
					{
						return Integer.compare(entitiesByChunk.get(c2), entitiesByChunk.get(c1));
					}
				});


		sender.sendInfo("Total chunks: $1", chunkCount);
		sender.sendInfo("Chunks with entities: $1", chunks.length);
		sender.sendInfo("Total entities: $1", entityCount);

		int index = 0;
		for (final Chunk chunk: chunks)
		{
			final int entities = chunk.getEntities().length;
			sender.sendInfoListItem(index, "Chunk $1: $2 entities", chunk, entities);
			if (super.getSettings().isCached(chunk)) sender.sendSuccess("Chunk $1 is cached.", chunk);
			++index;
		}

		return true;
	}

	private boolean doDebugChunk(final PagedOutputCommandSender sender, final String[] args)
	{
		Chunk chunk = null;
		boolean playerChunk = false;
		boolean wasLoaded = false;
		if (args.length == 0)
		{
			if (sender.isConsole()) sender.sendFailure("Cannot automatically get chunk for CONSOLE/REMOTE.");
			else chunk = sender.getLocation().getChunk();
			playerChunk = true;
		}
		else
		{
			final World world = sender.getWorld();
			if (world != null)
			{
				int x = 0;
				try { x = Integer.parseInt(args[0]); }
				catch (final Exception ex)
				{
					sender.sendFailure("Invalid value $1 for X coordinate.", args[0]);
					return false;
				}

				int z = 0;
				try { z = Integer.parseInt(args[1]); }
				catch (final Exception ex)
				{
					sender.sendFailure("Invalid value $1 for Z coordinate.", args[1]);
					return false;
				}

				if (world.isChunkLoaded(x, z)) chunk = world.getChunkAt(x, z);
				else
				{
					if (super.getFlagState(ExecutorBase.FLAG_BOOL_LOAD))
					{
						if (!world.loadChunk(x, z, false))
						{
							sender.sendFailure("Could not load chunk at ($1,$2) because is it not generated.", x, z);
							return true;
						}
						wasLoaded = true;
					}
					else
					{
						sender.sendFailure("Chunk at ($1,$2) is not loaded; use flag $3 to force load.", x, z, ExecutorBase.FLAG_BOOL_LOAD);
						return true;
					}
				}

				if (sender.isPlayer())
					playerChunk = (chunk.hashCode() == sender.getLocation().getChunk().hashCode());
			}
		}

		boolean success = false;
		if (chunk != null)
		{
			final Entity[] entities = chunk.getEntities();
			final Map<EntityType, Integer> entityCounts = new HashMap<EntityType, Integer>();

			if (playerChunk) entityCounts.put(EntityType.PLAYER, -1);

			for (final Entity entity: entities)
			{
				final EntityType type = entity.getType();
				if (!entityCounts.containsKey(type)) entityCounts.put(type, 0);
				entityCounts.put(type, entityCounts.get(type) + 1);
			}

			final int minBlockX = chunk.getX() * 16;
			int maxBlockX = minBlockX;
			if (minBlockX < 0) maxBlockX -= 15;
			else maxBlockX += 15;
			final int minBlockZ = chunk.getZ() * 16;
			int maxBlockZ = minBlockZ;
			if (minBlockZ < 0) maxBlockZ -= 15;
			else maxBlockZ += 15;

			sender.sendNotification("Debug info for chunk $1:", chunk);
			if (wasLoaded) sender.sendInfo(ChatColor.GREEN, "Chunk was loaded into memory for this operation.");
			sender.sendInfo("X coordinate in range: $1 to $2", minBlockX, maxBlockX);
			sender.sendInfo("Z coordinate in range: $1 to $2", minBlockZ, maxBlockZ);

			if (super.getSettings().isCached(chunk))
			{
				final String[] sources = super.getSettings().getChunkCacheSources(chunk);
				sender.sendInfo("Cached by the following source$3:", chunk.getX(), chunk.getZ(), Helpers.getPlural(sources.length));
				for (int i = 0; i < sources.length; ++i) sender.sendInfoListItem(i, sources[i]);
			}
			else sender.sendInfo("This chunk is not cached.");

			int listNumber = 0;
			for (final EntityType type: entityCounts.keySet())
			{
				final int count = entityCounts.get(type);
				if (count > 0)
				{
					if (listNumber == 0) sender.sendInfo("Entities in this chunk:");
					sender.sendInfoListItem(listNumber, "$1: $2", type, count);
					++listNumber;
				}
			}

			if (listNumber == 0) sender.sendInfo("No entities in this chunk.");

			success = true;
		}

		return success;
	}

	private boolean doCacheCommand(final PagedOutputCommandSender sender, final String[] args)
	{
		if (!super.getPlugin().isWorldGuardEnabled()) sender.sendFailure("WorldGuard support is not enabled.");

		final String cacheType = args[0].toLowerCase();
		World world = null;
		switch (cacheType)
		{
		case "wg":
		case "chunk":
		case "block":
			world = super.getWorld(sender);
			if (world == null) return true;
			break;
		}

		switch (cacheType)
		{
		case "wg":
			if (world != null)
			{
				ChunkExecutor.addWorldGuardRegion(super.getSettings(), sender, world, args[1], true);
				super.getSettings().save();
			}
			break;

		case "chunk":
		case "block":
			if (args.length != 2) return false;

			int chunkX = 0;
			int chunkZ = 0;

			boolean validInput = (world != null);

			if (validInput) try
			{
				chunkX = Integer.parseInt(args[1]);
			}
			catch (final NumberFormatException e)
			{
				validInput = false;
			}

			if (!validInput) sender.sendFailure("Invalid $1 X coordinate: $2.", cacheType, args[1]);
			else try
			{
				chunkZ = Integer.parseInt(args[2]);
			}
			catch (final NumberFormatException e)
			{
				validInput = false;
			}

			if (!validInput) sender.sendFailure("Invalid $1 Z coordinate: $2.", cacheType, args[2]);
			else
			{
				Chunk chunk = null;
				switch (cacheType)
				{
				case "chunk":
					chunk = world.getChunkAt(chunkX, chunkZ);
					break;
				case "block":
					final Block block = world.getBlockAt(chunkX, 0, chunkZ);
					chunk = world.getChunkAt(block);
					break;
				}

				if (chunk != null)
				{
					ChunkExecutor.addSingleChunk(super.getSettings(), sender, chunk, true);
					super.getSettings().save();
				}
			}
			break;
		case "here":
			if (ExecutorBase.isSenderConsole(sender, true)) return true;
			if (super.hasFlag(ExecutorBase.FLAG_WORLD, true, sender)) return false;

			final Player player = sender.getPlayer();
			final Chunk chunk = player.getWorld().getChunkAt(player.getLocation());
			ChunkExecutor.addSingleChunk(super.getSettings(), sender, chunk, true);
			super.getSettings().save();
			break;

		default:
			sender.sendFailure("The cache type $1 is invalid.", cacheType);
			break;
		}
		return true;
	}

	private boolean doListCommand(final PagedOutputCommandSender sender, final String[] args)
	{
		if (!super.getSettings().hasCacheSources()) sender.sendNotification("No sources defined.");
		else
		{
			sender.sendNotification("Chunk caching sources configured ($1):", super.getSettings().getCacheSourcesSize());
			int index = 0;
			for (final String source: super.getSettings().getCacheSources())
			{
				sender.sendInfoListItem(index, super.getSettings().getCacheSourceNameForDisplay(source));
				++index;
			}
			final int chunkMapSize = super.getSettings().getChunkMapSize(); 
			sender.sendNotification("Total: $1 chunk$2.", chunkMapSize, Helpers.getPlural(chunkMapSize));
		}
		return true;
	}

	private boolean doRemoveCommand(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args[0].equalsIgnoreCase("all"))
		{
			super.getSettings().clearCacheSources();
			sender.sendSuccess("Removed all sources.");
			super.getSettings().save();
		}
		else
		{
			int id = 0;
			try
			{
				id = Integer.parseInt(args[0]);
			}
			catch (final NumberFormatException e)
			{
				id = 0;
			}

			--id;
			if (id >= 0 && id < super.getSettings().getCacheSourcesSize())
			{
				final String source = super.getSettings().getCacheSourceByIndex(id);
				super.getSettings().removeCacheSourceByIndex(id);
				sender.sendSuccess("Removed source: $1.", source);
				super.getSettings().save();
			}
			else sender.sendFailure("Invalid or out of range source ID; please refer to /chunk list.");
		}
		return true;
	}

	private boolean doStatusCommand(final PagedOutputCommandSender sender, final String[] args)
	{
		final long upTime = System.currentTimeMillis() - super.getSettings().getStartMillis();
		sender.sendNotification("Chunks cached: $1.", super.getSettings().getChunkMapSize());
		sender.sendNotification("Server startup: $1 ago.", Helpers.DateTime.getHumanReadableFromMillis(upTime));

		final long totalUnloadsDeferred = super.getSettings().getTotalUnloadsDeferred();
		sender.sendNotification("Unloads deferred: $1 (of $2 total).", super.getSettings().getUnloadsDeferred(), totalUnloadsDeferred);

		final double hoursUp = (double)upTime / Helpers.DateTime.MillisecondsInAnHour;
		sender.sendNotification("Average rate: $1/hr.", Math.round((double)totalUnloadsDeferred / hoursUp));
		return true;
	}

	private boolean doQueryCommand(final PagedOutputCommandSender sender, final String[] args)
	{
		final World world = sender.getWorld();
		final Location location = sender.getPlayer().getLocation();
		final Chunk chunk = world.getChunkAt(location);
		if (super.getSettings().isCached(chunk))
		{
			final String[] sources = super.getSettings().getChunkCacheSources(chunk);
			sender.sendNotification("Chunk $1 is cached by the following source$2:", chunk, Helpers.getPlural(sources.length));
			for (final String s: sources) sender.sendInfo("    $1", s);
		}
		else
		{
			final long deferredUntil = super.getSettings().getDeferredUntil(chunk);
			if (deferredUntil == 0) sender.sendNotification("Chunk $1 is neither cached nor unload-deferred.", chunk);
			else
			{
				final long now = System.currentTimeMillis();
				if (deferredUntil <= now) sender.sendNotification("Chunk $1 has an expired deferred unload.", chunk);
				else sender.sendNotification(
						"Chunk $1 is unload-deferred for further $2.",
						chunk,
						Helpers.DateTime.getHumanReadableFromMillis(deferredUntil - now));
			}
		}
		return true;
	}

	public static void addWorldGuardRegion(final ChunkSettings settings, final PagedOutputCommandSender sender, final String worldName, final String regionName, final boolean saveToConfig)
	{
		final World world = sender.getWorld(worldName);
		if (world != null) ChunkExecutor.addWorldGuardRegion(settings, sender, world, regionName, saveToConfig);
		else sender.sendFailure(Helpers.Parameters.replace("Could not find world '$1'.", worldName));
	}

	public static void addWorldGuardRegion(final ChunkSettings settings, final PagedOutputCommandSender sender, final World world, final String regionName, final boolean saveToConfig)
	{
		final RegionManager regionManager = WGBukkit.getRegionManager(world);
		if (regionManager != null && regionManager.hasRegion(regionName))
		{
			final ProtectedRegion region = regionManager.getRegion(regionName);
			ChunkExecutor.addWorldGuardRegion(settings, sender, world, region, saveToConfig);
		}
		else sender.sendFailure("WorldGuard could not find region $1 in world $2.", regionName, world.getName());
	}

	public static void addWorldGuardRegion(final ChunkSettings settings, final PagedOutputCommandSender sender, final World world, final ProtectedRegion region, final boolean addToSources)
	{
		final String source = Helpers.Parameters.replace("wg $1 $2", world.getName(), region.getId());

		sender.sendNotification("Adding region $1 to cache list.", region.getId());

		if (region instanceof ProtectedPolygonalRegion)
			sender.sendInfo("Region $1 is polygonal; chunk(s) outside region may be cached.", region.getId());

		final BlockVector minPoint = region.getMinimumPoint();
		final BlockVector maxPoint = region.getMaximumPoint();

		sender.sendInfo("Region spans from ($1,$2) to ($3,$4).", minPoint.getBlockX(), minPoint.getBlockZ(), maxPoint.getBlockX(), maxPoint.getBlockZ());

		final Map<Integer, Chunk> chunks = new HashMap<Integer, Chunk>();
		for (int x = minPoint.getBlockX(); x <= maxPoint.getBlockX(); x += 16)
		{
			for (int z = minPoint.getBlockZ(); z <= maxPoint.getBlockZ(); z += 16)
			{
				final Block block = world.getBlockAt(x, 0, z);
				final Chunk chunk = block.getChunk();
				if (!chunks.containsKey(chunk.hashCode())) chunks.put(chunk.hashCode(), chunk);
			}
		}

		ChunkExecutor.enrolChunks(settings, sender, chunks.values());
		settings.addCacheSource(source, chunks.values());

		if (addToSources)
		{
			if (!settings.tryAddSource(source)) sender.sendFailure("Cache source $1 already exists.", source);
		}
	}

	public static void addSingleChunk(final ChunkSettings settings, final PagedOutputCommandSender sender, final String worldName, final String xCoord, final String zCoord, final boolean addToSources)
	{
		final World world = sender.getWorld(worldName);
		if (world != null)
		{
			int x = 0;
			int z = 0;

			boolean validInput = true;
			try
			{
				x = Integer.parseInt(xCoord);
				z = Integer.parseInt(zCoord);
			}
			catch (final NumberFormatException e)
			{
				sender.sendError("One or more coordinates not valid: [$1] and [$2].", xCoord, zCoord);
				validInput = false;
			}

			if (validInput)
			{
				final Chunk chunk = world.getChunkAt(x, z);
				ChunkExecutor.addSingleChunk(settings, sender, chunk, addToSources);
			}
		}
	}

	public static void addSingleChunk(final ChunkSettings settings, final PagedOutputCommandSender sender, final Chunk chunk, final boolean addToSources)
	{
		final String source = "chunk " + chunk.getWorld().getName() + " " + chunk.getX() + " " + chunk.getZ();

		sender.sendNotification("Adding chunk at ($1,$2) in $3 to cache list.", chunk.getX(), chunk.getZ(), chunk.getWorld().getName());

		ChunkExecutor.enrolChunk(settings, sender, chunk);
		settings.addCacheSource(source, chunk);

		if (addToSources)
		{
			if (!settings.tryAddSource(source))
				sender.sendFailure("Cache source $1 already exists.", source);
		}
	}

	private static void enrolChunks(final ChunkSettings settings, final PagedOutputCommandSender sender, final Collection<Chunk> chunks)
	{
		int addedCount = 0;
		int loadedCount = 0;
		
		Environment ignoredChunk = Environment.NORMAL;
		
		for (final Chunk chunk: chunks)
		{
			if (chunk.getWorld().getEnvironment() == Environment.NORMAL)
			{
				if (settings.tryAddToChunkMap(chunk))
				{
					if (ChunkExecutor.loadChunk(sender, chunk)) ++loadedCount;
					++addedCount;
				}
			}
			else ignoredChunk = chunk.getWorld().getEnvironment();
		}
		
		if (ignoredChunk != Environment.NORMAL)
			sender.sendFailure("One ore more chunks ignored ($1).", ignoredChunk);

		final int alreadyCached = chunks.size() - addedCount;
		if (alreadyCached == 0) sender.sendSuccess("Chunks cached: $1.", addedCount);
		else sender.sendSuccess("Chunks cached: $1 of $2 ($3 already cached).", addedCount, chunks.size(), alreadyCached);
		sender.sendSuccess("Chunks loaded: $1 ($2 already in memory).", loadedCount, addedCount - loadedCount);
	}

	private static boolean enrolChunk(final ChunkSettings settings, final PagedOutputCommandSender sender, final Chunk chunk)
	{
		boolean added = false;
		if (chunk.getWorld().getEnvironment() != Environment.NORMAL)
		{
			sender.sendFailure("Chunk $1 ignored: $2", chunk, chunk.getWorld().getEnvironment());
		}
		else if (settings.tryAddToChunkMap(chunk))
		{
			final boolean loaded = ChunkExecutor.loadChunk(sender, chunk);
			if (loaded) sender.sendSuccess("Chunk added and loaded.");
			else sender.sendSuccess("Chunk added, already loaded.");
			
			added = true;
		}
		else sender.sendFailure("Chunk $1 already in cache.", chunk);

		return added;
	}

	private static boolean loadChunk(final PagedOutputCommandSender sender, final Chunk chunk)
	{
		boolean loaded = false;
		if (!chunk.isLoaded())
		{
			if (chunk.load(true)) loaded = true;
			else sender.sendFailure(Helpers.Parameters.replace("Chunk could not be loaded at ($1,$2).", chunk.getX(), chunk.getZ()));
		}

		return loaded;
	}
}
