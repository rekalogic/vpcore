package com.rekalogic.vanillapod.vpcore.censor;

public final class CensoredWord
{
	private final String word;
	private final String behaviour;
	
	public String getWord() { return this.word; }
	public String getBehaviour() { return this.behaviour; }
	
	public CensoredWord(final String word, final String behaviour)
	{
		this.word = word;
		this.behaviour = behaviour;
	}
}
