package com.rekalogic.vanillapod.vpcore.censor;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.ban.BanSettings;
import com.rekalogic.vanillapod.vpcore.player.PlayerSettings;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class CensorSettings extends SettingsBase
{
	private final CensorDbInterface dbInterface;
	
	private PlayerSettings playerSettings = null;
	private BanSettings banSettings = null;

	private int sessionCensorLimit = 0;
	private int repeatMessageLimit = 0;
	private String punishmentType = "";
	private long punishmentDuration = 0;
	private String censorPunishmentMessage = "";
	private String spamPunishmentMessage = "";
	private String[] censoredWords = null;
	
	private final static int WordReplacementListLength = 1000;
	private String[] wordReplacementList = null;
	private int wordReplacementListIndex = 0;

	private final Map<UUID, Integer> sessionCensorCount = new ConcurrentHashMap<UUID, Integer>();
	
	private final Map<UUID, String> lastMessages = new ConcurrentHashMap<UUID, String>();
	private final Map<UUID, Integer> repeatMessageCount = new ConcurrentHashMap<UUID, Integer>();
	private final Map<String, String> censoredWordBehaviour = new ConcurrentHashMap<String, String>();
	
	public CensorSettings(VPPlugin plugin)
	{
		super(plugin);
		
		this.dbInterface = new CensorDbInterface(plugin);
	}
	
	public void setPlayerSettings(final PlayerSettings settings)
	{
		this.playerSettings = settings;
	}
	
	public void setBanSettings(final BanSettings settings)
	{
		this.banSettings = settings;
	}
	
	@Override
	protected void onLoad()
	{
		this.sessionCensorLimit = super.getInt("session-limit");
		this.repeatMessageLimit = super.getInt("repeat-message-limit");
		this.punishmentType = super.getString("punishment.type").toUpperCase();
		this.punishmentDuration = Helpers.DateTime.getMillis(super.getString("punishment.duration"));
		this.censorPunishmentMessage = super.getString("punishment.censor-message");
		this.spamPunishmentMessage = super.getString("punishment.spam-message");
	}

	@Override
	protected void onSave()
	{
		super.set("session-limit", this.sessionCensorLimit);
		super.set("repeat-message-limit", this.repeatMessageLimit);
		super.set("punishment.type", this.punishmentType);
		super.set("punishment.duration", Helpers.DateTime.getHumanReadableFromMillis(this.punishmentDuration));
		super.set("punishment.censor-message", this.censorPunishmentMessage);
		super.set("punishment.spam-message", this.spamPunishmentMessage);
	}
	
	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading censored phrases...");
		
		final CensoredWord[] words = this.dbInterface.getCensoredWords();
		this.censoredWords = new String[words.length];
		for (int i = 0; i < words.length; ++i)
		{
			final String word = words[i].getWord().toLowerCase();
			this.censoredWords[i] = word;
			this.censoredWordBehaviour.put(word, words[i].getBehaviour().toLowerCase());
		}

		Arrays.sort(
				this.censoredWords,
				new Comparator<String>()
				{
					@Override
					public int compare(final String o1, final String o2)
					{
						return Integer.compare(o2.length(), o1.length());
					}
				});
		
		super.info("Loading replacement words...");
		final String replacementWords[] = this.dbInterface.getReplacementWordList();
		this.wordReplacementList = new String[CensorSettings.WordReplacementListLength];
		for (int i = 0; i < CensorSettings.WordReplacementListLength; ++i)
		{
			final int index = super.getPlugin().getRandom().nextInt(replacementWords.length);
			final String word = replacementWords[index];
			
			if (word.length() > 0) this.wordReplacementList[i] = ChatColor.BOLD + word + ChatColor.RESET;
		}		

		super.onLoadDataStore();
	}
	
	@Override
	protected void onEmitInfo()
	{
		super.info("Censored phrases: $1", this.censoredWords.length);
		super.info("Max censored chats before punishment: $1", this.sessionCensorLimit);
		super.info("Max repeated censored chats before punishment: $1", this.repeatMessageLimit);
		
		switch (this.punishmentType.toLowerCase())
		{
		case "mute":
		case "tempban":
			super.info(
					"Punishment type: $1 (for $2)", 
					this.punishmentType,
					Helpers.DateTime.getHumanReadableFromMillis(this.punishmentDuration));
			break;
			
		case "kick":
		case "ban":
			super.info("Punishment type: $1", this.punishmentType);
			break;
			
		default:
			super.severe("Unsupported censor punishment type '$1'.", this.punishmentDuration);
			break;
		}
		
		super.info("Censor message: $1", this.censorPunishmentMessage);
		super.info("Spam message: $1", this.spamPunishmentMessage);
	}

	public String censorChat(final Player player, final String message)
	{
		if (this.censoredWords == null ||
				this.censoredWords.length == 0) return message;
					
		final boolean shouldCensor = player.hasPermission("vpcore.chat.censor") && !player.isOp();
		if (!shouldCensor) return message;
		
		final String cleanedMessage = message.toLowerCase().replaceAll("[^a-z]", "");
		String censoredMessage = message;
		boolean wasCensored = false;
			
		super.debug("Cleaned: $1", cleanedMessage);
		
		for (final String censoredWord: this.censoredWords)
		{
			if (cleanedMessage.contains(censoredWord))
			{
				if (censoredWord.length() == 0) continue;
				
				boolean isBanBehaviour = false;
				boolean isCensorBehaviour = false;
				boolean isKickBehaviour = false;

				final String behaviour = this.censoredWordBehaviour.get(censoredWord); 
				switch (behaviour)
				{
				case "ban": isBanBehaviour = true; break;
				case "censor": isCensorBehaviour = true; break;
				case "disconnect": isKickBehaviour = true; break;
				case "kick": isKickBehaviour = true; break;
				}
				
				if (isBanBehaviour)
				{
					final TaskBase banTask = new TaskBase(super.isDebug(), "CensorBanTask")
					{
						@Override
						protected void runTask()
						{
							final String reason = "chat content";
							banSettings.addBan(player, null, 0, reason);
							Helpers.Broadcasts.sendAll("$1 has been banned ($2).", player, reason);
						}
					};
					banTask.runTask(super.getPlugin());
					
					return null;
				}
				else if (isCensorBehaviour)
				{
					wasCensored = true;
					while (censoredMessage.toLowerCase().contains(censoredWord))
					{
						final String replacement = this.getCensorString(censoredWord);
						final int index = censoredMessage.toLowerCase().indexOf(censoredWord);
						final String before = censoredMessage.substring(0, index);
						final String after = censoredMessage.substring(index + censoredWord.length(), censoredMessage.length());
						censoredMessage = before + replacement + after;
					}
				}
				else if (isKickBehaviour)
				{
					final TaskBase kickTask = new TaskBase(super.isDebug(), "CensorKickTask")
					{
						@Override
						protected void runTask()
						{
							player.kickPlayer("You have been disconnected due to chat content.");
							Helpers.Broadcasts.sendAll("$1 has been kicked ($2).", player, "chat content");
							super.info("$1 was kicked for saying: $2", player, message);
						}
					};
					kickTask.runTask(super.getPlugin());
					
					return null;
				}
			}
		}
		
		final UUID uuid = player.getUniqueId();
		final String lastMessage = this.lastMessages.get(uuid);
		if (message.equalsIgnoreCase(lastMessage))
		{
			final int messageCount = this.repeatMessageCount.get(uuid) + 1;
			this.repeatMessageCount.put(uuid, messageCount);
			if (messageCount >= this.repeatMessageLimit)
			{
				this.enactSpamPunishment(player);
				censoredMessage = null;
			}
			else Helpers.Messages.sendNotification(player, "WARNING: Chat spam is not tolerated.");
		}
		else
		{
			this.lastMessages.put(uuid, message);
			this.repeatMessageCount.put(uuid, 1);
		}
		
		if (wasCensored)
		{
			super.info("$1 was censored for saying: $2", player, message);
			final int censorCount = this.sessionCensorCount.get(uuid) + 1;
			this.sessionCensorCount.put(uuid, censorCount);
			if (censorCount >= this.sessionCensorLimit)
			{
				super.info("$1 exceeded the session censor limit ($2).", player, this.sessionCensorLimit);
				this.enactCensorPunishment(player);
				censoredMessage = null;
			}
			else Helpers.Messages.sendNotification(player, "WARNING: Offensive language is not tolerated.");
		}

		return censoredMessage;
	}
	
	private String getCensorString(final String word)
	{
		if (this.wordReplacementListIndex >= CensorSettings.WordReplacementListLength)
			this.wordReplacementListIndex = 0;
		final String censorString = this.wordReplacementList[this.wordReplacementListIndex];
		++this.wordReplacementListIndex;
		return censorString;
	}
	
	private void enactSpamPunishment(final Player player) { this.enactPunishment(player, "chat spam", this.spamPunishmentMessage); }
	private void enactCensorPunishment(final Player player) { this.enactPunishment(player, "bad language", this.censorPunishmentMessage); }
	private void enactPunishment(final Player player, final String reason, final String message)
	{
		Helpers.Messages.sendNotification(player, message);
		
		final TaskBase task = new TaskBase(super.isDebug(), "DeletePlayerFiles")
		{
			@Override
			public void runTask()
			{
				switch (punishmentType.toLowerCase())
				{
				case "ban":
					banSettings.addBan(player, null, 0, message);
					Helpers.Broadcasts.sendAll("$1 has been banned ($2).", player, reason);
					break;
				case "kick":
					player.kickPlayer(message);
					Helpers.Broadcasts.sendAll("$1 has been kicked ($2).", player, reason);
					break;
				case "mute":
					playerSettings.mute(player, punishmentDuration);
					Helpers.Broadcasts.sendAll(
							"$1 has been muted ($2) for $3.",
							player,
							reason,
							Helpers.DateTime.getHumanReadableFromMillis(punishmentDuration));
					break;
				case "tempban":
					banSettings.addBan(player, null, punishmentDuration, message);
					Helpers.Broadcasts.sendAll(
							"$1 has been temp-banned ($2) for $3.",
							player,
							reason,
							Helpers.DateTime.getHumanReadableFromMillis(punishmentDuration));
					break;
				}
						
				resetSession(player);
			}
		};
		task.runTask(super.getPlugin());
	}

	public void resetSession(final OfflinePlayer player)
	{
		final UUID uuid = player.getUniqueId();
		this.sessionCensorCount.put(uuid, 0);
		this.lastMessages.put(uuid, "");
		this.repeatMessageCount.put(uuid, 0);
	}
}
