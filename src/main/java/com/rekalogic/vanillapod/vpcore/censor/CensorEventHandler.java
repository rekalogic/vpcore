package com.rekalogic.vanillapod.vpcore.censor;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;

public final class CensorEventHandler extends EventHandlerBase<CensorSettings>
{
	public CensorEventHandler(final PluginBase plugin, final CensorSettings settings)
	{
		super(plugin, settings);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	private void onAsyncPlayerChat(final AsyncPlayerChatEvent event)
	{
		if (event.isCancelled()) return;

		final Player player = event.getPlayer();
		final String messageLower = event.getMessage().toLowerCase();
		if (messageLower.contains("hero") && messageLower.contains("brine"))
		{
			if (!player.hasPermission("vpcore.moderator"))
			{
				final TaskBase task = new TaskBase(super.isDebug(), "HerobrineKick")
				{
					@Override
					protected void runTask()
					{
						player.kickPlayer(Long.toString(System.currentTimeMillis()));
						super.info("Kicked $1 for talking about Herobrine!", player);
					}					
				};
				task.runTask(super.getPlugin());
				event.setCancelled(true);
				return;
			}
		}

		final String message = super.getSettings().censorChat(player, event.getMessage());
		if (message == null) event.setCancelled(true);
		else event.setMessage(message);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	private void onPlayerJoin(final PlayerJoinEvent event)
	{
		super.getSettings().resetSession(event.getPlayer());
	}
}
