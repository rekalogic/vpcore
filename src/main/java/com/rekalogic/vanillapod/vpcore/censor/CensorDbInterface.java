package com.rekalogic.vanillapod.vpcore.censor;

import java.util.ArrayList;
import java.util.List;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class CensorDbInterface extends DatabaseInterfaceBase
{
	public CensorDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}

	public CensoredWord[] getCensoredWords()
	{
		final List<CensoredWord> censoredWords = new ArrayList<CensoredWord>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT c.phrase, cb.behaviour " +
				"FROM vp_censor c, vp_censor_behaviour cb " +
				"WHERE c.behaviourid = cb.id");
		while (result.moveNext())
			censoredWords.add(
					new CensoredWord(
							result.getString("phrase"),
							result.getString("behaviour")));
		connection.close();
		return censoredWords.toArray(new CensoredWord[0]);
	}

	public String[] getReplacementWordList()
	{
		final List<String> censoredPhrases = new ArrayList<String>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT word FROM vp_censor_replacement");
		while (result.moveNext()) censoredPhrases.add(result.getString("word"));
		connection.close();
		return censoredPhrases.toArray(new String[0]);
	}
}
