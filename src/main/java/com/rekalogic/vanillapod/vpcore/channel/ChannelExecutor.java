package com.rekalogic.vanillapod.vpcore.channel;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class ChannelExecutor extends ExecutorBase<ChannelSettings>
{
	public ChannelExecutor(final PluginBase plugin, final ChannelSettings settings)
	{
		super(plugin, settings, new String[] { "channel" });
	}
	
	@Override
	protected boolean checkCommandAllowed(final String command, final boolean isConsole)
	{
		switch (command)
		{
		case "channel": return !isConsole;
		}

		return super.checkCommandAllowed(command, isConsole);
	}
	
	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "channel": return (argCount >= 0 && argCount <= 2);
		}

		return super.checkParameters(command, argCount);
	}
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "channel":
			switch (argIndex)
			{
			case 0: return LookupSource.FROM_PLUGIN;
			case 1:
				if (args[0].equalsIgnoreCase("join")) return LookupSource.FROM_PLUGIN;
				break;
			}
			break;
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}
	
	@Override
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "channel":
			switch (argIndex)
			{
			case 0: return Arrays.asList("join", "leave", "list");
			case 1: return super.getSettings().getKnownChannelNames();
			}
			break;
		}
		return super.onTabComplete(sender, command, argIndex, args);
	}
	
	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		switch (command)
		{
		case "channel":
			if (args.length == 0) return this.doChannel(sender);
			switch (args[0].toLowerCase())
			{
			case "join":
				if (args.length == 2) return this.doJoinChannel(sender, args[1]);
				return false;
			case "leave":
				if (args.length == 1) return this.doLeaveChannel(sender);
				return false;
			case "list":
				if (args.length == 1) return this.doListChannels(sender);
				return false;
				
			default: return false;
			}
		}
		
		return super.onCommand(sender, command, label, args);
	}

	private boolean doListChannels(PagedOutputCommandSender sender)
	{
		final List<String> channelNames = super.getSettings().getKnownChannelNames();
		if (channelNames.size() == 0) sender.sendNotification("There are no active channels.");
		else
		{
			sender.sendNotification("The following channel$1 $2 active:", Helpers.getPlural(channelNames), Helpers.getIsAre(channelNames));
			String channelList = "";
			for (final String channelName: channelNames)
			{
				if (channelList.length() > 0) channelList += ", ";
				channelList += channelName;
			}
			sender.sendInfo(channelList);
		}
		return true;
	}

	private boolean doChannel(final PagedOutputCommandSender sender)
	{
		final Player player = super.getPlayer(sender);
		if (player == null) return true;
		
		final String channel = super.getSettings().getChannel(player);
		if (channel == null) sender.sendNotification("You are not a member of a channel.");
		else
		{
			int playerCount = 0;
			String memberList = "";
			for (final Player onlinePlayer: Bukkit.getOnlinePlayers())
			{
				if (onlinePlayer.equals(player)) continue;
				if (!player.canSee(onlinePlayer)) continue;
				if (channel.equals(super.getSettings().getChannel(onlinePlayer)))
				{
					++playerCount;
					if (memberList.length() > 0) memberList += ", ";
					memberList += onlinePlayer.getDisplayName();
				}
			}
			if (playerCount == 0) sender.sendSuccess("You are in the $1 channel (alone).", channel);
			else
			{
				sender.sendSuccess(
						"You are in the $1 channel with $2 other player$3:",
						channel,
						playerCount,
						Helpers.getPlural(playerCount));				
				sender.sendInfo(memberList);
			}
		}
		
		return true;
	}

	private boolean doJoinChannel(final PagedOutputCommandSender sender, final String channelName)
	{
		final Player player = super.getPlayer(sender);
		if (player == null) return true;

		super.getSettings().joinChannel(player, channelName);
		
		return true;
	}

	private boolean doLeaveChannel(final PagedOutputCommandSender sender)
	{
		final Player player = super.getPlayer(sender);
		if (player == null) return true;

		super.getSettings().leaveChannel(player, true);
		
		return true;
	}
}
