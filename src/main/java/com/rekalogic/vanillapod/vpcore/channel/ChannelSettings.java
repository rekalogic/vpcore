package com.rekalogic.vanillapod.vpcore.channel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class ChannelSettings extends SettingsBase
{
	private final Map<String, Queue<UUID>> channelMembership = new ConcurrentHashMap<String, Queue<UUID>>();
	private final Map<String, String> channelNames = new ConcurrentHashMap<String, String>();
	private final Map<UUID, String> playerChannels = new ConcurrentHashMap<UUID, String>();
	
	private char trigger = '@';
	private String format;
	private String modifiedFormat;
	
	public char getTrigger() { return this.trigger; }
	public String getFormat() { return this.modifiedFormat; }
	
	public ChannelSettings(final VPPlugin plugin)
	{
		super(plugin);
	}

	@Override
	protected void onLoad()
	{
		final String channelTriggerString = super.getString("trigger");
		if (channelTriggerString.length() > 0)
			this.trigger = channelTriggerString.charAt(0);
		this.format = super.getString("format");
		
		this.modifiedFormat = Helpers.formatMessage(this.format);
	}

	@Override
	protected void onSave()
	{
		super.set("trigger", this.trigger);
		super.set("format", this.format);
	}
	
	@Override
	protected void onEmitInfo()
	{
		super.info(
				"Sample channel message: $1",
				Helpers.Parameters.replace(
						this.format,
						"ChannelName",
						"Test message 123"));
		super.info("Channel trigger: $1", this.trigger);
		
		super.onEmitInfo();
	}
	
	public Queue<UUID> getChannelMembers(final String channelName)
	{
		Queue<UUID> members = null;
		final String channelNameLower = channelName.toLowerCase();
		if (this.channelMembership.containsKey(channelNameLower))
			members = this.channelMembership.get(channelNameLower);
		return members;
	}
	
	public String getChannelName(final String channelName)
	{
		return this.channelNames.get(channelName.toLowerCase());
	}
	
	public String getChannel(final Player player) { return this.getChannel(player.getUniqueId()); }
	public String getChannel(final UUID uuid)
	{
		String channelName = null;
		if (this.playerChannels.containsKey(uuid))
		{
			final String channelNameLower = this.playerChannels.get(uuid);
			channelName = this.channelNames.get(channelNameLower);
		}
		return channelName;
	}
	
	public List<String> getKnownChannelNames()
	{
		final List<String> channelNames = new ArrayList<String>();
		for (final String channelName: this.channelNames.values())
			channelNames.add(channelName);
		return channelNames;
	}

	public void joinChannel(final Player player, final String channelName)
	{
		final UUID uuid = player.getUniqueId();
		final String currentChannelName = this.getChannel(uuid);
		if (channelName.equalsIgnoreCase(currentChannelName))
		{
			Helpers.Messages.sendFailure(player, "You're already in the $1 channel.", currentChannelName);
			return;
		}
		
		this.leaveChannel(player, false);
		final String channelNameLower = channelName.toLowerCase();
		Queue<UUID> members = null;
		String realChannelName;
		if (this.channelNames.containsKey(channelNameLower))
		{
			realChannelName = this.channelNames.get(channelNameLower);
			members = this.channelMembership.get(channelNameLower);
		}
		else
		{
			realChannelName = channelName;
			members = new ConcurrentLinkedQueue<UUID>();
			this.channelMembership.put(channelNameLower, members);
			this.channelNames.put(channelNameLower, realChannelName);
		}
		
		members.add(uuid);
		this.playerChannels.put(uuid, channelNameLower);
		
		boolean showNotification = false;
		for (final Player onlinePlayer: Bukkit.getOnlinePlayers())
		{
			if (members.contains(onlinePlayer.getUniqueId()) ||
					onlinePlayer.hasPermission("vpcore.monitor.channel"))
					showNotification = this.getShouldNotify(onlinePlayer, player); 
			if (showNotification)
				Helpers.Messages.sendNotification(onlinePlayer, "$1 has joined the $2 channel.", player, realChannelName);
		}

		Helpers.Messages.sendSuccess(
				player,
				"You have joined the $1 channel.",
				this.getChannelName(channelNameLower));
	}
	
	public void leaveChannel(final Player player, final boolean reportFailure)
	{
		final UUID uuid = player.getUniqueId();
		if (this.playerChannels.containsKey(uuid))
		{
			final String channelNameLower = this.playerChannels.get(uuid);
			if (this.channelNames.containsKey(channelNameLower))
			{
				final String realChannelName = this.getChannelName(channelNameLower);
				final Queue<UUID> members = this.channelMembership.get(channelNameLower);
				members.remove(uuid);
				if (members.size() == 0)
				{
					this.channelMembership.remove(channelNameLower);
					this.channelNames.remove(channelNameLower);
				}

				for (final Player onlinePlayer: Bukkit.getOnlinePlayers())
				{
					boolean showNotification = false;
					if (members.contains(onlinePlayer.getUniqueId()) ||
							onlinePlayer.hasPermission("vpcore.monitor.channel"))
							showNotification = this.getShouldNotify(onlinePlayer, player); 
					if (showNotification)
						Helpers.Messages.sendNotification(onlinePlayer, "$1 has left the $2 channel.", player, realChannelName);
				}

				Helpers.Messages.sendNotification(
						player,
						"You have left the $1 channel.",
						realChannelName);
			}
			
			this.playerChannels.remove(uuid);
		}
		else if (reportFailure) Helpers.Messages.sendFailure(player, "You have not joined a channel.");
	}
	
	private boolean getShouldNotify(final Player toNotify, final Player actor)
	{
		if (toNotify.equals(actor)) return false;
		else if (!toNotify.canSee(actor)) return false;
		else return true;
	}
}
