package com.rekalogic.vanillapod.vpcore.channel;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class ChannelEventHandler extends EventHandlerBase<ChannelSettings>
{
	public ChannelEventHandler(final PluginBase plugin, final ChannelSettings settings)
	{
		super(plugin, settings);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onAsyncPlayerChat(final AsyncPlayerChatEvent event)
	{
		if (event.isCancelled()) return;
		
		final Player player = event.getPlayer();
		final String fullMessage = event.getMessage();
		if (fullMessage.length() > 0 &&
				fullMessage.charAt(0) == super.getSettings().getTrigger() &&
				player.hasPermission("vpcore.command.channel"))
		{
			final String channelName = super.getSettings().getChannel(player.getUniqueId());
			if (channelName == null)
			{
				Helpers.Messages.sendFailure(player, "You have not joined a channel.");
				event.setCancelled(true);
			}
			else
			{
				event.setFormat(
						Helpers.Parameters.replace(
								super.getSettings().getFormat(),
								channelName,
								event.getFormat()));
				event.setMessage(fullMessage.substring(1));

				final Player[] recipients = event.getRecipients().toArray(new Player[0]);
				for (final Player recipient: recipients)
				{
					if (!recipient.hasPermission("vpcore.monitor.channel"))
						event.getRecipients().remove(recipient);
				}
				for (final UUID uuid: super.getSettings().getChannelMembers(channelName))
				{
					final Player nonChannelPlayer = Bukkit.getPlayer(uuid);
					event.getRecipients().add(nonChannelPlayer);					
				}
			}

			return;
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerQuit(final PlayerQuitEvent event)
	{
		super.getSettings().leaveChannel(event.getPlayer(), false);
	}
}
