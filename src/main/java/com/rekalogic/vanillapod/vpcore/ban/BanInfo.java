package com.rekalogic.vanillapod.vpcore.ban;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class BanInfo
{
	private final UUID uuid;
	private final String name;
	private final UUID bannedBy;
	private final long bannedOn;
	private final UUID unbannedBy;
	private final long unbannedOn;
	private final long expireOn;
	private final String reason;
	private final boolean active;
	private final int groupId;

	public BanInfo(
			final UUID uuid,
			final String name,
			final UUID bannedBy,
			final long expireOn,
			final String reason,
			final int groupId)
	{
		this(uuid, name, bannedBy, System.currentTimeMillis(), null, 0, expireOn, reason, true, groupId);
	}

	public BanInfo(
			final UUID uuid,
			final String name,
			final UUID bannedBy,
			final long bannedOn,
			final UUID unbannedBy,
			final long unbannedOn,
			final long expireOn,
			final String reason,
			final boolean active,
			final int groupId)
	{
		this.uuid = uuid;
		this.name = name;
		this.bannedBy = bannedBy;
		this.bannedOn = bannedOn;
		this.unbannedBy = unbannedBy;
		this.unbannedOn = unbannedOn;
		this.expireOn = expireOn;
		this.reason = reason;
		this.active = active;
		this.groupId = groupId;
	}

	public UUID getUuid() { return this.uuid; }
	public String getName() { return this.name; }
	public UUID getBannedBy() { return this.bannedBy; }
	public long getBannedOn() { return this.bannedOn; }
	public UUID getUnbannedBy() { return this.unbannedBy; }
	public long getUnbannedOn() { return this.unbannedOn; }
	public long getExpireOn() { return this.expireOn; }
	public String getReason() { return this.reason; }
	public boolean isActive() { return this.active; }
	public int getGroupId() { return this.groupId; }
	
	public String getBannedByName() { return this.getNameOfPlayer(this.bannedBy); }
	public String getUnbannedByName() { return this.getNameOfPlayer(this.unbannedBy); }
	
	private String getNameOfPlayer(UUID uuid)
	{
		if (uuid == null) return "CONSOLE";
		else
		{
			final OfflinePlayer player = Helpers.Bukkit.getOfflinePlayer(uuid);
			return player.getName();
		}
	}
	
	public boolean isPermanent() { return (this.getExpireOn() == 0); }
	public boolean isTemporary() { return (this.getExpireOn() > 0); }
	public boolean isExpired() { return (this.isTemporary() && this.getExpireOn() < System.currentTimeMillis()); }
}
