package com.rekalogic.vanillapod.vpcore.ban;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.OfflinePlayer;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class BanSettings extends SettingsBase
{
	private String defaultBanReason = "";
	private int phpbbBannedGroupId = 0;
	private long recentBansSince = 0; 
	private long recentBansCacheDuration = 0;
	private long recentBansRefresh = 0;

	private final BanDbInterface dbInterface;

	private final Map<UUID, BanInfo> bans = new ConcurrentHashMap<UUID, BanInfo>();
	private BanInfo[] recentBans = null;

	public String getDefaultBanReason() { return this.defaultBanReason; }
	public long getRecentBansSince() { return this.recentBansSince; }
	public long getRecentBansCacheDuration() { return this.recentBansCacheDuration; }

	public BanSettings(final VPPlugin plugin)
	{
		super(plugin);

		this.dbInterface = new BanDbInterface(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.defaultBanReason = super.getString("default-ban-reason");
		this.phpbbBannedGroupId = super.getInt("phpbb-banned-group-id");
		this.recentBansSince = Helpers.DateTime.getMillis(super.getString("recentbans.since"));
		this.recentBansCacheDuration = Helpers.DateTime.getMillis(super.getString("recentbans.cache"));

		this.dbInterface.setDebug(super.isDebug());
	}

	@Override
	protected void onSave()
	{
		super.set("default-ban-reason", this.defaultBanReason);
		super.set("phpbb-banned-group-id", this.phpbbBannedGroupId);
		super.set("recentbans.cache", Helpers.DateTime.getHumanReadableFromMillis(this.recentBansCacheDuration));
	}

	@Override
	protected void onEmitInfo()
	{
		super.info("Default ban reason: $1", this.defaultBanReason);
		super.info("PHPBB banned group ID: $1", this.phpbbBannedGroupId);
		super.info("The /recentbans command will show bans within the last $1.", Helpers.DateTime.getHumanReadableFromMillis(this.recentBansSince));
		super.info("The /recentbans command will cache bans for $1.", Helpers.DateTime.getHumanReadableFromMillis(this.recentBansCacheDuration));
	}

	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading bans...");
		this.bans.clear();
		for (final BanInfo info: this.dbInterface.getActiveBans())
			this.bans.put(info.getUuid(), info);
		
		super.onLoadDataStore();
	}

	public BanInfo addBan(final OfflinePlayer player, final UUID bannedBy, final long duration, final String reason)
	{
		final UUID uuid = player.getUniqueId();
		long expiry = 0;
		if (duration > 0) expiry = System.currentTimeMillis() + duration;
		final int groupId = this.dbInterface.getPreviousGroupId(uuid);
		final BanInfo banInfo = new BanInfo(player.getUniqueId(), player.getName(), bannedBy, expiry, reason, groupId);

		if (this.bans.containsKey(uuid)) this.bans.remove(uuid);
		this.bans.put(uuid, banInfo);

		this.dbInterface.addBan(banInfo);
		if (banInfo.isPermanent())
			this.dbInterface.changeUserPhpbbGroup(banInfo.getUuid(), this.phpbbBannedGroupId, true);
		
		if (player.isOnline())
		{
			if (expiry == 0) player.getPlayer().kickPlayer(Helpers.Parameters.replace("You have been banned: $1", reason));
			else
			{
				final String durationString = Helpers.DateTime.getHumanReadableFromMillis(duration);
				player.getPlayer().kickPlayer(Helpers.Parameters.replace("You have been banned for $1: $2", durationString, reason));
			}
		}		

		return banInfo;
	}

	public void removeBan(final OfflinePlayer player, final UUID unbannedBy)
	{
		final UUID uuid = player.getUniqueId();
		BanInfo banInfo = null;
		if (this.bans.containsKey(uuid))
		{
			banInfo = this.bans.get(uuid);
			this.bans.remove(uuid);
		}

		this.dbInterface.removeBan(uuid, unbannedBy);
		if (banInfo.isPermanent())
		{
			if (banInfo.getGroupId() == 0) super.debug("$1 has no previous group.", player);
			else
			{
				this.dbInterface.changeUserPhpbbGroup(player.getUniqueId(), banInfo.getGroupId(), true);
				super.debug("Previous group for $1 is $2.", player, banInfo.getGroupId());
			}
		}
	}

	public BanInfo[] listBans()
	{
		return this.bans.values().toArray(new BanInfo[0]);
	}
	
	public BanInfo[] listRecentBans()
	{
		if (this.recentBans == null ||
				this.recentBansRefresh < System.currentTimeMillis() - this.recentBansCacheDuration)
			this.recentBans = this.dbInterface.getBansSince(this.recentBansSince);
		
		return this.recentBans;
	}

	public BanInfo getBanInfo(final OfflinePlayer player) { return this.getBanInfo(player.getUniqueId()); }
	public BanInfo getBanInfo(final UUID uuid)
	{
		BanInfo banInfo = null;
		if (this.bans.containsKey(uuid))
		{
			banInfo = this.bans.get(uuid);
			if (banInfo.isExpired()) banInfo = null;
		}
		return banInfo;
	}

	public BanInfo[] getBanHistory(final OfflinePlayer player) { return this.getBanHistory(player.getUniqueId()); }
	public BanInfo[] getBanHistory(final UUID uuid)
	{
		return this.dbInterface.getBans(uuid, false);
	}
}
