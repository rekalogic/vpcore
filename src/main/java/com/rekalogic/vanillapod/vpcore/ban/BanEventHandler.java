package com.rekalogic.vanillapod.vpcore.ban;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class BanEventHandler extends EventHandlerBase<BanSettings>
{
	private final BanSettings settings;

	public BanEventHandler(final PluginBase plugin, final BanSettings settings)
	{
		super(plugin, settings);

		this.settings = settings;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onAsyncPlayerPreLogin(final AsyncPlayerPreLoginEvent event)
	{
		final BanInfo banInfo = this.settings.getBanInfo(event.getUniqueId());
		if (banInfo == null) super.debug("No ban info for $1.", event.getUniqueId());
		else
		{
			super.debug("Ban info [$1] found for $2.", banInfo.getName(), event.getUniqueId());
			if (banInfo.isPermanent()) event.setKickMessage(Helpers.Parameters.replace("You are banned: $1", banInfo.getReason()));
			else if (banInfo.isTemporary()) event.setKickMessage(Helpers.Parameters.replace(
						"You are banned until $1 ($2): $3",
						Helpers.DateTime.getDateFromMillis(banInfo.getExpireOn()),
						Helpers.DateTime.getHumanReadableFromMillis(banInfo.getExpireOn() - System.currentTimeMillis(), true),
						banInfo.getReason()));
			event.setLoginResult(Result.KICK_BANNED);			
		}
	}
}
