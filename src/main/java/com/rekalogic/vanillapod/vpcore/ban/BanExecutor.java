package com.rekalogic.vanillapod.vpcore.ban;

import java.util.Arrays;
import java.util.Comparator;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;

import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class BanExecutor extends ExecutorBase<BanSettings>
{
	public BanExecutor(final PluginBase plugin, final BanSettings settings)
	{
		super(plugin, settings, "ban", "baninfo", "recentbans", "tempban", "unban");
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "ban": return (argCount >= 1);
		case "baninfo": return (argCount <= 1);
		case "tempban": return (argCount >= 2);
		case "unban": return (argCount == 1);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "ban": return new String[] { ExecutorBase.FLAG_BOOL_SILENT, ExecutorBase.FLAG_BOOL_CONFIRM };
		case "tempban": return new String[] { ExecutorBase.FLAG_BOOL_SILENT };
		case "unban": return new String[] { ExecutorBase.FLAG_BOOL_SILENT };
		}
		return null;
	}
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "ban":
		case "baninfo":
		case "tempban":
		case "unban":
			switch (argIndex)
			{
			case 0: return LookupSource.OFFLINE_PLAYER;
			}
		}
		return super.getParameterLookupCollection(command, argIndex, args);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String label, final String[] args)
	{
		switch (command)
		{
		case "ban": return this.doBan(sender, args);
		case "baninfo": return this.doBanInfo(sender, args);
		case "recentbans": return this.doRecentBans(sender, args);
		case "tempban": return this.doTempBan(sender, args);
		case "unban": return this.doUnban(sender, args);
		}

		return super.onCommand(sender, command, label, args);
	}

	private boolean doRecentBans(final PagedOutputCommandSender sender, final String[] args)
	{
		sender.sendNotification(
				"Bans in the last $1 (info is up to $2 old):",
				Helpers.DateTime.getHumanReadableFromMillis(super.getSettings().getRecentBansSince()),
				Helpers.DateTime.getHumanReadableFromMillis(super.getSettings().getRecentBansCacheDuration()));
		
		int banCount = 0;
		long now = System.currentTimeMillis();
		for (final BanInfo banInfo: super.getSettings().listRecentBans())
		{
			if (banInfo.isActive() && !banInfo.isExpired())
			{
				final OfflinePlayer player = Bukkit.getOfflinePlayer(banInfo.getUuid());
				if (player != null && player.hasPlayedBefore())
				{
					sender.sendInfoListItem(
							banCount,
							"$1 was banned $2 ago",
							player,
							Helpers.DateTime.getTruncatedAtMinuteTimeDifference(banInfo.getBannedOn()));
					if (banInfo.isTemporary()) sender.sendInfo(
							"      $1Expires in $2.",
							ChatColor.DARK_GRAY,
							Helpers.DateTime.getTruncatedAtMinuteTimeDifference(now, banInfo.getExpireOn()));
					++banCount;
				}
			}
		}
		
		if (banCount == 0) sender.sendInfo("  None.");
		
		return true;
	}

	private boolean doUnban(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;

		if (super.getSettings().getBanInfo(player) == null)
		{
			sender.sendFailure("$1 is not banned.", player);
		}
		else
		{
			UUID uuid = null;
			if (sender.isPlayer()) uuid = sender.getPlayer().getUniqueId();
			super.getSettings().removeBan(player, uuid);

			if (!super.hasFlag(ExecutorBase.FLAG_BOOL_SILENT)) Helpers.Broadcasts.sendAll("$1 has been unbanned.", player);
			sender.sendSuccess("$1 unbanned successfully.", player);
		}

		return true;
	}

	private boolean doBanInfo(final PagedOutputCommandSender sender, final String[] args)
	{
		OfflinePlayer player = null;
		if (args.length == 1)
		{
			player = super.getPlugin().getOfflinePlayer(sender, args[0]);
			if (player == null) return true;
		}

		if (player == null)
		{
			final BanInfo[] info = super.getSettings().listBans();
			Arrays.sort(
					info,
					new Comparator<BanInfo>()
					{
						@Override
						public int compare(final BanInfo o1, final BanInfo o2)
						{
							return o1.getName().compareTo(o2.getName());
						}
					});

			if (info.length == 0) sender.sendFailure("No banned players.");
			else sender.sendInfo("Total banned players: $1.", info.length);

			for (final BanInfo banInfo: info)
			{
				String duration = "permanently";
				final long expireOn = banInfo.getExpireOn();
				if (expireOn != 0)
				{
					final long diff = expireOn - System.currentTimeMillis();
					if (diff <= 0) continue;
					duration = Helpers.Parameters.replace(
							"until $1 ($2)",
							Helpers.DateTime.getDateFromMillis(expireOn),
							Helpers.DateTime.getHumanReadableFromMillis(diff));
				}
				final String bannedName = banInfo.getName();
				final OfflinePlayer bannedPlayer = Bukkit.getPlayer(banInfo.getUuid());
				String nameInfo = bannedName;
				if (bannedPlayer != null && !bannedPlayer.getName().equals(bannedName))
				{
					nameInfo = Helpers.Parameters.replace(
							"$1 (banned as $2)",
							bannedPlayer.getName(),
							bannedName);
				}

				sender.sendInfo("$1 banned $2", nameInfo, duration);
				sender.sendInfo(
						"  by $1 at $2",
						banInfo.getBannedByName(),
						Helpers.DateTime.getDateFromMillis(banInfo.getBannedOn()));
				sender.sendInfo("  $1", banInfo.getReason());
			}
		}
		else
		{
			final BanInfo[] allBanInfo = super.getSettings().getBanHistory(player);
			if (allBanInfo.length == 0) sender.sendSuccess("Player $1 has never been banned.", player);
			else if (!allBanInfo[0].isActive() ||
					allBanInfo[0].isExpired()) sender.sendSuccess("Player $1 is no longer banned.", player);
			else
			{
				sender.sendSuccess(
						"Player $1 is banned ($2).",
						player,
						(allBanInfo[0].isPermanent() ? "PERMANENT" : "TEMPORARY"));
				sender.sendInfo("Ban history for $1:", player);
			}
			
			for (final BanInfo banInfo: allBanInfo)
			{
				ChatColor colour = ChatColor.WHITE;
				if (!banInfo.isActive() || banInfo.isExpired()) colour = ChatColor.DARK_GRAY;
				String duration = "permanent";
				if (banInfo.getExpireOn() != 0)
					duration = Helpers.Parameters.replace(
							"expires $1",
							Helpers.DateTime.getDateFromMillis(banInfo.getExpireOn()));

				sender.sendInfo(
						colour + "$1 (by $2) [$3]",
						Helpers.DateTime.getDateFromMillis(banInfo.getBannedOn()),
						banInfo.getBannedByName(),
						duration);

				sender.sendInfo(colour + "--> $1", banInfo.getReason());

				if (banInfo.getUnbannedOn() != 0)
				{
					sender.sendInfo(
							ChatColor.DARK_GREEN + "--> Unbanned by $1 on $2",
							banInfo.getUnbannedByName(),
							Helpers.DateTime.getDateFromMillis(banInfo.getUnbannedOn()));
				}
			}
		}

		return true;
	}

	private boolean doBan(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;
		
		final String reason = Helpers.formatMessage(Helpers.Args.concatenate(args, 1));

		this.banPlayer(sender, player, 0, reason);
		return true;
	}

	private boolean doTempBan(final PagedOutputCommandSender sender, final String[] args)
	{
		final OfflinePlayer player = super.getPlugin().getOfflinePlayer(sender, args[0]);
		if (player == null) return true;

		final long duration = Helpers.DateTime.getMillis(args[1]);
		if (duration < 0) return false;
		else if (duration == 0) sender.sendFailure("You cannot tempban a player for $1.", "0s");
		else
		{
			final String reason = Helpers.formatMessage(Helpers.Args.concatenate(args, 2));

			if (player != null) this.banPlayer(sender, player, duration, reason);
		}
		return true;
	}

	private void banPlayer(final PagedOutputCommandSender sender, final OfflinePlayer player, final long duration, String reason)
	{
		if (player.isOnline())
		{
			if (sender.isPlayer() && player.equals(sender.getPlayer()))
			{
				sender.sendFailure("Haven't you got anything better to do?");
				return;
			}
			else if (player.getPlayer().hasPermission("vpcore.moderator"))
			{
				sender.sendFailure("You cannot ban $1.", player);
				return;
			}
		}

		if (reason.length() == 0) reason = super.getSettings().getDefaultBanReason();

		final BanInfo existingBanInfo = super.getSettings().getBanInfo(player);
		if (existingBanInfo != null)
		{
			if (super.getFlagState(ExecutorBase.FLAG_BOOL_CONFIRM)) sender.sendNotification(
					"Exising ban information for $1 updated.",
					player);
			else
			{
				sender.sendFailure(
						"$1 is already banned. Use flag $2 to update the ban information.",
						player,
						ExecutorBase.FLAG_BOOL_CONFIRM);
				return;
			}
		}
		else super.setConfirmFlag();

		UUID uuid = null;
		if (sender.isPlayer()) uuid = sender.getPlayer().getUniqueId();
		super.getSettings().addBan(player, uuid, duration, reason);

		if (existingBanInfo == null && !super.hasFlag(ExecutorBase.FLAG_BOOL_SILENT))
		{
			if (duration == 0) Helpers.Broadcasts.sendAll("$1 has been banned: $2", player, reason);
			else Helpers.Broadcasts.sendAll(
					"$1 has been banned for $2: $3", 
					player,
					Helpers.DateTime.getHumanReadableFromMillis(duration),
					reason);
		}
		sender.sendSuccess("$1 banned successfully.", player);
	}
}
