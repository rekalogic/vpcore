package com.rekalogic.vanillapod.vpcore.ban;

import java.util.ArrayList;
import java.util.UUID;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class BanDbInterface extends DatabaseInterfaceBase
{
	public BanDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}

	public void addBan(final BanInfo banInfo)
	{
		super.debug("Adding ban for $1.", banInfo.getUuid());
		final MySqlConnection connection = super.getConnection();
		connection.update("vp_baninfo", "active = 0", "uuid = ?", banInfo.getUuid());
		connection.insertInto(
				"vp_baninfo",
				"uuid, name, bannedby, bannedon, expireon, reason, groupid",
				banInfo.getUuid(),
				banInfo.getName(),
				banInfo.getBannedBy(),
				banInfo.getBannedOn(),
				banInfo.getExpireOn(),
				banInfo.getReason(),
				banInfo.getGroupId());
		connection.close();
	}

	public void removeBan(final UUID uuid, final UUID unbannedBy)
	{
		super.debug("Removing ban for $1.", uuid);
		final MySqlConnection connection = super.getConnection();
		connection.update(
				"vp_baninfo", "active = 0, unbannedby = ?, unbannedon = ?",
				"uuid = ? AND active = 1",
				unbannedBy,
				System.currentTimeMillis(),
				uuid);
		connection.close();
	}

	public BanInfo[] getActiveBans()
	{
		final ArrayList<BanInfo> activeBans = new ArrayList<BanInfo>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT * FROM vp_baninfo WHERE active = 1");
		while (result.moveNext())
			activeBans.add(this.getBanInfo(result));
		connection.close();
		return activeBans.toArray(new BanInfo[activeBans.size()]);
	}

	public BanInfo[] getBans(final UUID uuid, final boolean activeOnly)
	{
		super.debug("Getting ban info for $1.", uuid);

		final ArrayList<BanInfo> bans = new ArrayList<BanInfo>();
		String sql = "SELECT * FROM vp_baninfo WHERE uuid = ?";
		if (activeOnly) sql += " AND active = 1";
		sql += " ORDER BY id DESC";
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(sql, uuid);
		while (result.moveNext())
			bans.add(this.getBanInfo(result));
		connection.close();
		return bans.toArray(new BanInfo[bans.size()]);
	}
	
	public BanInfo[] getBansSince(final long since)
	{
		final ArrayList<BanInfo> bansSince = new ArrayList<BanInfo>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT * FROM vp_baninfo WHERE active = 1 AND bannedon >= ? ORDER BY bannedon DESC",
				System.currentTimeMillis() - since);
		while (result.moveNext())
			bansSince.add(this.getBanInfo(result));
		connection.close();
		return bansSince.toArray(new BanInfo[bansSince.size()]);
	}

	private BanInfo getBanInfo(final MySqlResult result)
	{
		return new BanInfo(
				result.getUuid("uuid"),
				result.getString("name"),
				result.getUuid("bannedby"),
				result.getLong("bannedon"),
				result.getUuid("unbannedby"),
				result.getLong("unbannedon"),
				result.getLong("expireon"),
				result.getString("reason"),
				result.getBoolean("active"),
				result.getInt("groupid"));
	}

	public int getPreviousGroupId(final UUID uuid)
	{
		int previousGroupId = 0;
		final MySqlConnection connection = super.getConnection();
		MySqlResult result = connection.execute(
				"SELECT groupid FROM vp_baninfo WHERE uuid = ? AND active = 1 AND expireon = 0",
				uuid);

		if (result.moveNext()) previousGroupId = result.getInt("groupid");
		else
		{
			result = connection.execute(
					"SELECT u.group_id " +
					"FROM mcforum.phpbb_users u, vp_player p " +
					"WHERE u.user_id = p.phpbb_userid AND uuid = ?",
					uuid);
			if (result.moveNext()) previousGroupId = result.getInt("group_id");
		}

		connection.close();
		return previousGroupId;
	}
}
