package com.rekalogic.vanillapod.vpcore.book;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class BookData
{
	private final int id;
	private final String name;
	private final long minDelay;
	private final int maxCopies;
	private final boolean onFirstJoin;
	
	private ItemStack book;
	private String title;

	public int getId() { return this.id; }
	public String getName() { return this.name; }
	public long getMinDelay() { return this.minDelay; }
	public int getMaxCopies() { return this.maxCopies; }
	public boolean getOnFirstJoin() { return this.onFirstJoin; }
	
	public ItemStack getBook() { return this.book; }
	public String getTitle() { return this.title; }
		
	public BookData(
			final int id,
			final String name,
			final long minDelay,
			final int maxCopies,
			final boolean onFirstJoin)
	{
		this.id = id;
		this.name = name;
		this.minDelay = minDelay;
		this.maxCopies = maxCopies;
		this.onFirstJoin = onFirstJoin;
	}
	
	public void setContent(final String loreName, final String author, final String title, final List<String> pages)
	{
		this.title = title;
		final List<String> colouredPages = new ArrayList<String>(pages.size());
		for (final String page: pages)
			colouredPages.add(Helpers.formatMessage(page));
		
		this.book = new ItemStack(Material.WRITTEN_BOOK, 1);
		final BookMeta meta = (BookMeta)this.book.getItemMeta();
		final ArrayList<String> lore = new ArrayList<String>();
		lore.add(loreName);
		meta.setAuthor(author);
		meta.setTitle(this.title);
		meta.setPages(colouredPages);
		meta.setLore(lore);
		this.book.setItemMeta(meta);
	}
}
