package com.rekalogic.vanillapod.vpcore.book;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class BookDbInterface extends DatabaseInterfaceBase
{
	public BookDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}

	public Map<Integer, BookData> loadAllBooks()
	{
		Map<Integer, BookData> books = new ConcurrentHashMap<Integer, BookData>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT * FROM vp_book ORDER BY id");
		while (result.moveNext())
		{
			final int id = result.getInt("id"); 
			final BookData book = new BookData(
					id,
					result.getString("name"),
					result.getLong("mindelay"),
					result.getInt("maxcopies"),
					result.getBoolean("onfirstjoin"));
			books.put(id, book);
		}
		connection.close();
		return books;
	}
	
	public Map<UUID, Map<Integer, BookReceiptData>> loadAllReceipts()
	{
		Map<UUID, Map<Integer, BookReceiptData>> receipts = new ConcurrentHashMap<UUID, Map<Integer, BookReceiptData>>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT * FROM vp_book_receipt ORDER BY uuid");
		while (result.moveNext())
		{
			final UUID uuid = result.getUuid("uuid");
			Map<Integer, BookReceiptData> receiptsByBook = null;
			if (receipts.containsKey(uuid)) receiptsByBook = receipts.get(uuid);
			else
			{
				receiptsByBook = new ConcurrentHashMap<Integer, BookReceiptData>();
				receipts.put(uuid, receiptsByBook);
			}
			final int bookId = result.getInt("bookid");
			final BookReceiptData receipt = new BookReceiptData(
					result.getInt("id"),
					uuid,
					bookId,
					result.getLong("lastgiven"),
					result.getInt("totalgiven"));
			receiptsByBook.put(bookId, receipt);
		}
		connection.close();
		return receipts;
	}

	public void createReceipt(final BookReceiptData receipt)
	{
		final MySqlConnection connection = super.getConnection();
		final int id = connection.insertInto(
				"vp_book_receipt",
				"uuid, bookid, lastgiven, totalgiven",
				receipt.getUuid(),
				receipt.getBookId(),
				receipt.getLastGiven(),
				receipt.getTotalGiven());		
		connection.close();
		receipt.setId(id);
	}

	public void updateReceipt(final BookReceiptData receipt)
	{
		final MySqlConnection connection = super.getConnection();
		connection.update(
				"vp_book_receipt",
				"lastgiven = ?, totalgiven = ?",
				"id = ?",
				receipt.getLastGiven(),
				receipt.getTotalGiven(),
				receipt.getId());
		connection.close();
	}
}
