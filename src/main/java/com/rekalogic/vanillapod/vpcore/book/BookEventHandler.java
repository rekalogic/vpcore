package com.rekalogic.vanillapod.vpcore.book;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class BookEventHandler extends EventHandlerBase<BookSettings>
{
	public BookEventHandler(final PluginBase plugin, final BookSettings settings)
	{
		super(plugin, settings);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	private void onPlayerJoin(final PlayerJoinEvent event)
	{
		if (super.getSettings().isEnabled())
		{
			final Player player = event.getPlayer();
			final OnFirstJoinBookTask task = new OnFirstJoinBookTask(this, player, super.getSettings());
			task.runTaskLater(super.getPlugin(), Helpers.DateTime.getTicksFromMillis(super.getSettings().getOnJoinDelay()));
		}
	}
}
