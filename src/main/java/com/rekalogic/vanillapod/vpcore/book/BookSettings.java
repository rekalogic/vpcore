package com.rekalogic.vanillapod.vpcore.book;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.configuration.ConfigurationSection;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class BookSettings extends SettingsBase
{
	private final BookDbInterface dbInterface;
	
	private String loreName = "";
	private String metaDir = "";
	private long onJoinDelay = 0;
	private Map<Integer, BookData> books = null;
	private Map<UUID, Map<Integer, BookReceiptData>> receipts = null;
	
	public long getOnJoinDelay() { return this.onJoinDelay; }

	public BookSettings(final VPPlugin plugin)
	{
		super(plugin);
		
		this.dbInterface = new BookDbInterface(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.loreName = super.getString("lore-name");
		this.metaDir = super.getString("meta-dir");
		this.onJoinDelay = Helpers.DateTime.getMillis(super.getString("delays.onjoin"));
	}

	@Override
	protected void onSave()
	{
		super.set("lore-name", this.loreName);
		super.set("meta-dir", this.metaDir);
		super.set("delays.onjoin", Helpers.DateTime.getHumanReadableFromMillis(this.onJoinDelay));
	}
	
	@Override
	protected void onEmitInfo()
	{
		super.info("Lore name: $1", this.loreName);
		super.info("Metadata storage: $1", super.getPlugin().getDataFolder().getAbsolutePath() + File.separatorChar + this.metaDir);
		super.info("Delay before running onJoin task: $1", Helpers.DateTime.getHumanReadableFromMillis(this.onJoinDelay));
	}
	
	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading books...");
		this.books = this.dbInterface.loadAllBooks();
		
		for (final BookData book: this.books.values())
		{
			super.loadDataStore(
					this.metaDir + File.separatorChar + book.getName() + ".yml",
					new IDataStoreLoader()
					{
						@Override
						public void load(ConfigurationSection section)
						{
							book.setContent(
									loreName,
									section.getString("author"),
									section.getString("title"),
									section.getStringList("pages"));
						}
					});
		}
		
		super.info("Loading book receipts...");
		this.receipts = this.dbInterface.loadAllReceipts();
		
		super.onLoadDataStore();
	}
	
	public BookData getBook(final int id)
	{
		BookData book = null;
		if (this.books.containsKey(id)) book = this.books.get(id);
		return book;
	}
	
	public BookReceiptData getReceipt(final UUID uuid, final int bookId)
	{
		Map<Integer, BookReceiptData> receiptsByBook = null;
		if (this.receipts.containsKey(uuid)) receiptsByBook = this.receipts.get(uuid);
		else
		{
			receiptsByBook = new HashMap<Integer, BookReceiptData>();
			this.receipts.put(uuid, receiptsByBook);
		}
		
		BookReceiptData receipt = null;
		if(receiptsByBook.containsKey(bookId)) receipt = receiptsByBook.get(bookId);
		else
		{
			receipt = new BookReceiptData(0, uuid, bookId, 0, 0);
			receiptsByBook.put(bookId, receipt);
		}
		
		return receipt;
	}

	public Collection<BookData> getBooks()
	{
		return this.books.values();
	}

	public void updateReceipt(final BookReceiptData receipt)
	{
		if (receipt.getId() == 0) this.dbInterface.createReceipt(receipt);
		else this.dbInterface.updateReceipt(receipt);
	}
}
