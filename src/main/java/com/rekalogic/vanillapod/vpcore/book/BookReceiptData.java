package com.rekalogic.vanillapod.vpcore.book;

import java.util.UUID;

public final class BookReceiptData
{
	private int id;
	private final UUID uuid;
	private final int bookId;
	private long lastGiven;
	private int totalGiven;
	
	public int getId() { return this.id; }
	public void setId(final int id) { this.id = id; }
	public UUID getUuid() { return this.uuid; };
	public int getBookId() { return this.bookId; };
	public long getLastGiven() { return this.lastGiven; };
	public int getTotalGiven() { return this.totalGiven; };
	
	public BookReceiptData(final int id, final UUID uuid, final int bookId, final long lastGiven, final int totalGiven)
	{
		this.id = id;
		this.uuid = uuid;
		this.bookId = bookId;
		this.lastGiven = lastGiven;
		this.totalGiven = totalGiven;
	}
	
	public void recordBookGiven()
	{
		this.lastGiven = System.currentTimeMillis();
		++this.totalGiven;
	}
}
