package com.rekalogic.vanillapod.vpcore.book;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.rekalogic.vanillapod.vpcore.util.LoggingBase;
import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public class OnFirstJoinBookTask extends TaskBase
{
	private final Player player;
	private final BookSettings settings;

	public OnFirstJoinBookTask(final LoggingBase loggingBase, final Player player, final BookSettings settings)
	{
		super(loggingBase);
		
		this.player = player;
		this.settings = settings;
	}

	@Override
	public void runTask()
	{
		for (final BookData bookData: this.settings.getBooks())
		{
			if (bookData.getOnFirstJoin())
			{
				final BookReceiptData receipt = this.settings.getReceipt(player.getUniqueId(), bookData.getId());
				if (receipt.getTotalGiven() == 0)
				{
					final ItemStack book = bookData.getBook();
					if (book == null)
					{
						super.severe("Book $1 could not be instantiated.", bookData.getId());
						continue;
					}
					
					final Inventory inventory = player.getInventory();
					if (Helpers.Item.canCarry(inventory, book))
					{
						inventory.addItem(book);
						receipt.recordBookGiven();
						settings.updateReceipt(receipt);
						
						Helpers.Messages.sendNotification(
								player,
								"You were given the book [$1] $2.",
								bookData.getId(),
								bookData.getTitle());
						
						super.info("Gave book [$1] '$2' to $3.", bookData.getId(), bookData.getTitle(), player);
					}
					else super.warning("Could not give book $1 to $2.", bookData.getId(), player);
				}
			}
		}
	}

}
