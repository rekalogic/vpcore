package com.rekalogic.vanillapod.vpcore.news;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class NewsSettings extends SettingsBase
{
	private final NewsDbInterface dbInterface;
	
	private int forumId = 0;
	private int maxItems = 0;
	private long refreshPeriod = 0;
	
	private long lastRefresh = 0;
	private final Queue<NewsData> news = new ConcurrentLinkedQueue<NewsData>();
	
	public int getForumId() { return this.forumId; }
	
	public NewsSettings(final VPPlugin plugin)
	{
		super(plugin);

		this.dbInterface = new NewsDbInterface(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.forumId = super.getInt("forum-id");
		this.maxItems = super.getInt("max-items");
		this.refreshPeriod = Helpers.DateTime.getMillis(super.getString("refresh"));
		
		this.lastRefresh = 0;
	}

	@Override
	protected void onSave()
	{
		super.set("forum-id", this.forumId);
		super.set("max-items", this.maxItems);
		super.set("refresh", Helpers.DateTime.getHumanReadableFromMillis(this.refreshPeriod));
	}

	@Override
	protected void onEmitInfo()
	{
		super.info("Forum ID: $1", this.forumId);
		super.info("News will refresh every $1.", Helpers.DateTime.getHumanReadableFromMillis(this.refreshPeriod));
	}
	
	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading news...");
		this.getNews();
		
		super.onLoadDataStore();
	}
	
	public NewsData[] getNews()
	{
		final long now = System.currentTimeMillis();
		if (this.lastRefresh < now - this.refreshPeriod)
		{
			this.news.clear();
			this.news.addAll(this.dbInterface.getNews(this.forumId, this.maxItems));
			super.info("Refreshed $1 news item$2.", this.news, Helpers.getPlural(this.news));
			this.lastRefresh = now;
		}
		
		final NewsData[] news = this.news.toArray(new NewsData[0]);
		Arrays.sort(
				news,
				new Comparator<NewsData>()
				{
					@Override
					public int compare(final NewsData o1, final NewsData o2)
					{						
						return Long.compare(o1.getPublished(), o2.getPublished());
					}
				});
				
		return news;
	}
}
