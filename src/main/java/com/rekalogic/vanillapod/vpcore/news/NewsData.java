package com.rekalogic.vanillapod.vpcore.news;

import org.apache.commons.lang.StringEscapeUtils;

import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class NewsData
{
	private final int topicId;
	private final long published;
	private final String subject;
	private final String lead;	
	private final boolean valid;
	
	public int getTopicId() { return this.topicId; }
	public long getPublished() { return this.published; }
	public String getSubject() { return this.subject; }
	public String getLead() { return this.lead; }
	public boolean isValid() { return this.valid; }
	
	private static final String LeadStartToken = "[lead:$1]";
	private static final String LeadEndToken = "[/lead:$1]";
	
	public String getLink()
	{
		return Helpers.Parameters.replace("http://mc.rv.rs/viewtopic.php?t=$1", this.topicId);
	}
	
	public NewsData(final int topicId, final long published, final String subject, final String body, final String bbcodeUid)
	{
		this.topicId = topicId;
		this.published = published;
		this.subject = subject;
		
		final String leadStartToken = Helpers.Parameters.replace(NewsData.LeadStartToken, bbcodeUid);
		final String leadEndToken = Helpers.Parameters.replace(NewsData.LeadEndToken, bbcodeUid);
		
		String lead = "";
		
		String matchBody = body.toLowerCase();
		if (matchBody.contains(leadStartToken) && matchBody.contains(leadEndToken))
		{
			final int startIndex = matchBody.indexOf(leadStartToken) + leadStartToken.length();
			final int endIndex = matchBody.indexOf(leadEndToken);
			
			if (startIndex < endIndex) lead = body.substring(startIndex, endIndex);
			else lead = Helpers.Parameters.replace("$1/$2/$3", startIndex, endIndex, matchBody);
		}
		
		this.lead = StringEscapeUtils.unescapeHtml(lead);
		this.valid = (!lead.equals(""));
	}
}
