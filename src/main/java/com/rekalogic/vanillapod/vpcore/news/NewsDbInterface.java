package com.rekalogic.vanillapod.vpcore.news;

import java.util.ArrayList;
import java.util.List;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class NewsDbInterface extends DatabaseInterfaceBase
{
	public NewsDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}

	public List<NewsData> getNews(final int forumId, final int limit)
	{
		final List<NewsData> news = new ArrayList<NewsData>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute(
				"SELECT t.topic_id, t.topic_time, t.topic_title, p.post_text, p.bbcode_uid " +
				"FROM mcforum.phpbb_topics t, mcforum.phpbb_posts p " +
				"WHERE t.forum_id = ? " +
				"AND p.post_id = t.topic_first_post_id " +
				"ORDER BY t.topic_id DESC " +
				"LIMIT ?",
				forumId,
				limit);
		while (result.moveNext())
		{
			final NewsData data = new NewsData(
					result.getInt("topic_id"),
					result.getLong("topic_time") * Helpers.DateTime.MillisecondsInASecond,
					result.getString("topic_title"),
					result.getString("post_text"),
					result.getString("bbcode_uid"));
			news.add(data);
		}
		return news;
	}
}
