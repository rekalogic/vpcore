package com.rekalogic.vanillapod.vpcore.news;

import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class NewsExecutor extends ExecutorBase<NewsSettings>
{
	public NewsExecutor(
			final PluginBase plugin,
			final NewsSettings settings)
	{
		super(plugin, settings, "news");
	}

	@Override
	protected boolean onCommand(
			final PagedOutputCommandSender sender,
			final String command,
			final String shortcut,
			final String[] args)
	{
		switch (command)
		{
		case "news": return this.doNews(sender, args);
		}
		return false;
	}

	private boolean doNews(final PagedOutputCommandSender sender, final String[] args)
	{
		final NewsData[] news = super.getSettings().getNews();
		for (final NewsData data: news)
		{
			if (data.isValid())
			{				
				sender.sendNotification(
						"[$1] $2",
						Helpers.DateTime.getDateFromMillis(data.getPublished()),
						data.getSubject());
				sender.sendInfo(data.getLead());
			}
		}
		return true;
	}
}
