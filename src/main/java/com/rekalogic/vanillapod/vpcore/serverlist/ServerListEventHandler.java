package com.rekalogic.vanillapod.vpcore.serverlist;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.server.ServerListPingEvent;

import com.rekalogic.vanillapod.vpcore.player.Group;
import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public class ServerListEventHandler extends EventHandlerBase<ServerListSettings>
{
	private final String mainWorldName;
	
	public ServerListEventHandler(final PluginBase plugin, final ServerListSettings settings)
	{
		super(plugin, settings);
		
		this.mainWorldName = Bukkit.getWorlds().get(0).getName();
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onServerListPing(final ServerListPingEvent event)
	{
		if (super.getSettings().isServerLocked()) event.setMaxPlayers(0);
		else event.setMaxPlayers(super.getSettings().getMaxPlayers());
		
		event.setMotd(super.getSettings().getServerListMotd());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onPlayerJoin(final PlayerJoinEvent event)
	{
		if (super.getSettings().isServerLocked())
		{
			final Player player = event.getPlayer();
			if (player.hasPermission("vpcore.moderator"))
			{
				Helpers.Messages.sendNotification(player, "SERVER IS LOCKED");
				Helpers.Messages.sendNotification(player, "$1$2", ChatColor.RESET, super.getSettings().getLockMessage());
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onPlayerLoginHighest(final PlayerLoginEvent event)
	{
		if (event.getResult() == PlayerLoginEvent.Result.KICK_FULL)
		{
			final Collection<? extends Player> onlinePlayers = Bukkit.getOnlinePlayers();
			final int maxPlayers = super.getSettings().getMaxPlayers();
			if (onlinePlayers.size() < maxPlayers) event.allow();
			else
			{
				int playerCount = 0;
				for (final Player onlinePlayer: onlinePlayers)
				{					
					if (!super.getPlugin().isVanished(onlinePlayer) &&
							!onlinePlayer.hasPermission("vpcore.join.full")) ++playerCount;
				}
				if (playerCount < maxPlayers) event.allow();
				else
				{
					final Player player = event.getPlayer();
					final Group group = super.getPlugin().getPermissionManager().getCurrentGroup(player);
					if (group == null || group.hasPermission(this.mainWorldName, "vpcore.join.full"))
					{
						event.allow();
						super.info("$1 was allowed to join full server.", player);
					}
					else
					{
						super.info("$1 was unable to join: server full.", player);
						Helpers.Broadcasts.sendPerms("vpcore.moderator", "$1 was unable to join: server full.", player);
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	private void onPlayerLoginNormal(final PlayerLoginEvent event)
	{
		if (super.getSettings().isServerLocked())
		{
			final Player player = event.getPlayer();
			final Group group = super.getPlugin().getPermissionManager().getCurrentGroup(player);
			if (group == null || !group.hasPermission(this.mainWorldName, "vpcore.join.locked"))
			{
				event.setKickMessage(super.getSettings().getLockMessage());
				event.setResult(Result.KICK_OTHER);
				Helpers.Broadcasts.sendPerms("vpcore.moderator", "$1 was unable to join: server locked.", player);
			}
		}
	}
}
