package com.rekalogic.vanillapod.vpcore.serverlist;

import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class ServerListSettings extends SettingsBase
{
	private final ServerListDbInterface dbInterface;

	private int maxPlayers = 0;
	private boolean isServerLocked = false;
	private String defaultLockMessage = "";
	private String currentLockMessage = "";
	private long lockReminderPeriod = 0;
	private ServerUnlockTask unlockTask = null;

	private final Random random;
	private final String[] randomMotds;
	private final int maxPreCachedMOTDs = 10000;

	private int nextMOTDIndex = 0;
	private List<String> allMotds = null;
	private String serverName = "";

	private boolean isStartupLockEnabled = false;
	private long startupLockDuration = 0;
	private String startupLockMessage = "";

	public int getMaxPlayers()
	{
		if (this.maxPlayers == 0) return Bukkit.getServer().getMaxPlayers();
		else return this.maxPlayers;
	}
	public boolean isServerLocked() { return this.isServerLocked; }
	public void setServerLocked(final boolean value) { this.isServerLocked = value; }
	public boolean isStartupLockEnabled() { return this.isStartupLockEnabled; }
	public long getStartupLockDuration() { return this.startupLockDuration; };
	public String getStartupLockMessage() { return this.startupLockMessage; };
	public boolean isIndefinite() { return this.isServerLocked && this.unlockTask.isIndefinite(); }

	public ServerListSettings(final VPPlugin plugin)
	{
		super(plugin);

		this.dbInterface = new ServerListDbInterface(plugin);
		this.random = plugin.getRandom();
		this.randomMotds = new String[this.maxPreCachedMOTDs];
	}

	@Override
	protected void onLoad()
	{
		this.serverName = super.getString("server-name");
		this.maxPlayers = super.getInt("max-players");
		this.defaultLockMessage = super.getString("default-lock-message");
		this.lockReminderPeriod = Helpers.DateTime.getMillis(super.getString("lock-reminder-period"));
		this.isStartupLockEnabled = super.getBoolean("startup-lock.enabled");
		this.startupLockDuration = Helpers.DateTime.getMillis(super.getString("startup-lock.duration"));
		this.startupLockMessage = super.getString("startup-lock.message");
		
		if (this.unlockTask != null) this.unlockTask.resetReminderPeriod(this.lockReminderPeriod);
	}
	
	@Override
	protected void onLoadDataStore()
	{
		super.info("Loading MOTDs...");

		this.allMotds = this.dbInterface.getMotdList();
		if (this.allMotds != null && this.allMotds.size() > 0)
		{
			for (int i = 0; i < this.maxPreCachedMOTDs; ++i)
			{
				this.randomMotds[i] = Helpers.formatMessage(
						this.allMotds.get(
								this.random.nextInt(
										this.allMotds.size())));
			}
		}
		
		super.onLoadDataStore();
	}

	@Override
	protected void onSave()
	{
		super.set("server-name", this.serverName);
		super.set("max-players", this.maxPlayers);
		super.set("default-lock-message", this.defaultLockMessage);
		super.set("lock-reminder-period", Helpers.DateTime.getHumanReadableFromMillis(this.lockReminderPeriod));
		super.set("startup-lock.enabled", this.isStartupLockEnabled);
		super.set("startup-lock.duration", Helpers.DateTime.getHumanReadableFromMillis(this.startupLockDuration));
		super.set("startup-lock.message", this.startupLockMessage);
	}

	@Override
	protected void onEmitSettings()
	{
		super.emitSetting("Startup Lock", this.isStartupLockEnabled, this.isEnabled());
	}

	@Override
	protected void onEmitInfo()
	{
		super.info("Server name: $1", this.serverName);
		if (this.maxPlayers == 0) super.info("Max players: $1 (server.properties)", this.getMaxPlayers());
		else super.info("Max players: $1", this.getMaxPlayers());
		
		if (this.allMotds != null && this.allMotds.size() > 0)
		{
			super.info("MOTDs configured: $1", this.allMotds.size());
		}
		else
		{
			super.warning("MOTD control is enabled, but no MOTDs are configured.");
			super.warning("The default server list MOTD (defined in bukkit.yml) will be used.");
		}

		super.info("Default lock message: $1", this.getLockMessage());
		super.info("Lock reminder: $1", Helpers.DateTime.getHumanReadableFromMillis(this.lockReminderPeriod));

		if (this.isStartupLockEnabled)
		{
			super.info(
					"The server will be locked for $1 after startup",
					Helpers.DateTime.getHumanReadableFromMillis(this.startupLockDuration));
			super.info("The startup lock message is set to: $1", this.startupLockMessage);
		}
	}

	public String getServerListMotd()
	{
		String strapline;
		if (this.isServerLocked) strapline = this.getLockMessage();
		else if (!super.getPlugin().isLiveServer()) strapline = ChatColor.LIGHT_PURPLE + ">> TEST SERVER <<";
		else if (this.allMotds.size() == 0) strapline = Bukkit.getMotd();
		else
		{
			strapline = this.randomMotds[this.nextMOTDIndex];
			++this.nextMOTDIndex;
			if (this.nextMOTDIndex >= this.maxPreCachedMOTDs) this.nextMOTDIndex = 0;
		}

		return Helpers.formatMessage(this.serverName) + "\n" + strapline;
	}

	public void clearMotds() { this.allMotds.clear(); }
	public void addMotd(final String motd) { this.allMotds.add(motd); }
	public boolean removeMotd(final int index)
	{
		if (index < 0 || index >= this.allMotds.size()) return false;
		this.allMotds.remove(index);
		return true;
	}

	public String[] getMotds()
	{
		return this.allMotds.toArray(new String[0]);
	}

	public void unlockServer()
	{
		this.isServerLocked = false;
		if (this.unlockTask != null)
		{
			this.unlockTask.cancel();
			this.unlockTask = null;
		}
	}

	public String getLockMessage()
	{
		String lockMessage = this.defaultLockMessage;
		if (this.isServerLocked)
		{
			lockMessage = this.currentLockMessage;
			if (this.unlockTask.isIndefinite()) lockMessage = lockMessage.replace("$t", "indefinite");
			else lockMessage = lockMessage.replace(
					"$t",
					Helpers.DateTime.getHumanReadableFromMillis(
							this.unlockTask.getRemainingLockDuration(), true));
		}

		return Helpers.formatMessage(lockMessage);
	}

	public long getLockDuration()
	{
		if (this.isServerLocked && this.unlockTask != null) return this.unlockTask.getRemainingLockDuration();
		else return 0;
	}

	public void lockServer(final PagedOutputCommandSender sender, String lockMessage, final long duration)
	{
		if (lockMessage.length() == 0) lockMessage = this.defaultLockMessage;
		this.currentLockMessage = lockMessage;

		final String durationString = (duration == 0 ? "indefinite" : Helpers.DateTime.getHumanReadableFromMillis(duration));

		if (this.isServerLocked)
		{
			this.unlockTask.setLockDuration(duration);
			sender.sendSuccess("Server lock updated ($1): $2", durationString, this.getLockMessage());
			Helpers.Broadcasts.sendPerms(
					"vpcore.moderator",
					"The server lock has been updated by $1.",
					sender);
			this.debug("Server LOCK UPDATED by $1 ($2).", sender, durationString);
		}
		else
		{
			this.isServerLocked = true;
			this.unlockTask = new ServerUnlockTask(this, duration, this.lockReminderPeriod);
			this.unlockTask.runTaskTimer(super.getPlugin(), 5, 20);

			sender.sendSuccess("Server locked ($1): $2", durationString, this.getLockMessage());
			Helpers.Broadcasts.sendPerms(
					"vpcore.moderator",
					"The server has been locked by $1.",
					sender);
			this.debug("Server LOCKED by $1 ($2).", sender, durationString);
		}
	}

	public void addLockDuration(long duration)
	{
		if (this.isServerLocked)
		{
			if (this.unlockTask.isIndefinite()) return;
			else this.unlockTask.setLockDuration(this.unlockTask.getRemainingLockDuration() + duration);
		}
	}
}
