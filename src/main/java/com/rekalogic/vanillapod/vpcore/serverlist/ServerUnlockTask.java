package com.rekalogic.vanillapod.vpcore.serverlist;

import com.rekalogic.vanillapod.vpcore.util.TaskBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class ServerUnlockTask extends TaskBase
{
	private final ServerListSettings settings;
	private long lockReminderPeriod;
	private long unlockTimestamp = System.currentTimeMillis();
	private final long lockTimestamp = System.currentTimeMillis();
	private long lastReminder = 0;
	private boolean isIndefinite = false;
	
	public boolean isIndefinite() { return this.isIndefinite; }

	public ServerUnlockTask(final ServerListSettings settings, final long lockDuration, final long lockReminderPeriod)
	{
		super(settings);
		this.settings = settings;
		this.setLockDuration(lockDuration);
		this.lockReminderPeriod = lockReminderPeriod;
	}

	public void setLockDuration(final long duration)
	{
		this.isIndefinite = (duration == 0);
		this.unlockTimestamp = System.currentTimeMillis() + duration;
	}

	public long getRemainingLockDuration()
	{
		return (this.unlockTimestamp - System.currentTimeMillis());
	}
	
	public void resetReminderPeriod(final long period)
	{
		this.lockReminderPeriod = period;
		this.lastReminder = System.currentTimeMillis() - (this.lockReminderPeriod + 1);
	}

	@Override
	public void runTask()
	{
		final long now = System.currentTimeMillis();
		if ((!this.isIndefinite && now >= this.unlockTimestamp) || !this.settings.isServerLocked())
		{
			super.cancel();
			if (this.settings.isServerLocked())
			{
				this.settings.setServerLocked(false);
				Helpers.Broadcasts.sendPerms(
						"vpcore.moderator",
						"The server has been automatically unlocked after $1.",
						Helpers.DateTime.getHumanReadableFromMillis(now - this.lockTimestamp));
				super.debug("Server UNLOCKED automatically - lock expired.");
			}
		}
		else if (this.settings.isServerLocked())
		{
			if (this.lastReminder + this.lockReminderPeriod <= now)
			{
				if (this.lastReminder > 0)
				{
					if (this.isIndefinite) Helpers.Broadcasts.sendPerms("vpcore.moderator", "THE SERVER IS LOCKED");
					else Helpers.Broadcasts.sendPerms(
							"vpcore.moderator",
							"THE SERVER IS LOCKED ($1 remaining).",
							Helpers.DateTime.getHumanReadableFromMillis(this.getRemainingLockDuration()));
				}
				this.lastReminder = now;
			}
		}
	}
}
