package com.rekalogic.vanillapod.vpcore.serverlist;

import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class ServerListExecutor extends ExecutorBase<ServerListSettings>
{
	public ServerListExecutor(final PluginBase plugin, final ServerListSettings settings)
	{
		super(plugin, settings, "serverlist", "lock", "lockadd", "querylock", "unlock");
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "lock": return true;
		case "lockadd": return (argCount == 1);
		case "serverlist": return (argCount >= 1);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected String[] getAllowedFlags(final String command)
	{
		switch (command)
		{
		case "lock": return new String[] { ExecutorBase.FLAG_TIME };
		}
		
		return super.getAllowedFlags(command);
	}
	
	@Override
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "serverlist":
			switch (argIndex)
			{
			case 0: return Arrays.asList("add", "list", "remove");
			}
		}
		
		return super.onTabComplete(sender, command, argIndex, args);
	}
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "serverlist":
			switch (argIndex)
			{
			case 0: return LookupSource.FROM_PLUGIN;
			}
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String shortcut, final String[] args)
	{
		switch (command)
		{
		case "lock": return this.doLock(sender, args);
		case "lockadd": return this.doLockAdd(sender, args);
		case "querylock": return this.doQueryLock(sender, args);
		case "unlock": return this.doUnlock(sender, args);
		case "serverlist":
		{
			final String action = args[1].toLowerCase();
			
			switch (action)
			{
			case "add":
				if (args.length <= 1) return false;
				break;
			case "list":
				if (args.length != 1) return false;
				break;
			case "remove":
				if (args.length != 2) return false;
				break;
			}

			switch (action)
			{
			case "add": return this.doAddCommand(sender, Helpers.Args.concatenate(args));
			case "list": return this.doListCommand(sender);
			case "remove": return this.doRemoveCommand(sender, args);
			}
		} break;
		}

		return false;
	}
	
	private boolean doQueryLock(final PagedOutputCommandSender sender, final String[] args)
	{
		if (super.getSettings().isServerLocked())
		{
			final long remainingDuration = super.getSettings().getLockDuration();
			Helpers.Messages.sendSuccess(
					sender,
					"The server is locked ($1): $2",
					(remainingDuration <= 0 ? "indefinite" : Helpers.DateTime.getHumanReadableFromMillis(remainingDuration)),
					super.getSettings().getLockMessage());
		}
		else sender.sendSuccess("The server is not locked.");

		return true;
	}

	private boolean doUnlock(final PagedOutputCommandSender sender, final String[] args)
	{
		if (super.getSettings().isServerLocked())
		{
			super.getSettings().unlockServer();
			sender.sendSuccess("The server has been unlocked.");
			Helpers.Broadcasts.sendPerms(
					"vpcore.moderator",
					"The server has been unlocked by $1.",
					sender);
			this.info("Server UNLOCKED by $1.", sender);
		}
		else sender.sendFailure("The server is not locked.");

		return true;
	}

	private boolean doLockAdd(final PagedOutputCommandSender sender, final String[] args)
	{
		final long duration = Helpers.DateTime.getMillis(args[0]);
		if (duration < 0) return false;
		
		if (super.getSettings().isServerLocked())
		{
			if (super.getSettings().isIndefinite()) sender.sendFailure("The server lock is indefinite.");
			else
			{
				super.getSettings().addLockDuration(duration);
				sender.sendSuccess(
						"Lock duration has been increased by $1 to $2.",
						Helpers.DateTime.getHumanReadableFromMillis(duration),
						Helpers.DateTime.getHumanReadableFromMillis(super.getSettings().getLockDuration()));
			}
		}
		else sender.sendFailure("The server is not locked.");

		return true;
	}

	private boolean doLock(final PagedOutputCommandSender sender, final String[] args)
	{
		final long duration = super.getFlagLong(ExecutorBase.FLAG_TIME, 0);
		super.getSettings().lockServer(sender, Helpers.Args.concatenate(args), duration);
		return true;
	}

	private boolean doRemoveCommand(final PagedOutputCommandSender sender, final String[] args)
	{
		if (args.length == 0) sender.sendFailure("Please specify ID of MOTD to remove (see /motd list), or $1.", "all");
		else
		{
			if (args[0].equalsIgnoreCase("all"))
			{
				super.getSettings().clearMotds();
				this.refreshMotds(sender);
			}
			else
			{
				int id = 0;
				try
				{
					id = Integer.parseInt(args[0]);
				}
				catch (final NumberFormatException e)
				{
					id = 0;
				}

				if (super.getSettings().removeMotd(id - 1)) this.refreshMotds(sender);
				else sender.sendFailure("Invalid or out of range MOTD ID; please refer to /motd list.");
			}
		}
		return true;
	}

	private void refreshMotds(final PagedOutputCommandSender sender)
	{
		sender.sendNotification("Cache will now be refreshed.");
		super.getSettings().save();
		super.getSettings().reload();
	}

	private boolean doListCommand(final PagedOutputCommandSender sender)
	{
		final String motds[] = super.getSettings().getMotds();
		if (motds.length == 0) sender.sendNotification("No MOTDs defined.");
		else
		{
			sender.sendInfo(ChatColor.LIGHT_PURPLE + "MOTDs:");
			for (int i = 0; i < motds.length; ++i)
				sender.sendInfoListItem(i, Helpers.formatMessage(motds[i]));
		}
		return true;
	}

	private boolean doAddCommand(final PagedOutputCommandSender sender, final String motd)
	{
		super.getSettings().addMotd(motd);
		this.refreshMotds(sender);
		sender.sendSuccess("Added MOTD: $1$2", ChatColor.RESET, Helpers.formatMessage(motd));
		return true;
	}
}
