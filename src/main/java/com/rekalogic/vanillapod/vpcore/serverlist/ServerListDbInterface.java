package com.rekalogic.vanillapod.vpcore.serverlist;

import java.util.ArrayList;
import java.util.List;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.db.DatabaseInterfaceBase;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlConnection;
import com.rekalogic.vanillapod.vpcore.util.db.MySqlResult;

public final class ServerListDbInterface extends DatabaseInterfaceBase
{
	public ServerListDbInterface(final VPPlugin plugin)
	{
		super(plugin);
	}

	public List<String> getMotdList()
	{
		final List<String> motds = new ArrayList<String>();
		final MySqlConnection connection = super.getConnection();
		final MySqlResult result = connection.execute("SELECT motd FROM vp_motd WHERE active = 1");
		while (result.moveNext()) motds.add(result.getString("motd"));
		connection.close();
		return motds;
	}
}
