package com.rekalogic.vanillapod.vpcore.sleep;

import com.rekalogic.vanillapod.vpcore.VPPlugin;
import com.rekalogic.vanillapod.vpcore.util.SettingsBase;

public final class SleepSettings extends SettingsBase
{
	private boolean isNightSkipPrevented = false;
	private String preventNightSkipMessage = "";

	public boolean isNightSkipPrevented() { return this.isNightSkipPrevented; }
	public void setNightSkipPrevented(final boolean  value) { this.isNightSkipPrevented = value; }
	public String getPreventNightSkipMessage() { return this.preventNightSkipMessage; }

	public SleepSettings(final VPPlugin plugin)
	{
		super(plugin);
	}

	@Override
	protected void onLoad()
	{
		this.isNightSkipPrevented = super.getBoolean("prevent-night-skip");
		this.preventNightSkipMessage = super.getString("prevent-night-skip-message");
	}

	@Override
	protected void onSave()
	{
		super.set("prevent-night-skip", this.isNightSkipPrevented);
		super.set("prevent-night-skip-message", this.preventNightSkipMessage);
	}

	@Override
	protected void onEmitSettings()
	{
		super.emitSetting("Prevent Night Skip", this.isNightSkipPrevented);
	}

	@Override
	protected void onEmitInfo()
	{
		if (this.isNightSkipPrevented)
		{
			this.info("Sleepers will be told: $1", this.preventNightSkipMessage);
		}
	}
}
