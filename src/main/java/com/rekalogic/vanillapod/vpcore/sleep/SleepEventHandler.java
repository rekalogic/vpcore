package com.rekalogic.vanillapod.vpcore.sleep;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.rekalogic.vanillapod.vpcore.util.EventHandlerBase;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class SleepEventHandler extends EventHandlerBase<SleepSettings>
{
	public SleepEventHandler(final PluginBase plugin, final SleepSettings settings)
	{
		super(plugin, settings);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onPlayerInteract(final PlayerInteractEvent event)
	{
		if (event.isCancelled()) return;

		if (event.getAction() == Action.RIGHT_CLICK_BLOCK)
		{
			final Block clickedBlock = event.getClickedBlock();
			if (clickedBlock != null &&
					clickedBlock.getType() == Material.BED_BLOCK &&
					super.getSettings().isNightSkipPrevented())
			{
				final Player player = event.getPlayer();
				final ItemStack itemInHand = player.getItemInHand();
				if (itemInHand == null || itemInHand.getType() == Material.AIR || !player.isSneaking())
				{
					event.setCancelled(true);
					Helpers.Messages.sendNotification(
							player,
							Helpers.formatMessage(super.getSettings().getPreventNightSkipMessage()));
				}
			}
		}
	}
}
