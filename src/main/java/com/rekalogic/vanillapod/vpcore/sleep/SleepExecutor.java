package com.rekalogic.vanillapod.vpcore.sleep;

import java.util.Arrays;
import java.util.List;

import org.bukkit.command.CommandSender;

import com.rekalogic.vanillapod.vpcore.util.ExecutorBase;
import com.rekalogic.vanillapod.vpcore.util.PagedOutputCommandSender;
import com.rekalogic.vanillapod.vpcore.util.PluginBase;
import com.rekalogic.vanillapod.vpcore.util.LookupSource;
import com.rekalogic.vanillapod.vpcore.util.Helpers.Helpers;

public final class SleepExecutor extends ExecutorBase<SleepSettings>
{
	public SleepExecutor(final PluginBase plugin, final SleepSettings settings)
	{
		super(plugin, settings, "sleep");
	}

	@Override
	protected boolean checkParameters(final String command, final int argCount)
	{
		switch (command)
		{
		case "sleep": return (argCount <= 1);
		}
		
		return super.checkParameters(command, argCount);
	}

	@Override
	protected List<String> onTabComplete(final CommandSender sender, final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "sleep":
			switch (argIndex)
			{
			case 0: return Arrays.asList("no", "yes");
			}
		}
		
		return super.onTabComplete(sender, command, argIndex, args);
	}
	
	@Override
	protected LookupSource getParameterLookupCollection(final String command, final int argIndex, final String[] args)
	{
		switch (command)
		{
		case "sleep":
			switch (argIndex)
			{
			case 0: return LookupSource.FROM_PLUGIN;
			}
		}
		
		return super.getParameterLookupCollection(command, argIndex, args);
	}

	@Override
	protected boolean onCommand(final PagedOutputCommandSender sender, final String command, final String shortcut, final String[] args)
	{
		if (args.length == 1)
		{
			if (args[0].equalsIgnoreCase("yes")
					|| args[0].equalsIgnoreCase("true")
					|| args[0].equalsIgnoreCase("enable")
					|| args[0].equalsIgnoreCase("on")) super.getSettings().setNightSkipPrevented(false);
			else if (args[0].equalsIgnoreCase("no")
					|| args[0].equalsIgnoreCase("false")
					|| args[0].equalsIgnoreCase("disable")
					|| args[0].equalsIgnoreCase("off")) super.getSettings().setNightSkipPrevented(true);
			else
			{
				sender.sendFailure("Unknown boolean value $1.", args[0]);
				return true;
			}

			super.getSettings().save();
		}

		if (super.getSettings().isNightSkipPrevented())
		{
			sender.sendSuccess("Players will be prevented from skipping night by sleeping.");
			sender.sendNotification(Helpers.Parameters.replace("Players will be told: $1", super.getSettings().getPreventNightSkipMessage()));
		}
		else sender.sendSuccess("Players will be allowed to skip night by sleeping.");

		return true;
	}
}
